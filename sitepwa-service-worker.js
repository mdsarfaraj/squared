//var CACHE_NAME = 'sitepwa-cache-v1.0';
var cachesVersion = 'sitepwa-cache-v1.3.0';
var pageCachesVersion = 'sitepwa-page-cache-v1.3.0';
var offlinePage = '/pwa/offline';
var urlBlacklist = ['/admin/', '/login/', '/install/', '/signup/'];
var baseUrl = '';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

workbox.routing.registerRoute(
  new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
  workbox.strategies.networkFirst({
    cacheName: 'google-fonts',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 100,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  new RegExp('^https://(.*).(?:fontawesome).com/(.*)'),
  workbox.strategies.networkFirst({
    cacheName: 'font-awesome',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      }),
    ],
  }),
);

function updateStaticCache() {
  offlinePage = baseUrl + offlinePage;
  var cached_urls = [offlinePage];
  return caches.open(cachesVersion)
    .then(function (cache) {
     // return cache.addAll(cached_urls);
      var request = new Request(offlinePage);
      return fetch(request)
      .then(function (response) {
        if (isCachableResponse(response)) {
          var copy = response.clone();
          cache.put(request, copy);
        }
      });
    });
}

function clearOldCaches() {
  return caches.keys().then(function (keys) {
    return Promise.all(
      keys.filter(function (key) {
        return key.indexOf(cachesVersion) !== 0 && key.indexOf(pageCachesVersion) !== 0;
      })
      .map(function (key) {
        caches.delete(key);
      })
      );
  });
}

function clearPageCaches() {
  caches.open(pageCachesVersion).then(function (cache) {
    cache.keys().then(function (keys) {
      keys.map(function (key) {
        cache.delete(key);
      })
    });
  });
}

function isHtmlRequest(request) {
  return request.headers.get('Accept') && request.headers.get('Accept').indexOf('text/html') !== -1;
}

function isJSONRequest(request) {
  return request.headers.get('Accept') && request.headers.get('Accept').indexOf('application/json') !== -1;
}

function isBlacklisted(url) {
  url = url + '/';
  return urlBlacklist.filter(function (bl) {
    return url.indexOf(bl) !== -1;
  }).length > 0;
}


function isCachableResponse(response) {
  return response && response.ok && response.status === 200 && response.type === 'basic';
}

function isStaticCachableResponse(response, sameOriginRequest) {
  if (!sameOriginRequest) {
    return response && (response.type === 'opaque' || (response.ok && response.status === 200 && response.type === 'basic'));
  }
  return response && response.ok && response.status === 200 && response.type === 'basic';
}

self.addEventListener('install', function (event) {
  baseUrl = event.currentTarget.location.pathname.replace('/sitepwa-service-worker.js', '');
  event.waitUntil(function () {
    updateStaticCache()
      .then(function () {
        self.skipWaiting();
      })
  });
});

self.addEventListener('activate', function (event) {
  event.waitUntil(clearOldCaches().then(function () {
    return updateStaticCache().then(function () {
      return self.clients.claim();
    });
  }));
});

self.addEventListener('fetch', function (event) {
  var request = event.request;

  if (request.method !== 'GET') {

    if (!navigator.onLine && isHtmlRequest(request)) {
      return event.respondWith(caches.match(offlinePage));
    }
    return;
  }

  var sameOriginRequest = event.request.url.startsWith(self.location.origin);
  if (!sameOriginRequest) {
    if (event.request.url.indexOf('/font-awesome/') !== -1) {
      var url = new URL(event.request.url);
      request = new Request(self.location.origin + baseUrl + url.pathname);
      sameOriginRequest = true;
    } else if(request.mode === 'same-origin' || request.mode === 'navigate') {
      request = new Request(request, {mode: 'no-cors', credentials: 'include'});
    }
  }

  if (isHtmlRequest(request) || isJSONRequest(request)) {

    event.respondWith(
      fetch(request)
      .then(function (response) {
        if (isCachableResponse(response) && !isBlacklisted(response.url)) {
          var copy = response.clone();
          caches.open(pageCachesVersion).then(function (cache) {
            cache.put(request, copy);
          });
        }
        return response;
      })
      .catch(function () {
        return caches.match(request)
          .then(function (response) {
            if (!response && request.mode === 'navigate') {
              return caches.match(offlinePage);
            }
            return response;
          });
      })
      );
  } else {
    if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
      return;
    }
    event.respondWith(
      caches.match(request)
      .then(function (response) {
        return response || fetch(request)
          .then(function (response) {
            if (isStaticCachableResponse(response, sameOriginRequest)) {
              var copy = response.clone();
              caches.open(cachesVersion).then(function (cache) {
                cache.put(request, copy);
              });
            }
            return response;
          })
      })
      );
  }
});

self.addEventListener('message', event => {
  if (event.data.command == 'clearPageCache') {
    clearPageCaches();
  }
});
