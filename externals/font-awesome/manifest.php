<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'font-awesome',
    'version' => '4.10.5',
    'revision' => '$Revision: 10270 $',
    'path' => 'externals/font-awesome',
    'repository' => 'socialengine.com',
    'title' => 'Font Awesome',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/font-awesome',
    )
  )
) ?>
