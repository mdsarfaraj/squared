<?php
return array(
  'package' => array(
    'type' => 'library',
    'name' => 'gif',
    'version' => '4.10.5',
    'revision' => '$Revision: 10171 $',
    'path' => 'application/libraries/GIF',
    'repository' => 'socialengine.com',
    'title' => 'GIF',
    'author' => 'Webligo Developments',
    'directories' => array(
      'application/libraries/GIF',
    )
  )
  )
?>
