<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: login.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<?php echo $this->form->render($this) ?>


<?php 

	if(isset($_COOKIE['rem_password'])){
		$passwordData = base64_decode($_COOKIE['rem_password']);
	}

?>

<script type="text/javascript">
	document.getElementById('password').value = '<?php echo $passwordData; ?>';
</script>

