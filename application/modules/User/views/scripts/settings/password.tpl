<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: password.tpl 9869 2013-02-12 22:37:42Z shaun $
 * @author     Steve
 */
?>
<?php if(!empty($_SESSION['requirepassword'] )){ ?>
    <div class="require_password">
        <?php echo $this->content()->renderWidget('core.menu-logo',array('disableLink'=>true)); ?>
        <?php echo $this->form->render($this) ?>
    </div>
<?php }else{ ?>
<?php echo $this->form->render($this) ?>
<?php } ?>