<?php

if ($this->categories instanceof Engine_Db_Table_Rowset) {
    $this->categories = Engine_Api::_()->getApi('category', 'radcodes')->rowsetToParentChildrenAssoc($this->categories);
}

$type = 'radcodes_category';
foreach ($this->categories[0] as $category) {
    $type = $category->getType();
    break;
}
?>
<?php if ($this->display_style == 'wide'): ?>
    <ul class="<?php echo $type.'_blocks'; ?> radcodes_category_blocks">
        <?php foreach ($this->categories[0] as $category): ?>
            <li>
                <?php echo $this->htmlLink($category->getHref(), $this->itemPhoto($category, 'thumb.normal'), array('class'=>'radcodes_category_photo')); ?>
                <div class="radcodes_category_info">
                    <?php $href = (isset($this->options['link_params'])) ? $category->getHref($this->options['link_params']) : $category->getHref(); ?>
                    <?php echo $this->htmlLink($href, $this->translate($category->getTitle()), array('class' => 'radcodes_category_title'))?>
                    <?php if ($category->getDescription()): ?>
                        <div class="radcodes_category_desc">
                            <?php echo $this->translate($category->getDescription()); ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($this->categories[$category->getIdentity()]) && count($this->categories[$category->getIdentity()])): ?>
                        <ul>
                            <?php foreach ($this->categories[$category->getIdentity()] as $subcategory): ?>
                                <li>
                                    <?php $href = (isset($this->options['link_params'])) ? $subcategory->getHref($this->options['link_params']) : $subcategory->getHref(); ?>
                                    <?php echo $this->htmlLink($href, $this->translate($subcategory->getTitle()));?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif;?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <ul class="<?php echo $type.'_list'; ?> radcodes_category_options category_options generic_list_widget">
        <?php foreach ($this->categories[0] as $category): ?>
            <li>
                <?php if (isset($this->categories[$category->getIdentity()]) && count($this->categories[$category->getIdentity()])): ?>
                    <span class="radcodes_categories_subcategory_toggle radcodes_categories_subcategory_toggle_collapse"><span>+</span></span>
                <?php endif; ?>
                <?php $href = (isset($this->options['link_params'])) ? $category->getHref($this->options['link_params']) : $category->getHref(); ?>
                <?php echo $this->htmlLink($href, $this->translate($category->getTitle()), array());?>
                <?php if (isset($this->categories[$category->getIdentity()]) && count($this->categories[$category->getIdentity()])): ?>
                    <ul class="<?php echo $type.'_list_sub'; ?> radcodes_category_options_sub" style="display: none;">
                        <?php foreach ($this->categories[$category->getIdentity()] as $subcategory): ?>
                            <li>
                                <?php $href = (isset($this->options['link_params'])) ? $subcategory->getHref($this->options['link_params']) : $subcategory->getHref(); ?>
                                <?php echo $this->htmlLink($href, $this->translate($subcategory->getTitle()));?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>

            </li>
        <?php endforeach;?>
    </ul>
    <script type="text/javascript">
        en4.core.runonce.add(function(){
            $$('span.radcodes_categories_subcategory_toggle').addEvent('click', function(){
                var radcodes_sub_cat = $(this).getParent().getChildren('ul');
                radcodes_sub_cat.toggle();
                if (radcodes_sub_cat.getStyle('display') == 'block') {
                    $(this).removeClass('radcodes_categories_subcategory_toggle_collapse').addClass('radcodes_categories_subcategory_toggle_expand');
                }
                else {
                    $(this).removeClass('radcodes_categories_subcategory_toggle_expand').addClass('radcodes_categories_subcategory_toggle_collapse');
                }
            });
        });
    </script>
<?php endif; ?>