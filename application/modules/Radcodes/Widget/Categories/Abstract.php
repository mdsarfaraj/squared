<?php

class Radcodes_Widget_Categories_Abstract extends Engine_Content_Widget_Abstract {

    protected $categoryType = null;
    protected $isOldFormat = false;

    public function indexAction()
    {
        $this->initDefaultViewVars();
        $this->view->categories = $categories = $this->getCategories();
        if (empty($categories))
        {
            return $this->setNoRender();
        }
    }

    protected function initDefaultViewVars()
    {
        $this->view->display_style = $this->_getParam('display_style', 'narrow');
        $this->view->category_type = $this->getCategoryType();
        $this->view->params = $this->_getAllParams();
        $this->view->options = $this->getViewOptions();
    }

    protected function getViewOptions()
    {
        return array('widget_element_id'=>$this->getElement()->getIdentity());
    }

    protected function getCategories()
    {
        if ($this->isOldFormat) {
            $api = Engine_Api::_()->getApi('category', 'radcodes');
            $categories = $api->getCategories($this->getCategoryType());
            $categories = $api->rowsetToParentChildrenAssoc($categories);
        }
        else {
            $categories = Engine_Api::_()->getItemTable($this->getCategoryType())->getParentChildrenAssoc();
        }
        return $categories;
    }

    protected function getCategoryType()
    {
        return $this->categoryType;
    }
}