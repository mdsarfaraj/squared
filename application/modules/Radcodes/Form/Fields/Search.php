<?php
/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Radcodes
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */

class Radcodes_Form_Fields_Search extends Fields_Form_Search
{
    public function init()
    {
        parent::init();

        $this->loadDefaultDecorators();

        $this
            ->setAttribs(array(
                'id' => 'filter_form',
                'class' => 'global_form_box classifieds_browse_filters field_search_criteria',
            ))
            ->setAction($_SERVER['REQUEST_URI'])
            ->setMethod('GET')
            ->getDecorator('HtmlTag')
            ->setOption('class', 'browseclassifieds_criteria classifieds_browse_filters');

        // Generate
        //$this->generate();

        // Add custom elements
        $this->getAdditionalOptionsElement();
    }
}