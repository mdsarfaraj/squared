<?php

/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Radcodes
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */


class Radcodes_Api_Category extends Core_Api_Abstract {

    public function rowsetToParentChildrenAssoc(Engine_Db_Table_Rowset $categories)
    {
        $assoc = array();
        if (!empty($categories)) {
            $assoc[0] = array();
            foreach ($categories as $category) {
                $assoc[0][$category->getIdentity()] = $category;
            }
        }
        return $assoc;
    }

    public function getCategories($type)
    {
        $table = Engine_Api::_()->getItemTable($type);
        $categories = $table->fetchAll($table->select()->order('order'));
        return $categories;
    }
}