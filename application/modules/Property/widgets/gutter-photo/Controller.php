<?php


class Property_Widget_GutterPhotoController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    
    // $request = Zend_Controller_Front::getInstance()->getRequest();
    // $propertyId = $request->getParam('property_id', 0);
    
    $currentProperty = Engine_Api::_()->core()->getSubject();
    
    // $table = Engine_Api::_()->getDbtable('properties', 'property');
    // $select = $table->select()->where("property_id = ?", $propertyId);
    // $tableData = $table->fetchRow($select);
    // $this->view->data = $tableData; 
    
    if($currentProperty->photo_id) {
        $photoUrl = $currentProperty->getPhotoUrl('thumb.profile');
    } else {
        $photoUrl = "application/modules/Property/externals/images/property_default.png";
    }
    
    $this->view->photoUrl = $photoUrl;

  }
}
