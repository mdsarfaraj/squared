<?php
/**
    This widget fetch all members from a specific property
 */
class Property_Widget_PropertyMembersController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $propertyId = $request->getParam('property_id', 0);

        $membersTable = Engine_Api::_()->getDbtable('members', 'property');
        $select = $membersTable->select()
        ->where("property_id = ?", $propertyId)
        ->where("status = ?",1);
        $membersTableData = $membersTable->fetchAll($select);
        
        $membersId = array();
        
        foreach ($membersTableData as $item) {
            $membersId[] = $membersTableData->members_id;
        }

        if(empty($membersId)){
            return $this->setNoRender();
        }

        $this->view->membersData = $membersTableData; 
    }
}
