<?php
/**
 
 */
?>

<div>
  <?php foreach ($this->membersData as $data): ?>
      <div class="propertyMembers-Container">
          <?php $user = Engine_Api::_()->getItem("user", $data->user_id); ?>
          
          <?php 
              if($user->photo_id){
                  $photoUrl = $user->getPhotoUrl('thumb.icon');
              }else{
                $photoUrl = "application/modules/Property/externals/images/nophoto_user_thumb_icon.png";
              }
          ?>

         <img src="<?php echo $photoUrl; ?>" alt="<?php echo $user->getTitle(); ?>" class="propertyuser-image">
         <p class="PropertyUserName">
            <?php echo $user->getTitle(); ?> <br>
         </p>
         <p class="PropertyUserDate"><span><?php echo $this->timestamp($data->living_since) ?></span></p>
      </div>
  <?php endforeach; ?>
</div>

<style type="text/css">
    
  .propertyuser-image {
    border: 1px solid #e4e4e4;
  }

  .propertyMembers-Container {
    border-bottom: 1px solid #dedede;
    background-color: #fff;
    padding: 10px;
    margin: 10px 0;
  }

  .propertyMembers-Container::after {
    content: "";
    clear: both;
    display: table;
  }

  .propertyMembers-Container img {
    float: left;
    max-width: 40px;
    width: 100%;
    margin-right: 20px;
  }


  .propertyMembers-Container img.right {
    float: right;
    margin-left: 20px;
    margin-right:0;
  }


  .PropertyUserName {
    float: left;
    color: #333;
    font-weight: 600;
    font-size: 14px;
    position: relative;
    margin-right: 67%!important;
    
  }

  .PropertyUserDate {
    float: left;
    color: #999;
    position: relative;
    margin-right: 67%!important;
    
  }
   
   /*@media only screen and (max-width: 600px) {
        .PropertyUserName {
        float: left;
        color: #333;
        font-weight: 600;
        font-size: 14px;
        position: relative;
        margin-right: 40%;
        
      }

      .PropertyUserDate {
        float: left;
        color: #999;
        position: relative;
        margin-right: 40%;
        
      }
  }
*/
</style>

