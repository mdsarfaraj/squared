<?php
/**
 
 */
?>

<div class="card">
  <img src="<?php echo $this->photoUrl; ?>" alt="Avatar" class="imgClass">
  <div class="containerImg">
    <p class="property-name"><?php echo $this->property->name; ?></p>
    <p class="property-desc"><?php echo $this->property->descript; ?> </p> 
    <p class="proeprty-link"><a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'view', 'property_id' => $this->property->property_id), 'view_property'); ?>">View</a></p>
  </div>
  
</div>

<!--Style Section-->
<style type="text/css">

.proeprty-link{
  margin-left: 90%;
}
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 30%;
}
.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
.imgClass{
  width: 200px;
  height: 200px;
  margin-right: auto;
  margin-left: auto;
  display: block;
}
.containerImg {
  padding: 2px 16px;
}
  
.property-name{
    font-weight: 200;
    font-size: 16px;
    position: relative;
}
 .property-desc {
    color: #999;
    font-size: 14px;
    text-align: justify;
    position: relative;
    
  }

  @media only screen and (max-width: 600px) {
    .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
      width: auto;
    }
  }

</style>

