<?php
/**
    This widget fetch all members from a specific property
 */
class Property_Widget_MembersPropertyController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {


       $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

        $membersTable = Engine_Api::_()->getDbtable('members', 'property');
        $select = $membersTable->select()
        ->where('user_id = ?',$viewer->getIdentity())
        ->where('status = ?',1);

        $membersTableData = $membersTable->fetchRow($select);
        $propertyId = $membersTableData->property_id;

        $UserId = $viewer->user_id;

        if($viewer->getIdentity()!=$UserId){
            return $this->setNoRender();
        }

        $propertyTable = Engine_Api::_()->getDbtable('properties', 'property');
        $select = $propertyTable->select()->where("property_id = ?", $propertyId);
        $propertyTableData = $propertyTable->fetchRow($select);
        $this->view->property = $propertyTableData;
        
        if($propertyTableData->photo_id) {
            $photoUrl = $propertyTableData->getPhotoUrl('thumb.profile');
        } else {
           $photoUrl = "application/modules/Property/externals/images/property_default.png";
        }
        
        $this->view->photoUrl = $photoUrl;
    }
}
