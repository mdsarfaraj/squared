<?php
class Property_Widget_PhotoMobileController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {

   /* if( Engine_Api::_()->getApi('core', 'sespwa')->isMobile()) {
        echo "Is Mobile";
    } else {
        echo "Is Not a Mobile";
    }*/
      

     if( Engine_Api::_()->sespwa()->isMobile()) {    
          $currentProperty = Engine_Api::_()->core()->getSubject();
          
          if($currentProperty->photo_id) {
          $photoUrl = $currentProperty->getPhotoUrl('thumb.profile');
          } else {
             $photoUrl = "application/modules/Property/externals/images/property_default.png";
          }
          $this->view->photoUrl = $photoUrl;
    }

  }

}