<?php
/**

 */
class Property_Widget_LinksMobileController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {
      if( Engine_Api::_()->sespwa()->isMobile()) {    
        $this->view->navigation = Engine_Api::_()
            ->getApi('menus', 'core')
            ->getNavigation('property_home');
      }else{
      	return $this->setNoRender();
      }

    }

}
