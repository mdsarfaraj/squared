<?php
/**
    This widget fetch all members from a specific property
 */
class Property_Widget_PropertyIssuesController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $propertyId = $request->getParam('property_id', 0);
        
        $issueTable = Engine_Api::_()->getDbtable('issues', 'property');
        $select = $issueTable->select()
        ->where("property_id = ?", $propertyId)
        ->order('issue_added_date DESC')
        ->limit(10)
        ;

       $issueTableData = $issueTable->fetchAll($select);

       $issueId = array();
       foreach($issueTableData as $item) {
         $issueId[] = $item->issue_id;
       } 

       if (empty($issueId)) {
          return $this->setNoRender();
       }

      $this->view->issueData = $issueTableData; 

    }
}
