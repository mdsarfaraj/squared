<?php
class Property_Widget_SearchPropertyController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Make form
    $this->view->form = $form = new Property_Form_Search();
 
    // Process form
    $p = Zend_Controller_Front::getInstance()->getRequest()->getParams();
    $form->isValid($p);
    $values = $form->getValues(); 
    $this->view->assign($values);

  }

}