<?php
/**

 */

class Property_Api_Core extends Core_Api_Abstract
{
	public function getPropertyName($pid){
		$table = Engine_Api::_()->getDbtable('properties', 'property');
		$select = $table->select()->where("property_id = ?", $pid);
        $tableData = $table->fetchRow($select);
        return $tableData->name;
	}
	public function getPropertyPhoto($pid){
		$table = Engine_Api::_()->getDbtable('properties', 'property');
		$select = $table->select()->where("property_id = ?", $pid);
        return $table->fetchRow($select);
	}

	public function getTenantName($userId){
		$user = Engine_Api::_()->getItem("user", $userId);
		return $user->getTitle();
	}

	public function getPropertyTenantID($propertyId){
		$propertyTable = Engine_Api::_()->getDbtable('properties', 'property');
		$select = $propertyTable->select()->where('property_id = ?',$propertyId);
		$propertyTableData = $propertyTable->fetchRow($select);
		return $propertyTableData->tenant_id;
	}

	public function getMembersID($id){
		$membersTable = Engine_Api::_()->getDbtable('members', 'property');
    	$select = $membersTable->select()->where("property_id = ?", $id);
    	$membersTableData = $membersTable->fetchRow($select);
		return $membersTableData->user_id;
	}

	public function getPropertyAllData(){
		$propertyTable = Engine_Api::_()->getDbtable('properties', 'property');
    	$select = $propertyTable->select();
    	$propertyTableData = $propertyTable->fetchAll($select);
		return $propertyTableData;
	}
}