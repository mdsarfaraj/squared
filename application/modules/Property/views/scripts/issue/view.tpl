

<!--Issue and Property Details Section-->
<div class="propertDetails">

  <div class="profile_fields">
      <h4>
          <span class="heading">Client Information</span>
      </h4>
          <ul>
              <li class="field_first_name">
                   <span>Property Name</span>
                   <span><?php echo $this->property_name; ?></span>
              </li> 
          </ul>
    </div>
<?php $raisedBy = Engine_Api::_()->getItem("user", $this->data->raised_by); ?>
    <div class="profile_fields">
      <h4>
          <span class="heading">Issue Information</span>
      </h4>
          <ul>
              <li class="field_first_name">
                   <span>Issue ID</span>
                   <span><?php echo $this->data->issue_id; ?></span>
              </li> 
               <li class="field_first_name">
                   <span>Title</span>
                   <span><?php echo $this->data->title; ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Description</span>
                   <span><?php echo $this->data->description; ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Raised By</span>
                   <span><?php echo $raisedBy; ?></span>
              </li> 
               <li class="field_first_name">
                   <span>Added Date</span>
                   <span><?php echo $this->locale()->toDateTime($this->data->issue_added_date); ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Fix By Date</span>
                   <span><?php echo $this->timestamp($this->data->starttime); ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Status</span>
                   <span>
                    <?php 
                      if($this->data->status == 1){
                        echo "Done";
                      }else{
                        echo "To Do";
                      }
                    ?>
                </span>
              </li> 
          </ul>
    </div>
<div>
   <img src="<?php echo $this->data->getPhotoUrl(); ?>" alt="" />
</div>
<br /><br />

<!--Comment Section-->
<div>
  <?php foreach ($this->comments as $commetMessage): ?>
      <div class="Comment-Container">
          <?php $user = Engine_Api::_()->getItem("user", $commetMessage->user_id); ?>
          
          <?php 
              if($user->photo_id){
                  $photoUrl = $user->getPhotoUrl('thumb.icon');
              }else{
                $photoUrl = "application/modules/Property/externals/images/nophoto_user_thumb_icon.png";
              }
          ?>
          
         <img src="<?php echo $photoUrl; ?>" alt="<?php echo $user->getTitle(); ?>" class="user-image">
         
         <p class="UserName">
            <?php echo $user->getTitle(); ?> <br>
            <span><?php echo $this->locale()->toDateTime($commetMessage->added_date) ?></span>
         </p>
         <span class="comment-message"><?php echo $commetMessage->message; ?></span>
      </div>
  <?php endforeach; ?>
</div>

<!--Reply Form-->
<div>
  <?php echo $this->form->render($this);?>
</div>


<!--Style Section-->
<style type="text/css">

   /*Issue and Property CSS*/
    .profile_fields{
        font-weight: 600px;
        font-size: 16px;
    }
    .heading{
        color: #1aa3b3;
        font-weight: 600;
        background: none;
        
        margin: auto;
        width: 50%;
        margin-bottom: 7%;
    }

   /*Comment CSS*/
    .centers {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 20%;
   }
    
    .user-image {
      border: 1px solid #e4e4e4;
    }

   

    .Comment-Container {
    border: 2px solid #dedede;
    background-color: #fff;
    border-radius: 5px;
    padding: 10px;
    margin: 10px 0;
  }

  .Comment-Container::after {
    content: "";
    clear: both;
    display: table;
  }

  .Comment-Container img {
    float: left;
    max-width: 40px;
    width: 100%;
    margin-right: 20px;
    border-radius: 50%;
  }


  .Comment-Container img.right {
    float: right;
    margin-left: 20px;
    margin-right:0;
  }


  .UserName {
    float: left;
    color: #999;
    position: relative;
    margin-right: 67%!important;
    
  }
  .comment-message {
    float: right;
    color: #1aa3b3;
  }

</style>