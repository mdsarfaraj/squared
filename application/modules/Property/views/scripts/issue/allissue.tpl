 <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s issue found", "%s issue found", $count),
  $this->locale()->toNumber($count)) ?>
</div>
<br />
 <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>

<div style="overflow-x:auto;">
<table class="property-issues">
	<thead>
		<tr>
			<th class="property-issues-th">ID</th>
			<th class="property-issues-th">Title</th>
			<th class="property-issues-th">Raised By</th>
			<th class="property-issues-th">Resolve By</th>
			<th class="property-issues-th">Added Date</th>
			<th class="property-issues-th">Status</th>
			<th class="property-issues-th">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->paginator as $item): ?>
		<?php
			if($item->status == 1){
				$Class = "done";
			}else{
				$Class = "normal";
			}
		?>
		<?php $RaisedBy = Engine_Api::_()->getItem("user", $item->raised_by); ?>
		<tr class="property-issues-tr <?php echo $Class; ?>">
			<td class="property-issues-td"><?php echo $item->getIdentity(); ?></td>
			<td class="property-issues-td"><p><?php echo $item->getTitle(); ?></p></td>
			<td class="property-issues-td"><p><?php echo $RaisedBy->getTitle(); ?></p></td>
			<td class="property-issues-td"><?php echo $this->timestamp($item->starttime); ?></td>
			<td class="property-issues-td"><?php echo $this->locale()->toDateTime($item->issue_added_date); ?></td>
			<td class="property-issues-td">
				<?php 
					if($item->status == 1){
						echo "Done";
					}else{
						echo "To Do";
					}
				?>	
			</td>

			<td class="property-issues-td">
				<a href="<?php echo $this->url(array('issue_id' => $item->getIdentity(),'property_id' => $item->property_id), 'property_issue_view_single'); ?>">View</a>
			</td>

		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div>

<?php else: ?>
	<div class="tip">
    <span>
      <?php echo $this->translate('There is not any issue yet.'); ?>
     
    </span>
  </div>
<?php endif; ?>

<div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
  )); ?>
</div>


<style type="text/css">
	.property-issues {
	  border-collapse: collapse;
	  border-spacing: 0;
	  width: 100%;
	  border: 1px solid #ddd;
	}
	.property-issues-th, .property-issues-td {
	  text-align: left;
	  padding: 16px;
	  width: auto;
	  border-bottom: 1px solid #ccc;
	}

	.property-issues-tr:nth-child(odd) {
	  background-color: #f2f2f2
	}

	tr.done > td {
    background: #5df3c6a3;
   }

   tr.normal > td {
    background: none;
   }

</style>