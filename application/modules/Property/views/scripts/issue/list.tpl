<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
<h1 class="heading">
  <?php echo "Issue List Of ".$this->property_name; ?>
</h1>

<br />

<div class='admin_results'>
  <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s issues found", "%s issues found", $count),
  $this->locale()->toNumber($count)) ?>
</div>


<div style="overflow-x:auto;">
	<table class="properties-property">
		<thead>
			<tr>
				<th class="properties-property-th">Title</th>
        <th class="properties-property-th">Raised By</th>
				<th class="properties-property-th">Added Date</th>
				<th class="properties-property-th">Fix By Date</th>
        <th class="properties-property-th">Status</th>
				<th class="properties-property-th">Action</th>
			</tr>
		</thead>

		<tbody>

        <!--Today date-->
		    <?php $todayDate =  date("Y-m-d h:i:s"); ?>
         <?php foreach( $this->paginator as $item ): ?>     
            <?php $raisedBy = Engine_Api::_()->getItem("user", $item->raised_by); ?>
           <!--check the issue status-->
          <?php
              if($item->status == 1){
                $Class = 'done';
              }
              else
              {
                if($item->issue_added_date > $item->starttime){
                $Class = 'danger';
                }else if($item->starttime == $todayDate){
                  $Class = 'warning';
                }else{
                  $Class = 'normal';
                }
              }
          ?>
        <!--checking ended-->  

  				<tr class="<?php echo $Class; ?>">
           <td class='properties-property-td'><p><?php echo $item->getTitle(); ?></p></td>
            <td class='properties-property-td'><?php echo $raisedBy->getTitle(); ?></td>
  					<td class='properties-property-td'><?php echo $this->locale()->toDateTime($item->issue_added_date); ?></td>
  					<td class='properties-property-td'><?php echo $this->locale()->toDateTime($item->starttime); ?></td>

            <td class='properties-property-td'><p>
              <?php 
                if($item->status == 1){
                  echo "Done";
                }else{
                  echo "To Do";
                }
              ?>  
              </p></td>

  					<td class='properties-property-td'>

  					<a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'view', 'issue_id' => $item->getIdentity(),'property_id' => $item->property_id), 'property_issue_view_single'); ?>">View</a> |&nbsp; 
  					<a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'view', 'issue_id' => $item->getIdentity()), 'default'); ?>">Reply</a>

  				</td></tr>

			  <?php endforeach; ?>
</tbody>
</table>


<?php else: ?>
	<div class="tip">
    <span>
      <?php echo $this->translate('There is not any issue yet.'); ?>
     
    </span>
  </div>
<?php endif; ?>
</div>

</div>

<div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
  )); ?>
</div>

<style type="text/css" >
   .properties-property {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }

    .properties-property-th, .properties-property-td {
      text-align: left;
      padding: 16px;
      width: auto;
      border-bottom: 1px solid #ccc;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }
   
  .heading{
    font-size: 16px;
  }
  /* .headingH1{
    border-bottom: 1px solid #f1f1f1;
    margin-top: 1%;
  } */
    tr.done > td {
    background: #5df3c6a3;
  }

  tr.warning > td {
    background: yellow;
  }

  tr.danger > td {
    background: #f79898;
  }

  tr.normal > td {
    background: none;
  }
</style>