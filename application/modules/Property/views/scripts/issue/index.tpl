<h1 class="heading">
  <?php echo $this->translate("All Issues") ?>
</h1>
<br />
<div class='admin_results'>
  <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s issues found", "%s issues found", $count),
  $this->locale()->toNumber($count)) ?>
</div>


<?php if(Engine_Api::_()->sitepwa()->isMobileDevice() || Engine_Api::_()->sitepwa()->isTabletDevice()): ?>


  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <?php foreach($this->paginator as $item): ?>
       <?php $raisedBy = Engine_Api::_()->getItem("user", $item->raised_by); ?>
<table class="properties-property">
  <thead>
    <tr>
      <th class="properties-property-th">Property Name</th>
      <th class="properties-property-th th1"><?php echo $item->name; ?></th>
    </tr>
    <tr>
      <th class="properties-property-th">Title</th>
      <th class="properties-property-th th1"><?php echo $item->getTitle(); ?></th>
    </tr>
    <tr>
      <th class="properties-property-th">Raised By</th>
      <th class="properties-property-th th1"><?php echo $raisedBy->getTitle(); ?></th>
    </tr>
    <tr>
      <th class="properties-property-th">Added Date</th>
      <th class="properties-property-th th1"><?php echo $this->locale()->toDateTime($item->issue_added_date); ?></th>
    </tr>
    <tr>
      <th class="properties-property-th">Fix By Date</th>
      <th class="properties-property-th th1"><?php echo $this->locale()->toDateTime($item->starttime) ?></th>
    </tr>
     <tr>
      <th class="properties-property-th">Status</th>
      <th class="properties-property-th th1"><?php
         if($item->status == 1){
          echo "Done";
         }else{
          echo "To Do";
         }
      ?></th>
    </tr>
  </thead>
</table>

<table class="properties-property">
  <thead>
    <th class="properties-property-th th1">
    
    <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'view', 'issue_id' => $item->getIdentity(), 'property_id' => $item->property_id), 'property_issue_view_single'); ?>">View</a> |&nbsp;
    <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'edit', 'issue_id' => $item->getIdentity()), 'default'); ?>">Edit</a> |&nbsp;  
    
    <a  href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'delete', 'issue_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'default'); ?>" class="buttonlink smoothbox">Delete</a> |&nbsp;  
       
    <a  href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'state', 'issue_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'default'); ?>" class="buttonlink smoothbox">Status</a> 
    </th>
  </thead>
</table>
<br />
  <?php endforeach; ?>
<?php endif; ?>




<?php else: ?>







<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
<div style="overflow-x:auto;">
	<table class="properties-property">
		<thead>
			<tr>
        <th class="properties-property-th">Client Name</th>
				<th class="properties-property-th">Title</th>
				<th class="properties-property-th">Added Date</th>
				<th class="properties-property-th">Fix By Date</th>
        <th class="properties-property-th">Status</th>
				<th class="properties-property-th">Action</th>
			</tr>
		</thead>

		<tbody>
        <?php $todayDate =  date("Y-m-d h:i:s"); ?>
        <?php foreach( $this->paginator as $item ): ?>

        <!--check the issue status-->
          <?php
              if($item->status == 1){
                $Class = 'done';
              }
              else
              {
                if($item->added_date > $item->starttime){
                $Class = 'danger';
                }else if($item->starttime == $todayDate){
                  $Class = 'warning';
                }else{
                  $Class = 'normal';
                }
              }
          ?>
        <!--checking ended-->    

  				<tr class="<?php echo $Class; ?>">
            <td class='properties-property-td'><p><?php echo $item->name?></p></td>
            <td class='properties-property-td'><p><?php echo $item->title?></p></td>

  					<td class='properties-property-td'><?php echo $this->locale()->toDateTime($item->issue_added_date) ?></td>

  					<td class='properties-property-td'><?php echo $this->locale()->toDateTime($item->starttime) ?></td>

            <td class='properties-property-td'>
                <?php 
                  if($item->status == 1){
                    echo "Done";
                  }else{
                    echo "To Do";
                  }
                ?> 
            </td>

  					<td class='properties-property-td'>
            <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'view', 'issue_id' => $item->getIdentity(), 'property_id' => $item->property_id), 'property_issue_view_single'); ?>">View</a> |&nbsp;
            <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'edit', 'issue_id' => $item->getIdentity()), 'default'); ?>">Edit</a> |&nbsp;  
            <a  style="padding-left: 0px;" href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'delete', 'issue_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'default'); ?>" class="buttonlink smoothbox">Delete</a> |&nbsp;  
             <a  style="padding-left: 0px;" href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'state', 'issue_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'default'); ?>" class="buttonlink smoothbox">Status</a> 

  				</td>

        </tr>
			  <?php endforeach; ?>
</tbody>
</table>
</div>
</div>

</div>
<?php else: ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('There is not any issue yet.'); ?>
    </span>
  </div>
<?php endif; ?>

<?php endif; ?>

<div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
  )); ?>
</div>

<style type="text/css" >
      .properties-property {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }

    .properties-property-th, .properties-property-td {
      text-align: left;
      padding: 16px;
      width: auto;
      border-bottom: 1px solid #ccc;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }
    .add_button {
    margin-bottom: 20px;
    min-width: 100%;
    background-color: #1aa3b3;
    padding: 8px 16px;
    border-radius: 2px;
    color: #fff !important;
    transition: all 0.5s;
    font-weight: 400;
  }

  tr.done > td {
    background: #5df3c6a3;
  }

  tr.warning > td {
    background: yellow;
  }

  tr.danger > td {
    background: #f79898;
  }

  tr.normal > td {
    background: none;
  }

  .heading{
    font-size: 1.2rem !important;
    color: #1aa3b3;
    text-align: left;
    line-height: 20px;
  }

</style>