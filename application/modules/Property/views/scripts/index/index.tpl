<h1 class="heading">
  <?php echo "Clients"?>
</h1>
<br />
<div class='admin_results'>
  <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s clients found", "%s clients found", $count),
  $this->locale()->toNumber($count)) ?>
</div>

<div>
  <?php echo $this->htmlLink(
      $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'create'), 'property_create', true),
      $this->translate('Add New Client'),
      array('class' => 'addnewproperty', 'style' => 'float: right')
    ); ?>
</div>

<br />
<br />



<?php if(Engine_Api::_()->sitepwa()->isMobileDevice() || Engine_Api::_()->sitepwa()->isTabletDevice()): ?>
  
  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
      <?php foreach($this->paginator as $item): ?>
  <table class="properties-property">
    <thead>
      <tr>
        <th class="properties-property-th">Name</th>
        <th class="properties-property-th th1"><?php echo $item->name; ?></th>
      </tr>
      <tr>
        <th class="properties-property-th">Address</th>
        <th class="properties-property-th th1"><?php echo $item->address; ?></th>
      </tr>
      <tr>
        <th class="properties-property-th">City</th>
        <th class="properties-property-th th1"><?php echo $item->addr2; ?></th>
      </tr>
      <tr>
        <th class="properties-property-th">State</th>
        <th class="properties-property-th th1"><?php echo $item->addr3; ?></th>
      </tr>
      <tr>
        <th class="properties-property-th">Zip Code</th>
        <th class="properties-property-th th1"><?php echo $item->postcode; ?></th>
      </tr>
    </thead>
  </table>

  <table class="properties-property">
    <thead>
      <th class="properties-property-th th1">
         <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'view','property_id' => $item->getIdentity()), 'view_property'); ?>">View</a>&nbsp;|&nbsp;
      
      <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'edit', 'property_id' => $item->getIdentity()), 'property_edit'); ?>">Edit</a>&nbsp;|&nbsp;

      <a  href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'delete', 'property_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'property_delete'); ?>" class="buttonlink smoothbox">Delete</a>&nbsp;|&nbsp;
     
     <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'list','property_id' => $item->getIdentity()), 'property_issue_list'); ?>">Issue</a>&nbsp;|&nbsp;

     <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'create', 'property_id' => $item->getIdentity()), 'property_issue_create'); ?>">Add Issue</a>&nbsp;|&nbsp;

     <a href="<?php echo $this->url(array('property_id' => $item->getIdentity()), 'property_add_members'); ?>">Add Member</a>
      </th>
    </thead>
  </table>
  <br />
    <?php endforeach; ?>
  <?php endif; ?>

<?php else: ?>


<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
<div style="overflow-x:auto;">
	<table class="properties-property">
		<thead>
			<tr>
				<th class="properties-property-th">Client Name</th>
				<th class="properties-property-th">Address</th>
				<th class="properties-property-th">City</th>
				<th class="properties-property-th">State</th>
				<th class="properties-property-th">Zip Code</th>
				<th class="properties-property-th">Action</th>
			</tr>
		</thead>

		<tbody>
            <?php foreach( $this->paginator as $item ): ?>
  				<tr>
            <td class='properties-property-td'><?php echo $item->name?></td>
  					<td class='properties-property-td'><?php echo $item->address?></td>
  					<td class='properties-property-td'><?php echo $item->addr2?></td>
  					<td class='properties-property-td'><?php echo $item->addr3?></td>
  					<td class='properties-property-td'><?php echo $item->postcode?></td>
  					<td class='properties-property-td'>
          
          <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'view','property_id' => $item->getIdentity()), 'view_property'); ?>">View</a>&nbsp;|&nbsp;

  					<a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'edit', 'property_id' => $item->getIdentity()), 'property_edit'); ?>">Edit</a>&nbsp;|&nbsp;  


  					<a  style="padding-left: 0px;"; href="<?php echo $this->url(array('module' => 'property', 'controller' => 'index', 'action' => 'delete', 'property_id' => $item->getIdentity(), 'format' => 'smoothbox'), 'property_delete'); ?>" class="buttonlink smoothbox">Delete</a>&nbsp;|&nbsp; 

             <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'list','property_id' => $item->getIdentity()), 'property_issue_list'); ?>">Issue</a>&nbsp;|&nbsp;

            <a href="<?php echo $this->url(array('module' => 'property', 'controller' => 'issue', 'action' => 'create', 'property_id' => $item->getIdentity()), 'property_issue_create'); ?>">Add Issue</a>&nbsp;|&nbsp;

            <a href="<?php echo $this->url(array('property_id' => $item->getIdentity()), 'property_add_members'); ?>">Add Member</a>

  				</td>
        </tr>

			  <?php endforeach; ?>

    </tbody>
</table>
</div>

<?php else: ?>
    <div class="tip">
    <span>
      <?php echo $this->translate('There is not any Client yet.'); ?>
    </span>
  </div> 

<?php endif; ?>

<div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
  )); ?>
</div>

<?php endif; ?>



<style type="text/css" >
  .properties-property {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
  }
  
  .properties-property-th, .properties-property-td {
    text-align: left;
    padding: 16px;
    width: auto;
    border-bottom: 1px solid #ccc;
  }

  tr:nth-child(even) {
    background-color: #f2f2f2;
  }
  
  .add_button {
    margin-bottom: 20px;
    min-width: 100%;
    background-color: #1aa3b3;
    padding: 8px 16px;
    border-radius: 2px;
    color: #fff !important;
    transition: all 0.5s;
    font-weight: 400;
  }
  .heading{
    font-size: 18px;
  }
  .addnewproperty{
      background-color: #1aa3b3;
      color: white !important;
      padding: 0.6em 1.2em;
      text-decoration: none;
      font-weight: 600px;
      border-radius: 2px;
      text-transform: uppercase;
  }

</style>