
<div class="propertDetails">
    <div class="profile_fields">
        <h4>
            <span class="heading">Client Information</span>
        </h4>
          <ul>
              <li class="field_first_name">
                   <span>Name</span>
                   <span><?php echo $this->data->name; ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Type</span>
                   <span><?php echo $this->data->type; ?></span>
              </li> 
          </ul>
    </div>

    <div class="profile_fields">
        <h4>
            <span class="heading">Location</span>
        </h4>
          <ul>
              <li class="field_first_name">
                   <span>Address</span>
                   <span><?php echo $this->data->address; ?></span>
              </li>
               <li class="field_first_name">
                   <span>City</span>
                   <span><?php echo $this->data->addr2; ?></span>
              </li> 
              <li class="field_first_name">
                   <span>State</span>
                   <span><?php echo $this->data->addr3; ?></span>
              </li> 
              <li class="field_first_name">
                   <span>Zip Code</span>
                   <span><?php echo $this->data->postcode; ?></span>
              </li> 
          </ul>
    </div>


    <div class="profile_fields">
        <h4>
            <span class="heading">About</span>
        </h4>
          <ul>
              <li class="field_first_name">
                   <span>Description</span>
                   <span><?php echo $this->data->descript; ?></span>
              </li> 
          </ul>
    </div>
</div>

<style type="text/css">
    .profile_fields{
        font-weight: 600px;
        font-size: 16px;
    }
    .heading{
        color: #1aa3b3;
        font-weight: 600;
        background: none;
        
        margin: auto;
        width: 50%;
        margin-bottom: 7%;
    }
</style>