<?php

/**

 * SocialEngine

 *

 * @category   Application_Extensions

 * @package    Blog

 * @copyright  Copyright 2006-2010 Webligo Developments

 * @license    http://www.socialengine.com/license/

 * @version    $Id: Blog.php 10072 2013-07-24 22:38:42Z john $

 * @author     Jung

 */





/**

 * @category   Application_Extensions

 * @package    Blog

 * @copyright  Copyright 2006-2010 Webligo Developments

 * @license    http://www.socialengine.com/license/

 */



class Property_Model_Property extends Core_Model_Item_Abstract

{
 public function setPhoto($photo)

  {

    if( $photo instanceof Zend_Form_Element_File ) {

      $file = $photo->getFileName();

    } elseif( is_array($photo) && !empty($photo['tmp_name']) ) {

      $file = $photo['tmp_name'];

    } elseif( is_string($photo) && file_exists($photo) ) {

      $file = $photo;

    } else {

      throw new Blog_Model_Exception('Invalid argument passed to setPhoto: ' . print_r($photo, 1));

    }



    $name = basename($file);

    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';

    $params = array(

      'parent_type' => 'property',

      'parent_id' => $this->getIdentity()

    );



    // Save

    $storage = Engine_Api::_()->storage();



    // Resize image (main)

    $image = Engine_Image::factory();

    $image->open($file)

      ->resize(720, 720)

      ->write($path . '/m_' . $name)

      ->destroy();



    // Resize image (profile)

    $image = Engine_Image::factory();

    $image->open($file)

      ->resize(200, 400)

      ->write($path . '/p_' . $name)

      ->destroy();



    // Resize image (normal)

    $image = Engine_Image::factory();

    $image->open($file)

      ->resize(140, 160)

      ->write($path . '/in_' . $name)

      ->destroy();





    // Resize image (icon)

    $image = Engine_Image::factory();

    $image->open($file);



    $size = min($image->height, $image->width);

    $x = ($image->width - $size) / 2;

    $y = ($image->height - $size) / 2;



    $image->resample($x, $y, $size, $size, 48, 48)

      ->write($path . '/is_' . $name)

      ->destroy();



    // Store

    $iMain = $storage->create($path . '/m_' . $name, $params);

    $iProfile = $storage->create($path . '/p_' . $name, $params);

    $iIconNormal = $storage->create($path . '/in_' . $name, $params);

    $iSquare = $storage->create($path . '/is_' . $name, $params);



    $iMain->bridge($iProfile, 'thumb.profile');

    $iMain->bridge($iIconNormal, 'thumb.normal');

    $iMain->bridge($iSquare, 'thumb.icon');



    // Remove temp files

    @unlink($path . '/p_' . $name);

    @unlink($path . '/m_' . $name);

    @unlink($path . '/in_' . $name);

    @unlink($path . '/is_' . $name);



    // Update row

    $this->updated_date = date('Y-m-d H:i:s');

    $this->photo_id = $iMain->getIdentity();

    $this->save();



    return $this;

  }





// public function importFile($filename)

//   {

//     if( $filename instanceof Zend_Form_Element_File ) {

//       $file = $filename->getFileName();

//     } elseif( is_array($filename) && !empty($filename['tmp_name']) ) {

//       $file = $filename['tmp_name'];

//     } elseif( is_string($photo) && file_exists($filename) ) {

//       $file = $filename;

//     } else {

//       throw new Blog_Model_Exception('Invalid argument passed to setPhoto: ' . print_r($filename, 1));

//     }



//     $name = basename($file);

//     $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';

//     // $params = array(

//     //   'parent_type' => 'property',

//     //   'parent_id' => $this->getIdentity()

//     // );



//     // Save

//     $storage = Engine_Api::_()->property();



      

//     // $row = 1;

//     if (($handle = fopen($name, "r")) !== FALSE) {

//     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {







//         echo "<pre>";

//           print_r($data);

//         echo "</pre>";

//         // $num = count($data);

//         // echo "<p> $num fields in line $row: <br /></p>";

//         // $row++;

//         // for ($c=0; $c < $num; $c++) {

//         //     echo $data[$c] . "<br />";

//         // }

//     }

//     fclose($handle);

//   }





//     // @unlink($path,$name);

   

//     // // Update row

//     // $this->added_date = date('Y-m-d H:i:s');

//     // $this->photo_id = $iMain->getIdentity();

//     // $this->save();



//     return $this;

//   }



}

