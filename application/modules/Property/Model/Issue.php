<?php
/**
 
 */

class Property_Model_Issue extends Core_Model_Item_Abstract
{

   public function getHref($params = array())
  {
    $params = array_merge(array(
      'route' => 'property_issue_view_single',
      'reset' => true,
      'property_id' => $this->property_id,
      'issue_id' => $this->issue_id,
    ), $params);
    $route = $params['route'];
    $reset = $params['reset'];
    unset($params['route']);
    unset($params['reset']);
    return Zend_Controller_Front::getInstance()->getRouter()
      ->assemble($params, $route, $reset);
  }

  public function getPhotoUrl($type = null)
  {
    if( $this->photo_id ) {
      return parent::getPhotoUrl($type);
    }
   // return $this->getOwner()->getPhotoUrl($type);
  }

 public function setPhoto($photo)
  {
    if( $photo instanceof Zend_Form_Element_File ) {
      $file = $photo->getFileName();
    } elseif( is_array($photo) && !empty($photo['tmp_name']) ) {
      $file = $photo['tmp_name'];
    } elseif( is_string($photo) && file_exists($photo) ) {
      $file = $photo;
    } else {
      throw new Blog_Model_Exception('Invalid argument passed to setPhoto: ' . print_r($photo, 1));
    }

    $name = basename($file);
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
    $params = array(
      'parent_type' => 'property',
      'parent_id' => $this->getIdentity()
    );

    // Save
    $storage = Engine_Api::_()->storage();

    // Resize image (main)
    $image = Engine_Image::factory();
    $image->open($file)
      ->resize(720, 720)
      ->write($path . '/m_' . $name)
      ->destroy();

    
    // Resize image (normal)
    $image = Engine_Image::factory();
    $image->open($file)
      ->resize(140, 160)
      ->write($path . '/in_' . $name)
      ->destroy();


    

    // Store
    $iMain = $storage->create($path . '/m_' . $name, $params);
    $iIconNormal = $storage->create($path . '/in_' . $name, $params);

    $iMain->bridge($iIconNormal, 'thumb.normal');

    // Remove temp files

    @unlink($path . '/m_' . $name);
    @unlink($path . '/in_' . $name);


    // Update row
    $this->updated_date = date('Y-m-d H:i:s');
    $this->photo_id = $iMain->getIdentity();
    $this->save();

    return $this;
  }

}
