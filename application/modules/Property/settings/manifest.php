<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'property',
    'version' => '0.0.1',
    'path' => 'application/modules/Property',
    'title' => 'property',
    'description' => '',
    'author' => 'Abhisek Jaiswal',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    
    'directories' => 
    array (
      0 => 'application/modules/Property',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/property.csv',
    ),
  ),
 // Items ---------------------------------------------------------------------
'items' => array(
  'property',
  'property_issue',
  'property_member',
),
//Routes-----------------------------------------------------------------------
'routes' => array(

   'property_general' => array(
        'route' => 'clients/:action/*',
        'defaults' => array(
            'module' => 'property',
            'controller' => 'index',
            'action' => 'index',
        ),
        'reqs' => array(
        'action' => '(index|create|view|edit|delete|upload)',
      ),
    ),
    'property_create' => array(
      'route' => 'client/add/:action',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'index',
        'action' => 'create',
      ),
    ),
    'property_edit' => array(
      'route' => 'client/edit/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'index',
        'action' => 'edit',
      ),
    ),
    'property_delete' => array(
      'route' => 'client/delete/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'index',
        'action' => 'delete',
      ),
    ),
    'property_issue_create' => array(
      'route' => 'client/issue/create/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'issue',
        'action' => 'create',
      ),
      'reqs' => array(
        'property_id' => '\d+'
      ),
    ),
   'property_issue_general' => array(
      'route' => 'client/issues/:action/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'issue',
        'action' => 'index',
      ),
      'reqs' => array(
        'action' => '(index|create|view|edit|delete)',
      ),
    ),
    'property_issue_list' => array(
      'route' => 'client/issue/list/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'issue',
        'action' => 'list',
      ),
      'reqs' => array(
        'property_id' => '\d+'
      ),
    ),
    'all_issues_view' => array(
      'route' => 'client/issue/allissue/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'issue',
        'action' => 'allissue',
      ),
      'reqs' => array(
        'property_id' => '\d+'
      ),
    ),
    'add_members_property' => array(
      'route' => 'client/member/add/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'member',
        'action' => 'add',
      ),
      'reqs' => array(
        'property_id' => '\d+'
      ),
    ),
    'view_property_issue' => array(
      'route' => 'client/issue/view/:issue_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'issue',
        'action' => 'view',
      ),
      'reqs' => array(
        'issue_id' => '\d+'
      ),
    ),
    'view_property' => array(
      'route' => 'client/index/view/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'index',
        'action' => 'view',
      ),
      'reqs' => array(
        'property_id' => '\d+'
      ),
    ),
    'property_issue_view_single' => array(
        'route' => 'client/issue/:property_id/:issue_id/*',
        'defaults' => array(
          'module' => 'property',
          'controller' => 'issue',
          'action' => 'view',
        ),
        'reqs' => array(
          'property_id' => '\d+',
          'issue_id' => '\d+'
        ),
    ),
    'property_add_members' => array(
      'route' => 'client/member/:property_id/*',
      'defaults' => array(
        'module' => 'property',
        'controller' => 'member',
        'action' => 'add',
      ),
      'reqs' => array(
        'property_id' => '\d+',
      ),
    ),
),

);