<?php
/**
 
 */
return array(
  array(
    'title' => 'Property Gutter Photo',
    'description' => 'Displays owner\'s or/and property\'s photo in the property gutter.',
    'category' => 'Properties',
    'type' => 'widget',
    'name' => 'property.gutter-photo',
  ),

  array(
    'title' => 'Property Issue Photo',
    'description' => 'Displays owner\'s or/and issue\'s photo in the issue gutter.',
    'category' => 'Properties',
    'type' => 'widget',
    'name' => 'property.issue-photo',
  ),
  
  array(
    'title' => 'Property Quick Links',
    'description' => 'Displays a list of quick links.',
    'category' => 'Properties',
    'type' => 'widget',
    'name' => 'property.property-links',
  ),

   array(
    'title' => 'Property members',
    'description' => 'This widget fetch all members from a specific property',
    'category' => 'Properties',
    'type' => 'widget',
    'name' => 'property.property-members',
  ),

   array(
  'title' => 'List Issues',
  'description' => 'This widget fetch all issues from a specific property',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.property-issues',
  ),

   array(
  'title' => 'Members Property',
  'description' => 'This widget show the tenant property.',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.members-property',
  ),

   array(
  'title' => 'Property Mobile Photo',
  'description' => 'This widget show property photo on mobile.',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.photo-mobile',
  ),

  array(
  'title' => 'Property Mobile Quick Links',
  'description' => 'This widget show property quick links on mobile.',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.links-mobile',
  ),

  array(
  'title' => 'Property Search',
  'description' => 'This widget search property through address',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.search-property',
  ),

  array(
  'title' => 'Property Search Issue',
  'description' => 'This widget search property issue.',
  'category' => 'Properties',
  'type' => 'widget',
  'name' => 'property.search-issue',
  ),


) ?>