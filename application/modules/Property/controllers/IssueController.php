<?php
/*

*/

class Property_IssueController extends Core_Controller_Action_Standard
{
   
   public function init(){
    
      $propertyId = $this->_getParam('property_id',0);
      $viewer = Engine_Api::_()->user()->getViewer();

      $table = Engine_Api::_()->getDbTable('members', 'property');
      $select = $table->select()->where("property_id = ?", $propertyId);
      $data = $table->fetchAll($select);

      $ids = array();
      foreach($data as $item) {
         $ids[] = $item->user_id;
      }


      if($viewer->getIdentity()){
        if($viewer->level_id == "6" || $viewer->isAdmin() || in_array($viewer->getIdentity(), $ids)){
          return true;
        }else{
          return $this->_forward('notfound', 'error', 'core'); 
        }
      }else{
        return $this->_forward('notfound', 'error', 'core'); 
      }


   }

    public function indexAction(){

         $this->view->formFilter = $formFilter = new Property_Form_Issue_Search();
      $page = $this->_getParam('page', 1);

     $Propertytable = Engine_Api::_()->getDbtable('properties', 'property');
     $Issuetable = Engine_Api::_()->getDbtable('issues', 'property');
        $select = $Issuetable->select()
       ->setIntegrityCheck(false)
       ->from($Issuetable->info('name'))
       ->join($Propertytable->info('name'), $Propertytable->info('name').'.property_id = '.$Issuetable->info('name').'.property_id');

      // Process form
      $values = array();
      if( $formFilter->isValid($this->_getAllParams()) ) {
        $values = $formFilter->getValues();
      }

      foreach( $values as $key => $value ) {
        if( null === $value ) {
          unset($values[$key]);
        }
      }

      $this->view->assign($values);

      $valuesCopy = array_filter($values);
      // Set up select info
     

      if( !empty($values['name']) ) {
        $select->where('name LIKE ?', '%' . $values['name'] . '%');
      }

      if( !empty($values['title']) ) {
        $select->where('title LIKE ?', '%' . $values['title'] . '%');
      }

      if( !empty($values['status']) ) {
        $select->where('status LIKE ?', '%' . $values['status'] . '%');
      }

      // Make paginator
      $this->view->paginator = $paginator = Zend_Paginator::factory($select);
      $this->view->paginator->setItemCountPerPage(20);
      $this->view->paginator = $paginator->setCurrentPageNumber( $page );
      $this->view->formValues = $valuesCopy;

      $this->_helper->content
      //->setNoRender()
      ->setEnabled()
      ;

    }

    public function createAction() {
        //get the current viewer id
        $user = Engine_Api::_()->user()->getViewer();

        $propertyId = $this->_getParam('property_id',0);

        if($propertyId <= 0){
          return $this->_forward('notfound', 'error', 'core');
        }
        
        //get property details
        $propertyData = Engine_Api::_()->getItem('property',$propertyId);

        //if property id not match
        if(!$propertyData->property_id){
          return $this->_forward('notfound', 'error', 'core');
        }

        //get the property owner object
        $owner = Engine_Api::_()->getItem('user', $propertyData['tenant_id']);

        //issue create form 
        $this->view->form  = $form = new Property_Form_Issue_Create();

        //set the title of form
        $form->setTitle('Add issue for '.$propertyData->name);

        //issueTable
        $issueTable = Engine_Api::_()->getDbtable('issues', 'property');

        if( !$this->getRequest()->isPost() ) {
          return;
        }

        if( !$form->isValid($this->getRequest()->getPost())) {
          return;
        }
        $db = $issueTable->getAdapter();
        $db->beginTransaction();

        try{
            $values = $form->getValues();

            //params
            $issueParams = array(
              'title' => $values['title'],
              'description' => $values['description'],
              'starttime' => $values['starttime'],
              'property_id' => $propertyData->getIdentity(),
              'raised_by' => $user->getIdentity(),
              'issue_added_date' => date('Y-m-d H:i:s'),
              'updated_date' => date('Y-m-d H:i:s'),
            );

            //save
            $issueData = $issueTable->createRow();
            $issueData->setFromArray($issueParams);
            $issueData->save();
    
            if( !empty($values['photo']) ) {
              $issueData->setPhoto($form->photo);
            }
            
            //mail params
            $MailParams = array(
                    'date' => time(),
                    'sender_title' => $user->getTitle(),
                    'sender_email' => $user->email,
                    'recipient_title' => $owner->getTitle(),
                    'recipient_link'=>$owner->getHref(),
            );

            // Add notification
            $notifyTable = Engine_Api::_()->getDbtable('notifications', 'activity');
            $notifyTable->addNotification($owner, $user, $owner, 'issue_added',$MailParams);
      
          $db->commit();

        }catch( Exception $e ) {
          $db->rollBack();
          throw $e;
        }
    
        //redirect
        return $this->_helper->redirector->gotoRoute(array('issue_id'=>$issueData->getIdentity(), 'property_id' => $issueData->property_id), 'property_issue_view_single', true);

    }


    public function editAction(){
        //get the issue id from url
        $IssueId = $this->_getParam('issue_id');

        if($IssueId <= 0){
           return $this->_forward('notfound', 'error', 'core');
        }

        //get the issue table data
        $IssueTableData = Engine_Api::_()->getItem('property_issue',$IssueId);

        if(!$IssueTableData->issue_id){
           return $this->_forward('notfound', 'error', 'core');
        }

        //get the property name from Api
        $propertyId = $IssueTableData->property_id;
        $propertyName = Engine_Api::_()->property()->getPropertyName($propertyId);

        $this->view->form = $form = new Property_Form_Issue_Edit();
        $form->setTitle('Edit Issue of '.$propertyName);

        $form->populate($IssueTableData->toArray());

        if( !$this->getRequest()->isPost() ) {
          return;
        }

        if( !$form->isValid($this->getRequest()->getPost()) ) {
          return;
        }

        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        try{
          $values=$form->getValues();
          $IssueTableData->setFromArray($values);
          $IssueTableData->updated_date = date('Y-m-d H:i:s');
          $IssueTableData->save();
  
          if( !empty($values['photo']) ) {
            $IssueTableData->setPhoto($form->photo);
          }
          $db->commit();
        }catch( Exception $e ) {
          $db->rollBack();
          throw $e;
        }
       
        $this->_helper->redirector->gotoRoute(array(), 'property_issue_general', true);

    }

    public function deleteAction(){
        //get the issue id from the url
        $IssueId = $this->_getParam('issue_id',0);
        if($IssueId <= 0){
          return $this->_forward('notfound', 'error', 'core');
        }

        $IssueTableData = Engine_Api::_()->getItem('property_issue',$IssueId);
      
        if(!$IssueTableData->issue_id){
          return $this->_forward('notfound', 'error', 'core');
        }
        //get the property name from Api
        $propertyId = $IssueTableData->property_id;
        $propertyName = Engine_Api::_()->property()->getPropertyName($propertyId);

        //Delete Form
        $this->view->form = $form = new Property_Form_Issue_Delete();
         $form->setTitle('Delete Issue of '.$propertyName);
       
        if( !$this->getRequest()->isPost() ) {
          return;
        }

        if( !$form->isValid($this->getRequest()->getPost()) ) {
          return;
        }

        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();
        
        try{
          $IssueTableData->delete();
          $db->commit();
        }catch( Exception $e ) {
          $db->rollBack();
          throw $e;
        }
       

        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('Issue deleted successfully.');
        return $this->_forward('success' ,'utility', 'core', array(
          'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'property_issue_general', true),
          'messages' => Array($this->view->message)
        ));
    }

    public function listAction(){

     //get the property_id from the url
     $propertyId = $this->_getParam('property_id',0); 
     $issueTable = Engine_Api::_()->getDbtable('issues', 'property');
     
     if($propertyId <= 0){
        return $this->_forward('notfound', 'error', 'core');
     }
     $select = $issueTable->select()->where('property_id = ?',$propertyId);
     
     //get the property name from the Api
     $propertyName = Engine_Api::_()->property()->getPropertyName($propertyId);
     $this->view->property_name = $propertyName;

     $page = $this->_getParam('page',1);
     $this->view->paginator = $paginator = Zend_Paginator::factory($select);
     $this->view->paginator->setItemCountPerPage(10);
     $this->view->paginator->setCurrentPageNumber($page);
  }

  public function viewAction(){

      //get the user_id form the user table
      $viewer = Engine_Api::_()->user()->getViewer();

      $issueTable = Engine_Api::_()->getDbtable('issues', 'property');
      $IssueId = $this->_getParam('issue_id',0);
      if($IssueId <= 0){
         return $this->_forward('notfound', 'error', 'core');
      }
      $select = $issueTable->select()->where("issue_id = ?", $IssueId);
      $IssueTableData = $issueTable->fetchRow($select);
      $this->view->data = $IssueTableData;
      
      if(!$IssueTableData->issue_id){
         return $this->_forward('notfound', 'error', 'core');
      }

      //get the property name from Api
      $propertyId = $IssueTableData->property_id;
      $propertyName = Engine_Api::_()->property()->getPropertyName($propertyId);
      $this->view->property_name = $propertyName;

      //get the property data from Api
      $propertyId = $IssueTableData->property_id;
      $propertyData = Engine_Api::_()->property()->getPropertyPhoto($propertyId);
      
      //set the subject
       Engine_Api::_()->core()->setSubject($propertyData);

        // conversion table
        $ConversionTable = Engine_Api::_()->getDbtable('issueconversations', 'property');
      
        //Reply form
         $this->view->form = $form = new Property_Form_Issue_Reply();

         if($this->getRequest()->isPost() && 
          $form->isValid($this->getRequest()->getPost())){
              //process
              try{
                  $db = $ConversionTable->getAdapter();
                  $db->beginTransaction();
                  $values = $form->getValues();

                  //check if issue_id value empty
                  if(empty($values['issue_id'])){
                      $values['issue_id'] =$IssueTableData->issue_id;
                  }

                  //check if property_id value empty
                  if(empty($values['property_id'])){
                      $values['property_id'] =$propertyId;
                  }

                  //check if user_id value empty
                  if(empty($values['user_id'])){
                      $values['user_id'] = $viewer->getIdentity();
                  }
                
                  $conversion = $ConversionTable->createRow();
                  $conversion->setFromArray($values);
                  $conversion->added_date = date('Y-m-d H:i:s');
                  $conversion->save();

                  //get the teant id
                  $tenantID = Engine_Api::_()->property()->getPropertyTenantID($conversion->property_id);
                  $user = Engine_Api::_()->getItem('user',$tenantID);
                  //get the issue id
                  $issue = Engine_Api::_()->getItem("property_issue", $conversion['issue_id']);

                  //Mail 

                  $MailParams = array(
                    'date' => time(),
                    'sender_title' => $viewer->getTitle(),
                    'sender_email' => $viewer->email,
                    'object_link'=>$IssueTableData->getHref(),
                  );

                  // Add notification
                  $notificationTable = Engine_Api::_()->getDbtable('notifications', 'activity');
                  $notificationTable->addNotification($user, $viewer, $viewer, 'issue_comment',$MailParams);

                  $db->commit();
                  
                  //reset the form
                  $form->reset();

              }catch( Exception $e ) {
                return $this->exceptionWrapper($e, $form, $db);
              }
         }
         //fetching comments
         $selectComment = $ConversionTable->select()
         ->where('issue_id = ?',$IssueId)
         ->order('added_date DESC')
         ;
         $CommentData = $ConversionTable->fetchAll($selectComment);
         $this->view->comments = $CommentData;

        
      $this->_helper->content
          //->setNoRender()
        ->setEnabled()
     ;

  }


     public function stateAction(){
        
        //get the issue id from the url
        $IssueId = $this->_getParam('issue_id',0);

        //if url value is less than equal to 0
        if($IssueId <= 0){
          return $this->_forward('notfound', 'error', 'core');
        }
        $issueTable = Engine_Api::_()->getDbtable("issues", "property");
        $selectIssueTable = $issueTable->select()->where("issue_id = ?", $IssueId);
        $IssueTableData = $issueTable->fetchRow($selectIssueTable);

        //if url value unknown
        if(!$IssueTableData->issue_id){
          return $this->_forward('notfound', 'error', 'core');
        }

        //get the property name from Api
        $propertyId = $IssueTableData->property_id;
        $PropertyName = Engine_Api::_()->property()->getPropertyName($propertyId);

        //Status Form
         $this->view->form = $form = new Property_Form_Issue_State();
         $form->setTitle('Done Issue Status Of '.$PropertyName);
       
        if( !$this->getRequest()->isPost() ) {
          return;
        }

        if( !$form->isValid($this->getRequest()->getPost()) ) {
          return;
        }
        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();
        
       //check the status if 0
        if($IssueTableData->status == 0){
          $IssueTableData->status = 1;
          $IssueTableData->save();
          $db->commit();
          $this->view->status = true;
          $this->view->message = Zend_Registry::get('Zend_Translate')->_('Issue status is done now.');
        }else{
          $this->view->status = true;
          $this->view->message = Zend_Registry::get('Zend_Translate')->_('Issue status is already done.');
        }

      return $this->_forward('success' ,'utility', 'core', array(
        'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'property_issue_general', true),
        'messages' => Array($this->view->message)
      ));

    }

    public function allissueAction(){  
        $propertyId = $this->_getParam('property_id',0);

        if($propertyId<=0){
          return $this->_forward('notfound', 'error', 'core'); 
        } 

        $propertyData = Engine_Api::_()->property()->getPropertyPhoto($propertyId);

        if($propertyData->property_id != $propertyId){
          return $this->_forward('notfound', 'error', 'core');
        }
        //set the subject
        Engine_Api::_()->core()->setSubject($propertyData);

        $issueTable = Engine_Api::_()->getDbtable("issues", "property");
        $select = $issueTable->select()->where("property_id = ?", $propertyId);

         $page = $this->_getParam('page',1);
         $this->view->paginator = $paginator = Zend_Paginator::factory($select);
         $this->view->paginator->setItemCountPerPage(10);
         $this->view->paginator->setCurrentPageNumber($page);

       $this->_helper->content
          //->setNoRender()
        ->setEnabled();

    }

}