<?php
/*

*/

class Property_MemberController extends Core_Controller_Action_Standard
{
    public function init(){

    	//set the viewer permission
    	$viewer = Engine_Api::_()->user()->getViewer();

    	if(!$viewer->getIdentity()){
    	   return $this->_forward('notfound', 'error', 'core'); 
    	}

    	if($viewer->level_id != "6" && !$viewer->isAdmin()){
    		return $this->_forward('notfound', 'error', 'core'); 
    	}

    }

    public function addAction()
    {
	    /*Start autocompleter code*/

	    // Get params
	    $propertyId = $this->_getParam('property_id',0);
	    $multi = $this->_getParam('multi');
	    $to = $this->_getParam('user_id');
	    $viewer = Engine_Api::_()->user()->getViewer();
	    $toObject = null;

	    // Build
	    $isPopulated = false;
	    if( !empty($to) && (empty($multi) || $multi == 'user') ) {
	      $multi = null;
	      // Prepopulate user
	      $toUser = Engine_Api::_()->getItem('user', $to);

	      if( $toUser instanceof User_Model_User &&
	          (!$viewer->isBlockedBy($toUser) && !$toUser->isBlockedBy($viewer)) &&
	          isset($toUser->user_id)) {
	        $this->view->toObject = $toObject = $toUser;
	        $form->toValues->setValue($toUser->getGuid());
	        $isPopulated = true;
	      } else {
	        $multi = null;
	        $to = null;
	      }
	    } else if( !empty($to) && !empty($multi) ) {
	      // Prepopulate group/event/etc
	      $item = Engine_Api::_()->getItem($multi, $to);
	      // Potential point of failure if primary key column is something other
	      // than $multi . '_id'
	      $item_id = $multi . '_id';
	      if( $item instanceof Core_Model_Item_Abstract &&
	          isset($item->$item_id) && (
	            $item->isOwner($viewer) ||
	            $item->authorization()->isAllowed($viewer, 'edit')
	          )) {
	        $this->view->toObject = $toObject = $item;
	        $form->toValues->setValue($item->getGuid());
	        $isPopulated = true;
	      } else {
	        $multi = null;
	        $to = null;
	      }
	    }
	    $this->view->isPopulated = $isPopulated;

	    // Build normal
	    if( !$isPopulated ) {
	      // Apparently this is using AJAX now?
	     $friends = $viewer->membership()->getMembers();
	     $data = array();
	     foreach( $friends as $friend ) {
	       $data[] = array(
	         'label' => $friend->getTitle(),
	         'id' => $friend->getIdentity(),
	         'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
	       );
	     }
	     $this->view->friends = Zend_Json::encode($data);
	    }
	    
	    // Assign the composing stuff
	    $composePartials = array();
	    $prohibitedPartials = array('_composeTwitter.tpl', '_composeFacebook.tpl');
	    foreach( Zend_Registry::get('Engine_Manifest') as $data ) {
	      if( empty($data['composer']) ) {
	        continue;
	      }
	      foreach( $data['composer'] as $type => $config ) {
	        // is the current user has "create" privileges for the current plugin
	        if ( isset($config['auth'], $config['auth'][0], $config['auth'][1]) ) {
	          $isAllowed = Engine_Api::_()
	            ->authorization()
	            ->isAllowed($config['auth'][0], null, $config['auth'][1]);

	          if ( !empty($config['auth']) && !$isAllowed ) {
	            continue;
	          }
	        }
	        if( !in_array($config['script'][0], $prohibitedPartials) ) {
	          $composePartials[] = $config['script'];
	        }
	      }
	    }
	    $this->view->composePartials = $composePartials;
	    // $this->view->composePartials = $composePartials;

	    // Get config
	    $this->view->maxRecipients = $maxRecipients = 10;
	    /*end autocompleter code */
	   
	    //property member code 
	   
	  
	  	//get the property name from Api
	  	$propertyName = Engine_Api::_()->property()->getPropertyName($propertyId);

	      //property members table
		$membersTable = Engine_Api::_()->getDbtable('members', 'property');

		//member add form
		$this->view->form = $form = new Property_Form_Member_Add();
		$form->setTitle('Add Members In '.$propertyName);

		 if( !$this->getRequest()->isPost() ) {
			 return;
		 }

		if( !$form->isValid($this->getRequest()->getPost()) ) {
			 return;
		 }

	      $db = $membersTable->getAdapter();
	      $db->beginTransaction();

	      try{
		      	 //get value form
		      $values = $form->getValues();
		      $toValues = explode(",",$values['toValues']);

		     if(count($toValues) > 1){
		        foreach($toValues as $item) {  
		            $itemData = array();
		            $itemData['property_id'] = $propertyId;
		            $itemData['user_id'] = $item;
		            $itemData['living_since'] = $values['living_since'];
		            $itemData['added_date'] = $values['added_date'];
		            $itemData['updated_date'] = $values['updated_date'];
		            $propertyMember = $membersTable->createRow();
		            $propertyMember->setFromArray($itemData);
		            $propertyMember->save(); 
		        }
		    }else{
			      $values['property_id'] = $propertyId;
			      $values['user_id'] = $toValues[0];
			      $propertyMember = $membersTable->createRow();
			      $propertyMember->setFromArray($values);
			      $propertyMember->save();
		    }
		    $db->commit();
		} catch( Exception $e ){
            $db->rollBack();
            throw $e;
        }
	    
	    $this->_helper->redirector->gotoRoute(array(), 'property_general', true);
	      // return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
}