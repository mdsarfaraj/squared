<?php
/**


*/

class Property_IndexController extends Core_Controller_Action_Standard

{

  public function init(){

      $propertyId = $this->_getParam('property_id',0);
      $viewer = Engine_Api::_()->user()->getViewer();

      $table = Engine_Api::_()->getDbTable('members', 'property');
      $select = $table->select()->where("property_id = ?", $propertyId);
      $data = $table->fetchAll($select);

      $ids = array();
      foreach($data as $item) {
         $ids[] = $item->user_id;
      }


      if($viewer->getIdentity()){
        if($viewer->level_id == "6" || $viewer->isAdmin() || in_array($viewer->getIdentity(), $ids)){
          return true;
        }else{
          return $this->_forward('notfound', 'error', 'core'); 
        }
      }else{
        return $this->_forward('notfound', 'error', 'core'); 
      }

  }

  public function indexAction()
  {
      //search form
    $this->view->formFilter = $formFilter = new Property_Form_Search();

     //property table
     $propertyTable = Engine_Api::_()->getDbtable('properties', 'property');

     $select = $propertyTable->select(); 
     $page = $this->_getParam('page',1);

     // Process form
      $values = array();
      if( $formFilter->isValid($this->_getAllParams()) ) {
        $values = $formFilter->getValues();
      }

      foreach( $values as $key => $value ) {
        if( null === $value ) {
          unset($values[$key]);
        }
      }

      $this->view->assign($values);

      $valuesCopy = array_filter($values);
      // Set up select info
     

      if( !empty($values['address']) ) {
        $select->where('address LIKE ?', '%' . $values['address'] . '%');
      }


      //Make Paginator
      $this->view->paginator = $paginator = Zend_Paginator::factory($select);
      $this->view->paginator->setItemCountPerPage(50);
      $this->view->paginator = $paginator->setCurrentPageNumber( $page );
      $this->view->formValues = $valuesCopy;


     $this->_helper->content
      //->setNoRender()
      ->setEnabled()
      ;


  }

  public function viewAction(){

    //get the property id from url
    $propertyId = $this->_getParam('property_id',0);
    
    if($propertyId <= 0){
      return $this->_forward('notfound', 'error', 'core');
    }
    $propertyTable = Engine_Api::_()->getDbtable('properties', 'property');
    $select = $propertyTable->select()->where("property_id = ?", $propertyId);
    $propertyTableData = $propertyTable->fetchRow($select);

    if(!$propertyTableData->property_id){
      return $this->_forward('notfound', 'error', 'core');
    }

    // Set Subject of Property 
    Engine_Api::_()->core()->setSubject($propertyTableData);
    
    $this->view->data = $propertyTableData;
    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;

  }

  public function createAction()

  {

  	 $this->view->form = $form = new Property_Form_Create();

  		$propertyTable = Engine_Api::_()->getDbtable('properties', 'property');

      //members table
      $membersTable = Engine_Api::_()->getDbtable('members', 'property');

   		 if( !$this->getRequest()->isPost() ) {
     		 return;
   		 }

   		if( !$form->isValid($this->getRequest()->getPost()) ) {
     		 return;
   		 }

      $db = $propertyTable->getAdapter();
      $db->beginTransaction();

      try{
             //get value form
          $values = $form->getValues();

          //get the tenant id from toValues
          $tenantId = $values['toValues'];
          if(empty($values['tenant_id'])){
             $values['tenant_id'] = $tenantId;
          }

          $property = $propertyTable->createRow();
          $property->setFromArray($values);
          $property->added_date = date('Y-m-d H:i:s');
          $property->updated_date = date('Y-m-d H:i:s');
          $property->save();
          if( !empty($values['photo']) ) {
            $property->setPhoto($form->photo);
          }

          //array for members table
          $membersData = array();
          $membersData['property_id'] = $property['property_id'];
          $membersData['user_id'] = $property['tenant_id'];
          $membersData['living_since'] = $values['living_since'];
          $membersData['added_date'] = date('Y-m-d H:i:s');
          $membersData['updated_date'] = date('Y-m-d H:i:s');

          //insert record into members table
          $members = $membersTable->createRow();
          $members->setFromArray($membersData);
          $members->save();

          $db->commit();
        } catch( Exception $e ){
            $db->rollBack();
            throw $e;
        }

      // return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
         return $this->_helper->redirector->gotoRoute(array('property_id'=>$property->getIdentity()), 'view_property', true);
  }





  public function editAction() {

      // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;

    //get the property id form the url
    $propertyId = $this->_getParam('property_id');
    if($propertyId<=0){
      return $this->_forward('notfound', 'error', 'core');
    }
    
    //get the details of specific id
    $propertyTableData = Engine_Api::_()->getItem('property',$propertyId);

    if(!$propertyTableData->property_id){
       return $this->_forward('notfound', 'error', 'core');
    }

    //set the subject
    Engine_Api::_()->core()->setSubject($propertyTableData);

    //select the data from the members table
    $membersTable = Engine_Api::_()->getDbtable('members', 'property');
    
    $selectMember = $membersTable->select()
    ->where('user_id = ?',$propertyTableData->tenant_id)
    ->where('status = ?',1);

    $membersTableData = $membersTable->fetchRow($selectMember);
    //get the data of living since
    $livingSince = $membersTableData->living_since;
    
    //Make a Form
    $this->view->form = $form = new Property_Form_Edit();
    //set the title of the form
    $form->setTitle('Edit Client Of '.$propertyTableData->name);

    if(!$propertyTableData->tenant_id){
      $form->removeElement('tenant_id');
      $form->removeElement('living_since');
    }
    //get the name of the tenant from the
    $tenantName = Engine_Api::_()->property()->getTenantName($propertyTableData->tenant_id);
    $propertyTableData->tenant_id = $tenantName;

    //populate the form
    $propertyData = $propertyTableData->toArray();
    $propertyData['living_since'] = $livingSince;

    $form->populate($propertyData);

    //check the form and post
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !$form->isValid($this->getRequest()->getPost()) ) {
       return;
    }

    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    try{
        $values=$form->getValues();

        if(empty($values['toValues'])){
          $values['tenant_id'] = $membersTableData->user_id;
        }else{
          $values['tenant_id'] = $values['toValues'];
        }

        $propertyTableData->setFromArray($values);
        $propertyTableData->updated_date = date('Y-m-d H:i:s');
        $propertyTableData->save();

        if( !empty($values['photo']) ) {
          $propertyTableData->setPhoto($form->photo);
        }
         
        if($values['tenant_id'] != $membersTableData->user_id){
          
            $membersTable->update(array(
              'status' => 0,
              'left_on' => date('Y-m-d'),
              'updated_date' => date('Y-m-d H:i:s'),
            ), array(
              'user_id = ?' => $membersTableData->user_id,
              'property_id = ?' => $propertyId,
            ));


             //array for members table
            $membersData = array();
            $membersData['property_id'] = $propertyId;
            $membersData['user_id'] = $values['tenant_id'];
            $membersData['living_since'] = $values['living_since'];
            $membersData['added_date'] = date('Y-m-d H:i:s');
            $membersData['updated_date'] = date('Y-m-d H:i:s');

            //insert record into members table
            $members = $membersTable->createRow();
            $members->setFromArray($membersData);
            $members->save();
        }

        $db->commit();
    }catch( Exception $e ) {
        $db->rollBack();
        throw $e;
    }
    
    return $this->_helper->redirector->gotoRoute(array('property_id'=>$propertyTableData->getIdentity()), 'view_property', true);
  }



  public function deleteAction(){
    //get the property id from the url or routes
    $propertyId = $this->_getParam('property_id');
    
    //proeprty table
  	$propertyTable = Engine_Api::_()->getDbtable('properties', 'property');
    $select = $propertyTable->select()->where("property_id = ?", $propertyId);
    $propertyTableData = $propertyTable->fetchRow($select);
    
    //Delete Form
    $this->view->form = $form = new Property_Form_Delete();
    $form->setTitle('Delete Client '.$propertyTableData->name);
    

    // Check post/form
    if( !$this->getRequest()->isPost() ) {
      return;
    }
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

   /* $db = Engine_Db_Table::getDefaultAdapter();*/
    $db = $propertyTable->getAdapter();
    $db->beginTransaction();

    $propertyTableData->delete();
    $db->commit();

    $this->view->status = true;

    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Client deleted successfully.');
    return $this->_forward('success' ,'utility', 'core', array(
      'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module'=>'property','controller'=>'index','action' => 'index'), 'property_general', true),'messages' => Array($this->view->message)
    ));

}



public function uploadAction(){

  //upload form
  $this->view->form = $form = new Property_Form_Upload();

  //property table
  $propertyTable = Engine_Api::_()->getDbtable('properties', 'property');

  //check form/post
  if( !$this->getRequest()->isPost() ) {
      return;
  }

  if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
  }

  if( $form->file instanceof Zend_Form_Element_File ) {
      $filename = $form->file->getFileName();
      echo $filename;
  }else {
      echo "Hello Error";
  }

  $row = 1;

  if (($handle = fopen($filename, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;

        $insertData = array();
        $insertData['address'] = $data[0];
        $insertData['addr2'] = $data[1];
        $insertData['addr3'] = $data[2];
        $insertData['postcode'] = $data[3];
        
        $property = $propertyTable->createRow();
        $property->setFromArray($insertData);
        $property->save();
      }
     fclose($handle);
  }

}
  
  
    public function suggestUserAction()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        if( !$viewer->getIdentity() ) {
          $data = null;
        } else {
          $data = array();
          $table = Engine_Api::_()->getItemTable('user');
          
          $select = Engine_Api::_()->getDbtable('users', 'user')->select();
          $select->where('user_id <> ?', $viewer->user_id);
          
          $blockedUserIds = !$viewer->isAdmin() ? $viewer->getAllBlockedUserIds() : array();
          if( $blockedUserIds ) {
            $select->where('user_id NOT IN(?)', (array) $blockedUserIds);
          }
          if( 0 < ($limit = (int) $this->_getParam('limit', 10)) ) {
            $select->limit($limit);
          }
    
          if( null !== ($text = $this->_getParam('search', $this->_getParam('value'))) ) {
            $select->where('`'.$table->info('name').'`.`displayname` LIKE ?', '%'. $text .'%');
          }
          
          $ids = array();
          foreach( $select->getTable()->fetchAll($select) as $friend ) {
            $data[] = array(
              'type'  => 'user',
              'id'    => $friend->getIdentity(),
              'guid'  => $friend->getGuid(),
              'label' => $friend->getTitle(),
              'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
              'url'   => $friend->getHref(),
            );
            $ids[] = $friend->getIdentity();
            $friend_data[$friend->getIdentity()] = $friend->getTitle();
          }
    
          // first get friend lists created by the user
        //   $listTable = Engine_Api::_()->getItemTable('user_list');
        //   $lists = $listTable->fetchAll($listTable->select()->where('owner_id = ?', $viewer->getIdentity()));
        //   $listIds = array();
        //   foreach( $lists as $list ) {
        //     $listIds[] = $list->list_id;
        //     $listArray[$list->list_id] = $list->title;
        //   }
    
          // check if user has friend lists
        //   if( $listIds ) {
        //     // get list of friend list + friends in the list
        //     $listItemTable = Engine_Api::_()->getItemTable('user_list_item');
        //     $uName = Engine_Api::_()->getDbtable('users', 'user')->info('name');
        //     $iName  = $listItemTable->info('name');
            
        //     $listItemSelect = $listItemTable->select()
        //       ->setIntegrityCheck(false)
        //       ->from($iName, array($iName.'.listitem_id', $iName.'.list_id', $iName.'.child_id',$uName.'.displayname'))
        //       ->joinLeft($uName, "$iName.child_id = $uName.user_id")
        //       //->group("$iName.child_id")
        //       ->where('list_id IN(?)', $listIds);
    
        //     $listItems = $listItemTable->fetchAll($listItemSelect);
    
        //     $listsByUser = array();
        //     foreach( $listItems as $listItem ) {
        //       $listsByUser[$listItem->list_id][$listItem->user_id]= $listItem->displayname ;
        //     }
            
        //     foreach ($listArray as $key => $value){
        //       if (!empty($listsByUser[$key])){
        //         $data[] = array(
        //           'type' => 'list',
        //           'friends' => $listsByUser[$key],
        //           'label' => $value,
        //         );
        //       }
        //     }
        //   }
        }
    
        if( $this->_getParam('sendNow', true) ) {
          return $this->_helper->json($data);
        } else {
          $this->_helper->viewRenderer->setNoRender(true);
          $data = Zend_Json::encode($data);
          $this->getResponse()->setBody($data);
        }
    }
  
  

}

