

<?php
/**
 
 */
class Property_Form_Create extends Engine_Form
{
  public $_error = array();

  public function init()
  {   
    $this->setTitle('Add Your Client Details')
      ->setDescription('')
      ->setAttrib('id', 'messages_compose');
   
      //Property name
    $this->addElement('Text', 'name', array(
      'label' => 'Client Name',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      ),
      'autofocus' => 'autofocus',
    ));
    
    // Address
    $this->addElement('Textarea', 'address', array(
      'label' => 'Address',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      ),
      'attribs' => array('rows' => 2, 'cols' => 180, 'style' => 'min-height:20px;'),
    ));
    $this->addElement('Text', 'addr2', array(
      'label' => 'City',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      )
    ));
    $this->addElement('Text', 'addr3', array(
      'label' => 'State',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      )
    ));
    $this->addElement('Text', 'postcode', array(
      'label' => 'Post Code',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      )
    ));
    
    // Propery Type
    $propertytypes=array(
      ''=>'Select Client Type',
    	'Single-Family Home Co-op'=>'Single-Family Home Co-op',
    	'Multi-Family Home'=>'Multi-Family Home'

    );

      $this->addElement('Select', 'type', array(
        'label' => 'Client Type',
        'multiOptions' => $propertytypes,
        // 'required'=>true,
      ));
    
    
      //photo
    $this->addElement('File', 'photo', array(
      'label' => 'Choose Your Photo',
    ));
    
    $this->photo->addValidator('Extension', false, 'jpg,png,gif,jpeg');


    /* copy from Compose.php */
     // init to
    $this->addElement('Text', 'tenant_id', array(
        'label'=>'Owner Name',
        'id'=>'to',
        'autocomplete'=>'off'
    ));

    Engine_Form::addDefaultDecorators($this->tenant_id);

    // Init to Values
    $this->addElement('Hidden', 'toValues', array(
      'label' => 'Send To',
      'order' => 8,
      'filters' => array(
        'HtmlEntities'
      ),
    ));
    Engine_Form::addDefaultDecorators($this->toValues);

    /* end copy  */

    $this->addElement('Date','living_since',array(
      'label' => 'Living Since',
      // 'required' => true,
    ));

     $this->addElement('Textarea', 'descript', array(
      'label' => 'Description',
      'allowEmpty' => false,
      // 'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      ),
      'attribs' => array('rows' => 2, 'cols' => 180, 'style' => 'min-height:20px;'),
    ));


    // Element: submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Submit',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'Cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => 'javascript:history.back();',
      //'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
}
