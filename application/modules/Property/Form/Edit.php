<?php

/**

 */

class Property_Form_Edit extends Property_Form_Create
{
  public function init()
  {
    parent::init();
    $this->setTitle('Edit Client Details')
      ->setDescription('');
    $this->submit->setLabel('Save Changes');
  }
}