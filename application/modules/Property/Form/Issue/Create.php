<?php
/**
 
 */
class Property_Form_Issue_Create extends Engine_Form
{
  public function init()
  {   
    $this->setTitle('Create Client Issue')
      ->setDescription('');
  
    // Title of property issue
    $this->addElement('Text', 'title', array(
      'label' => 'Title',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '63'))
      ),
      'autofocus' => 'autofocus',
    ));
    
    //  issue discription
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      ),
       'attribs' => array('rows' => 3, 'cols' => 180, 'style' => 'min-height:20px;'),
    ));
   
      //photo
    $this->addElement('File', 'photo', array(
      'label' => 'Photo',
    ));
    
    $this->photo->addValidator('Extension', false, 'jpg,png,gif,jpeg');

    
   /* $this->addElement('Date','starttime',array(
        'label' => 'Fixed By Date',
        'required' => true,
    ));*/

    //Fixed By date
    $starttime = new Engine_Form_Element_CalendarDateTime('starttime');
    $starttime->setLabel("Fixed By Date");
    $starttime->setAllowEmpty(false);
    $starttime->setValue(date('Y-m-d'));
    $this->addElement($starttime);


    // Element: submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Submit',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'Cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => 'javascript:history.back();',
      //'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
}