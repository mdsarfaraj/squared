<?php
/**
 
 */
class Property_Form_Issue_Search extends Engine_Form
{
  public function init()
  {
    $this
      ->setAttribs(array(
        'id' => 'filter_form',
        'class' => 'global_form_box',
      ))
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ->setMethod('GET')
      ;
    
    $this->addElement('Text', 'name', array(
      'label' => 'Client Name',
    ));

    $this->addElement('Text', 'title', array(
      'label' => 'Title',
    ));

    $this->addElement('Select', 'status', array(
      'label' => 'Status',
      'multiOptions' => array("0"=>"To Do", "1"=>"Done"),
    ));


    $this->addElement('Button', 'find', array(
      'type' => 'submit',
      'label' => 'Search',
      'ignore' => true,
    ));

    
  }
}
