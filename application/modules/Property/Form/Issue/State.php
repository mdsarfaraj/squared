<?php
/**
 
 */
class Property_Form_Issue_State extends Engine_Form
{
  public function init()
  {
    $this->setTitle('Issue Status')
      ->setDescription('Are you sure you want to done issue status ?');

    $this->addElement('Button', 'submit', array(
      'label' => 'Done',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => '',
      'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }
}