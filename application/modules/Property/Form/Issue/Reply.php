<?php
/**
 
 */
class Property_Form_Issue_Reply extends Engine_Form
{
  public function init()
  {   
    $this->setTitle('')
      ->setDescription('');
  
    // Title of property issue
    $this->addElement('Textarea', 'message', array(
      'label' => 'Leave your message',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '63'))
      ),
      'autofocus' => 'autofocus',
    ));
    
    $this->addElement('hidden', 'issue_id', 
      array( 
        'order' => 1005, 
        'value' => ''
      )
    );

     $this->addElement('hidden', 'property_id', 
      array( 
        'order' => 1006, 
        'value' => ''
      )
    );

     $this->addElement('hidden', 'user_id', 
      array( 
        'order' => 1007, 
        'value' => ''
      )
    );

    $this->addElement('Button', 'submit', array(
      'label' => 'Reply',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

  }
  
  public function postEntry()
  {
    $values = $this->getValues();

    $message = $values['message'];
    $property_id = $values['property_id'];
    $issue_id = $values['issue_id'];
    $user_id = $values['user_id'];

    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    try {

      $table = Engine_Api::_()->getDbtable('issueconversations', 'property');

      $row = $table->createRow();
      $row->message = $message;
      $row->issue_id = $issue_id;
      $row->property_id = $property_id;
      $row->user_id = $user_id;
      $row->added_date = date('Y-m-d H:i:s');

      $row->save();
      $db->commit();
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
  }
}