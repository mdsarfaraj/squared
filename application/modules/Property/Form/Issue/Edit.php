<?php
/**
 
 */
class Property_Form_Issue_Edit extends Property_Form_Issue_Create
{
  public function init()
  {
    parent::init();
    $this->setTitle('Edit Issue Details')
      ->setDescription('');
    $this->submit->setLabel('Save Changes');
  }
}