<?php
/**
 
 */
class Property_Form_Member_Add extends Engine_Form
{
  public $_error = array();

  public function init()
  {   
	    $this->setTitle('Add Client Members')
	      // ->setDescription('Here you can add family members in property.')
	      ->setAttrib('id', 'messages_compose');
   
	    $this->addElement('hidden', 'property_id', array(
	      'order' => 1, 
	      'value' => ''
	    ));

	     $this->addElement('Text', 'user_id', array(
        'label'=>'Username',
        'id'=>'to',
        'autocomplete'=>'off'
      ));

      Engine_Form::addDefaultDecorators($this->user_id);

      // Init to Values
      $this->addElement('Hidden', 'toValues', array(
        'label' => 'Send To',
        'required' => true,
        'allowEmpty' => false,
        'order' => 2,
        'validators' => array(
          'NotEmpty'
        ),
        'filters' => array(
          'HtmlEntities'
        ),
      ));
      Engine_Form::addDefaultDecorators($this->toValues);



      $this->addElement('Date','living_since',array(
        'label' => 'Living Since',
        'required' => true,
      ));

       $this->addElement('hidden', 'added_date', array(
        'order' => 3, 
        'value' => date('Y-m-d H:i:s'),
      ));

      $this->addElement('hidden', 'updated_date', array(
        'order' => 4, 
        'value' => date('Y-m-d H:i:s'),
      ));



  	   $this->addElement('Button', 'submit', array(
        'label' => 'Add Member',
        'type' => 'submit',
        'ignore' => true,
        'decorators' => array('ViewHelper')
      ));

      $this->addElement('Cancel', 'cancel', array(
        'label' => 'cancel',
        'link' => true,
        'prependText' => ' or ',
        'href' => '',
        'onclick' => 'parent.Smoothbox.close();',
        'decorators' => array(
          'ViewHelper'
        )
      ));
      $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
      $button_group = $this->getDisplayGroup('buttons');


    }
}
