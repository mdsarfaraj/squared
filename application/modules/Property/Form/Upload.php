<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Create.php 10168 2014-04-17 16:29:36Z andres $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Blog
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Property_Form_Upload extends Engine_Form
{
  public $_error = array();

  public function init()
  {   
    $this->setTitle('CSV FILE UPLOAD');
      
    $this->addElement('File', 'file', array(
      'label' => 'Choose Your (*.csv) File',
    ));
    
    //$this->photo->addValidator('Extension', false, 'csv,json');

    $this->addElement('Button', 'submit', array(
      'label' => 'Upload',
      'type' => 'submit',
    ));
  }
  
}
