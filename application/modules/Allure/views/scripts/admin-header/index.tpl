<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<h2>
    <?php echo 'Responsive Allure Theme'; ?>
</h2>

<div class='seaocore_admin_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
</div>
<br/>
<?php $url =  $this->url(array('module' => 'allure', 'controller' => 'landing-page', 'action' => 'slider'), 'admin_default', true);?>
<div class="tip">
    <span><?php echo $this->translate("If you want to add or modify the logo of Landing Page, please <a href='$url'>click here</a>.") ?></span>
</div>
<div class='seaocore_settings_form'>
    <div class='settings'>
        <?php echo $this->form->render($this); ?>
    </div>
</div>