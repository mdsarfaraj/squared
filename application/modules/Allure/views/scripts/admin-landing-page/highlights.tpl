<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<h2>
    <?php echo 'Responsive Allure Theme'; ?>
</h2>

<div class='seaocore_admin_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
</div>
<div class='seaocore_sub_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->subNavigation)->render() ?>
</div>
<?php
  $settingsUrl = $this->url(array('module' => 'allure', 'controller' => 'landing-page', 'action' => 'highlights'), 'admin_default', false);
  $editUrl = $this->url(array('module' => 'allure', 'controller' => 'landing-page', 'action' => 'list-highlights'), 'admin_default', false);
?>
<div class='tabs seaocore_sub_tabs'>
  <ul class="navigation">    
    <li  class="<?php echo ($this->selectedMenuType == 'view')? 'active': ''; ?>">
      <a href="<?php echo $settingsUrl; ?>"><?php echo $this->translate("Settings"); ?></a>
    </li>
    <li  class="<?php echo ($this->selectedMenuType == 'edit')? 'active': ''; ?>">
      <a href="<?php echo $editUrl; ?>"><?php echo $this->translate("Manage Highlights Block"); ?></a>
    </li>
  </ul>
</div>

<div class='seaocore_settings_form'>
  <br>
  <div class='settings'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>

<script type="text/javascript">
  var form = document.getElementById("form-upload");
  window.addEvent('domready', function () {
    showVideoUrl();
  });


  function showVideoUrl() {
    if (form.elements["allure_landing_highlights_attachVideo"].value == 1) {
      $('allure_landing_highlights_videoEmbed-wrapper').style.display = 'block';
    } else {
      $('allure_landing_highlights_videoEmbed-wrapper').style.display = 'none';
    }
  }

</script>