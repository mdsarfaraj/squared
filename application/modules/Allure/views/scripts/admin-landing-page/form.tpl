<?php

/**
* SocialEngine
*
* @category   Application_Extensions
* @package    Sitemusic
* @copyright  Copyright 2017-2022 BigStep Technologies Pvt. Ltd.
* @license    http://www.socialengineaddons.com/license/
* @version    $Id: form.tpl 2017-03-27 9:40:21Z SocialEngineAddOns $
* @author     SocialEngineAddOns
*/
?>
<?php echo $this->form->setAttrib('class', 'global_form_popup')->render($this) ?>


<?php if( @$this->closeSmoothbox ): ?>
<script type="text/javascript">
  TB_close();
</script>
<?php endif; ?>
