<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<h2>
    <?php echo 'Responsive Allure Theme'; ?>
</h2>

<div class='seaocore_admin_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
</div>
<div class='seaocore_sub_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->subNavigation)->render() ?>
</div>
<div class="tip">
    <span><?php echo $this->translate("To set up this section place Responsive Allure Theme - Promotional Banner widget on your landing page via layout editor.") ?></span>
</div>
<div class='seaocore_settings_form'>
    <div class='settings'>
        <?php echo $this->form->render($this); ?>
    </div>
</div>

<script type="text/javascript">
	function changeActionButtons(value) {
		if(value == 1) {
			$('allure_landing_appbanner_actionText-wrapper').show();
			$('allure_landing_appbanner_actionUrl-wrapper').show();
			$('allure_landing_appbanner_appstoreUrl-wrapper').hide();
			$('allure_landing_appbanner_playstoreUrl-wrapper').hide();
		} else {
			$('allure_landing_appbanner_actionText-wrapper').hide();
			$('allure_landing_appbanner_actionUrl-wrapper').hide();
			$('allure_landing_appbanner_appstoreUrl-wrapper').show();
			$('allure_landing_appbanner_playstoreUrl-wrapper').show();
		} 
	}
	window.addEvent('domready', function(){
		if($('allure_landing_appbanner_buttons-1').checked) {
			changeActionButtons(1);
		} else {
			changeActionButtons(0);
		}
	});
</script>