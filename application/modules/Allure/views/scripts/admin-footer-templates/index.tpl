<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<h2><?php echo "Responsive Allure Theme" ?></h2>

<?php if (count($this->navigation)): ?>
    <div  class='seaocore_admin_tabs tabs clr'>
        <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
    </div>

    <div  class='seaocore_sub_tabs tabs clr'>
        <?php echo $this->navigation()->menu()->setContainer($this->subNavigation)->render() ?>
    </div>
<?php endif; ?>
<?php
$this->form->setDescription("Here, you can manage footer templates.");
$this->form->getDecorator('Description')->setOption('escape', false);
?>
<div class='seaocore_settings_form'>
    <div class='settings'>
        <?php echo $this->form->render($this); ?>
    </div>
</div>
<?php
$coreSettings = Engine_Api::_()->getApi('settings', 'core');
$allureFooterBackground = $coreSettings->getSetting('allure.footer.background', 2);
$allureFooterShowLogo = $coreSettings->getSetting('allure.footer.show.logo', 1);
$allureTwitterFeed = $coreSettings->getSetting('allure.twitter.feed', 0);
$allureFooterShowFooterHtmlBlock = $coreSettings->getSetting('allure.footer.show.footer.html.block', 1);
$showAllureFooterTemplate = $coreSettings->getSetting('allure.footer.templates', 2);
$localeMultiOptions = Engine_Api::_()->allure()->getLanguageArray();
$total_allowed_languages = Count($localeMultiOptions);
if (!$allureFooterShowFooterHtmlBlock) {
    if (!empty($localeMultiOptions)) {
        foreach ($localeMultiOptions as $key => $label) {
            ?>
            <script type="text/javascript">
                $("allure_footer_lending_page_block_<?php echo $key; ?>-wrapper").style.display = 'none';
            </script>
            <?php
        }
    }
}
?>

<script type="text/javascript">
    window.addEvent('domready', function () {
        showFooterBackgroundImage('<?php echo $allureFooterBackground; ?>');
        showFooterLogo('<?php echo $allureFooterShowLogo; ?>');
        displayFooterHtmlBlock('<?php echo $showAllureFooterTemplate; ?>');
        showTwitterFeed('<?php echo $allureTwitterFeed; ?>');
        $('note_description-label').hide();
    });

    function showFooterBackgroundImage(val) {

        if (val == 1) {
            $('allure_footer_backgroundimage-wrapper').style.display = 'none';
        } else {
            $('allure_footer_backgroundimage-wrapper').style.display = 'block';
        }

    }
    function showFooterLogo(val) {
        if (val == 1) {
            $('allure_footer_select_logo-wrapper').style.display = 'block';
        } else {
            $('allure_footer_select_logo-wrapper').style.display = 'none';
        }
    }
    
    function showTwitterFeed(val) {
        if (val == 1) {
            $('allure_twitterCode-wrapper').style.display = 'block';
        } else {
            $('allure_twitterCode-wrapper').style.display = 'none';
        }
    }

    function displayFooterHtmlBlock(val) {

        if (<?php echo $showAllureFooterTemplate; ?> == 1 || <?php echo $showAllureFooterTemplate; ?> == 3) {
            val == 0;
        }

        if (val == 2) {
<?php
if (!empty($localeMultiOptions)) {
    foreach ($localeMultiOptions as $key => $label) {
        ?>
                    $("allure_footer_lending_page_block_<?php echo $key; ?>-wrapper").style.display = 'block';
    <?php
    }
}
?>

        } else {
<?php
if (!empty($localeMultiOptions)) {
    foreach ($localeMultiOptions as $key => $label) {
        ?>
                    $("allure_footer_lending_page_block_<?php echo $key; ?>-wrapper").style.display = 'none';
    <?php
    }
}
?>
        }

    }
</script>
