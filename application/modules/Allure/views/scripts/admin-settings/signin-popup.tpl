<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: signin-popup.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<h2>
    <?php echo 'Responsive Allure Theme'; ?>
</h2>

<div  class='seaocore_admin_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
</div>
<?php if (!Engine_Api::_()->hasModuleBootstrap('sitemenu')): ?>
<div class="tip">
  <span>
    The given set of settings will work only if <a href="https://www.socialengineaddons.com/socialengine-advanced-menus-plugin-interactive-attractive-navigation" target="_blank">Advanced Menus Plugin - Interactive and Attractive Navigation</a> is installed on your website.
  </span>
</div>
<?php endif; ?>
<div class='seaocore_settings_form'>
    <div class='settings'>
        <?php echo $this->form->render($this); ?>
    </div>
</div>