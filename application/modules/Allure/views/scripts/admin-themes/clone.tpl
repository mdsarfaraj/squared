<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: clone.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Steve
 */
?>
<h2><?php echo $this->translate("Responsive Allure Theme") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='seaocore_admin_tabs tabs clr'>
    <?php
    // Render the menu
    //->setUlClass()
    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>
<div class='seaocore_sub_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->subNavigation)->render() ?>
</div>
<div class="settings">
<?php echo $this->form->render($this) ?>
</div>