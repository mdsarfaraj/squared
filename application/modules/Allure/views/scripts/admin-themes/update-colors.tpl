<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>

<h2><?php echo $this->translate("Responsive Allure Theme") ?></h2>

<?php if( count($this->navigation) ): ?>
	<div class='seaocore_admin_tabs tabs clr'>
		<?php
		// Render the menu
		//->setUlClass()
		echo $this->navigation()->menu()->setContainer($this->navigation)->render()
		?>
	</div>
<?php endif; ?>
<div class='seaocore_sub_tabs tabs clr'>
    <?php echo $this->navigation()->menu()->setContainer($this->subNavigation)->render() ?>
</div>

<div class='clear'>
  <div class='settings'>

		<?php echo $this->form->render($this); ?>

  </div>
</div>
<script type="text/javascript">
  function spwThemeUpdateMethod(showElement) {
    var hideElement = showElement == 'group' ? 'single' : 'group';
    $$('.constant_color_' + hideElement + '_element').hide();
    $$('.constant_color_' + showElement + '_element').show();
    
    if(showElement == 'group') {
      $('allure_header_constants-wrapper').hide();
      $('allure_footer_constants-wrapper').hide();
      $('allure_body_constants-wrapper').hide();
    } else {
      $('allure_header_constants-wrapper').show();
      $('allure_footer_constants-wrapper').show();
      $('allure_body_constants-wrapper').show();
    }
  }
  spwThemeUpdateMethod($('allure_update_method').value);
</script>