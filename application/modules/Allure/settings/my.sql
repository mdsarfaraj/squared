/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: my.sql 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

--
-- Dumping data for table `engine4_core_modules`
--

INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('allure', 'Responsive Allure Theme', 'Responsive Allure Theme', '4.10.3p22', 1, 'extra') ;


DROP TABLE IF EXISTS `engine4_allure_images`;
CREATE TABLE IF NOT EXISTS `engine4_allure_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR( 64 ) NOT NULL,
  `icon_id` int(11) NOT NULL DEFAULT "0",
  `file_id` int(11) NOT NULL DEFAULT "0",
  `enabled` tinyint(4) NOT NULL DEFAULT "1",
  `order` tinyint(4) NOT NULL DEFAULT "99",
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `engine4_allure_subscriptions`;
CREATE TABLE IF NOT EXISTS `engine4_allure_subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR( 64 ) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT "0",
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY  (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`)
VALUES ("allure_admin_settings", "allure", "SEAO - Responsive Allure Theme", NULL , '{"route":"admin_default","module":"allure","controller":"settings"}', "core_admin_main_plugins", NULL , "1", "0", "999"),
("allure_admin_settings_index", "allure", "Global Settings", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"index"}', "allure_admin_main", "", 1, 0, 1),
("allure_admin_configure_pages", "allure", "Configure Pages", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"configure-pages"}', "allure_admin_main", "", 1, 0, 22),

("allure_admin_landingpage", "allure", "Landing Page", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "slider"}', "allure_admin_main", "", 1, 0, 9),
("allure_admin_landingpage_slider", "allure", "Landing Page Slider", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "slider"}', "allure_admin_landingpage", "", 1, 0, 1),
("allure_admin_landingpage_cta", "allure", "Action Buttons", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "cta-buttons"}', "allure_admin_landingpage", "", 1, 0, 11),
("allure_admin_landingpage_text_banner", "allure", "Banner Tagline", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "text-banner"}', "allure_admin_landingpage", "", 1, 0, 7),
("allure_admin_landingpage_app_banner", "allure", "Promotion Banner", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "app-banner"}', "allure_admin_landingpage", "", 1, 0, 13),
("allure_admin_landingpage_stats", "allure", "Achievement Block", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "stats"}', "allure_admin_landingpage", "", 1, 0, 5),
("allure_admin_landingpage_services", "allure", "Services Block", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "services"}', "allure_admin_landingpage", "", 1, 0, 9),
("allure_admin_landingpage_highlights", "allure", "Highlights Block", "", '{"route":"admin_default","module":"allure","controller":"landing-page", "action": "highlights"}', "allure_admin_landingpage", "", 1, 0, 3),
("allure_admin_settings_signin_popup", "allure", "Manage Sign-in Popup", "Allure_Plugin_Menus", '{"route":"admin_default","module":"allure","controller":"settings", "action": "signin-popup"}', "allure_admin_main", "", 1, 0, 7),
("allure_admin_header_index", "allure", "Manage Header", "", '{"route":"admin_default","module":"allure","controller":"header", "action": "index"}', "allure_admin_main", "", 1, 0, 11),

("allure_admin_theme_customization", "allure", "Theme Customization", "", '{"route":"admin_default","module":"allure","controller":"themes"}', "allure_admin_main", "", 1, 0, 5),
("allure_admin_theme_customization_color", "allure", "Color Editor", "", '{"route":"admin_default","module":"allure","controller":"themes"}', "allure_admin_theme_customization", "", 1, 0, 1),
("allure_admin_settings_custom_css", "allure", "Custom CSS", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"custom-css"}', "allure_admin_theme_customization", "", 1, 0, 7),

("allure_admin_font_index", "allure", "Manage Fonts", "", '{"route":"admin_default","module":"allure","controller":"fonts", "action": "index"}', "allure_admin_theme_customization", "", 1, 0, 3),
("allure_admin_layout_index", "allure", "Layout Settings", "", '{"route":"admin_default","module":"allure","controller":"layout", "action": "index"}', "allure_admin_theme_customization", "", 1, 0, 5),

("allure_admin_settings_images", "allure", "Slider Images", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"landing-images"}', "allure_admin_main", "", 1, 0, 15),
("allure_admin_settings_landing_images", "allure", "Landing Page Slider Images", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"landing-images"}', "allure_admin_settings_images", "", 1, 0, 1),
("allure_admin_settings_inner_images", "allure", "Inner Page Slider Images", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"inner-images"}', "allure_admin_settings_images", "", 1, 0, 2),
("allure_admin_settings_footer", "allure", "Manage Footer", "", '{"route":"admin_default","module":"allure","controller":"footer-templates"}', "allure_admin_main", "", 1, 0, 13),
("allure_admin_footer_templates", "allure", "Footer Settings", "", '{"route":"admin_default","module":"allure","controller":"footer-templates"}', "allure_admin_settings_footer", "", 1, 0, 1),
("allure_admin_settings_footer_menu", "allure", "Footer Menu", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"footer-menu"}', "allure_admin_settings_footer", "", 1, 0, 2),
("allure_admin_settings_subscription", "allure", "Newsletter Subscription", "", '{"route":"admin_default","module":"allure","controller":"subscription","action":"index"}', "allure_admin_main", "", 1, 0, 19),
("allure_admin_settings_faq", "allure", "FAQs", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"faq"}', "allure_admin_main", "", 1, 0, 999);

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`)
VALUES ('allure_admin_main_theme', "allure", "Responsive Allure Theme", "" , '{"route":"admin_default","module":"allure","controller":"settings"}', "core_admin_main", NULL , "1", "0", "999");

DROP TABLE IF EXISTS `engine4_allure_banners`;
CREATE TABLE IF NOT EXISTS `engine4_allure_banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR( 64 ) NOT NULL,
  `icon_id` int(11) NOT NULL DEFAULT "0",
  `file_id` int(11) NOT NULL DEFAULT "0",
  `enabled` tinyint(4) NOT NULL DEFAULT "1",
  `order` tinyint(4) NOT NULL DEFAULT "99",
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;


DROP TABLE IF EXISTS `engine4_allure_services`;
CREATE TABLE IF NOT EXISTS `engine4_allure_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR( 64 ) NOT NULL,
  `description` VARCHAR( 512 ) NOT NULL,
  `file_id` int(11) NOT NULL DEFAULT "0",
  `enabled` tinyint(4) NOT NULL DEFAULT "1",
  `order` tinyint(4) NOT NULL DEFAULT "99",
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

INSERT IGNORE INTO `engine4_allure_services` (`service_id`, `title`, `description`, `file_id`, `enabled`, `order`) VALUES
(1, 'Responsive Communities', 'SEAO packages come up with a pinch of magic. Responsive Design makes your community look good on any screen (desktops, tablets, and phones).', '0', 1, 1),
(2, 'Flexible', 'Flexibility makes a community engagement much easier. Having the ability to fabricate it the way you want is always propitious.', '0', 1, 2),
(3, 'Advertising', 'Advertisements are an excellent way to monetize a social network. It employs an openly sponsored, non-personal message to promote or sell a product, service or idea.', '0', 1, 3),
(4, 'E-Commerce', 'E-commerce is the activity of buying or selling online. E-commerce draws on technologies such as mobile commerce, electronic funds transfer, internet marketing etc', '0', 1, 4),
(5, 'Secure', 'Complete Security is the suite of products that enables Enterprise professionals to deliver protection without pausing to mitigate risk and ensure uninterrupted performance.', '0', 1, 5),
(6, 'Drag and Drop content Management', 'Drag and drop features make the creation of website user friendly. Simply say no to codes. Everything in the community is completely cooked for you.', '0', 1, 6),
(7, 'Anti Spam features', 'Keeping community free of spams helps in keeping member’s social experience enjoyable and clutter-free.', '0', 1, 7),
(8, 'Everything collaborated at one place', 'Want to make the best ever community? Join us. We provides numerous features tailored at one place.', '0', 1, 8),
(9, 'Eye Catching Community', 'Best communities are those which seize the visitors by merely looking at them.', '0', 1, 9);

DROP TABLE IF EXISTS `engine4_allure_highlights`;
CREATE TABLE IF NOT EXISTS `engine4_allure_highlights` (
  `highlights_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR( 64 ) NOT NULL,
  `description` VARCHAR( 512 ) NOT NULL,
  `file_id` int(11) NOT NULL DEFAULT "0",
  `enabled` tinyint(4) NOT NULL DEFAULT "1",
  `order` tinyint(4) NOT NULL DEFAULT "99",
  PRIMARY KEY (`highlights_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

INSERT IGNORE INTO `engine4_allure_highlights` (`highlights_id`,`title`, `description`, `file_id`, `enabled`, `order`) VALUES 
(1,'Solicit small donations with a big impact', 'Even small donations can go a long way and make a big impact on charities. A few subtle words changes could lead to many more donors.', '0', '1', '1'),
(2,'Mobilize supporters', 'Every supporter is a potential outreach hub in his or her social universe. Mobilization involves people to do things.', '0', '1', '2'),
(3,'Connect people', 'Connecting more faces around the world makes your virtual family even more enjoyable and happy.', '0', '1', '3'),
(4,'Raise awareness', 'Doing something which you think can solve the problem and let people know regarding it.', '0', '1', '4'),
(5,'Give people a place to meet virtually', 'In this life full of hustles, a platform is needed to give people a virtual company to enjoy.', '0', '1', '5'),
(6,'Business around the world', 'To work until the world knows you. Work all around the world to reach at peak of success.', '0', '1', '6'),
(7, 'Promote affiliate products and services', 'Promoting your expertise is the initial step to grow. Agile promotions always culminate the work.', '0', '1', '7'),
(8, 'Share your expertise and experiences', 'Spread your expertise around the world to make people reach you. Share your experiences with your network to make people get inspired by you.', '0', '1', '8');


DELETE FROM `engine4_core_settings` WHERE `engine4_core_settings`.`name` = "allure.navi.auth" LIMIT 1;

DELETE FROM `engine4_core_settings` WHERE `engine4_core_settings`.`name` = "allure.manage.type" LIMIT 1;
	
DELETE FROM `engine4_core_settings` WHERE `engine4_core_settings`.`name` = "allure.info.type" LIMIT 1;
	
INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES 
	("allure.manage.type", "' . $var1 . '"),
	("allure.info.type", "' . $var2 . '");


INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES
('allure.global.type', '0'),
('allure.isActivate', '1');


INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
("allure_footer_first_column", "allure", "First Column", NULL, '{"uri":"javascript:void(0)"}', "allure_footer", NULL, "1", "1", "1"),
("allure_footer_first_column_1", "allure", "First Column - 1", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "2"), 
("allure_footer_first_column_2", "allure", "First Column - 2", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "3"), 
("allure_footer_first_column_3", "allure", "First Column - 3", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "4"), 
("allure_footer_first_column_4", "allure", "First Column - 4", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "5"), 
("allure_footer_first_column_5", "allure", "First Column - 5", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "6"), 

("allure_footer_second_column", "allure", "Second Column", NULL , '{"uri":"javascript:void(0)"}', "allure_footer", NULL , "1", "1", "10"), 
("allure_footer_second_column_1", "allure", "Second Column - 1", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "11"), 
("allure_footer_second_column_2", "allure", "Second Column - 2", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "12"), 
("allure_footer_second_column_3", "allure", "Second Column - 3", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "13"),
("allure_footer_second_column_4", "allure", "Second Column - 4", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "14"),
("allure_footer_second_column_5", "allure", "Second Column - 5", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "15"),

("allure_footer_third_column", "allure", "Third Column", NULL , '{"uri":"javascript:void(0)"}', "allure_footer", NULL , "1", "1", "20"), 
("allure_footer_third_column_1", "allure", "Third Column - 1", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "21"), 
("allure_footer_third_column_2", "allure", "Third Column - 2", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "22"), 
("allure_footer_third_column_3", "allure", "Third Column - 3", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "23"),
("allure_footer_third_column_4", "allure", "Third Column - 4", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "24"),
("allure_footer_third_column_5", "allure", "Third Column - 5", "" , '{"route":"default"}', "allure_footer", NULL , "1", "1", "25");

INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`, `order`) VALUES 
("allure_footer", "standard", "Responsive Allure Theme - Footer Menu", "1");

 

DROP TABLE IF EXISTS `engine4_allure_themes`;
CREATE TABLE IF NOT EXISTS `engine4_allure_themes` (
  `theme_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`theme_id`),
  UNIQUE KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

--
-- Dumping data for table `engine4_allure_themes`
--

INSERT IGNORE INTO `engine4_allure_themes` ( `name`, `title`, `description`, `active`, `type`) VALUES
('default', 'Pink', '', 1, 1),
('skyblue', 'Sky Blue', '', 0, 1),
('green', 'Green', '', 0, 1),
('orange', 'Orange', '', 0, 1),
('yellow', 'Yellow', '', 0, 1),
('red', 'Red', '', 0, 1),
('darkpink', 'Dark Pink', '', 0, 2),
('darkskyblue', 'Dark Sky Blue', '', 0, 2),
('darkgreen', 'Dark Green', '', 0, 2),
('darkorange', 'Dark Orange', '', 0, 2),
('darkyellow', 'Dark Yellow', '', 0, 2),
('darkred', 'Dark Red', '', 0, 2);

UPDATE `engine4_core_modules` SET `enabled` = '0' WHERE `engine4_core_modules`.`name` = 'mobi';

INSERT IGNORE INTO `engine4_core_themes` (`name`, `title`, `description`, `active`) VALUES
('allure', 'Responsive Allure Theme', '', 1);
UPDATE `engine4_core_themes` SET `active` = '0';
UPDATE `engine4_core_themes` SET `active` = '1' WHERE `engine4_core_themes`.`name` = 'allure';