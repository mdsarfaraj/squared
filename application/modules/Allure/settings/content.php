<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: content.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
$view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
$setttingsUrl = $view->url(array('module' => 'allure', 'controller' => 'landing-page'));
$footerTemplate = $view->url(array('module' => 'allure', 'controller' => 'footer-templates'));
$headerSettings = $view->url(array('module' => 'allure', 'controller' => 'header'));
$innerImagesSettings = $view->url(array('module' => 'allure', 'controller' => 'settings', 'action' => 'inner-images'));

$onloadScript = " <script>
 window.addEvent('domready', function () {
      $('title-wrapper').style.display = 'none';
});


</script>";
return array(
    array(
        'title' => 'Responsive Allure Theme - Footer Text',
        'description' => 'You can place this widget in the footer and can set text accordingly from the ‘Language Manager’ under ‘Layout’ section available in the admin panel of your site. For more detail, please read FAQ section from Admin Panel => Responsive Allure Theme => FAQs',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.homepage-footertext',
        'adminForm' => array(
            'elements' => array(
                array(
                    'Radio',
                    'show_signup_popup_footer',
                    array(
                        'label' => "Do you want to open SignUp form in pop-up when create account button is clicked in this widget?",
                        'multiOptions' => array(
                            1 => 'Yes, Show SignUp form in popup.',
                            0 => 'No, Do not show SignUp form in popup.'
                        ),
                        'value' => 1,
                    )
                ),
            )
        )
    ),
    array(
        'title' => 'Responsive Allure Theme - Footer Menu',
        'description' => 'Displays the site-wide footer menu. You can edit its content in your menu editor. To edit other settings and content click <a href = "'.$footerTemplate.'" target = "_blank">here</a>',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.menu-footer',
        'requirements' => array(
            'header-footer',
        ),
        'adminForm' => array(
            'elements' => array(
            )
        ),
    ),
    array(
        'title' => 'Responsive Allure Theme - Inner Page Slider Images',
        'description' => 'Displays the slider images uploaded by you in Admin Panel => Slider Images =>  Inner page Slider Images . This widget can be placed on any widgetized page. To upload more images click <a href = "'.$innerImagesSettings.'" target = "_blank">here</a>',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.banner-images',
        'adminForm' => 'Allure_Form_Admin_Widget_BannerContent',
        'autoEdit' => 'true'
    ),
    array(
        'title' => 'Responsive Allure Theme - Landing Page Slider',
        'description' => 'Displays images uploaded by you in Admin Panel => Slider Images => Landing Page Image Slider Images on landing page. To upload more images click <a href = "'.$setttingsUrl.'/slider" target = "_blank">here</a>',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.images',
        'autoEdit' => 'true'
    ),
    array(
      'title' => 'Responsive Allure Theme - Services Block',
      'description' => 'Displays all the services provided by you via your website. Click on <a href = "'.$setttingsUrl.'/services" target = "_blank">edit</a> to modify the content for this widget.',
      'decorators' => array('ViewHelper', array('Description', array('placement' => 'PREPEND', 'escape' => false))),
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.our-services',
      'defaultParams' => array(
        'title' => 'Checkout Our Services',
        'titleCount' => true,
      ),
    ),
  array(
      'title' => 'Responsive Allure Theme - Achievements Block',
      'description' => 'Displays the achievements along with the achievement count. Click on <a href = "'.$setttingsUrl.'/stats" target = "_blank">edit</a> to modify the content for this widget..',
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.stats-block',
      'defaultParams' => array(
        'title' => '',
        'titleCount' => true,
      ),
//      'adminForm' => 'Allure_Form_Admin_Widget_Stats',
      'autoEdit' => 'true',
    ),
  array(
      'title' => 'Responsive Allure Theme - Highlights Block',
      'description' => 'Displays the highlights of the website. Click on <a href = "'.$setttingsUrl.'/highlights" target = "_blank">edit</a> to modify the content for this widget.',
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.highlights-block',
      'defaultParams' => array(
        'title' => '',
        'titleCount' => true,
      ),
      'adminForm' => array(
        'elements' => array(
          array(
            'Text',
            'description',
            array(
              'label' => 'Description',
              'value' => '',
            )
          ),
        ),
      ),
    ),
  array(
      'title' => 'Responsive Allure Theme - Sign In and Sign Up Banner',
      'description' => 'Displays the banner image behind the Sign In and Sign Up form. Upload preferred banner image in Admin Panel => Appearance => File & Media Manager.',
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.form-banner',
      'defaultParams' => array(
        'title' => '',
      ),
      'adminForm' => 'Allure_Form_Admin_Widget_Banner',
      'autoEdit' => 'true',
    ),
	array(
      'title' => 'Responsive Allure Theme - Header',
      'description' => 'Displays the header options on your site. You can edit its content in your menu editor. Click on <a href = "'.$headerSettings.'" target = "_blank">edit</a> to modify the other content for this widget.',
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.header',
      'defaultParams' => array(
        'title' => '',
        ),
      'autoEdit' => 'true',
    ),
	array(
      'title' => 'Responsive Allure Theme - Action Buttons',
      'description' => 'Displays the custom content added for landing page. Click on <a href = "'.$setttingsUrl.'/cta-buttons" target = "_blank">edit</a> to modify the content for this widget.',
      'category' => 'SEAO - Responsive Allure Theme',
      'type' => 'widget',
      'name' => 'allure.static-buttons',
      'defaultParams' => array(
        'title' => '',
        ),
    //  'adminForm' => 'Allure_Form_Admin_Widget_Buttons',
      'autoEdit' => 'true',
    ),
    array(
        'title' => 'Responsive Allure Theme - Promotional Banner',
        'description' => '<a href = "'.$setttingsUrl.'/app-banner" target = "_blank">Click here</a> for settings.',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.app-promotion'
    ),
    array(
        'title' => 'Responsive Allure Theme -  Banner Tagline Block',
        'description' => 'Displays the banner tagline on your landing page. Click on <a href = "'.$setttingsUrl.'/text-banner" target = "_blank">Click here</a> to modify the content for this widget.',
        'category' => 'SEAO - Responsive Allure Theme',
        'type' => 'widget',
        'name' => 'allure.text-banner'
    ),

  )
?>