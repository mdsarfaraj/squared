


-- delete the previous tab to make them in sub tabs. IT SHOULD BE REMOVED WHILE FINAL RELEASE OR CODE REVIEW
DELETE FROM `engine4_core_menuitems` WHERE `engine4_core_menuitems`.`name` = 'allure_admin_theme_customization';
DELETE FROM `engine4_core_menuitems` WHERE `engine4_core_menuitems`.`name` = 'allure_admin_settings_custom_css';
 


INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`)
VALUES ("allure_admin_configure_pages", "allure", "Configure Pages", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"configure-pages"}', "allure_admin_main", "", 1, 0, 2),
("allure_admin_theme_customization", "allure", "Theme Customization", "", '{"route":"admin_default","module":"allure","controller":"themes"}', "allure_admin_main", "", 1, 0, 5),
("allure_admin_theme_customization_color", "allure", "Color Editor", "", '{"route":"admin_default","module":"allure","controller":"themes"}', "allure_admin_theme_customization", "", 1, 0, 1),
("allure_admin_settings_custom_css", "allure", "Custom CSS", "", '{"route":"admin_default","module":"allure","controller":"settings","action":"custom-css"}', "allure_admin_theme_customization", "", 1, 0, 3);