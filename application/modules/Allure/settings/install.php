<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: install.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
require_once realpath(dirname(__FILE__)) . '/sitemodule_install.php';

class Allure_Installer extends SiteModule_Installer
{
  /* If client are used the below plugins then need to check for upgrade */ 
  protected $_deependencyVersion = array(
    'advancedactivity' => '4.10.3p20',
    'communityad' => '4.10.3p1',
    'seaocore' => '4.10.3p15',
    'sitemenu' => '4.10.3p9',
    'sitealbum' => '4.10.3p7',
    'sitemember' => '4.10.3p12',
    'siteadvsearch' => '4.10.3p2',
    'sitereview' => '4.10.3p4',
    'siteevent' => '4.10.3p14',
    'siteemailverification' => '4.10.3p3',
    'siteotpverifier' => '4.10.3p4',
    'sitecontentcoverphoto' => '4.10.3p1',
    'sitelogin' => '4.10.3p3',
    'sitepushnotification' => '4.10.3p1',
    'siteshare' => '4.10.3p2',
    'siteusercoverphoto' => '4.10.3p4',
    'userconnection' => '4.10.3',
    'sitemulticurrency' => '4.10.3p3',
    'sitevideo' => '4.10.3p9',
  );

  public function onInstall()
  {
    $db = $this->getDb();

    $select = new Zend_Db_Select($db);
    $select
      ->from('engine4_core_modules')
      ->where('name = ?', 'sitemenu')
      ->where('enabled = ?', 1);
    $check_sitemenu = $select->query()->fetchObject();
    if( !empty($check_sitemenu) ) {
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_auth';");
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_admin';");
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_signin';");
      $db->query("INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('allure_core_mini_admin', 'allure', 'Admin', 'Allure_Plugin_Menus', '', 'user_settings', '', 1, 0, 10),
( 'allure_core_mini_auth', 'allure', 'Sign Out', 'Allure_Plugin_Menus', '', 'user_settings', '', 1, 0, 11),
( 'allure_core_mini_signin', 'allure', 'Sign In', 'Allure_Plugin_Menus', '', 'core_mini', '', 1, 0, 12);
");
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_admin';");
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_auth';");
      $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_signin';");

      $select = new Zend_Db_Select($db);
      $select
        ->from('engine4_core_modules')
        ->where('name = ?', 'siteeventticket')
        ->where('enabled = ?', 1);
      $check_siteeventticket = $select->query()->fetchObject();
      if( !empty($check_siteeventticket) ) {
        $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` = 'core_mini_siteeventticketmytickets';");

        $db->query('INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
("allure_siteeventticket_main_ticket", "siteeventticket", "My Tickets", "Allure_Plugin_Menus", \'{"route":"siteeventticket_order", "action":"my-tickets"}\', "user_settings", "", 1, 0, 9)');
      }
    }



    $this->_createCustomizationFile("/application/themes/allure/customization.css", '/* ADD CUSTOM STYLE */');
    $this->_createCustomizationFile("/public/seaocore_themes/allureThemeGeneralConstants.css", '/* EDIT CONSTANTS, DO NOT UPDATE IT*/');
    $this->updatePage('core_help_privacy', 'Your Privacy - Our Concern', 'We Respect Your Right to Privacy...');
    $this->updatePage('core_help_terms', 'Trust But Verify', 'We Believe on you so You Can');
    $this->updatePage('core_help_contact', 'We Cherish Interactions', 'Because We Know Your Time Is Precious So, Get In Touch Now..');
    parent::onInstall();
  }

//  public function onPostInstall()
//  {
//    if( $this->_databaseOperationType != 'install' ) {
//      return;
//    }
//
//    //RUN THE FOLLOWING CODE ONLY IN CASE OF INSTALL
//    $view = new Zend_View();
//    $baseUrl = (!empty($_ENV["HTTPS"]) && 'on' == strtolower($_ENV["HTTPS"]) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . str_replace('install/', '', $view->url(array(), 'default', true));
//    $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
//    $actionName = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
//    if( $actionName == 'install' ) {
//      $redirector->gotoUrl($baseUrl . 'admin/allure/settings/activate/redirect/install');
//    } else {
//      $redirector->gotoUrl($baseUrl . 'admin/allure/settings/activate/redirect/query');
//    }
//  }

  public function onEnable()
  {
    $this->hideHeaderFooterOnPages(true);
    $db = $this->getDb();
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_auth';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_admin';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_signin';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_admin';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_auth';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'allure_core_mini_signin';");
    parent::onEnable();
  }

  public function onDisable()
  {
    $this->hideHeaderFooterOnPages(false);
    $db = $this->getDb();
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_auth';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_admin';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '0' WHERE `engine4_core_menuitems`.`name` LIKE '%_mini_signin';");

    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'core_mini_auth';");
    $db->query("UPDATE `engine4_core_menuitems` SET `enabled` = '1' WHERE `engine4_core_menuitems`.`name` LIKE 'core_mini_admin';");
    parent::onDisable();
  }

  private function _createCustomizationFile($filePath, $data)
  {
    $filePath = str_replace('/', DS, $filePath);
    $realfilePath = APPLICATION_PATH . $filePath;
    $global_directory_name = dirname($realfilePath);
    $is_dir_exist = @is_dir($realfilePath);
    if( !$is_dir_exist ) {
      mkdir($global_directory_name);
    }
    @chmod($global_directory_name, 0777);

    if( !is_readable($global_directory_name) ) {
      return $this->_error("<span style='color:red'>Note: You do not have readable permission on the path below, please give 'chmod 777 recursive permission' on it to continue with the installation process : <br /> 
  Path Name: <b>" . $global_directory_name . "</b></span>");
    }

    $is_file_exist = @file_exists($realfilePath);
    if( empty($is_file_exist) ) {
      if( !is_writable($global_directory_name) ) {
        return $this->_error("<span style='color:red'>Note: You do not have writable permission on the path below, please give 'chmod 777 recursive permission' on it to continue with the installation process : <br /> 
  Path Name: " . $global_directory_name . "</span>");
      }

      $fh = @fopen($realfilePath, 'w');
      @fwrite($fh, $data);
      @fclose($fh);

      @chmod($realfilePath, 0777);
    }
  }

  private function updatePage($pageName, $title, $description)
  {
    $db = $this->getDb();
    $page_id = $db->select()
      ->from('engine4_core_pages', 'page_id')
      ->where('name = ?', $pageName)
      ->limit(1)
      ->query()
      ->fetchColumn();
    $tableNameContentName = "engine4_core_content";
    $top_content_id = $db->select()
      ->from($tableNameContentName, 'content_id')
      ->where('page_id =?', $page_id)
      ->where('name =?', 'top')
      ->query()
      ->fetchColumn();
    if( empty($top_content_id) ) {
      $db->insert('engine4_core_content', array(
        'type' => 'container',
        'name' => 'top',
        'page_id' => $page_id,
        'parent_content_id' => null,
        'order' => 1,
        'params' => ''
      ));
      $content_id = $db->lastInsertId('engine4_core_content');
      $middle_content_id = $db->select()
        ->from($tableNameContentName, 'content_id')
        ->where('page_id =?', $page_id)
        ->where('parent_content_id =?', $content_id)
        ->where('name =?', 'middle')
        ->query()
        ->fetchColumn();

      if( empty($middle_content_id) ) {
        $db->insert('engine4_core_content', array(
          'type' => 'container',
          'name' => 'middle',
          'page_id' => $page_id,
          'parent_content_id' => $content_id,
          'order' => 2,
          'params' => ''
        ));

        $content_id = $db->lastInsertId('engine4_core_content');

        $middle_banner_id = $db->select()
          ->from($tableNameContentName, 'content_id')
          ->where('page_id =?', $page_id)
          ->where('parent_content_id =?', $content_id)
          ->where('name =?', 'allure.banner-images')
          ->query()
          ->fetchColumn();
        if( !$middle_banner_id ) {
          $db->insert('engine4_core_content', array(
            'type' => 'widget',
            'name' => 'allure.banner-images',
            'page_id' => $page_id,
            'parent_content_id' => $content_id,
            'order' => 1,
            'params' => '{"showBanners":"1","selectedBanners":"","width":"","height":"280","speed":"5000","order":"2","allureHtmlTitle":"' . $title . '","allureHtmlDescription":"' . $description . '","title":"","nomobile":"0","name":"allure.banner-images"}'
          ));
        }
      }
      $db->query("UPDATE `engine4_core_content` SET  `order` =  '2' WHERE  `engine4_core_content`.`page_id` = $page_id AND `engine4_core_content`.`name` = 'main' LIMIT 1 ;");
    } else {
      $middle_content_id = $db->select()
        ->from($tableNameContentName, 'content_id')
        ->where('page_id =?', $page_id)
        ->where('parent_content_id =?', $top_content_id)
        ->where('name =?', 'middle')
        ->query()
        ->fetchColumn();

      if( !empty($middle_content_id) ) {

        $middle_banner_id = $db->select()
          ->from($tableNameContentName, 'content_id')
          ->where('page_id =?', $page_id)
          ->where('parent_content_id =?', $middle_content_id)
          ->where('name =?', 'allure.banner-images')
          ->query()
          ->fetchColumn();

        if( !$middle_banner_id ) {
          $db->insert('engine4_core_content', array(
            'type' => 'widget',
            'name' => 'allure.banner-images',
            'page_id' => $page_id,
            'parent_content_id' => $middle_content_id,
            'order' => 1,
            'params' => '{"showBanners":"1","selectedBanners":"","width":"","height":"280","speed":"5000","order":"2","allureHtmlTitle":"' . $title . '","allureHtmlDescription":"' . $description . '","title":"","nomobile":"0","name":"allure.banner-images"}'
          ));
        }
        $db->query("UPDATE `engine4_core_content` SET  `order` =  '2' WHERE  `engine4_core_content`.`page_id` = $page_id AND `engine4_core_content`.`name` = 'main' LIMIT 1;");
        $db->query("DELETE FROM `engine4_core_content` WHERE `engine4_core_content`.`page_id` = $page_id AND `engine4_core_content`.`name` = 'spectacular.banner-images' LIMIT 1;");
      }
    }
  }

  public function hideHeaderFooterOnPages($hide)
  {
    $db = $this->getDb();
    if( $hide ) {
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default-simple' WHERE `engine4_core_pages`.`name` = 'core_error_requireuser';");
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default-simple' WHERE `engine4_core_pages`.`name` = 'user_auth_login';");
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default-simple' WHERE `engine4_core_pages`.`name` = 'user_signup_index';");
    } else {
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default' WHERE `engine4_core_pages`.`name` = 'core_error_requireuser';");
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default' WHERE `engine4_core_pages`.`name` = 'user_auth_login';");
      $db->query("UPDATE `engine4_core_pages` SET `layout` = 'default' WHERE `engine4_core_pages`.`name` = 'user_signup_index';");
    }
  }

}