<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: manifest.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'allure',
        'version' => '4.10.3p22',
        'path' => 'application/modules/Allure',
        'title' => 'Responsive Allure Theme',
        'description' => 'Responsive Allure Theme',
        'author' => '<a href="http://www.socialengineaddons.com" style="text-decoration:underline;" target="_blank">SocialEngineAddOns</a>',
        'sku' => 'seao-allure',
        'callback' =>
        array(
            'path' => 'application/modules/Allure/settings/install.php',
            'class' => 'Allure_Installer',
        ),
        'dependencies' => array(
          array(
            'type' => 'module',
            'name' => 'core',
            'minVersion' => '4.10.3',
          ),
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'directories' =>
        array(
            0 => 'application/modules/Allure',
            1 => 'application/themes/allure',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/allure.csv',
        ),
    ),
    'hooks' => array(
        array(
            'event' => 'onRenderLayoutDefault',
            'resource' => 'Allure_Plugin_Core'
        ),
        array(
            'event' => 'onRenderLayoutDefaultSimple',
            'resource' => 'Allure_Plugin_Core',
        ),
    ),
    //Items ---------------------------------------------------------------------
    'items' => array(
        'allure_image',
        'allure_banner',
        'allure_service',
        'allure_highlight',
    )
);
?>
