<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php
$this->headScript()
  ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/scripts/jquery.min.js');
$this->headScript()
  ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/scripts/wow.js');
$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/styles/animate.css');

$target = '';
if( !empty($this->new_tab) ) {
  $target = '_blank';
}
?>
<script>
  new WOW().init();
</script>
<div class="allure_static_buttons">
  <div class="allure_static_buttons_inner">
    <?php if( !empty(trim($this->title1)) ) : ?>
      <a class = "button_link" href="<?php echo $this->url1 ?>" target="<?php echo $target ?>" class="wow animated slideInUp">
        <img src="<?php echo $this->icon1Url; ?>">
        <img class="dnone" src="<?php echo $this->icon1HoverUrl; ?>">
        <?php echo $this->translate($this->title1) ?>
      </a>
    <?php endif; ?>

    <?php if( !empty(trim($this->title2)) ) : ?>
      <a class = "button_link" href="<?php echo $this->url2 ?>" target="<?php echo $target ?>" class="wow animated slideInUp">
        <img src="<?php echo $this->icon2Url; ?>">
        <img class="dnone" src="<?php echo $this->icon2HoverUrl; ?>">
        <?php echo $this->translate($this->title2) ?>
      </a>
    <?php endif; ?>

    <?php if( !empty(trim($this->title3)) ) : ?>
      <a class = "button_link" href="<?php echo $this->url3 ?>" target="<?php echo $target ?>" class="wow animated slideInUp">
        <img src="<?php echo $this->icon3Url; ?>">
        <img class="dnone" src="<?php echo $this->icon3HoverUrl; ?>">
        <?php echo $this->translate($this->title3) ?>
      </a>
    <?php endif; ?>
  </div>
</div>

<script type="text/javascript">
  $$('.button_link').addEvent('mouseenter', function(event){
    event.target.getChildren()[0].addClass('dnone');
    event.target.getChildren()[1].removeClass('dnone');
  });
  $$('.button_link').addEvent('mouseleave', function(event){
    event.target.getChildren()[0].removeClass('dnone');
    event.target.getChildren()[1].addClass('dnone');
  });
</script>
<style type="text/css">
  .dnone {
    display: none;
  }
</style>