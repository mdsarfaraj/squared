<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_StaticButtonsController extends Engine_Content_Widget_Abstract {

    public function indexAction() {

      $coreSettings = Engine_Api::_()->getApi('settings', 'core');

      $this->view->title1 = $coreSettings->getSetting('allure.landing.cta.title1', 'Create Your Abstract');
      $this->view->title2 = $coreSettings->getSetting('allure.landing.cta.title2', 'Dig into Our Events');
      $this->view->title3 = $coreSettings->getSetting('allure.landing.cta.title3', 'Chamber of our Videos');
      $this->view->icon1Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.icon1', ''), 1);
      $this->view->icon2Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.icon2', ''), 2);
      $this->view->icon3Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.icon3', ''), 3);

      $this->view->icon1HoverUrl = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.hover.icon1', ''), 1, true);
      $this->view->icon2HoverUrl = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.hover.icon2', ''), 2, true);
      $this->view->icon3HoverUrl = $this->getIconUrl($coreSettings->getSetting('allure.landing.cta.hover.icon3', ''), 3, true);

      $this->view->url1 = $coreSettings->getSetting('allure.landing.cta.url1', '');
      $this->view->url2 = $coreSettings->getSetting('allure.landing.cta.url2', '');
      $this->view->url3 = $coreSettings->getSetting('allure.landing.cta.url3', '');
      $this->view->new_tab = $coreSettings->getSetting('allure.landing.cta.newtab', 0);
    }
    public function getIconUrl($file_id, $iconNumber, $hover = false) {
        if($hover) {
          $iconUrl = $defaultIcon = $this->view->layout()->staticBaseUrl . 'application/modules/Allure/externals/images/cta/static_button_'.$iconNumber.'_hover.png';
        } else {
          $iconUrl = $defaultIcon = $this->view->layout()->staticBaseUrl . 'application/modules/Allure/externals/images/cta/static_button_'.$iconNumber.'.png';
        }
        if($file_id) {
            $icon = Engine_Api::_()->storage()->get($file_id);
            $iconUrl = ( $icon ) ? $icon->getPhotoUrl() : $defaultIcon;
        } 
        return $iconUrl;
    }
}
