<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
$showFooterBackgroundImage = "'" . $this->showFooterBackgroundImage . "'";
?>

<?php if( $this->showFooterTip ) : ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Please configure footer menu from admin panel.'); ?>
    </span>
  </div>
<?php  endif; ?>

<div class="allure_footer_content_block">
  <?php
  $tempFlag = $flag = 0;
  $footerContent = $footertempCatContent = '';
  $count = 1;
  ?>

  <?php
  if( $this->showFooterLogo ) {
    if( $this->selectFooterLogo ) {
      echo '<div class="allure_footer_logo"><a href="' . $this->url(array('action' => 'home'), "user_general", true) . '"><img src="' . $this->selectFooterLogo . '"></img></a></div>';
    } else {
      $title = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE'));
      $route = $this->viewer()->getIdentity() ? array('route' => 'user_general', 'action' => 'home') : array('route' => 'default');
      echo '<div class="allure_footer_logo">' . $this->htmlLink($route, $title) . '</div>';
    }
  }
  ?>
  <?php
    if( $this->allurefooterLendingBlockValue ):
      echo '<div class="allure_footer_desc">' . $this->allurefooterLendingBlockValue . '</div>';
    else:
      echo '<div class="allure_footer_desc">' . $this->translate('Explore & Watch videos that you have always dreamed of, and post & share your videos to connect with own community.') . '</div>';
    endif; 
  ?>
  <ul class="contact_info">
<?php if( !empty($this->mobile) ): ?>
      <li>
        <i class="fa fa-phone"></i>
        <span><?php echo $this->mobile; ?></span>
      </li>
    <?php endif; ?>
<?php if( !empty($this->mail) ): ?>
      <li>
        <i class="fa fa-envelope-o"></i>

        <a href="mailto:<?php echo $this->mail;?>"><?php echo $this->mail; ?></a>
      </li>
    <?php endif; ?>
<?php if( !empty($this->website) ): ?>
      <li>
        <i class="fa fa-globe"></i>
        <a href="<?php echo $this->website; ?>" target="_blank"><?php echo $this->website; ?></a>
      </li>
<?php endif; ?>
       <?php if( !$this->settings('allure.fotter.subscribeus', 1) && !empty($this->social_links_array[0]) && ( !empty($this->facebook_url) || !empty($this->twitter_url) ||  !empty($this->linkedin_url) || !empty($this->youtube_url) || !empty($this->pinterest_url) )): ?>
      <li style="margin-top: 10px;">
        <div class="social_connect">
          <div class="allure_footer_bottom_block_inner">
            <h5><?php echo $this->translate("Connect With Us") ?></h5>
            <ul class="">
              <?php if( !empty($this->facebook_url) ) : ?>
              <li><a href="<?php echo $this->facebook_url ?>" target="_blank" title="<?php echo $this->facebook_title ?>"><i class="fa fa-facebook"></i></a></li>
              <?php endif; ?>

              <?php if( !empty($this->twitter_url) ) : ?>
                <li><a href="<?php echo $this->twitter_url ?>" target="_blank" title="<?php echo $this->twitter_title ?>"><i class="fa fa-twitter"></i></a></li>
              <?php endif; ?>

              <?php if( !empty($this->linkedin_url) ) : ?>
                <li><a href="<?php echo $this->linkedin_url ?>" target="_blank" title="<?php echo $this->linkedin_title ?>" ><i class="fa fa-linkedin" ></i></a></li>
              <?php endif; ?>

              <?php if( !empty($this->youtube_url) ) : ?>
                <li><a href="<?php echo $this->youtube_url ?>" target="_blank" title="<?php echo $this->youtube_title ?>"><i class="fa fa-youtube"></i></a></li>
              <?php endif; ?>

              <?php if( !empty($this->pinterest_url) ) : ?>
                <li><a href="<?php echo $this->pinterest_url ?>" target="_blank" title="<?php echo $this->pinterest_title ?>"><i class="fa fa-pinterest"></i></a></li>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </li>
  <?php endif; ?>
  </ul>
</div>

<?php
foreach( $this->navigation as $navigation ) :
  if( $navigation->uri == 'javascript:void(0)' ) :
    if( $count > 2 && $this->allureTwitterFeed ) {
      break;
    }
    if( !empty($tempFlag) ) :
      $footerContent .= '</ul></div></div>';
    endif;
    $tempFlag = 1;

    if( !empty($navigation->icon) ) :
      $footertempCatContent = '<div class="allure_footer_content_block" id="allure_footer_block_' . $count . '" style="background-image:url(\'' . $navigation->icon . '\'); background-repeat:no-repeat;"><div class="allure_footer_content_inner"><ul>';
    else:
      $footertempCatContent = '<div class="allure_footer_content_block" id="allure_footer_block_' . $count . '"><div class="allure_footer_content_inner"><ul><li class="allure_footer_block_head">' . $this->translate($navigation->getLabel()) . '</li>';
    endif;
    $count++;
  else:
    if( !empty($footertempCatContent) ) :
      $footerContent .= $footertempCatContent;
      $footertempCatContent = '';
    endif;
    if( !empty($navigation->icon) ) :
      $tempContent = '<img src="' . $navigation->icon . '" title="' . $this->translate($navigation->getLabel()) . '" />' . ' ' . $this->translate($navigation->getLabel());
    else:
      $tempContent = $this->translate($navigation->getLabel());
    endif;

    if( isset($navigation->target) ) :
      $footerContent .= '<li><a href="' . $navigation->getHref() . '" target="' . $navigation->target . '">' . $tempContent . '</a></li>';
    else:
      $footerContent .= '<li><a href="' . $navigation->getHref() . '">' . $tempContent . '</a></li>';
    endif;
  endif;

endforeach;

if( !empty($tempFlag) ) :
  $footerContent .= '</ul></div></div>';
endif;

if( !empty($footerContent) ) :
  echo $footerContent;

endif;
?>

<?php if( $count > 2 && $this->allureTwitterFeed ) : ?>
  <div class="allure_footer_content_block">
    <div class="allure_footer_content_inner">
      <?php echo $this->twitterCode ?>
    </div>
  </div>
<?php endif; ?>

<?php if($this->settings('allure.fotter.subscribeus', 1)): ?>
<div class="allure_footer_bottom_block">
  <?php if( !empty($this->mobile) ): ?>
    <div class="contact_no">
      <div class="allure_footer_bottom_block_inner">
        <h5><?php echo $this->translate("Call Us Now") ?></h5>
        <span><?php echo $this->mobile ?></span>
      </div>
    </div>
  <?php endif; ?>

  <?php if( !empty($this->social_links_array[0]) && ( !empty($this->facebook_url) || !empty($this->twitter_url) ||  !empty($this->linkedin_url) || !empty($this->youtube_url) || !empty($this->pinterest_url) )): ?>
  <div class="social_connect">
    <div class="allure_footer_bottom_block_inner">
      <h5><?php echo $this->translate("Connect With Us") ?></h5>
      <ul class="">
        <?php if( !empty($this->facebook_url) ) : ?>
          <li><a href="<?php echo $this->facebook_url ?>" target="_blank" title="<?php echo $this->facebook_title ?>"><i class="fa fa-facebook"></i></a></li>
        <?php endif; ?>

        <?php if( !empty($this->twitter_url) ) : ?>
          <li><a href="<?php echo $this->twitter_url ?>" target="_blank" title="<?php echo $this->twitter_title ?>"><i class="fa fa-twitter"></i></a></li>
        <?php endif; ?>

        <?php if( !empty($this->linkedin_url) ) : ?>
          <li><a href="<?php echo $this->linkedin_url ?>" target="_blank" title="<?php echo $this->linkedin_title ?>" ><i class="fa fa-linkedin" ></i></a></li>
        <?php endif; ?>

        <?php if( !empty($this->youtube_url) ) : ?>
          <li><a href="<?php echo $this->youtube_url ?>" target="_blank" title="<?php echo $this->youtube_title ?>"><i class="fa fa-youtube"></i></a></li>
        <?php endif; ?>

        <?php if( !empty($this->pinterest_url) ) : ?>
          <li><a href="<?php echo $this->pinterest_url ?>" target="_blank" title="<?php echo $this->pinterest_title ?>"><i class="fa fa-pinterest"></i></a></li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>

  <div class="newsletter_subscribe">
    <div class="allure_footer_bottom_block_inner">      
     <?php if(Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitenewsletter')):?>
      <?php $content = $this->content()->renderWidget("sitenewsletter.newsletter-subscribe", array('title' => "Subscribe Us")); ?>
        <?php if(trim(strip_tags($content))): ?>
         <h5><?php echo $this->translate("Subscribe Us") ?></h5>
         <?php echo $content;?>
        <?php endif; ?>
      <?php else: ?>
      <h5><?php echo $this->translate("Subscribe Us") ?></h5>
      <form>
        <input type="text" placeholder="<?php echo $this->translate("Your Email")?>" id="subscriber_email" value="<?php echo $this->viewerEmail; ?>">
        <button id="subscribe_button"><?php echo $this->translate("Subscribe") ?></button>
        <div class="subscribe_msg" id="subscribe_msg"></div>
      </form>
      <?php endif; ?>
    </div>
  </div>  
</div>
<?php endif; ?>

<style>
<?php if($this->selectFooterBackground == 2): ?>
  .layout_page_footer { background-image: url(<?php echo $showFooterBackgroundImage; ?>);}
<?php endif; ?>
</style>
<?php if($this->settings('allure.fotter.subscribeus', 1) && !Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitenewsletter')): ?>
<script type="text/javascript">
  $('subscribe_button').addEvent('click', function (event) {
    event.preventDefault();
    $('subscribe_msg').innerHTML = '';
    var email = $('subscriber_email').get('value');
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( !reg.test(String(email).toLowerCase()) ) {
      $('subscribe_msg').setStyle('color', '#ff0000');
      $('subscribe_msg').innerHTML = 'Please enter valid email';
      return;
    }
    en4.core.request.send(new Request.JSON({
      url : '<?php echo $this->url(array('action' => 'index', 'controller' => 'subscription', 'module' => 'allure'), 'default');?>',
      data : {
        format : 'json',
        email : $('subscriber_email').get('value')
      },
      onSuccess : function(data) {
        if( data['resp'] ) {
          $('subscribe_msg').setStyle('color', '#179617');
        } else {
          $('subscribe_msg').setStyle('color', '#ff0000');
        }

        $('subscribe_msg').innerHTML = data['msg'];
      }
    }));
    });
</script>
<?php endif; ?>