<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_MenuFooterController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {

    $islanguage = $this->view->translate()->getLocale();
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    if( !strstr($islanguage, '_') ) {
      $islanguage = $islanguage . '_default';
    }
    $viewer = Engine_Api::_()->user()->getViewer();
    $this->view->viewerEmail = '';
    if( isset($viewer->email) ) {
      $this->view->viewerEmail = $viewer->email;
    }

    //No need of tip now, we'll give default color and image option, we can remove the code to show tip
    if( empty($coreSettings->getSetting('allure.footer.background', 2)) ) {

      if( isset($viewer->level_id) && $viewer->level_id == 1) {
        $this->view->showFooterTip = 1;
      } else {
        return $this->setNoRender();
      }
    }

    $keyForSettings = str_replace('_', '.', $islanguage);
    $allurefooterLendingBlockValue = $coreSettings->getSetting('allure.footer.lending.block.languages.' . $keyForSettings, null);

    $allurefooterLendingBlockTitleValue = $coreSettings->getSetting('allure.footer.lending.block.title.languages.' . $keyForSettings, null);
    if( empty($allurefooterLendingBlockValue) ) {
      $allurefooterLendingBlockValue = $coreSettings->getSetting('allure.footer.lending.block', null);
    }

    if( !empty($allurefooterLendingBlockValue) )
      $this->view->allurefooterLendingBlockValue = @base64_decode($allurefooterLendingBlockValue);
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation("allure_footer");
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->view->selectFooterBackground = $coreSettings->getSetting('allure.footer.background', 2);

    $this->view->showFooterBackgroundImage = $coreSettings->getSetting('allure.footer.backgroundimage', 'application/modules/Allure/externals/images/default_footer_bg.png');
    $this->view->showFooterLogo = $coreSettings->getSetting('allure.footer.show.logo', 1);
    $this->view->selectFooterLogo = $coreSettings->getSetting('allure.footer.select.logo');

    $this->view->allureTwitterFeed = $coreSettings->getSetting('allure.twitter.feed', 0);
    $this->view->mobile =  $coreSettings->getSetting('allure.mobile', '+1-777-777-7777');
    $this->view->mail =  $coreSettings->getSetting('allure.mail', 'info@test.com');
    $this->view->website = $coreSettings->getSetting('allure.website', 'www.example.com');
    $this->view->twitterCode =  $coreSettings->getSetting('allure.twitterCode', '');
    
    $this->view->social_links_array = $social_link_array = $coreSettings->getSetting('allure.social.links', array("facebooklink", "twitterlink", "pininterestlink", "youtubelink", "linkedinlink"));
    if( !empty($social_link_array) ) {
      if( in_array('facebooklink', $social_link_array) ) {
        $this->view->facebook_url = $coreSettings->getSetting('allure.facebook.url', 'http://www.facebook.com/');
        $this->view->facebook_title = $coreSettings->getSetting('allure.facebook.title', 'Like us on Facebook');
      }
      
      if( in_array('pininterestlink', $social_link_array) ) {
        $this->view->pinterest_url = $coreSettings->getSetting('allure.pinterest.url', 'https://www.pinterest.com/');
        $this->view->pinterest_title = $coreSettings->getSetting('allure.pinterest.title', 'Pinterest');
      }
      
      if( in_array('twitterlink', $social_link_array) ) {
        $this->view->twitter_url = $coreSettings->getSetting('allure.twitter.url', 'https://www.twitter.com/');
        $this->view->twitter_title = $coreSettings->getSetting('allure.twitter.title', 'Follow us on Twitter');
      }
      
      if( in_array('youtubelink', $social_link_array) ) {
        $this->view->youtube_url = $coreSettings->getSetting('allure.youtube.url', 'http://www.youtube.com/');
        $this->view->youtube_title = $coreSettings->getSetting('allure.youtube.title', 'Youtube');
      }

      if( in_array('linkedinlink', $social_link_array) ) {
        $this->view->linkedin_url = $coreSettings->getSetting('allure.linkedin.url', 'https://www.linkedin.com/');
        $this->view->linkedin_title = $coreSettings->getSetting('allure.linkedin.title', 'LinkedIn');
      }
    }
  }

}
