<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_headerController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $logoPath = $coreSettings->getSetting('allure.header.logo.image', '');
    $this->view->logoParams = array('logo' => $logoPath);
    $this->view->showMenu = true;

    $isSitemenuEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitemenu');
    $request = Zend_Controller_Front::getInstance()->getRequest();
    $pageIdentity = join('-', array(
      $request->getModuleName(),
      $request->getControllerName(),
      $request->getActionName()
    ));

    $themes = Engine_Api::_()->getDbtable('themes', 'core')->fetchAll();
    $activeTheme = $themes->getRowMatching('active', 1);
    $headerPosition = $coreSettings->getSetting('allure.landing.slider.header.position', 1);

    if( $activeTheme->name == 'allure' && $pageIdentity == 'core-index-index' && $headerPosition == 1) {
      $this->view->showMenu = false;
    }
    $deafultHeaderOptions = array(
      'logo',
      'mini_menu',
      'main_menu',
      'search_box',
    );
    $this->view->loggedInWidgets = $coreSettings->getSetting('allure.header.loggedin.widgets', $deafultHeaderOptions);
    $this->view->loggedOutWidgets = $coreSettings->getSetting('allure.header.loggedout.widgets', $deafultHeaderOptions);
    $this->view->fixedMenu = $fixedMenu = $coreSettings->getSetting('allure.header.menu.fixed', 0);
    $this->view->headerClass = 'allure_fullheader_fixed';
    if( $isSitemenuEnable ) {
      $desktopMenuCount = $coreSettings->getSetting('allure.header.desktop.totalmenu', 6);
      $truncationLimit = 200;
      $showCart = $coreSettings->getSetting('allure.header.display.cart', 1);
      $location = $coreSettings->getSetting('allure.header.display.location', 0);
      $showIcons = $coreSettings->getSetting('allure.header.minimenu.design', 1);
      $this->view->fixedMenu = $fixedMenu = $coreSettings->getSetting('allure.header.menu.fixed', 0);
      $this->view->headerClass = $fixedMenu ? 'allure_fullheader_fixed' : 'allure_topheader_fixed';
      $this->view->menuPosition = $coreSettings->getSetting('allure.header.menu.position', 1);

      $this->view->menuParams = array(
        'sitemenu_on_logged_out' => 1,
        'sitemenu_totalmenu' => $desktopMenuCount,
        'sitemenu_truncation_limit_content' => $truncationLimit,
        'sitemenu_truncation_limit_category' => $truncationLimit,
        'sitemenu_show_cart' => $showCart,
        'sitemenu_is_fixed' => 1,
        'sitemenu_show_link_icon' => $coreSettings->getSetting('allure.header.menu.icon', 1), // $this->_getParam('sitemenu_show_link_icon', 0)
      );

      $this->view->miniMenuParams = array(
        'changeMyLocation' => $location,
        'sitemenu_show_icon' => $showIcons,
        'sitemenu_show_in_mini_options' => 6,//IN CASE OF 6 ADV MENU SEARCH BAR WILL NOT BE SHOWN IN HEADER
        'sitemenu_location_box_width' => '' //set the width blank to override width of Sitemenu/widgets/menu-mini/index.tpl line 447 
      );

      $this->view->PopupVisibilty = $coreSettings->getSetting('allure.signin.popup.visibility', 0);
      $showCloseIcon = $coreSettings->getSetting('allure.signin.popup.close', 1);
      if( $showCloseIcon != 1 ) {
        $this->view->popupClosable = 'false';
      }

      $this->view->showPopup = false;
      if( $activeTheme->name == 'allure' && empty(Engine_Api::_()->user()->getViewer()->getIdentity()) && $coreSettings->getSetting('allure.signin.popup.display', 1) ) {

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $pageIdentity = join('-', array(
          $request->getModuleName(),
          $request->getControllerName(),
          $request->getActionName()
        ));

        $excudedPages = array('user-auth-login', 'user-signup-index', 'user-auth-forgot', 'core-error-requireuser');
        if( !in_array($pageIdentity, $excudedPages) ) {
          $this->view->showPopup = true;
        }
      }
    }
  }

}
