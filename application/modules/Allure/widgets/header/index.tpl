<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<script type="text/javascript">
  $(document).getElement('body').addClass('global_allure_header_body_wapper');
</script>
<?php
  $isSiteadvsearchEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('siteadvsearch');
  $viewerId = $this->viewer()->getIdentity();

  $displayWidgets = $this->loggedOutWidgets;
  if( !empty($viewerId) ) {
    $displayWidgets = $this->loggedInWidgets;
  }
?>
<?php if( in_array('search_box', $displayWidgets) ) :?>
  <?php
  $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
  ?>
<?php endif; ?>
<div class="allure_top_header" id="allure_top_header_wrapper">
	<div class="allure_top_header_container <?php if($this->viewer()->getIdentity()): ?> _logged_in <?php endif;?> ">
    <?php if( in_array('logo', $displayWidgets) ) :?>
      <div class="allure_logo">
        <?php echo $this->content()->renderWidget("core.menu-logo", $this->logoParams);?>
      </div>
    <?php endif; ?>

    <?php if( $this->showMenu && in_array('main_menu', $displayWidgets) && empty($this->menuParams)): ?>
      <div class="allure_mainmenu">
        <?php echo $this->content()->renderWidget("allure.browse-menu-main", array());?>
        <?php // echo $this->content()->renderWidget("core.menu-main", array());?>
      </div>
    <?php endif; ?>

    <?php if( in_array('search_box', $displayWidgets) ) :?>
      <div class="allure_search">
		    <a id="responsive_search_toggle" class="responsive_search_toggle"><i class="fa fa-search"></i></a>
		  <div id="allure_fullsite_search" class="allure_fullsite_search">
			  <?php if( !empty($isSiteadvsearchEnable) ) :?>
          <?php echo $this->content()->renderWidget("siteadvsearch.search-box", array("widgetName" => "advmenu_mini_menu",));?>
        <?php else: ?>
        <form action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get"  >
        	<input name='query' id='global_search_field' type="text" placeholder="<?php echo $this->translate("Search here...") ?>"/>
        	<button><i class="fa fa-search"></i></button>
        </form>
			   <?php endif; ?>
			  </div>

      </div>
    <?php endif; ?>

    <?php if( in_array('mini_menu', $displayWidgets) ) :?>
      <div class="allure_minimenu">
        <?php if( !empty($this->miniMenuParams) ) :?>
          <?php echo $this->content()->renderWidget("sitemenu.menu-mini", $this->miniMenuParams);?>
        <?php else: ?>
          <?php echo $this->content()->renderWidget("core.menu-mini", array());
          ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>
	</div>
</div>


  <?php if( in_array('main_menu', $displayWidgets) &&  !empty($this->menuParams) && $this->showMenu ) :?>
    <div class="allure_mainmenu">
      <?php if( $this->menuPosition == 2) : ?>
        <?php echo $this->content()->renderWidget("sitemenu.vertical-menu-main", $this->menuParams);?>
      <?php else: ?>
        <?php echo $this->content()->renderWidget("sitemenu.menu-main", $this->menuParams);?>
      <?php endif; ?>
    </div>
  <?php endif; ?>

<!-------------------------------Display signin popup on page load------------------------------------------------>
<?php if( !empty($this->showPopup) ) :?>
  <script type="text/javascript">
    var showSigninPopup = false;
      <?php if( empty($this->PopupVisibilty) ) :?>
        showSigninPopup = true;
      <?php else: ?>
        var popupVisibilty = '<?php echo $this->PopupVisibilty;?>';
        if( typeof(Storage) !== "undefined" ) {
          var popupClosedOn = localStorage.getItem("popupClosedOn");
          if( popupClosedOn == null || popupClosedOn == undefined ) {
            showSigninPopup = true;
            localStorage.setItem("popupClosedOn", new Date());
          } else {
            var todayDate = new Date();
            var closedDate = new Date(popupClosedOn);
            var timeDiff =  Math.abs(todayDate.getTime() - closedDate.getTime());
            var diffDays = timeDiff / (1000 * 3600 * 24);
            if( popupVisibilty <= diffDays ) {
              showSigninPopup = true;
              localStorage.setItem("popupClosedOn", new Date());
            }

          }
        }

      <?php endif; ?>

    if( showSigninPopup ) {
      window.addEvent('domready', function () {
        advancedMenuUserLoginOrSignUp('login', '', '', <?php echo $this->popupClosable; ?>);
      });
    }
  </script>
<?php endif; ?>

<script type="text/javascript">
      var scrollTopPrev = 0;
      var headerHeight = $('allure_top_header_wrapper').getSize().y;
      <?php if(in_array($this->fixedMenu, array(0, 1))): ?>
      en4.core.runonce.add(function () {
        var headerElement = $$('.layout_page_header');
          if( headerElement.length === 0 ) {
            return;
          }
          headerElement = headerElement[0];
          var height = headerElement.getCoordinates().height;
          function headerScrolling() {
            var scrollTop = window.getScrollTop();
            if (scrollTop > height*1.2 && !headerElement.hasClass('<?php echo $this->headerClass ?>')) {
              headerElement.getParent().setStyle('minHeight', height+'px');
              headerElement.addClass('<?php echo $this->headerClass ?>');
              headerElement.setStyle('top', 0);
            } else if (scrollTop < height && headerElement.hasClass('<?php echo $this->headerClass ?>')) {
              headerElement.removeClass('<?php echo $this->headerClass ?>');
              headerElement.getParent().setStyle('minHeight');
            }
            headerElement.setStyle('top', '-'+scrollTop+'px');
          }
          window.addEvent('scroll', headerScrolling);
      });
      <?php endif; ?>
      en4.core.runonce.add(function () {
        if ($$('.layout_allure_images').length == 0) {
          return;
        }
        var headerFixed = function() {
          var scrollTop = window.getScrollTop();
            if(scrollTop == 0) {
              if($$('.layout_allure_images').hasClass('allure_landing_header_fixed')[0]) {
                $$('.layout_allure_images').removeClass('allure_landing_header_fixed');
                $$('.layout_allure_images')[0].getElement('.allure_images_page_container').setStyle('paddingTop');
              }
              return;
            }
            if(scrollTop < scrollTopPrev) {
              if(!$$('.layout_allure_images').hasClass('allure_landing_header_fixed')[0]) {
                $$('.layout_allure_images').addClass('allure_landing_header_fixed');
                $$('.layout_allure_images')[0].getElement('.allure_images_page_container').setStyle('paddingTop', $$('.layout_allure_images')[0].getElement('.allure_header_wrapper').getCoordinates().height+'px');
              }
            } else {
              if($$('.layout_allure_images').hasClass('allure_landing_header_fixed')[0]) {
                $$('.layout_allure_images').removeClass('allure_landing_header_fixed');
                $$('.layout_allure_images')[0].getElement('.allure_images_page_container').setStyle('paddingTop');
              }
            } 
            scrollTopPrev = scrollTop;
        };
        window.addEvent('scroll', headerFixed);
      });
  <?php if( in_array('search_box', $displayWidgets) ) :?>
      $('responsive_search_toggle').addEvent('click', function(){
        if(!$('allure_fullsite_search').hasClass('responsive_search_show')) {
          $('allure_fullsite_search').addClass('responsive_search_show');
        } else {
          $('allure_fullsite_search').removeClass('responsive_search_show');
        }
      });
  <?php endif; ?>
<?php if( in_array('search_box', $displayWidgets) ) :?>
  var requestURL = '<?php echo $this->url(array('module' => 'allure', 'controller' => 'general', 'action' => 'get-search-content'), "default", true) ?>';
if($('global_search_field')) {
  contentAutocomplete = new Autocompleter.Request.JSON('global_search_field', requestURL, {
      'postVar': 'text',
      'cache': false,
      'minLength': 1,
      'selectFirst': false,
      'selectMode': 'pick',
      'autocompleteType': 'tag',
      'className': 'tag-autosuggest adsearch-autosuggest adsearch-stoprequest',
      'maxChoices': 8,
      'indicatorClass': 'allure-search-loading',
      'customChoices': true,
      'filterSubset': true,
      'multiple': false,
      'injectChoice': function (token) {
          if (typeof token.label != 'undefined') {
              var seeMoreText = '<?php echo $this->string()->escapeJavascript($this->translate('See more results for') . ' '); ?>';
              if (token.type == 'no_resuld_found') {                
                  var choice = new Element('li', {'class': 'autocompleter-choices', 'id': 'allure_search_'+token.type});
                  new Element('div', {'html': token.label, 'class': 'autocompleter-choicess'}).inject(choice);
                  choice.inject(this.choices);
                  choice.store('autocompleteChoice', token);
                return;
              }
              if (token.item_url != 'seeMoreLink') {
                  var choice = new Element('li', {'class': 'autocompleter-choices', 'html': token.photo, 'item_url': token.item_url, onclick: 'javascript: showSearchResultPage("' + token.item_url + '")'});
                  var divEl = new Element('div', {
                      'html': token.type ? this.options.markQueryValueCustom.call(this, (token.label)) : token.label,
                      'class': 'autocompleter-choice'
                  });

                  new Element('div', {
                      'html': token.type, //this.markQueryValue(token.type)  
                      'class': 'seaocore_txt_light f_small'
                  }).inject(divEl);

                  divEl.inject(choice);
                  new Element('input', {
                      'type': 'hidden',
                      'value': JSON.encode(token)
                  }).inject(choice);
                  this.addChoiceEvents(choice).inject(this.choices);
                  choice.store('autocompleteChoice', token);
              }
              if (token.item_url == 'seeMoreLink') {
                  var titleAjax1 = encodeURIComponent($('global_search_field').value);
                  var choice = new Element('li', {'class': 'autocompleter-choices', 'html': '', 'id': 'stopevent', 'item_url': ''});
                  new Element('div', {'html': seeMoreText + '"' + titleAjax1 + '"', 'class': 'autocompleter-choicess', onclick: 'javascript:seeMoreSearchResults()'}).inject(choice);
                  this.addChoiceEvents(choice).inject(this.choices);
                  choice.store('autocompleteChoice', token);
              }
          }
      }, 
      markQueryValueCustom: function (str) {
          return (!this.options.markQuery || !this.queryValue) ? str
                  : str.replace(new RegExp('(' + ((this.options.filterSubset) ? '' : '^') + this.queryValue.escapeRegExp() + ')', (this.options.filterCase) ? '' : 'i'), '<b>$1</b>');
      },
  });
}
  function showSearchResultPage(url) {
     window.location.href = url;
  }
  function seeMoreSearchResults() {

      $('stopevent').removeEvents('click');
      var url = '<?php echo $this->url(array('controller' => 'search'), 'default', true); ?>' + '?query=' + encodeURIComponent($('global_search_field').value) + '&type=' + 'all'; 
      window.location.href = url;
  }
  $('global_search_field').addEvent('keydown', function(event){
    if (event.key == 'enter') {
      $('allure_fullsite_search').submit();
    }
  });
  <?php endif; ?>
</script>