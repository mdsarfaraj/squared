<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php if( !empty( $this->bgImage) ) :  ?>
  <style>
    .allure_counter_container {
      background-image: url(<?php echo $this->bgImage; ?>);
    }
  </style>
<?php endif;  ?>

<div class="allure_counter_container" id ='allure_counter_container'>
	<div class="allure_container">
		<div class="allure_counter_statistic">
      <?php if( !empty((int) $this->count1) ) :  ?>
        <div class="allure_counter_statistic_3">
          <div class="allure_counter_wrapper">
            <img src="<?php echo $this->icon1Url; ?>">
            <h4><?php echo $this->translate($this->count1) ?>+</h4>
            <p><?php echo $this->translate($this->stat1) ?></p>
          </div>
        </div>
      <?php endif;  ?>

      <?php if( !empty((int) $this->count2) ) :  ?>
        <div class="allure_counter_statistic_3">
          <div class="allure_counter_wrapper">
            <img src="<?php echo $this->icon2Url; ?>">
            <h4><?php echo $this->translate($this->count2) ?>+</h4>
            <p><?php echo $this->translate($this->stat2) ?></p>
          </div>
        </div>
      <?php endif;  ?>

      <?php if( !empty((int) $this->count3) ) :  ?>
        <div class="allure_counter_statistic_3">
          <div class="allure_counter_wrapper">
            <img src="<?php echo $this->icon3Url; ?>">
            <h4><?php echo $this->translate($this->count3) ?>+</h4>
            <p><?php echo $this->translate($this->stat3) ?></p>
          </div>
        </div>
      <?php endif;  ?>

      <?php if( !empty((int) $this->count4) ) :  ?>
        <div class="allure_counter_statistic_3">
          <div class="allure_counter_wrapper">
            <img src="<?php echo $this->icon4Url; ?>">
            <h4><?php echo $this->translate($this->count4) ?>+</h4>
            <p><?php echo $this->translate($this->stat4) ?></p>
          </div>
        </div>
      <?php endif;  ?>
		</div>
	</div>
</div>
<script type="text/javascript">
  var alreadyStarted = false;
  window.addEventListener("scroll", function() {
    var el = $('allure_counter_container');
    var rect = el.getBoundingClientRect();
    if (!alreadyStarted && (rect.top - el.offsetTop) <= 500) {
      alreadyStarted = true;
      $$('.allure_counter_wrapper').each(function (el) {
        startCounter(el.getElement('h4'), 0, parseInt(el.getElement('h4').get('text').replace('+', '')), 1000);
      });
    }
  });
  function startCounter(el, start, end, duration) {
    var current = start;
    var interval = Math.abs(Math.ceil(end / duration));
    var timer = setInterval(function() {
      current += interval;
      if (current >= end) {
        el.innerHTML = end + '+';
        clearInterval(timer);
      }
      el.innerHTML = current + '+';
    }, 1);
  }
</script>
