<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_StatsBlockController extends Engine_Content_Widget_Abstract {

    public function indexAction() {

        $coreSettings = Engine_Api::_()->getApi('settings', 'core');
        $this->view->bgImage = $coreSettings->getSetting('allure.landing.stats.bgimage', '');
        $this->view->stat1 = $coreSettings->getSetting('allure.landing.stats.title1', 'Clients');
        $this->view->stat2 = $coreSettings->getSetting('allure.landing.stats.title2', 'Products');
        $this->view->stat3 = $coreSettings->getSetting('allure.landing.stats.title3', 'Reviews');
        $this->view->stat4 = $coreSettings->getSetting('allure.landing.stats.title4', 'Projects Done');
        $this->view->icon1Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.stats.icon1', ''), 1);
        $this->view->icon2Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.stats.icon2', ''), 2);
        $this->view->icon3Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.stats.icon3', ''), 3);
        $this->view->icon4Url = $this->getIconUrl($coreSettings->getSetting('allure.landing.stats.icon4', ''), 4);
        $this->view->count1 = $coreSettings->getSetting('allure.landing.stats.count1', '7000');
        $this->view->count2 = $coreSettings->getSetting('allure.landing.stats.count2', '100');
        $this->view->count3 = $coreSettings->getSetting('allure.landing.stats.count3', '975');
        $this->view->count4 = $coreSettings->getSetting('allure.landing.stats.count4', '12597');

    }

    public function getIconUrl($file_id, $iconNumber) {
        $iconUrl = $defaultIcon = $this->view->layout()->staticBaseUrl . 'application/modules/Allure/externals/images/stats/stats_'.$iconNumber.'.png';
        if($file_id) {
            $icon = Engine_Api::_()->storage()->get($file_id);
            $iconUrl = ( $icon ) ? $icon->getPhotoUrl() : $defaultIcon;
        } 
        return $iconUrl;
    }

}
