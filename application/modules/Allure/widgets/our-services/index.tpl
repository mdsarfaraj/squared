<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
$this->headScript()
        ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/scripts/jquery.min.js');
$this->headScript()
->appendFile($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/scripts/wow.js');
$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/styles/animate.css');
?>

 <script>
	  new WOW().init();
</script>

<div class="allure_icons_container">
  <div class="allure_icons_wrapper">
    <?php $count = 0; ?>
    <?php foreach( $this->services as $service ): ?>
      <?php
        $iconUrl = $defaultIcon = $this->layout()->staticBaseUrl . 'application/modules/Allure/externals/images/services/service_'.$service->service_id.'.png';
        if($service->file_id) {
           $icon = Engine_Api::_()->storage()->get($service->file_id);
           $iconUrl = ( $icon ) ? $icon->getPhotoUrl() : $defaultIcon;
        } 
      ?>
      <?php if( !($count % 3) || $count == 0 ): ?>
        <div class="allure_icons_inner">
        <?php endif; ?>

        <div class="allure_icons_content_4 wow animated fadeInUp">
          <div class="allure_icons_content_4_inner">
            <img src="<?php echo $iconUrl; ?>">
            <h4><a href="#"><?php echo $this->translate($service->title) ?></a></h4>
            <p><?php echo $this->translate($service->description) ?></p>
          </div>
        </div>

        <?php $count++; ?>
        <?php if( !($count % 3) || $count === count($this->services)) : ?>
        </div>
      <?php endif; ?>

    <?php endforeach; ?>
  </div>
</div>