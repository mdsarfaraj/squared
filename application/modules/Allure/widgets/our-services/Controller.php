<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_Widget_OurServicesController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $params = array('enabled' => 1);
    $this->view->services = Engine_Api::_()->getDbtable('services', 'allure')->getServices($params);
    if( !count($this->view->services) ) {
      return $this->setNoRender();
    }
  }

}
