<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-03-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style>
  #global_content_simple {display: block;}
  #global_content{width: 100%; margin-top: -20px;}
<?php if( !empty($this->gradientColor1) && !empty($this->gradientColor2) ): ?>
  /* banner gradient */
  .layout_allure_form_banner > .allure_form_banner_wrapper {
    background: <?php echo $this->gradientColor2?>;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right,<?php echo $this->gradientColor1?>, <?php echo $this->gradientColor2?>);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, <?php echo $this->gradientColor1?>, <?php echo $this->gradientColor2?>); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  }
<?php endif; ?>
</style>

<div class="allure_form_banner_wrapper" <?php if( $this->imgPath ): ?> style="background-image: url(<?php echo $this->imgPath?>)" <?php endif;?>>
  <section>
    <div>
    <?php
      $siteTitle = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE'));
      $siteTitle = $this->translate($siteTitle);
      $logo  = $this->logo;
      $route = $this->viewer()->getIdentity()
                   ? array('route'=>'user_general', 'action'=>'home')
                   : array('route'=>'default');
    ?>

    <?php if( !empty($logo) ) : ?>
      <?php echo $this->htmlLink($route, $this->htmlImage($logo, array('alt'=>$siteTitle))); ?>
    <?php else : ?>
      <h1>
        <?php echo $this->htmlLink($route, $siteTitle); ?>
      </h1>
    <?php endif; ?>
        <?php if( $this->description ): ?>
          <article>
            <?php echo $this->translate($this->description) ?>
          </article>
        <?php endif; ?>
    </div>
  </section>
</div>
<?php
  $signinHref = $this->url(array('module' => 'user', 'action'=> 'login'), 'user_login', true);
  $signupBottomText = $this->translate("Already a member? <a href='%s'>Sign In</a>", $signinHref);
  ?>
<?php
  $signupHref = $this->url(array('module' => 'user', 'action'=> 'index'), 'user_signup', true);
  $signinBottomText = $this->translate("Don't have an account? <a href='%s'>Sign Up</a>", $signupHref);
?>
<div id="allure-form-banner-signup" style="display:none">
  <?php echo $signupBottomText; ?>
</div>
<div id="allure-form-banner-signin" style="display:none">
  <?php echo $signinBottomText; ?>
</div>
<script type="text/javascript">
  var signupBottomText = '';
  var signinBottomText = '';
  var bottomText = '';
  var url = window.location.href;
  window.addEvent('domready', function () {

    if( url.indexOf('signup') !== -1 ) {
      bottomText = $('allure-form-banner-signup').get('html');
    } else {
      bottomText = $('allure-form-banner-signin').get('html');
    }
    
    var bottomTextElement = new Element('p', {
      'class': 'allure_signin_signup_switch',
      'html': bottomText,
    });
    bottomTextElement.inject($$('.layout_middle')[0]);
  });

</script>