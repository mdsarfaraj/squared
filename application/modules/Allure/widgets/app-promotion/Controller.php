<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Bigstep
 * @copyright  Copyright 2010-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-02-19 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_AppPromotionController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
      $coreSettings = Engine_Api::_()->getApi('settings', 'core');
      $this->view->bgImage = $coreSettings->getSetting('allure.landing.appbanner.bgimage', '');
      $this->view->title = $coreSettings->getSetting('allure.landing.appbanner.title', 'Download our latest app');
      $this->view->description = $coreSettings->getSetting('allure.landing.appbanner.description', 'Enabling Business with a new perspective using Mobile apps. Get your community in the hands of your customers with our beautiful mobile solutions.');
      $this->view->appstoreUrl = $coreSettings->getSetting('allure.landing.appbanner.appstoreUrl', '#');
      $this->view->playstoreUrl = $coreSettings->getSetting('allure.landing.appbanner.playstoreUrl', '#');
      $this->view->actionButtonUrl = $coreSettings->getSetting('allure.landing.appbanner.actionUrl', '#');
      $this->view->actionButtonText = $coreSettings->getSetting('allure.landing.appbanner.actionText', 'Action Button');
      $this->view->showButtons = $coreSettings->getSetting('allure.landing.appbanner.buttons', 0);

      $this->view->target = '';
      $newTab = $coreSettings->getSetting('allure.landing.appbanner.newtab', 1);

      if( !empty($newTab) ) {
        $this->view->target = '_blank';
      }
    }

}
