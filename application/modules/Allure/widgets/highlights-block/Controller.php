<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_Widget_highlightsBlockController extends Engine_Content_Widget_Abstract {

  public function indexAction() {
    $params = array('enabled' => 1);
    $this->view->description  = $this->_getParam('description', '');
    $this->view->highlights = Engine_Api::_()->getDbtable('highlights', 'allure')->getHighlights($params);
    $this->view->highlightsSettings = Engine_Api::_()->getApi('settings', 'core')->getSetting('allure.landing.highlights');

    if( !count($this->view->highlights) ) {
      return $this->setNoRender();
    }
  }

}
