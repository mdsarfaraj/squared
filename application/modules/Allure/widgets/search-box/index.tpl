<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get">
    <input <?php if ($this->viewer()->getIdentity()): ?> style="width:<?php echo $this->searchbox_width; ?>px;" <?php else: ?> <?php endif; ?> type="text" class="text suggested" name="query" id="_titleAjax" size="20" placeholder="<?php echo $this->translate('Search...') ?>" alt='<?php echo $this->translate('Search') ?>'> 
    <i class="fa fa-close" id="close_search_icon" onclick="hideSearchBox()"></i>
   <!-- <button type="button" onclick="this.form.submit();"></button> -->
</form>
