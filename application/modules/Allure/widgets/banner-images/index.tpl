<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php
$baseURL = $this->baseUrl();
?>

<script type="text/javascript">
  $$('.layout_allure_banner_images').each(function (el) {
    el.inject($('global_header'), 'after');
  });
  en4.core.runonce.add(function () {
    var durationOfRotateImage = <?php echo!empty($this->defaultDuration) ? $this->defaultDuration : 500; ?>;
    var slideshowDivObj = $('slide-images');
    var imagesObj = slideshowDivObj.getElements('img');
    var indexOfRotation = 0;

    imagesObj.each(function (img, i) {
      if (i > 0) {
        img.set('opacity', 0);
      }
    });

    var show = function () {
      imagesObj[indexOfRotation].fade('out');
      indexOfRotation = indexOfRotation < imagesObj.length - 1 ? indexOfRotation + 1 : 0;
      imagesObj[indexOfRotation].fade('in');
    };
    show.periodical(durationOfRotateImage);
  });
</script>

<style type="text/css">
  .layout_allure_banner_images #slide-images {
    width: <?php echo!empty($this->slideWidth) ? $this->slideWidth . 'px;' : '100%'; ?>;
    height: <?php echo $this->slideHeight . 'px'; ?>;
  }
</style>

<div id="slide-images" class="allure_slideblock">
  <?php
  foreach( $this->list as $imagePath ):
    if( !is_array($imagePath) ):
      $iconSrc = "application/modules/Allure/externals/images/" . $imagePath;
    else:
      $iconSrc = Engine_Api::_()->allure()->displayPhoto($imagePath['file_id'], 'thumb.icon');
    endif;
    if( !empty($iconSrc) ):
      ?>
      <div class="slideblok_image">
        <img src="<?php echo $iconSrc; ?>" />
      </div>
      <?php
    endif;
  endforeach;
  ?>
  <section class="bannerimage-text">
    <div>
      <?php if( $this->allureHtmlTitle ): ?>
        <h1><?php echo $this->translate($this->allureHtmlTitle); ?></h1>
      <?php endif; ?>
      <?php if( $this->allureHtmlDescription ): ?>
        <article><?php echo $this->translate($this->allureHtmlDescription); ?></article>
      <?php endif; ?>
    </div>
  </section>
</div>