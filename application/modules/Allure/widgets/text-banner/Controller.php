<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2010-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2011-08-026 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_TextBannerController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
      $coreSettings = Engine_Api::_()->getApi('settings', 'core');
      $this->view->text = $coreSettings->getSetting('allure.landing.tbanner.text', 'Grapple our bundle of joyous faces.');
      $this->view->ctatext = $coreSettings->getSetting('allure.landing.tbanner.ctatext', 'Purchase Now');
      $this->view->url = $coreSettings->getSetting('allure.landing.tbanner.url', '#');
      $this->view->newtab = $coreSettings->getSetting('allure.landing.tbanner.newtab', 1);
    }

}
