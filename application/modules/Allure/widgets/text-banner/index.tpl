<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<section class="allure_text_banner_container_fluid">
  <div class="allure_text_banner_container">
    <div class="allure_text_banner_content_wrapper">
      <h2><?php echo $this->text; ?></h2>
      <?php if( !empty($this->url) ) : ?>
        <button onClick="allure_buttonclick()"> <?php echo $this->translate($this->ctatext); ?></button>
      <?php endif; ?>
    </div>
  </div>
</section>

<script type="text/javascript">
  function allure_buttonclick() {
    <?php if( !empty($this->newtab) ) :?>
      window.open('<?php echo $this->url;?>');
    <?php else: ?>
      window.location = '<?php echo $this->url;?>';
    <?php endif; ?>
  }

</script>