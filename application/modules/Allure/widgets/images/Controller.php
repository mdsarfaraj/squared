<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Widget_ImagesController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $this->getElement()->removeDecorator('Title');
    $this->view->coreSettings = $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->view->isSitemenuExist = $isSitemenuExist = Engine_Api::_()->hasModuleBootstrap('sitemenu');
    $tempSitemenuLtype = $tempHostType = 8756;

    $allureGlobalType = $coreSettings->getSetting('allure.global.type', 0);
    $hostType = str_replace('www.', '', strtolower($_SERVER['HTTP_HOST']));
    $allureManageType = $coreSettings->getSetting('allure.manage.type', 1);
    $allureInfoType = $coreSettings->getSetting('allure.info.type', 1);
    $allureLtype = $coreSettings->getSetting('allure.lsettings', 0);

    $this->view->showImages = $coreSettings->getSetting('allure.landing.slider.images', 1);
    $selectedImages = array();
    if( !$this->view->showImages ) {
      $selectedImages = $coreSettings->getSetting('allure.landing.slider.selectedImages');

      if( empty($selectedImages) ) {
        return $this->setNoRender();
      }

      $this->view->list = $getImages = Engine_Api::_()->getItemTable('allure_image')->getImages(array('enabled' => 1, 'selectedImages' => $selectedImages), array('file_id'));
    } else {
      $this->view->list = $getImages = Engine_Api::_()->getItemTable('allure_image')->getImages(array('enabled' => 1), array('file_id'));
    }
    $order = $this->_getParam("order", 2);
    if( !COUNT($getImages) ) {
      $this->view->list = Engine_Api::_()->allure()->setImageOrder(array("1.jpg", "2.jpg", "3.jpg"), $order);
    } else {
      $getImagesArray = $getImages->toArray();
      $this->view->list = Engine_Api::_()->allure()->setImageOrder($getImagesArray, $order);
    }

    $this->view->defaultDuration = $coreSettings->getSetting('allure.landing.slider.speed', 5000);
    $this->view->slideWidth = $coreSettings->getSetting('allure.landing.slider.width', null);
    $this->view->slideHeight = $coreSettings->getSetting('allure.landing.slider.height', 583);
    $this->view->showLogo = $coreSettings->getSetting('allure.landing.slider.showLogo', 1);
    $this->view->logo = $coreSettings->getSetting('allure.landing.slider.logo');
    $this->view->insideHeader = $coreSettings->getSetting('allure.landing.slider.header.position', 1);

    $this->view->allureHtmlTitle = $coreSettings->getSetting('allure.landing.slider.bannerTitle', 'Engage in marvelous social territory');

    $description1 = $coreSettings->getSetting("allure.landing.slider.description1", "Our Commonality is pledged to provide you the best in sphere.");
    $description2 = $coreSettings->getSetting("allure.landing.slider.description2", '');
    $description3 = $coreSettings->getSetting("allure.landing.slider.description3", '');
    
    $description = array();
    if( !empty(trim($description1)) ) {
      $description[] = $this->view->translate($description1);
    }
    if( !empty(trim($description2)) ) {
      $description[] = $this->view->translate($description2);
    }

    if( !empty(trim($description3)) ) {
     $description[] = $this->view->translate($description3);
    }
    $this->view->description = $description;

    $this->view->allureSignupLoginButton = $coreSettings->getSetting('allure.landing.slider.showButton', 1);
    $this->view->signupLoginPopup = $coreSettings->getSetting("allure.landing.slider.signupLoginPopup", 1);
    $this->view->max = $coreSettings->getSetting("allure.landing.slider.max", 4);
    $this->view->showSearch = $coreSettings->getSetting("allure.landing.slider.showSearch", 1);

    if( !count($this->view->list) ) {
      $db = Engine_Db_Table::getDefaultAdapter();
      $page_id = Engine_Api::_()->allure()->getWidgetizedPageId(array('name' => 'core_index_index'));
      $db->query("UPDATE `engine4_core_pages` SET  `layout` =  '' WHERE  `engine4_core_pages`.`page_id` = $page_id LIMIT 1 ;");
      return $this->setNoRender();
    }

    if( empty($allureGlobalType) ) {
      for( $check = 0; $check < strlen($hostType); $check++ ) {
        $tempHostType += @ord($hostType[$check]);
      }

      for( $check = 0; $check < strlen($allureLtype); $check++ ) {
        $tempSitemenuLtype += @ord($allureLtype[$check]);
      }
    }

    $islanguage = $this->view->translate()->getLocale();

    if( !strstr($islanguage, '_') ) {
      $islanguage = $islanguage . '_default';
    }

    $keyForSettings = str_replace('_', '.', $islanguage);
    $allureLendingBlockValue = $coreSettings->getSetting('allure.lending.block.languages.' . $keyForSettings, null);

    $allureLendingBlockTitleValue = $coreSettings->getSetting('allure.lending.block.title.languages.' . $keyForSettings, null);
    if( empty($allureLendingBlockValue) ) {
      $allureLendingBlockValue = $coreSettings->getSetting('allure.lending.block', null);
    }
    if( empty($allureLendingBlockTitleValue) ) {
      $allureLendingBlockTitleValue = $coreSettings->getSetting('allure.lending.block.title', null);
    }

     $this->view->getImageSrcPoint = false;
    if( (empty($allureGlobalType)) && (($allureManageType != $tempHostType) || ($allureInfoType != $tempSitemenuLtype)) ) {
      $this->view->getImageSrcPoint = true;
      // return $this->setNoRender();
    }

    if( !empty($allureLendingBlockValue) )
      $this->view->allureLendingBlockValue = @base64_decode($allureLendingBlockValue);
    if( !empty($allureLendingBlockTitleValue) )
      $this->view->allureLendingBlockTitleValue = @base64_decode($allureLendingBlockTitleValue);
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    $this->view->removePadding = false;

    //GET CONTENT ID
    $content_id = $this->view->identity;
    $content_page_id = Engine_Api::_()->allure()->getContentPageId(array('content_id' => $content_id));
    $layoutValue = Engine_Api::_()->allure()->getWidgetizedPageLayoutValue(array('page_id' => $content_page_id));

    if( $layoutValue == 'default-simple' ) {
     // Zend_Layout::startMvc()->setViewBasePath(APPLICATION_PATH . "/application/modules/Allure/layouts", 'Core_Layout_View');
      $this->view->removePadding = true;
    }

    $allureLendingBlockValue = $coreSettings->getSetting('allure.lending.block', null);

    $this->view->showCloseIcon = $coreSettings->getSetting('allure.signin.popup.close', 1);
    $popupVisibility = $coreSettings->getSetting('allure.signin.popup.visibility', 0);

    if( $popupVisibility != 0 || $this->view->showCloseIcon != 1 ) {
      $this->view->popupClosable = 'false';
    }

    $themes = Engine_Api::_()->getDbtable('themes', 'core')->fetchAll();
    $activeTheme = $themes->getRowMatching('active', 1);

    $this->view->isAlluretheme = false;
    if( $activeTheme->name == 'allure' ) {
      $this->view->isAlluretheme = true;
    }

//    $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
//    $isSubscriptionEnabled = false;
//    $this->view->show_signup_popup = true;
//    if( empty($viewer_id) ) {
//      $tempClassArray = array(
//        'Payment_Plugin_Signup_Subscription',
//        'Sladvsubscription_Plugin_Signup_Subscription'
//      );
//      $db = Zend_Db_Table_Abstract::getDefaultAdapter();
//      $subscriptionObj = $db->query('SELECT `class` FROM `engine4_user_signup` WHERE  `enable` = 1 ORDER BY `engine4_user_signup`.`order` ASC LIMIT 1')->fetch();
//      if( !empty($subscriptionObj) && isset($subscriptionObj['class']) && !empty($subscriptionObj['class']) && in_array($subscriptionObj['class'], $tempClassArray) ) {
//        $isSubscriptionEnabled = true;
//      }
//    }
//    if( !empty($isSubscriptionEnabled) ) {
//      $this->view->show_signup_popup = false;
//    }

    // Advanced Menu settings
    $params = array();
    if (Engine_Api::_()->hasModuleBootstrap('sitemenu')) {
      $params['sitemenu_on_logged_out'] = 1;
      $params['sitemenu_totalmenu'] = $params['sitemenu_totalmenu_tablet'] = $coreSettings->getSetting("allure.landing.slider.max", 6);
      $params['sitemenu_truncation_limit_content'] = $params['sitemenu_truncation_limit_category'] = $coreSettings->getSetting("allure.landing.slider.truncationContent", 6);
      $params['sitemenu_show_cart'] = $coreSettings->getSetting("allure.landing.slider.showCart", 1);
      $params['sitemenu_show_extra_on'] = $coreSettings->getSetting("allure.landing.slider.showCartOn", 1);

      $params['sitemenu_show_link_icon'] = 0;
      $params['sitemenu_is_more_link'] = 1;
      $params['sitemenu_show_in_main_options'] = 1;
      $params['changeMyLocation'] = 0;
      $params['sitemenu_box_shadow'] = 0;
      $params['sitemenu_separator_style'] = 0;
      $params['sitemenu_more_link_icon'] = 0;
      $params['sitemenu_is_arrow'] = 1;
      $params['sitemenu_menu_corners_style'] = 0;
      $params['sitemenu_main_menu_height'] = 20;
      $params['sitemenu_is_fixed'] = 0;
      $params['sitemenu_fixed_height'] = 0;
      $params['sitemenu_style'] = 1;
    }
    $this->view->menuParams = $params;
  }

  private function setOrder($imageArray, $order) {
    if( !empty($order) && $order == 1 ) {
      $imageArray = @array_reverse($imageArray);
    } else if( !empty($order) && $order == 2 ) {
      @shuffle($imageArray);
    }
    return $imageArray;
  }

}