<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style>
  <?php if( $this->insideHeader == 2 ) : ?>
    #global_page_core-index-index #global_header {display: block;}
    #global_page_core-index-index #allure_landing_slider_header {display: none;}
  <?php else: ?>
    #global_page_core-index-index #global_header {display: none;}
    #global_page_core-index-index #allure_landing_slider_header {display: block;}
  <?php endif; ?>
	#global_page_core-index-index #global_wrapper{padding-top: 0;
	margin-top: 0;}
</style>

<?php
$baseURL = $this->baseUrl();
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Seaocore/externals/scripts/jquery.min.js');
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Allure/externals/scripts/typeWriter.js');
?>
<script type="text/javascript">
    if (typeof (window.jQuery) != 'undefined') {
        jQuery.noConflict();

<?php if ($this->removePadding): ?>
            jQuery("#global_wrapper").css('padding-top', '0px');
<?php endif; ?>
        setTimeout(function () {
            if (jQuery(".layout_middle").children().length == 1) {
                jQuery("#global_footer").css('margin-top', '165px');
            }
        }, 100);
    }
    var widgetName = 'layout_allure_images';
</script>
<?php if( $this->showSearch == 1 ) : ?>
  <?php echo $this->content()->renderWidget("allure.search-box", array('allure_search_width' => 1200));?>
<?php endif; ?>
<?php
if ((!empty($this->allureSignupLoginLink) || !empty($this->allureSignupLoginButton)) && !empty($this->isSitemenuExist) && !$this->viewer->getIdentity() && !empty($this->signupLoginPopup)):
    echo $this->partial(
            '_addLoginSignupPopupContent.tpl', 'sitemenu', array(
        'isUserLoginPage' => 0,
        'isUserSignupPage' => 0,
        'isPost' => $this->isPost,
        'sitemenuEnableLoginLightbox' => 1, //$this->show_login,
        'sitemenuEnableSignupLightbox' => 1,
        'showCloseIcon' => $this->showCloseIcon,
        'isAlluretheme' => $this->isAlluretheme,
    ));

    Zend_Registry::set('sitemenu_mini_menu_widget', 1);


endif;
?>

<?php if (!empty($this->isSitemenuExist) && (!empty($this->allureSignupLoginLink) || !empty($this->allureSignupLoginButton))): ?>

    <?php
    $this->headLink()
            ->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sitemenu/externals/styles/style_sitemenu.css');
    ?>
    <?php
    $this->headScript()
            ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sitemenu/externals/scripts/core.js');
    ?>
<?php endif; ?>

<?php
include APPLICATION_PATH . '/application/modules/Allure/views/scripts/_imageContent.tpl';
?> 


<script>
  <?php if( $this->showSearch == 1 ) : ?>
      if( $('menu_search_icon') && $$('.layout_allure_search_box') ) {
        $$('.layout_allure_search_box').each(function(item){
          item.style.display = 'none';
        });
      }
      function showSearchBox() {
        $$('.layout_allure_search_box').each(function(item){
          item.style.display = 'block';
        });
      }

      function hideSearchBox() {
        $$('.layout_allure_search_box').each(function(item){
          item.style.display = 'none';
        });
      }

  <?php endif; ?>
</script>