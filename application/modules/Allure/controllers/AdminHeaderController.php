<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminLayoutController.php 2018-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_AdminHeaderController extends Core_Controller_Action_Admin
{

  public function indexAction()
  {

    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_main', array(), 'allure_admin_header_index');

    //MAKE FORM
    $this->view->form = $form = new Allure_Form_Admin_Settings_Header();

    // Check method/data
    if( !$this->getRequest()->isPost() ) {
      return;
    }
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $values = $form->getValues();
    foreach( $values as $key => $value ) {
      if( $coreSettings->hasSetting( $key, $value ) ) {
        $coreSettings->removeSetting($key);
      }
      $coreSettings->setSetting($key, $value);
    }
    $form->addNotice('Your changes have been saved.');
  }

}
