<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminLayoutController.php 2018-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_AdminLayoutController extends Core_Controller_Action_Admin
{

  public function indexAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_main', array(), 'allure_admin_theme_customization');
    $this->view->subNavigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_theme_customization', array(), 'allure_admin_layout_index');

    //MAKE FORM
    $this->view->form = $form = new Allure_Form_Admin_Settings_Layout();


    if( !$this->getRequest()->isPost() )
      return;

    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }


    $values = $form->getValues();

    $fileName = 'allureThemeGeneralConstants.css';
    $valuesWithPx = $values;
    $valuesWithPx['allure_layout_theme_width'] = $valuesWithPx['allure_layout_theme_width'].'px';
    $valuesWithPx['allure_layout_left_column_width'] = $valuesWithPx['allure_layout_left_column_width'].'px';
    $valuesWithPx['allure_layout_right_column_width'] = $valuesWithPx['allure_layout_right_column_width'].'px';
    $successfullySaved = Engine_Api::_()->getApi('customization', 'allure')->setConstants($valuesWithPx, $fileName);

    foreach( $values as $key => $value ) {
        Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
    }

    if( !empty($successfullySaved) ) {
      Core_Model_DbTable_Themes::clearScaffoldCache();

      $form->addNotice('Changes successfully saved. If changes not reflect to frontend then please change the mode to Development.');
    }
  }

}
