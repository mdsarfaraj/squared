<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: InstallController.php 6590 2018-26-01 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_InstallController extends Core_Controller_Action_Standard
{

  protected $_token;

  public function init()
  {
    $this->_helper->contextSwitch->initContext();
    $settingsTable = Engine_Api::_()->getDbtable('settings', 'core');
    $row = $settingsTable->fetchRow($settingsTable->select()
        ->where('name = ?', 'allure.install.ssotoken'));
    $this->_token = $row ? $row->value : null;    
    $token = $this->_getParam('key', false);
    if( $token != $this->_token ) {
      exit(0);
    }
  }

  public function postInstallAction()
  {
    //CHANGE LAYOUT OF HEADER,FOOTER,SIGNIN,SIGNUP AND LANDING PAGE
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setDefaultLayout(array('allure_landing_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setHeaderLayout(array('allure_header_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setFooterLayout(array('allure_footer_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInPageLayout(array('allure_login_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInRequiredPageLayout(array('allure_login_required_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignUpPageLayout(array('allure_signup_page_layout' => 1));
    $settingsTable = Engine_Api::_()->getDbtable('settings', 'core');
    $settingsTable->delete(array('name = ?' => 'allure.install.ssotoken'));
    exit(0);
  }

}