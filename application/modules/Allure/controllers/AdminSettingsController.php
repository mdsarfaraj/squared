<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminSettingsController.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_AdminSettingsController extends Core_Controller_Action_Admin
{

  public function __call($method, $params)
  {
    /*
     * YOU MAY DISPLAY ANY ERROR MESSAGE USING FORM OBJECT.
     * YOU MAY EXECUTE ANY SCRIPT, WHICH YOU WANT TO EXECUTE ON FORM SUBMIT.
     * REMEMBER:
     *    RETURN TRUE: IF YOU DO NOT WANT TO STOP EXECUTION.
     *    RETURN FALSE: IF YOU WANT TO STOP EXECUTION.
     */

    if( !empty($method) && $method == 'Allure_Form_Admin_Settings' ) {
      
    }
    return true;
  }

  public function indexAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_settings_index');

    $this->view->isModsSupport = Engine_Api::_()->allure()->isModulesSupport();
    $this->view->form = $form = new Allure_Form_Admin_Settings_Global();

    if( $this->getRequest()->isPost() && $form->isValid($this->_getAllParams()) ) {
      $values = $form->getValues();
      
      foreach( $values as $key => $value ) {
        Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
      } 
      $form->addNotice('Your changes have been saved.'); 
    }
  }

  public function configurePagesAction() {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_configure_pages');

     
    $this->view->form = $form = new Allure_Form_Admin_Settings_Configure();

    if( $this->getRequest()->isPost() && $form->isValid($this->_getAllParams()) ) {
      $values = $form->getValues();
      
      foreach( $values as $key => $value ) {
        Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
      }
      $view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
      $URL = $view->baseUrl() . '/admin/content';

      if ($values['allure_landing_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setDefaultLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'landing_page_backup', 'name' => 'core_index_index'));
      }

      if ($values['allure_header_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setHeaderLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'header_backup', 'name' => 'header'));
      }

      if ($values['allure_footer_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setFooterLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'footer_backup', 'name' => 'footer'));
      }

      if ($values['allure_login_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInPageLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'user_auth_login_backup', 'name' => 'user_auth_login'));
      }

      if ($values['allure_login_required_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInRequiredPageLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'core_error_requireuser_backup', 'name' => 'core_error_requireuser'));
      }
      
      if ($values['allure_signup_page_layout']) {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignUpPageLayout($values);
      } else {
        Engine_Api::_()->getApi('pagelayouts', 'allure')->restorePageBackup(array('pageUrl' => 'user_signup_index_backup', 'name' => 'user_signup_index'));
      }
      $form->addNotice('Your changes have been saved. Please check your pages layout from <a href="' . $URL . '" target="_blank">here</a>.');
    }
  }

  public function activateAction() {
    //CHANGE LAYOUT OF HEADER,FOOTER,SIGNIN,SIGNUP AND LANDING PAGE
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setDefaultLayout(array('allure_landing_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setHeaderLayout(array('allure_header_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setFooterLayout(array('allure_footer_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInPageLayout(array('allure_login_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignInRequiredPageLayout(array('allure_login_required_page_layout' => 1));
    Engine_Api::_()->getApi('pagelayouts', 'allure')->setSignUpPageLayout(array('allure_signup_page_layout' => 1));

    $redirect = $this->_getParam('redirect', false);
    if( $redirect == 'install' ) {
      $this->_redirect('install/manage');
    } elseif( $redirect == 'query' ) {
      $this->_redirect('install/manage/complete');
    }
  }


  public function signinPopupAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_settings_signin_popup');

    $this->view->form = $form = new Allure_Form_Admin_Settings_SigninPopup();

    if( $this->getRequest()->isPost() && $form->isValid($this->_getAllParams()) ) {
      $values = $form->getValues();

      foreach( $values as $key => $value ) {
        Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
      }
      $form->addNotice('Your changes have been saved.');
    }
  }

  public function landingImagesAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_settings_images');

     $this->view->subNavigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_settings_images', array(), 'allure_admin_settings_landing_images');
    $this->view->list = Engine_Api::_()->getItemTable('allure_image')->getImages();
  }

  public function innerImagesAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_settings_images');

    $this->view->subNavigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_settings_images', array(), 'allure_admin_settings_inner_images');
    $this->view->list = Engine_Api::_()->getItemTable('allure_banner')->getBanners();
  }

  public function setOrderAction()
  {
    if( empty($_POST) || empty($_POST['order']) ) {
      return;
    }

    $item = $_POST['item'];
    foreach( $_POST['order'] as $key => $value ) {
      if( strstr($key, "content_") ) {
        $keyArray = explode("content_", $key);
        $itemId = end($keyArray);

        if( !empty($itemId) ) {
          $obj = Engine_Api::_()->getItem($item, $itemId);
          print_r($obj->toArray());
          $obj->order = $value;
          $obj->save();
        }
      }
    }
  }

  public function addImagesAction()
  {
    $this->view->form = $form = new Allure_Form_Admin_Images_Add();
    $table = Engine_Api::_()->getItemTable('allure_image');
    //CHECK POST
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    //CHECK VALIDITY
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    //PROCESS
    $values = $form->getValues();

    $db = $table->getAdapter();
    $db->beginTransaction();
    try {

      $title = Engine_Api::_()->getItemTable('allure_image')->getTitleMatch($values['title']);

      $row = $table->createRow();
      $row->setFromArray($values);
      $id = $row->save();

      if( !empty($values['photo']) ) {
        $row->setPhoto($form->photo);
      }


      if( $title ) {
        $values['title'] = $values['title'] . '-' . $id;
      }

      if( $row ) {
        $row->title = $values['title'];
        $row->save();
      }

      //COMMIT
      $db->commit();
      return $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => true,
          'parentRefresh' => true,
          'messages' => array(Zend_Registry::get('Zend_Translate')->_('Image Successfully Added'))
      ));
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
  }

  public function addBannersAction()
  {
    $this->view->form = $form = new Allure_Form_Admin_Banners_Add();
    $table = Engine_Api::_()->getItemTable('allure_banner');
    //CHECK POST
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    //CHECK VALIDITY
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    //PROCESS
    $values = $form->getValues();

    $db = $table->getAdapter();
    $db->beginTransaction();
    try {
      $title = Engine_Api::_()->getItemTable('allure_banner')->getTitleMatch($values['title']);

      $row = $table->createRow();
      $row->setFromArray($values);
      $id = $row->save();

      if( !empty($values['photo']) ) {
        $row->setPhoto($form->photo);
      }

      if( $title ) {
        $values['title'] = $values['title'] . '-' . $id;
      }

      if( $row ) {
        $row->title = $values['title'];
        $row->save();
      }

      //COMMIT
      $db->commit();
      return $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => true,
          'parentRefresh' => true,
          'messages' => array(Zend_Registry::get('Zend_Translate')->_('Image Successfully Added'))
      ));
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }
  }

  public function deleteAction()
  {

    $this->_helper->layout->setLayout('admin-simple');

    $this->view->id = $id = $this->_getParam('id');

    if( $this->getRequest()->isPost() ) {
      $item = Engine_Api::_()->getItem('allure_image', $id);

      $item->delete();
      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => 10,
        'parentRefresh' => 10,
        'messages' => array('Deleted Succesfully.')
      ));
    }
  }

  public function enabledAction()
  {
    $id = $this->_getParam('id');
    if( !empty($id) ) {
      $item = Engine_Api::_()->getItem('allure_image', $id);
      $item->enabled = !$item->enabled;
      $item->save();
    }

    $this->_redirect('admin/allure/settings/landing-images');
  }

  public function enabledBannersAction()
  {
    $id = $this->_getParam('id');
    if( !empty($id) ) {
      $item = Engine_Api::_()->getItem('allure_banner', $id);
      $item->enabled = !$item->enabled;
      $item->save();
    }

    $this->_redirect('admin/allure/settings/inner-images');
  }

  public function deleteBannersAction()
  {

    $this->_helper->layout->setLayout('admin-simple');

    $this->view->id = $id = $this->_getParam('id');

    if( $this->getRequest()->isPost() ) {
      $item = Engine_Api::_()->getItem('allure_banner', $id);

      $item->delete();
      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => 10,
        'parentRefresh' => 10,
        'messages' => array('Deleted Succesfully.')
      ));
    }
  }

  public function faqAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('allure_admin_main', array(), 'allure_admin_settings_faq');
    $this->view->faq_id = $faq_id = $this->_getParam('faq', 'faq_1');
  }

  public function footerMenuAction()
  {
    $this->_redirect('admin/menus/index?name=allure_footer');
  }

  public function placeHtaccessFileAction()
  {
    if( $this->getRequest()->isPost() ) {
      $successfullyAdded = false;
      $getFileContent = '<FilesMatch ".(ttf|otf|woff)$">
    Header set Access-Control-Allow-Origin "*"
</FilesMatch>';

      $global_directory_name = APPLICATION_PATH . '/application/themes/allure';
      $global_settings_file = $global_directory_name . '/.htaccess';
      $is_file_exist = @file_exists($global_settings_file);

      // IF FILE NOT EXIST THEN CREATE NEW .HTACCESS FILE THERE.
      if( empty($is_file_exist) ) {
        if( is_dir($global_directory_name) ) {
          @mkdir($global_directory_name, 0777);

          $fh = @fopen($global_settings_file, 'w') or die('Unable to create .htaccess file; please give the CHMOD 777 recursive permission to the directory "' . APPLICATION_PATH . '/application/themes/allure' . '" and then try again.');
          @fwrite($fh, $getFileContent);
          @fclose($fh);

          @chmod($global_settings_file, 0777);
          $successfullyAdded = true;
        }
      } else {
        if( !is_writable($global_settings_file) ) {
          @chmod($global_settings_file, 0777);
          if( !is_writable($global_settings_file) ) {
            $form->addError('Unable to create .htaccess file; please give the CHMOD 777 recursive permission to the directory "' . APPLICATION_PATH . '/application/themes/allure' . '" and then try again.');
            return;
          }
        }
        $successfullyAdded = @file_put_contents($global_settings_file, $getFileContent);
      }

      if( !empty($successfullyAdded) ) {
        $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => 10,
          'parentRefresh' => 10,
          'messages' => array('File Succesfully Created.')
        ));
      }
    }
  }

  public function placeCustomizationFileAction()
  {
    if( $this->getRequest()->isPost() ) {
      $global_directory_name = APPLICATION_PATH . '/application/themes/allure';
      @chmod($global_directory_name, 0777);

      if( !is_readable($global_directory_name) ) {
        $this->view->error_message = "<span style='color:red'>Note: You do not have readable permission on the path below, please give 'chmod 777 recursive permission' on it to continue with the installation process : <br /> 
Path Name: <b>" . $global_directory_name . "</b></span>";
        return;
      }

      $global_settings_file = $global_directory_name . '/customization.css';
      $is_file_exist = @file_exists($global_settings_file);
      if( empty($is_file_exist) ) {
        @chmod($global_directory_name, 0777);
        if( !is_writable($global_directory_name) ) {
          $this->view->error_message = "<span style='color:red'>Note: You do not have writable permission on the path below, please give 'chmod 777 recursive permission' on it to continue with the installation process : <br /> 
Path Name: " . $global_directory_name . "</span>";
          return;
        }

        $fh = @fopen($global_settings_file, 'w');
        @fwrite($fh, '/* ADD CUSTOM STYLE */');
        @fclose($fh);

        @chmod($global_settings_file, 0777);
      }

      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => 10,
        'parentRefresh' => 10,
        'messages' => array('File Succesfully Created.')
      ));
    }
  }

  //ACTION FOR MULTI-DELETE OF IMAGES
  public function multiDeleteAction()
  {

    if( $this->getRequest()->isPost() ) {

      $values = $this->getRequest()->getPost();

      foreach( $values as $key => $value ) {

        if( $key == 'delete_' . $value ) {

          $item = Engine_Api::_()->getItem('allure_image', $value);

          $item->delete();
        }
      }
    }
    return $this->_helper->redirector->gotoRoute(array('action' => 'images'));
  }

  //ACTION FOR MULTI-DELETE OF BANNERS
  public function multiDeleteBannersAction()
  {

    if( $this->getRequest()->isPost() ) {

      $values = $this->getRequest()->getPost();

      foreach( $values as $key => $value ) {

        if( $key == 'delete_' . $value ) {

          $item = Engine_Api::_()->getItem('allure_banner', $value);

          $item->delete();
        }
      }
    }
    return $this->_helper->redirector->gotoRoute(array('action' => 'banners'));
  }

  public function customCssAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_main', array(), 'allure_admin_theme_customization');
    $this->view->subNavigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_theme_customization', array(), 'allure_admin_settings_custom_css');
    $filePath = APPLICATION_PATH . '/application/themes/allure/customization.css';
    $isFileExist = @file_exists($filePath);

    $this->view->message = '';
    $this->view->sucess = false;

    if( empty($isFileExist) ) {
      $fh = @fopen($filePath, 'w') or die('Unable to write customization CSS file; please give the CHMOD 777 recursive permission to the directory /application/themes/allure/, then try again.');
      @fclose($fh);
    } elseif( !is_writable($filePath) ) {
      @chmod($filePath, 0777);
      if( !is_writable($filePath) ) {
        $this->view->message = 'Unable to write customization CSS file; please give the CHMOD 777 recursive permission to the directory /application/themes/allure/, then try again.';
        return;
      }
    }

    $this->view->fileContent = file_get_contents($filePath);

    // Check method/data
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !isset($_POST['allure_custom_css']) ) {
      return;
    }

    if( @file_put_contents($filePath, $_POST['allure_custom_css']) ) {
      @chmod($filePath, 0777);
      $this->view->message = 'Your changes have been saved.';
      $this->view->sucess = true;
    }
    $this->view->fileContent = $_POST['allure_custom_css'];
  } 
}
