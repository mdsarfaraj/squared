<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminFontsController.php 2018-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_AdminFontsController extends Core_Controller_Action_Admin
{

  public function indexAction()
  {
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_main', array(), 'allure_admin_theme_customization');
    $this->view->subNavigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('allure_admin_theme_customization', array(), 'allure_admin_font_index');
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $font = $coreSettings->getSetting('allure.fonts.selected.font', 0);
    $fontType = $this->getRequest()->getParam('fontType', $font);
    //MAKE FORM
    $this->view->form = $form = new Allure_Form_Admin_Settings_Fonts(array('fontType' => $fontType));

    if( !$this->getRequest()->isPost() )
      return;

    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    $values = $form->getValues();

    $fileName = 'allureThemeGeneralConstants.css';
    $valuesWithPx = $values;
    $valuesWithPx['allure_fonts_body_font_size'] = $valuesWithPx['allure_fonts_body_font_size'].'px';
    $valuesWithPx['allure_fonts_heading_fontsize'] = $valuesWithPx['allure_fonts_heading_fontsize'].'px';
    $valuesWithPx['allure_fonts_mainmenu_font_size'] = $valuesWithPx['allure_fonts_mainmenu_font_size'].'px';
    $valuesWithPx['allure_fonts_tab_font_size'] = $valuesWithPx['allure_fonts_tab_font_size'].'px';
 
    if($fontType == 0) {
      //WE HAVE TO SAVE THE GOOGLE FONT AND WEB SAFE IN ONE CONSTANT
      $valuesWithPx['allure_fonts_body_font_family'] = $valuesWithPx['allure_fonts_body_font_family_google'];
      $valuesWithPx['allure_fonts_heading_font_family'] = $valuesWithPx['allure_fonts_heading_font_family_google'];
      $valuesWithPx['allure_fonts_mainmenu_font_family'] = $valuesWithPx['allure_fonts_mainmenu_font_family_google'];
      $valuesWithPx['allure_fonts_tab_font_family'] = $valuesWithPx['allure_fonts_tab_font_family_google'];
      unset($valuesWithPx['allure_fonts_body_font_family_google'], $valuesWithPx['allure_fonts_heading_font_family_google'], $valuesWithPx['allure_fonts_mainmenu_font_family_google'], $valuesWithPx['allure_fonts_tab_font_family_google']);
    }

    $successfullySaved = Engine_Api::_()->getApi('customization', 'allure')->setConstants($valuesWithPx, $fileName);

    foreach( $values as $key => $value ) {
      Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
    }

    if( !empty($successfullySaved) ) {
      Core_Model_DbTable_Themes::clearScaffoldCache();

      $form->addNotice('Changes successfully saved. If changes not reflect to frontend then please change the mode to Development.');
    }
  }

}
