<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Layout.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Header extends Engine_Form
{

  public function init()
  {
    $this->setTitle("Manage Header");
    $this->setDescription("Here you can manage settings related to header.");

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $isSitemenuEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitemenu');
    $isStoreproductEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitestoreproduct');

    $imgOptions = array('' => 'Text-only (No logo)');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');
    $files = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $files as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;

      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;

      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;

      $imgOptions['public/admin/' . $basename] = $basename;
    }

    $this->addElement('Select', 'allure_header_logo_image', array(
      'label' => 'Select Logo',
      'description' => 'Select the logo for your website. [Note: Logo must be uploaded prior from Admin Panel → Appearance → File & Media Manager.]',
      'multiOptions' => $imgOptions,
      'value' => $coreSettings->getSetting('allure.header.logo.image', ''),
    ));

    $headerOptions = array(
      'logo' => 'Site Logo',
      'mini_menu' => 'Mini Menu',
      'main_menu' => 'Main Menu ',
      'search_box' => 'Search Box',
    );

    $deafultHeaderOptions = array(
      'logo',
      'mini_menu',
      'main_menu',
      'search_box',
    );

    $this->addElement('MultiCheckbox', 'allure_header_loggedin_widgets', array(
      'label' => 'Header Elements for Logged In Members',
      'description' => 'Select the elements you want to display in header section for logged in members.',
      'multiOptions' => $headerOptions,
      'value' => $coreSettings->getSetting('allure.header.loggedin.widgets', $deafultHeaderOptions),
    ));
    
    $this->addElement('MultiCheckbox', 'allure_header_loggedout_widgets', array(
      'label' => 'Header Elements for Non-Logged In Members',
      'description' => 'Select the options you want to display in header section for non-logged in / guest members.',
      'multiOptions' => $headerOptions,
      'value' => $coreSettings->getSetting('allure.header.loggedout.widgets', $deafultHeaderOptions),
    ));
    $options =  array(
        '1' => 'Fix header along with main menu',
        '0' => 'Fix header without main menu',
        '2' => 'Don\'t fix the header'
     );
    if(empty($isSitemenuEnable)) {
      unset($options[1]);
      $options[0] = 'Yes, Fix the header';
    }
    $this->addElement('Radio', 'allure_header_menu_fixed',
        array(
            'label' => 'Fix Header',
            'description' => 'How do you want Header to behave on scroll?',
            'multiOptions' => $options,
            'value' => $coreSettings->getSetting('allure.header.menu.fixed', 0),
      ));

    if( $isSitemenuEnable ) {

      $this->addElement('Radio', 'allure_header_menu_position',
        array(
            'label' => 'Main menu Position',
            'description' => 'How do you want to show main menu?',
            'multiOptions' => array(
               '1' => 'Horizontal',
               '2' => 'Vertical'
            ),
            'value' => $coreSettings->getSetting('allure.header.menu.position', 1),
      ));

      $this->addElement('Radio', 'allure_header_minimenu_design',
        array(
            'label' => 'Mini menu icons / label',
            'description' => 'Select the design type you want to show for your mini menu. (Note: Not all menus support icons. Also, you will be able to configure the icons only for the menus you add from the Menu Editor.)',
            'multiOptions' => array(
               '1' => 'Show icons',
               '0' => 'Show labels with icons'
            ),
            'value' => $coreSettings->getSetting('allure.header.minimenu.design', 1),
      ));

      $this->addElement('Radio', 'allure_header_menu_icon',
        array(
            'label' => 'Main menu icons',
            'description' => 'Do you want to show icons in main menu (sub menus will not be affected)?',
            'multiOptions' => array(
               '1' => 'Yes',
               '0' => 'No'
            ),
            'value' => $coreSettings->getSetting('allure.header.menu.icon', 1),
      ));

      $this->addElement('Text', 'allure_header_desktop_totalmenu', array(
        'label' => 'Menu count',
        'description' => 'How many menu tabs do you want to show? (others menus will go under more section)',
        'value' => $coreSettings->getSetting('allure.header.desktop.totalmenu', 6),
      )); 

      $this->addElement('radio', 'allure_header_display_location', array(
        'label' => "Display Location field",
        'description' => "Do you want to enable 'Location' field, using which users can set their default location?",
        'multiOptions' => array(
          '1' => 'Yes',
          '0' => 'No',
        ),
        'value' => $coreSettings->getSetting('allure.header.display.location', 0),
      ));
      
      if( $isStoreproductEnable ) {
        $this->addElement('radio', 'allure_header_display_cart', array(
          'label' => 'Do you want to show cart icon?',
          'description' => '(Note: After enabling this setting, caching of Main Menu will not work on your website.)',
          'multiOptions' => array(
            1 => 'Yes',
            0 => 'No'
          ),
          'value' => $coreSettings->getSetting('allure.header.display.cart', 1),
        ));
      }
    } else {
      $this->addElement('Text', 'allure_header_desktop_totalmenuwith_dots', array(
        'label' => 'Menu Count with 3 Dots',
        'description' => 'How many menu items you want to show? (Note: Other menu items will go under 3 dots section.)',
        'value' => $coreSettings->getSetting('allure.header.desktop.totalmenuwith.dots', 4),
      ));
    }

    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}
