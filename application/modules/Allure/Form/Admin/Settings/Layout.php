<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Layout.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Layout extends Engine_Form
{

  public function init()
  {
    $this->setTitle("Layout Settings");
    $this->setDescription("Here you can manage the layout of this theme.");

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');

    $this->addElement('Text', 'allure_layout_theme_width', array(
      'label' => 'Theme Width',
      'description' => 'Enter total width of theme [Note: Minimum width should be - 1200px]',
      'required' => true,
      'value' => $coreSettings->getSetting('allure.layout.theme.width', '1200'),
    ));

    $this->addElement('Text', 'allure_layout_left_column_width', array(
      'label' => 'Left Column Width',
      'description' => 'Enter width for left column [Note: Width should be in range from 220 - 300 px. Width of center column will be set after deducting the width of left and right column.]',
      'required' => true,
      'allowEmpty' => false,
      'value' => $coreSettings->getSetting('allure.layout.left.column.width', '250'),
    ));

    $this->addElement('Text', 'allure_layout_right_column_width', array(
      'label' => 'Right Column Width',
      'description' => 'Enter width for right column [Note: Width should be in range from 220 - 300 px. Width of center column will be set after deducting the width of left and right column.]',
      'required' => true,
      'allowEmpty' => false,
      'value' => $coreSettings->getSetting('allure.layout.right.column.width', '250'),
    ));

    $this->addElement('Radio', 'allure_layout_hide_left_column', array(
      'label' => 'Hide Left Column in Mobile Device',
      'description' => 'Do you want to hide left column in Mobile devices?',
      'multiOptions' => array(
        'none' => 'Yes',
        'block' => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.layout.hide.left.column', 'none'),
    ));

    $this->addElement('Radio', 'allure_layout_hide_right_column', array(
      'label' => 'Hide Right Column in Mobile Device',
      'description' => 'Do you want to hide right column in Mobile devices?',
      'multiOptions' => array(
        'none' => 'Yes',
        'block' => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.layout.hide.right.column', 'block'),
    ));
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}
