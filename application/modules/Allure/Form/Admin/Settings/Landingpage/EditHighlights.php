<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Stats.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Landingpage_EditHighlights extends Engine_Form
{

  public function init()
  {
    $this->setMethod('post')
      ->setAttrib('class', 'global_form_box');


    $this->addElement('Text', 'title', array(
      'label' => 'Title',
      'required' => true,
    ));

    $this->addElement('File', 'icon', array(
        'label' => 'Upload Icon',
    )); 
    
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
      'required' => true,
    ));

    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => '',
      'onClick' => 'javascript:parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');

  }

}

?>
