<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemusic
 * @copyright  Copyright 2017-2022 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Delete.php 2017-03-27 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Landingpage_Delete extends Engine_Form
{

  public function init()
  {
    $this->setTitle('Delete Entry')
      ->setDescription('Are you sure you want to delete this entry?')
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ->setMethod('POST');
    ;

    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Delete Entry',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => '',
      'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }

}
