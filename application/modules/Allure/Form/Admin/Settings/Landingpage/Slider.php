<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Layout.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Landingpage_Slider extends Engine_Form
{

  public function init()
  {
    $description = sprintf(Zend_Registry::get('Zend_Translate')->_("Here you can manage slider appearing on landing page. You have the privilege to select preferred images for the slider. To upload new Slider images please go to Slider Images >> Landing Page Slider Images section in the admin panel of this theme. <a title='Preview - Slider' href='application/modules/Allure/externals/images/screenshots/banner.png' target='_blank' class='allure_icon_view' > </a>"));
    $this->setTitle("Manage Slider");
    $this->setDescription("$description");
  
    $this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOption('escape', false);

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->setAttrib('id', 'form-upload');
    $this->addElement('Radio', 'allure_landing_slider_images', array(
      'label' => 'Slider Images',
      'description' => "Select images that you want to show in image slider on landing page??",
      'multiOptions' => array(
        1 => 'Show All Images.',
        0 => 'Select particular images.'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.images', 1),
      'onclick' => 'showMultiCheckboxImageOptions()'
    ));

    $listImage = Engine_Api::_()->getItemTable('allure_image')->getImages(array('enabled' => 1));
    $listArray = array();
    foreach( $listImage->toArray() as $images ) {
      $listArray[$images['image_id']] = $images['title'];
    }

    $this->addElement('MultiCheckbox', 'allure_landing_slider_selectedImages', array(
      'multiOptions' => $listArray,
      'label' => '',
      'description' => 'Please select images for Slider.',
      'value' => $coreSettings->getSetting('allure.landing.slider.selectedImages', ''),
    )); 
    
    $this->addElement('Text', 'allure_landing_slider_height', array(
      'label' => 'Slider Height',
      'description' => 'Enter height for the images.',
      'value' => $coreSettings->getSetting('allure.landing.slider.height', 583),
    ));

    $this->addElement('Text', 'allure_landing_slider_speed', array(
      'label' => 'Slider Speed',
      'description' => 'Enter time delay for images to rotate in image slider (in milliseconds (ms)).',
      'value' => $coreSettings->getSetting('allure.landing.slider.speed', 5000),
    ));

    $this->addElement('Radio', 'allure_landing_slider_order', array(
      'label' => 'Order for Images',
      'description' => 'In which sequence do you want to rotate the images?',
      'multiOptions' => array(
        2 => 'Randomly',
        1 => 'Descending',
        0 => 'Ascending'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.order', 2),
    ));

    // Get available files
    $logoOptions = array('' => 'Text-only (No logo)');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');

    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $logoOptions['public/admin/' . $basename] = $basename;
    }

    $this->addElement('Radio', 'allure_landing_slider_showLogo', array(
      'label' => 'Display Logo',
      'description' => "Do you want to display your website's logo on the top-left side of image slider?",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.showLogo', 1),
      'onclick' => 'showLogoOptions()'
    ));

    $this->addElement('Select', 'allure_landing_slider_logo', array(
      'label' => 'Select Logo',
      'description' => 'Select the site logo for your website. [Note: You can upload logo from: "Layout" > "File & Media Manager".]',
      'value' => $coreSettings->getSetting('allure.landing.slider.logo', ''),
      'multiOptions' => $logoOptions,
    ));


    $this->addElement('Radio', 'allure_landing_slider_header_position', array(
      'label' => 'Header Position',
      'description' => "Where do you want to show Header?",
      'multiOptions' => array(
        1 => 'Inside slider',
        2 => 'Outside slider (Global Header)'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.header.position', 1),
      'onclick' => 'showBrowseMenusOptions()'
    )); 

    $this->addElement('Radio', 'allure_landing_slider_showSearch', array(
      'label' => 'Display Search Box',
      'description' => 'Do you want to show search box on the top of landing page?',
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.showSearch', 1),
    ));

    if( Engine_Api::_()->hasModuleBootstrap('sitemenu') ) {
      
      $this->addElement('Text', 'allure_landing_slider_max', array(
        'label' => 'Menu Items Count',
        'description' => "How many menu items do you want to show in the header?",
        'value' => $coreSettings->getSetting('allure.landing.slider.max', 6),
      ));

      $this->addElement('Text', 'allure_landing_slider_truncationContent', array(
        'label' => 'Menu Truncation Limit',
        'description' => "Enter the title truncation limit for the menu and categories title.",
        'value' => $coreSettings->getSetting('allure.landing.slider.truncationContent', 20),
      )); 
      
      $this->addElement('Radio', 'allure_landing_slider_showCart', array(
        'label' => 'show cart icon',
        'description' => 'Do you want to show cart icon? (Note: This setting will work only if you have our Stores / Marketplace - Ecommerce Plugin. After enabling this setting, caching of Main Menu will not work on your website.)',
        'multiOptions' => array(
          1 => 'Yes',
          0 => 'No'
        ),
        'value' => $coreSettings->getSetting('allure.landing.slider.showCart', 1),
      ));

      $this->addElement('Radio', 'allure_landing_slider_showCartOn', array(
        'label' => 'When to show "Cart Icon"',
        'description' => 'When do you want to show "Cart Icon" (if you have our Stores / Marketplace - Ecommerce Plugin)',
        'multiOptions' => array(
          1 => 'Always',
          0 => 'On Scroll'
        ),
        'value' => $coreSettings->getSetting('allure.landing.slider.showCartOn', 1),
      ));
    }
    $this->addElement('Text', 'allure_landing_slider_bannerTitle', array(
      'label' => 'Slider Title',
      'description' => 'Enter the title text you want to display on the image slider?',
      'style' => 'width:350px;',
      'value' => $coreSettings->getSetting('allure.landing.slider.bannerTitle', 'Engage in marvelous social territory'),
    ));

    $this->addElement('Text', 'allure_landing_slider_description1', array(
      'label' => 'Moving Text on Slider',
      'description' => 'Enter the text you want to display on the image slider. These texts will be shown as moving texts. [Note: Maximum three lines can be added.]',
      'style' => 'width:350px;',
      'value' => $coreSettings->getSetting('allure.landing.slider.description1', 'Our Community is pledged to provide you the best in sphere.'),
    ));

    $this->addElement('Text', 'allure_landing_slider_description2', array(
      'label' => '',
      'description' => '',
      'style' => 'width:350px;',
      'value' => $coreSettings->getSetting('allure.landing.slider.description2', ''),
    ));

    $this->addElement('Text', 'allure_landing_slider_description3', array(
      'label' => '',
      'description' => '',
      'style' => 'width:350px;',
      'value' => $coreSettings->getSetting('allure.landing.slider.description3', ''),
    ));


    $this->addElement('Radio', 'allure_landing_slider_showButton', array(
      'label' => 'Sign In & Sign Up Buttons',
      'description' => "Do you want to show Sign In and Sign Up buttons on this image slider?",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.showButton', 1),
    ));

    $this->addElement('Radio', 'allure_landing_slider_signupLoginPopup', array(
      'label' => 'Pop-up for signIn/ SignUp',
      'description' => "Do you want Sign In and Sign Up form to be opened in a pop-up box?",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.landing.slider.signupLoginPopup', 1),
    ));

    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));
  }

}
?>

<script type="text/javascript">
  var form = document.getElementById("form-upload");
  window.addEvent('domready', function () {

    showLogoOptions();
    showBrowseMenusOptions();
    showMultiCheckboxImageOptions();
  });

  function showMultiCheckboxImageOptions() {
    if (form.elements["allure_landing_slider_images"].value == 1) {
      $('allure_landing_slider_selectedImages-wrapper').style.display = 'none';
    } else {
      $('allure_landing_slider_selectedImages-wrapper').style.display = 'block';
    }
  }

  function showBrowseMenusOptions() {
    if (form.elements["allure_landing_slider_header_position"].value == 1) {
      $('allure_landing_slider_max-wrapper').style.display = 'block';
      $('allure_landing_slider_showSearch-wrapper').style.display = 'block';
      <?php if( Engine_Api::_()->hasModuleBootstrap('sitemenu') ):?>
        $('allure_landing_slider_truncationContent-wrapper').style.display = 'block';
        $('allure_landing_slider_showCart-wrapper').style.display = 'block';
        $('allure_landing_slider_showCartOn-wrapper').style.display = 'block';
      <?php endif; ?>
    } else {
      $('allure_landing_slider_max-wrapper').style.display = 'none';
      $('allure_landing_slider_showSearch-wrapper').style.display = 'none';
      <?php if( Engine_Api::_()->hasModuleBootstrap('sitemenu') ):?>
        $('allure_landing_slider_truncationContent-wrapper').style.display = 'none';
        $('allure_landing_slider_showCart-wrapper').style.display = 'none';
        $('allure_landing_slider_showCartOn-wrapper').style.display = 'none';
       <?php endif; ?>
    }
  }

  function showLogoOptions() {
    if (form.elements["allure_landing_slider_showLogo"].value == 1) {
      $('allure_landing_slider_logo-wrapper').style.display = 'block';
    } else {
      $('allure_landing_slider_logo-wrapper').style.display = 'none';
    }
  }

</script>