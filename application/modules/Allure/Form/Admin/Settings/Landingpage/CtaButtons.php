<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Layout.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Landingpage_CtaButtons extends Engine_Form
{

  public function init()
  {
    $description = sprintf(Zend_Registry::get('Zend_Translate')->_("Here you can manage the content for Action Buttons. <a title='Preview - Cta Butotns' href='application/modules/Allure/externals/images/screenshots/cta-buttons.png' target='_blank' class='allure_icon_view' > </a>"));
    $this->setTitle("Manage CTA Buttons");
    $this->setDescription("$description");
    $this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOption('escape', false);
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->setAttrib('id', 'form-upload');
     $this->addElement('Text', 'allure_landing_cta_title1', array(
        'label' => 'Title for Button 1',
        'value' => $coreSettings->getSetting('allure.landing.cta.title1', 'Create Your Abstract'),
      ));

      $this->addElement('File', 'allure_landing_cta_icon1', array(
         'label' => 'Upload Button 1 Icon',
      )); 
      $this->addElement('File', 'allure_landing_cta_hover_icon1', array(
         'label' => 'Upload Button 1 Hover Icon',
      )); 

      $this->addElement('Text', 'allure_landing_cta_url1', array(
        'label' => 'URL for Button 1',
        'value' => $coreSettings->getSetting('allure.landing.cta.url1', ''),
      ));

      $this->addElement('Text', 'allure_landing_cta_title2', array(
        'label' => 'Title for Button 2',
        'value' => $coreSettings->getSetting('allure.landing.cta.title2', 'Dig into Our Events'),
      ));

      $this->addElement('File', 'allure_landing_cta_icon2', array(
         'label' => 'Upload Button 2 Icon',
      )); 
      $this->addElement('File', 'allure_landing_cta_hover_icon2', array(
         'label' => 'Upload Button 2 Hover Icon',
      ));

      $this->addElement('Text', 'allure_landing_cta_url2', array(
        'label' => 'URL for Button 2',
        'value' => $coreSettings->getSetting('allure.landing.cta.url2', ''),
      ));

      $this->addElement('Text', 'allure_landing_cta_title3', array(
        'label' => 'Title for Button 3',
        'value' => $coreSettings->getSetting('allure.landing.cta.title3', 'Chamber of our Videos'),
      ));

      $this->addElement('File', 'allure_landing_cta_icon3', array(
         'label' => 'Upload Button 3 Icon',
      )); 
      $this->addElement('File', 'allure_landing_cta_hover_icon3', array(
         'label' => 'Upload Button 3 Hover Icon',
      ));

      $this->addElement('Text', 'allure_landing_cta_url3', array(
        'label' => 'URL for Button 3',
        'value' => $coreSettings->getSetting('allure.landing.cta.url3', ''),
      ));

      $this->addDisplayGroup( array('allure_landing_cta_title1', 'allure_landing_cta_url1', 'allure_landing_cta_icon1', 'allure_landing_cta_hover_icon1'), 'allure_landing_cta_block1');
      $this->addDisplayGroup( array('allure_landing_cta_title2', 'allure_landing_cta_url2', 'allure_landing_cta_icon2', 'allure_landing_cta_hover_icon2'), 'allure_landing_cta_block2');
      $this->addDisplayGroup( array('allure_landing_cta_title3', 'allure_landing_cta_url3', 'allure_landing_cta_icon3', 'allure_landing_cta_hover_icon3'), 'allure_landing_cta_block3');

      $this->addElement('radio', 'allure_landing_cta_newtab', array(
        'label' => "Open URL in New Tab?",
        'multiOptions' => array(
          '1' => 'Yes',
          '0' => 'No',
        ),
        'value' => $coreSettings->getSetting('allure.landing.cta.newtab', 0),
      ));

      $this->addElement('Button', 'submit', array(
        'label' => 'Save Changes',
        'type' => 'submit',
        'decorators' => array(
          'ViewHelper',
        ),
      ));
  }
}
?>

