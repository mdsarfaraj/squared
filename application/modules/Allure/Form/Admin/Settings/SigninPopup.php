<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id:SigninPopup.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_SigninPopup extends Engine_Form
{

  public function init()
  {
    $this->setTitle("Manage Sign-in Popup");
    $this->setDescription("Here, you can manage settings relevant to Sign-In Popup.");

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');

    $this->addElement('Radio', 'allure_signin_popup_display', array(
      'label' => 'Auto Display of Sign-In Popup',
      'description' => 'Do you want sign-in popup to be displayed automatically when users visit your website?',
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.signin.popup.display', 1),
    ));

    $this->addElement('Text', 'allure_signin_popup_visibility', array(
      'label' => 'Auto Display Duration',
      'description' => "Enter the time duration in day(s) after which you want the Sign-In popup to show up again(Enter '0' to display every time page loads.)",
      'value' => $coreSettings->getSetting('allure.signin.popup.visibility', 0),
    ));

    $this->addElement('Radio', 'allure_signin_popup_close', array(
      'label' => 'Allow Sign-In / Sign-Up Popup Closure',
      'description' => 'Do you want to allow users to be able to close sign-in / sign-up popup?',
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No [Note: User will have to Sign-In or Sign Up then only user will be able to access your website.]'
      ),
      'value' => $coreSettings->getSetting('allure.signin.popup.close', 1),
    ));

    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}
