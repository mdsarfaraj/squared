<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Settings.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Configure extends Engine_Form
{

  public function init()
  {
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->setTitle(sprintf(Zend_Registry::get('Zend_Translate')->_("Configure Pages")))
      ->setDescription(sprintf(Zend_Registry::get('Zend_Translate')->_("Here you can configure pages and can create backup of pages. If you want to get the previous set up of landing page and other sections of your website, you can configure it from here.")));

    $this->addElement('Radio', 'allure_landing_page_layout', array(
      'label' => 'Allure Landing Page',
      'description' => "Do you want Landing Page setup of Responsive Allure theme? If you choose ‘Yes’ you will get the Landing Page as default setup of Allure Theme. If chosen ‘No’, you will get the previous setup of Landing Page which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.landing.page.layout', 1),
    ));

    $this->addElement('Radio', 'allure_header_page_layout', array(
      'label' => 'Allure Header',
      'description' => "Do you want Header as Responsive Allure Theme? If you choose ‘Yes’ you will get the header as in default setup of Allure Theme. If chosen ‘No’, you will get the previous Header which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.header.page.layout', 1),
    ));

    $this->addElement('Radio', 'allure_footer_page_layout', array(
      'label' => 'Allure Footer',
      'description' => "Do you want Footer as Responsive Allure Theme? If you choose ‘Yes’ you will get the Footer as in default setup of Allure Theme. If chosen ‘No’, you will get the previous Footer which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.footer.page.layout', 1),
    ));

    $this->addElement('Radio', 'allure_login_page_layout', array(
      'label' => 'Allure Login Page',
      'description' => "Do you want Login page setup of Responsive Allure theme? If you choose ‘Yes’ you will get the Login page as default setup of Allure Theme. If chosen ‘No’, you will get the previous setup of Login page which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.login.page.layout', 1),
    ));

    $this->addElement('Radio', 'allure_login_required_page_layout', array(
      'label' => 'Allure Login Required Page',
      'description' => "Do you want Login Required Page setup of Responsive Allure theme? If you choose ‘Yes’ you will get the Login Required Page as default setup of Allure Theme. If chosen ‘No’, you will get the previous setup of Login Required Page which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.login.required.page.layout', 1),
    ));

    $this->addElement('Radio', 'allure_signup_page_layout', array(
      'label' => 'Allure Sign Up Page',
      'description' => "Do you want Sign Up Page setup of Responsive Allure theme? If you choose ‘Yes’ you will get the Sign Up Page as default setup of Allure Theme. If chosen ‘No’, you will get the previous setup of Sign Up Page which was before the installation of Responsive Allure Theme.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettings->getSetting('allure.signup.page.layout', 1),
    )); 
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));
  }

}
