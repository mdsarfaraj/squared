<?php


/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Layout.php 2018-04-05 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Settings_Fonts extends Engine_Form
{
  protected $_fontType;

  public function getFontType()
  {
    return $this->_fontType;
  }

  public function setFontType($fontType)
  {
    $this->_fontType = $fontType;
    return $this;
  }

  public function init()
  {

    $this->setTitle("Manage Fonts");
    $this->setDescription("Here you can manage fonts for your website. [Note: This set of settings will affect the fonts of your entire website.]");

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');

    $fontsOptions = array('0' => 'Google Fonts', '1' => 'Web Safe');
    $webSafeFonts = array(
       'Georgia, serif' => 'Georgia, serif',
       '"Palatino Linotype", "Book Antiqua", Palatino' => '"Palatino Linotype", "Book Antiqua", Palatino',
       '"Times New Roman", Times, serif' => '"Times New Roman", Times, serif',
       'Arial, Helvetica, sans-serif' => 'Arial, Helvetica, sans-serif',
       '"Arial Black", Gadget, sans-serif' => '"Arial Black", Gadget, sans-serif',
       '"Comic Sans MS", cursive, sans-serif' => '"Comic Sans MS", cursive, sans-serif',
       'Impact, Charcoal, sans-serif' => 'Impact, Charcoal, sans-serif',
       '"Lucida Sans Unicode", "Lucida Grande", sans-serif' => '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
       'Tahoma, Geneva, sans-serif' => 'Tahoma, Geneva, sans-serif',
       '"Trebuchet MS", Helvetica, sans-serif' => '"Trebuchet MS", Helvetica, sans-serif',
       'Verdana, Geneva, sans-serif' => 'Verdana, Geneva, sans-serif',
       '"Courier New", Courier, monospace' => '"Courier New", Courier, monospace',
       '"Lucida Console", Monaco, monospace' => '"Lucida Console", Monaco, monospace'
    );
    $descriptionGoogleFont = 'Add code of Google font family below. You can see Google fonts from <a href = "https://fonts.google.com/" target = "_blank">here</a><br>[Sample format: "Roboto", sans-serif]';

    $this->addElement('Select', 'allure_fonts_selected_font', array(
      'label' => 'Font Type',
      'description' => 'Select font type which you want to implement on your website.',
      'multiOptions' => $fontsOptions,
      'onchange' => 'fetchFontSettings(this.value)',
      'value' =>  $this->getFontType(),
    ));

    if($this->_fontType) {
      $this->addElement('Select', 'allure_fonts_body_font_family', array(
        'label' => 'Font Family for Body Tag',
        'description' => 'Select font family for text added in body tag.',
        'multiOptions' => $webSafeFonts,
        'value' => $coreSettings->getSetting('allure.fonts.body.font.family', ''),
      ));
    } else {
      $this->addElement('Text', 'allure_fonts_body_font_family_google', array(
        'description' => $descriptionGoogleFont,
        'label' => 'Font Family for Body',
        'required' => true,
        'value' => $coreSettings->getSetting('allure.fonts.body.font.family.google', '"Roboto", sans-serif'),
      ));
      $this->allure_fonts_body_font_family_google->addDecorator('Description', array('placement' => 'PREPEND', 'class' => 'description', 'escape' => false));
    } 

    $this->addElement('Text', 'allure_fonts_body_font_size', array(
      'label' => 'Font Size for Body Tag',
      'description' => 'Enter font size for text added in body tag.',
      'required' => true,
      'value' => $coreSettings->getSetting('allure.fonts.body.font.size', 13 ),
    )); 
    if($this->_fontType) {
      $this->addElement('Select', 'allure_fonts_heading_font_family', array(
        'label' => 'Font Family for Headings',
        'description' => 'Select the font family for headings.',
        'multiOptions' => $webSafeFonts,
        'value' => $coreSettings->getSetting('allure.fonts.heading.font.family', ''),
      ));
    } else {
      $this->addElement('Text', 'allure_fonts_heading_font_family_google', array(
        'description' => $descriptionGoogleFont,
        'label' => 'Font Family for Headings',
        'required' => true,
        'value' => $coreSettings->getSetting('allure.fonts.heading.font.family.google', '"Roboto", sans-serif'),
      ));
      $this->allure_fonts_heading_font_family_google->addDecorator('Description', array('placement' => 'PREPEND', 'class' => 'description', 'escape' => false));
    }

    $this->addElement('Text', 'allure_fonts_heading_fontsize', array(
      'label' => 'Font Size for Headings',
      'description' => 'Enter font size for headings.',
      'required' => true,
      'value' => $coreSettings->getSetting('allure.fonts.heading.fontsize', 15 ),
    ));

    if($this->_fontType) {
      $this->addElement('Select', 'allure_fonts_mainmenu_font_family', array(
        'label' => 'Font Family for Main Menu',
        'description' => 'Select font family for main menu.',
        'multiOptions' => $webSafeFonts,
        'value' => $coreSettings->getSetting('allure.fonts.mainmenu.font.family', ''),
      ));
    } else {
      $this->addElement('Text', 'allure_fonts_mainmenu_font_family_google', array(
        'description' => $descriptionGoogleFont,
        'label' => 'Main menu Font Family',
        'required' => true,
        'value' => $coreSettings->getSetting('allure.fonts.mainmenu.font.family.google', '"Roboto", sans-serif'),
      ));
      $this->allure_fonts_mainmenu_font_family_google->addDecorator('Description', array('placement' => 'PREPEND', 'class' => 'description', 'escape' => false));
    }

    $this->addElement('Text', 'allure_fonts_mainmenu_font_size', array(
      'label' => 'Font Size for Main Menu',
      'description' => 'Enter font size for main menu.',
      'required' => true,
      'value' => $coreSettings->getSetting('allure.fonts.mainmenu.font.size', 14 ),
    ));

    if($this->_fontType) {
      $this->addElement('Select', 'allure_fonts_tab_font_family', array(
        'label' => 'Font Family for Tabs',
        'description' => 'Select font family for menu tabs.',
        'multiOptions' => $webSafeFonts,
        'value' => $coreSettings->getSetting('allure.fonts.tab.font.family', ''),
      ));
    } else {
      $this->addElement('Text', 'allure_fonts_tab_font_family_google', array(
        'description' => $descriptionGoogleFont,
        'label' => 'Font Family for Menu Tabs',
        'required' => true,
        'value' => $coreSettings->getSetting('allure.fonts.tab.font.family.google', '"Roboto", sans-serif'),
      ));
      $this->allure_fonts_tab_font_family_google->addDecorator('Description', array('placement' => 'PREPEND', 'class' => 'description', 'escape' => false));
    }
    $this->addElement('Text', 'allure_fonts_tab_font_size', array(
      'label' => 'Font Size for Tabs under Menu',
      'description' => 'Enter font size for tabs under menu.',
      'required' => true,
      'value' => $coreSettings->getSetting('allure.fonts.tab.font.size', 13 ),
    ));
    
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}
