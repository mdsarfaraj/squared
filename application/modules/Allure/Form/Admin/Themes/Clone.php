<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Clone.php 10164 2014-04-14 15:35:35Z lucas $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Allure_Form_Admin_Themes_Clone extends Engine_Form
{
  public function init()
  {
    $this
      ->setTitle('Theme Manager')
      ->setDescription('Here, you can create a new color scheme by cloning the existing theme. You can customize various parameters of the cloned theme, like, color, font size, font family etc.')
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ;

    $this->addElement('Text', 'title', array(
      'label' => 'Theme Title',
      'description' => 'Enter the title for new color scheme.',
      'required' => true
    ));

    $this->addElement('Textarea', 'description', array(
      'label' => 'Theme Description',
    ));

    $this->addElement('Select', 'clonedname', array(
      'label' => 'Base Color Scheme',
      'multiOptions' => array(),
    ));
 
    // Init submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Clone',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addElement('Cancel', 'cancel', array(
      'prependText' => ' or ',
      'link' => true,
      'label' => 'Cancel',
      'onclick' => 'history.go(-1); return false;',
      'decorators' => array(
        'ViewHelper'
      )
    ));
  }
}