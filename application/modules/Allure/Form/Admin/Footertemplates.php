<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Customization.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Form_Admin_Footertemplates extends Engine_Form
{

  public function init()
  {


    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->setTitle("Footer Templates");
    $this->setDescription("Here, you can manage settings related to footer.");
    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    $logoBackgroundOptions = array('application/modules/Allure/externals/images/default_footer_bg.png' => 'Default Image');
    $logoOptions = array('' => 'Text-only (No logo)');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');
    $view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $logoBackgroundOptions['public/admin/' . $basename] = $basename;
      $logoOptions['public/admin/' . $basename] = $basename;
    }

    $this->addElement('Radio', 'allure_footer_background', array(
      'description' => 'Choose footer background for your website.',
      'label' => 'Footer Background',
      'multiOptions' => array(
        2 => 'Color & Image',
        1 => 'Only Color',
      ),
      'onclick' => 'showFooterBackgroundImage(this.value);',
      'value' => $coreSettings->getSetting('allure.footer.background', 2),
    ));

    $this->addElement('Select', 'allure_footer_backgroundimage', array(
      'description' => 'Select background image for footer of your website.',
      'label' => 'Footer Background Image',
      'multiOptions' => $logoBackgroundOptions,
      'value' => $coreSettings->getSetting('allure.footer.backgroundimage', 'application/modules/Allure/externals/images/default_footer_bg.png'),
    ));

    $this->addElement('Radio', 'allure_footer_show_logo', array(
      'description' => 'Do you want to show website’s logo in footer?',
      'label' => 'Show Logo in Footer',
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No',
      ),
      'onclick' => 'showFooterLogo(this.value);',
      'value' => $coreSettings->getSetting('allure.footer.show.logo', 1),
    ));


    $this->addElement('Select', 'allure_footer_select_logo', array(
      'description' => 'Select your website’s logo to be placed in the footer.',
      'label' => 'Footer Logo',
      'multiOptions' => $logoOptions,
      'value' => $coreSettings->getSetting('allure.footer.select.logo'),
    ));

    $allurefooterLendingBlockValue = $coreSettings->getSetting('allure.footer.lending.block', null);
    if( empty($allurefooterLendingBlockValue) || is_array($allurefooterLendingBlockValue) ) {
      $allurefooterLendingBlockValue = 'Explore &amp; Watch videos that you have always dreamed of, and post &amp; share your videos to connect with own community.';
    } else {
      $allurefooterLendingBlockValue = @base64_decode($allurefooterLendingBlockValue);
    }

    //WORK FOR MULTILANGUAGES START
    $localeMultiOptions = Engine_Api::_()->allure()->getLanguageArray();

    $defaultLanguage = $coreSettings->getSetting('core.locale.locale', 'en');
    $total_allowed_languages = Count($localeMultiOptions);
    $slidableBlocks = array();
    if( !empty($localeMultiOptions) ) {
      $count = 0;
      foreach( $localeMultiOptions as $key => $label ) {
        $lang_name = $label;
        if( isset($localeMultiOptions[$label]) ) {
          $lang_name = $localeMultiOptions[$label];
        }

        $page_block_field = "allure_footer_lending_page_block_$key";

        if( !strstr($key, '_') ) {
          $key = $key . '_default';
        }

        $keyForSettings = str_replace('_', '.', $key);
        $allurefooterLendingBlockValueMulti = $coreSettings->getSetting('allure.footer.lending.block.languages.' . $keyForSettings, null);
        if( empty($allurefooterLendingBlockValueMulti) ) {
          $allurefooterLendingBlockValueMulti = $allurefooterLendingBlockValue;
        } else {
          $allurefooterLendingBlockValueMulti = @base64_decode($allurefooterLendingBlockValueMulti);
        }

        $page_block_label = sprintf(Zend_Registry::get('Zend_Translate')->_("Footer Title in %s"), $lang_name);

        if( $total_allowed_languages <= 1 ) {
          $page_block_field = "allure_footer_lending_page_block";
          $page_block_label = "Footer Title";
        } elseif( $label == 'en' && $total_allowed_languages > 1 ) {
          $page_block_field = "allure_footer_lending_page_block";
        }

        $plugins = "directionality,advlist,autolink,lists,link,image,charmap,print,preview,hr,anchor,"
          . "pagebreak,searchreplace,wordcount,visualblocks,visualchars,code,fullscreen,insertdatetime,"
          . "media,nonbreaking,save,table,contextmenu,directionality,emoticons,paste,textcolor,imagetools,colorpicker,autosave";

        $editorOptions = array(
          'upload_url' => false,
          'menubar' => true,
          'forced_root_block' => false,
          'force_p_newlines' => false,
          'plugins' => $plugins,
          'toolbar1' => "ltr,rtl,undo,redo,removeformat,pastetext,|,code,link,media,image,emoticons,|,bullist,numlist,|,print,preview,fullscreen",
          'toolbar2' => "fontselect,fontsizeselect,bold,italic,underline,strikethrough,forecolor,backcolor,|,alignleft,aligncenter,alignright,alignjustify,|,outdent,indent,blockquote",
          'image_advtab' => true,
        );
        $editorOptions['height'] = '500px';

        $this->addElement('Textarea', $page_block_field, array(
          'label' => $page_block_label,
          'description' => "Configure the title which will display on the top of the footer.",
          'attribs' => array('rows' => 24, 'cols' => 80, 'style' => 'width:200px; max-width:200px; height:240px;'),
          'value' => $allurefooterLendingBlockValueMulti,
          'filters' => array(
            new Engine_Filter_Html(),
            new Engine_Filter_Censor()),
          'editorOptions' => $editorOptions,
        ));

        if( $total_allowed_languages > 1 && $count == 0 ) {
          $this->addElement('Dummy', 'show_hide_link', array(
            'decorators' => array(array('ViewScript', array(
                  'viewScript' => '_clickableLink.tpl',
                  'class' => 'form element'
                ))),
            'ignore' => true,
          ));
        }

        if( $page_block_field != "allure_footer_lending_page_block_en" && $total_allowed_languages > 1 ) {
          $slidableBlocks[] = $page_block_field;
        }
      }
      if( $total_allowed_languages > 1 ) {
        $this->addDisplayGroup($slidableBlocks, 'slideable_language_options');
      }
    }
    //WORK FOR MULTILANGUAGES END
    $tempLogoOptions = array();
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');
    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $tempLogoOptions['public/admin/' . $basename] = $basename;
    }

    $this->addElement('Text', 'allure_mobile', array(
      'description' => 'Please enter your contact number.',
      'label' => 'Contact Number',
      'value' => $coreSettings->getSetting('allure.mobile', '+1-777-777-7777')
    ));

    $this->addElement('Text', 'allure_mail', array(
      'description' => 'Please enter URL of your website.',
      'label' => 'Email Address',
      'value' => $coreSettings->getSetting('allure.mail', 'info@test.com')
    ));

    $this->addElement('Text', 'allure_website', array(
      'description' => 'Please enter URL of your website.',
      'label' => 'Website URL',
      'value' => $coreSettings->getSetting('allure.website', 'www.example.com')
    ));

    $this->addElement('Radio', 'allure_fotter_subscribeus', array(
      'description' => 'Do you want to display "Subscribe Us" form?',
      'label' => 'Subscribe Us',
      'multiOptions' => array(
        1 => 'Yes ',
        0 => 'No',
      ),
      'value' => $coreSettings->getSetting('allure.fotter.subscribeus', 1),
    ));
        
    $this->addElement('Radio', 'allure_twitter_feed', array(
      'description' => 'Do you want to display twitter feeds? [Note: Feeds will be displayed in place of third column of footer menu.]',
      'label' => 'Twitter Feed',
      'multiOptions' => array(
        1 => 'Yes ',
        0 => 'No',
      ),
      'onclick' => 'showTwitterFeed(this.value);',
      'value' => $coreSettings->getSetting('allure.twitter.feed', 0),
    ));
    
    $this->addElement('Text', 'allure_twitterCode', array(
      'description' => 'Paste Embed Code of Twitter.',
      'label' => 'Twitter Embed code',
      'value' => $coreSettings->getSetting('allure.twitterCode', '')
    ));

    $this->addElement('MultiCheckbox', 'allure_social_links', array(
      'description' => 'Select the social links you want to be available in this block.',
      'label' => 'Social Links',
      'multiOptions' => array(
        "facebooklink" => "Facebook Link",
        "twitterlink" => "Twitter Link",
        "pininterestlink" => "Pinterest Link",
        "youtubelink" => "YouTube Link",
        "linkedinlink" => "LinkedIn Link"
      ),
      'value' => $coreSettings->getSetting('allure.social.links', array("facebooklink", "twitterlink", "pininterestlink", "youtubelink", "linkedinlink"))
    ));

    $this->addElement('Dummy', 'note_description', array(
      'description' => 'Note: If you leave any URL box blank then that particular social link will not appear in the footer of your website.'
      )
    );

    //FOR FACEBOOK SOCIAL LINK
    $this->addElement('Dummy', 'facebook', array(
      'label' => '1. Facebook'
      )
    );

    $this->addElement('Text', 'allure_facebook_url', array(
      'label' => 'URL',
      'value' => $coreSettings->getSetting('allure.facebook.url', 'http://www.facebook.com/')
      )
    );

    $this->addElement('Text', 'allure_facebook_title', array(
      'label' => 'Text on Hover',
      'value' => $coreSettings->getSetting('allure.facebook.title', 'Like us on Facebook')
      )
    );
    $this->addDisplayGroup(array('allure_facebook_url', 'allure_facebook_title'), 'allure_facebook_block');

    //WORK FOR TWITTER SOCIAL LINK
    $this->addElement('Dummy', 'twitter', array(
      'label' => '2. Twitter'
      )
    );

    $this->addElement('Text', 'allure_twitter_url', array(
      'label' => 'Url',
      'value' => $coreSettings->getSetting('allure.twitter.url', 'https://www.twitter.com/')
      )
    );

    $this->addElement('Text', 'allure_twitter_title', array(
      'label' => 'Text on Hover',
      'value' => $coreSettings->getSetting('allure.twitter.title', 'Follow us on Twitter')
      )
    );
    $this->addDisplayGroup(array('allure_twitter_url', 'allure_twitter_title'), 'allure_twitter_block');

    //WORK FOR PININTEREST SOCIAL LINK
    $this->addElement('Dummy', 'pinterest', array(
      'label' => '3. Pinterest'
      )
    );

    $this->addElement('Text', 'allure_pinterest_url', array(
      'label' => 'Url',
      'value' => $coreSettings->getSetting('allure.pinterest.url', 'https://www.pinterest.com/')
      )
    );

    $this->addElement('Text', 'allure_pinterest_title', array(
      'label' => 'Text on Hover',
      'value' => $coreSettings->getSetting('allure.pinterest.title', 'Pinterest')
      )
    );
    $this->addDisplayGroup(array('allure_pinterest_url', 'allure_pinterest_title'), 'allure_pinterest_block');

    //WORK FOR YOUTUBE SOCIAL LINK
    $this->addElement('Dummy', 'youtube', array(
      'label' => '4. YouTube'
      )
    );

    $this->addElement('Text', 'allure_youtube_url', array(
      'label' => 'Url',
      'value' => $coreSettings->getSetting('allure.youtube.url', 'http://www.youtube.com/')
      )
    );

    $this->addElement('Text', 'allure_youtube_title', array(
      'label' => 'Text on Hover',
      'value' => $coreSettings->getSetting('allure.youtube.title', 'Youtube')
      )
    );
    $this->addDisplayGroup(array('allure_youtube_url', 'allure_youtube_title'), 'allure_youtube_block');

    //WORK FOR LinkedIn SOCIAL LINK
    $this->addElement('Dummy', 'linkedin', array(
      'label' => '5. LinkedIn'
      )
    );

    $this->addElement('Text', 'allure_linkedin_url', array(
      'label' => 'Url',
      'value' => $coreSettings->getSetting('allure.linkedin.url', 'https://www.linkedin.com/')
      )
    );

    $this->addElement('Text', 'allure_linkedin_title', array(
      'label' => 'Text on Hover',
      'value' => $coreSettings->getSetting('allure.linkedin.title', 'LinkedIn')
      )
    );
    $this->addDisplayGroup(array('allure_linkedin_url', 'allure_linkedin_title'), 'allure_linkedin_block');


    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper',
      ),
    ));
  }

}