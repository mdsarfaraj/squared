<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Highlights.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Allure_Model_DbTable_Highlights extends Engine_Db_Table
{
  protected $_rowClass = 'Allure_Model_Highlight';

  public function getHighlights($params = array()) {

    $select = $this->select();

    if( isset($params['enabled']) ) {
      $select->where('enabled = ?', $params['enabled']);
    }
    $select->order("order ASC");

    return $this->fetchAll($select);
  }

}
