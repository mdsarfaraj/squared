<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Allure
 * @copyright  Copyright 2015-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Subscriptions.php 2015-06-04 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Allure_Model_DbTable_Subscriptions extends Engine_Db_Table {

    protected $_name = 'allure_subscriptions';
    protected $_rowClass = "Allure_Model_Subscription";

    public function isSubscribed($email) {
      $tableName = $this->info('name');
      $select = $this->select();
      $data = $select->from($tableName, 'email')
        ->where('email = ?', $email)
        ->query()
        ->fetchColumn();

      if( $data ) {
        return true;
      }

      return false;
    }

    public function getEmailList() {
      $tableName = $this->info('name');
      $select = $this->select()
        ->from($tableName, 'email');

      foreach( $select->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0) as $email ) {
        $emails[] = $email;
      }
      return $emails;
    }

}
