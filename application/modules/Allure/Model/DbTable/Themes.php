<?php

/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Themes.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Allure_Model_DbTable_Themes extends Core_Model_DbTable_Themes
{
	public function getThemes($params = array()) {
	    $tableName = $this->info('name');
	    $select = $this->select(); 
	    if (isset($params['type'])) {
	        $select->where('type = ?', $params['type']);
	    }
	    if (isset($params['themeIdDesc']) && !empty($params['themeIdDesc'])) {
	        $select->order("theme_id DESC");
	    } 
	    return $this->fetchAll($select);
	}
}