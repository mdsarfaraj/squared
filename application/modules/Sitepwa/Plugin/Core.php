<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Core.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Plugin_Core extends Zend_Controller_Plugin_Abstract
{

  public function routeShutdown(Zend_Controller_Request_Abstract $request)
  {
    if( !Zend_Registry::isRegistered('sitepwa_content_ids') ) {
      Zend_Registry::set('sitepwa_content_ids', array());
    }
    $pwa = $request->getParam("pwa");
    $session = new Zend_Session_Namespace('sitepwa');
    $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
    if( $pwa == "1" ) {
      $pwa = true;
    } elseif( $pwa == "0" ) {
      $pwa = false;
    } elseif( $request->getParam("utm_source") === 'sitepwa' ) {
      $pwa = true;
      $session->pwaApp = true;
    } else {
      if( isset($session->pwa) ) {
        $pwa = $session->pwa;
      } else {
        $enable = $coreSettingsApi->getSetting('sitepwa.mode.enable', 1);
        // CHECK TO SEE IF MOBILE
        $pwa = $enable && (Engine_Api::_()->sitepwa()->isMobileDevice() || Engine_Api::_()->sitepwa()->isTabletDevice());
      }
    }
    $session->pwa = $pwa;

    if( !$pwa ) {
      return;
    }
    $this->_loadViewHelperPath();

    // CHECK IF ADMIN
    if( substr($request->getPathInfo(), 1, 5) == "admin" ) {
      return;
    }
    $module = $request->getModuleName();
    $controller = $request->getControllerName();
    $action = $request->getActionName();
    if( $module == 'core' && $controller == 'widget' && $action == 'index' && $request->getParam('content_id', false) && $session->pwaPageName && is_array($session->pwaPageContentIds) && in_array($request->getParam('content_id', false), $session->pwaPageContentIds) ) {
      $request->setModuleName('sitepwa');
    }

    $loader = Engine_Loader::getInstance();
    $className = 'Sitepwa_Api_Menus';
    Engine_Loader::loadClass($className);
    if( method_exists($loader, 'setComponent') ) {
      $loader->setComponent('Core_Api_Menus', new $className());
    }
    $this->_loadViewHelperPath();
    $content = Engine_Content::getInstance();
    // if( get_class($content->getStorage()) === 'Core_Model_DbTable_Pages' ) {
    // Set storage
    $contentTable = Engine_Api::_()->getDbtable('pages', 'sitepwa');
    $content->setStorage($contentTable);
    //}
  }

  public function onRenderLayoutDefault($event, $mode = null)
  {

    $this->_onLoad($event);
    $view = $event->getPayload();
    if( !($view instanceof Zend_View_Interface) ) {
      return;
    }

    if( !Engine_Api::_()->sitepwa()->hasEnabledPWAmode() ) {
      return;
    }
    $session = new Zend_Session_Namespace('sitepwa');
    $session->pwaPageName = Zend_Registry::isRegistered('sitepwa_content_page_id') ? Zend_Registry::get('sitepwa_content_page_id') : false;
    $session->pwaPageContentIds = Zend_Registry::get('sitepwa_content_ids');
  }

  public function onRenderLayoutDefaultSimple($event, $mode = null)
  {
    $this->_onLoad($event);
  }

  public function onCorePageUpdateAfter($event)
  {
    $payload = $event->getPayload();
    $tagMapTable = Engine_Api::_()->getDbtable('PageMaps', 'sitepwa');
    $tagMapTable->updateMapRow($payload);
  }

  public function onCorePageDeleteBefore($event)
  {
    $payload = $event->getPayload();
    Engine_Api::_()->getDbtable('content', 'sitepwa')->delete(array(
      'page_id = ?' => $payload->page_id,
    ));
    Engine_Api::_()->getDbtable('PageMaps', 'sitepwa')->delete(array(
      'page_map_id = ?' => $payload->page_id,
    ));
  }

  private function _onLoad($event)
  {
    $view = $event->getPayload();
    if( !($view instanceof Zend_View_Interface) ) {
      return;
    }

    if( !Engine_Api::_()->sitepwa()->hasEnabledPWAmode() ) {
      return;
    }

    $view->headPWA();
    if( $view->settings('sitepwa.theme.dispaly', 1) ) {
      // Add themes
      $themeTable = Engine_Api::_()->getDbtable('themes', 'sitepwa');

      $themeSelect = $themeTable->select()
        ->where('active = ?', 1)
        ->limit(1);
      $theme = $themeTable->fetchRow($themeSelect);
      if( $theme ) {
        $themeName = 'sitepwa/' . $theme->name;
        $themes[] = $themeName;
        $themesInfo[$themeName] = include APPLICATION_PATH_COR . DS
          . 'themes' . DS . 'sitepwa' . DS . $theme->name . DS . 'manifest.php';
      }

      $view->layout()->themes = $themes;
      $view->layout()->themesInfo = $themesInfo;
      Zend_Registry::set('Themes', $themesInfo);
    }
  }

  protected function _loadViewHelperPath()
  {
    $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
    $view = $viewRenderer->view;
    if( is_null($view) ) {
      return $this;
    }

    $path = APPLICATION_PATH . '/application/modules/Sitepwa/View/Helper/';
    $prefix = 'Sitepwa_View_Helper_';
    $view->addHelperPath($path, $prefix);
    return $this;
  }

}