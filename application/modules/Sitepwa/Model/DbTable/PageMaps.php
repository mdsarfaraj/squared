<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: PageMaps.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Model_DbTable_PageMaps extends Engine_Db_Table
{

  protected $_name = 'sitepwa_page_maps';

  public function insertMapRow($row)
  {
    $this->insert(array('sitepwa_layout' => $row->layout, 'page_map_id' => $row->page_id));
  }

  public function updateMapRow($row)
  {
    $rowArray = $row->toArray();
    $data = array();
    foreach( $this->_getCols() as $col ) {
      if( isset($rowArray[$col]) ) {
        $data[$col] = $rowArray[$col];
      }
    }
    if( empty($data) ) {
      return;
    }
    if( empty($data['page_map_id']) ) {
      $this->insertMapRow($row);
    } else {
      $this->update($data, array(
        'page_map_id = ?' => $row->page_id
      ));
    }
  }

}