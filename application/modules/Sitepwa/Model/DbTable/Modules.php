<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Modules.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Model_DbTable_Modules extends Engine_Db_Table
{

  protected $_enabledModuleNamesMobile;

  public function getEnabledModuleNames()
  {

    //Get list of all modules from sitepwa module table which are enabled in core module table & in sitepwa module table.
    $coreModulesName = Engine_Api::_()->getDbtable('modules', 'core')->info('name');

    $smModulesName = $this->info('name');

    $select = $this->select()
      ->from($smModulesName, "$smModulesName.name")
      ->setIntegrityCheck(false)
      ->join($coreModulesName, "($smModulesName.name = $coreModulesName.name)", array());



    $enabledModuleNames = $select->where("$coreModulesName.enabled = ?", 1)
      ->query()
      ->fetchAll(Zend_Db::FETCH_COLUMN);



    return $enabledModuleNames;
  }

  public function getManageModulesList($params = array())
  {

    //Get list of all modules from sitepwa module table which are enabled in core module table and visible in sitepwa.
    $coreModulesName = Engine_Api::_()->getDbtable('modules', 'core')->info('name');

    $smModulesName = $this->info('name');

    $column_array = array("$smModulesName.name", "$smModulesName.integrated", "$smModulesName.integrated", "$coreModulesName.title");



    $select = $this->select()
      ->from($smModulesName, $column_array)
      ->setIntegrityCheck(false)
      ->join($coreModulesName, "($smModulesName.name = $coreModulesName.name)", array());
    if( isset($params['integrated']) ) {
      $select->where("$smModulesName.integrated = ?", $params['integrated']);
    }
    $modules = $this->fetchAll($select);

    return $modules;
  }

  public function getExtensionsList($params = array())
  {
    if( !isset($params['modulename']) || !$params['modulename'] )
      return;
    //Get list of all modules from sitepwa module table which are enabled in core module table and visible in sitepwa.
    $coreModulesName = Engine_Api::_()->getDbtable('modules', 'core')->info('name');

    $smModulesName = $this->info('name');

    $select = $this->select()
      ->from($smModulesName, array("$smModulesName.name"))
      ->setIntegrityCheck(false)
      ->join($coreModulesName, "($smModulesName.name = $coreModulesName.name)", array())
      ->where("$smModulesName.name Like ?", $params['modulename'] . "%")
      ->where("$smModulesName.name <> ?", $params['modulename']);

    $modules = $this->fetchAll($select);
    return $modules;
  }
}