<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Pages.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Model_DbTable_Pages extends Core_Model_DbTable_Pages
{

  protected $_name = 'core_pages';

  public function loadMetaData(Engine_Content $contentAdapter, $name)
  {
    $metaData = parent::loadMetaData($contentAdapter, $name);
    if( is_array($metaData) && !empty($metaData) ) {
      $metaData['layout'] = $metaData['sitepwa_layout'];
    }

    return $metaData;
  }

  public function loadContent(Engine_Content $contentAdapter, $name)
  {
    if( is_array($name) ) {
      $name = join('_', $name);
    }
    if( !is_string($name) && !is_numeric($name) ) {
      throw new Exception('not string');
    }

    $select = $this->select()->where('name = ?', $name)->orWhere('page_id = ?', $name);
    $page = $this->fetchRow($select);

    if( !is_object($page) ) {
      // throw?
      return null;
    }
    $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
    if( $page->name == 'footer' && !$coreSettingsApi->getSetting('sitepwa.layout.footer', 0) ) {
      // throw?
      return null;
    }
    // Get all content
    $contentTable = Engine_Api::_()->getDbtable('content', 'sitepwa');
    $select = $contentTable->select()
      ->where('page_id = ?', $page->page_id)
      ->order('order ASC')
    ;
    $content = $contentTable->fetchAll($select);
    if( count($content) == 0 ) {
      $contentTable = Engine_Api::_()->getDbtable('content', 'core');
      $select = $contentTable->select()
        ->where('page_id = ?', $page->page_id)
        ->order('order ASC')
      ;
      $content = $contentTable->fetchAll($select);
      // Create structure
      $structure = $this->preparePWAContentArea($content);
    } else {
      // Create structure
      $structure = $this->prepareContentArea($content);
      if( !in_array($page->page_id, array(1, 2)) ) {
        Zend_Registry::set('sitepwa_content_page_id', $page->page_id);
      }
    }

    // Create element (with structure)
    $element = new Engine_Content_Element_Container(array(
      'class' => 'layout_page_' . $page->name . ' sitepwa_layout_page_' . $page->name,
      'elements' => $structure
    ));

    return $element;
  }

  public function prepareContentArea($content, $current = null)
  {
    // Get parent content id
    $parent_content_id = null;
    if( null !== $current ) {
      $parent_content_id = $current->content_id;
    }

    // Get children
    $children = $content->getRowsMatching('parent_content_id', $parent_content_id);
    if( empty($children) && null === $parent_content_id ) {
      $children = $content->getRowsMatching('parent_content_id', 0);
    }

    // Get struct
    $struct = array();
    foreach( $children as $child ) {
      if( $child->type == 'widget' ) {
        $widget_ids = Zend_Registry::get('sitepwa_content_ids');
        $widget_ids[$child->content_id] = $child->page_id;
        Zend_Registry::set('sitepwa_content_ids', $widget_ids);
      }
      $elStruct = $this->createElementParams($child);
      if( $elStruct ) {
        $elStruct['elements'] = $this->prepareContentArea($content, $child);
        $struct[] = $elStruct;
      }
    }

    return $struct;
  }

  public function preparePWAContentArea($content, $current = null)
  {
    // Get parent content id
    $parent_content_id = null;
    if( null !== $current ) {
      $parent_content_id = $current->content_id;
    }

    // Get children
    $children = $content->getRowsMatching('parent_content_id', $parent_content_id);
    if( empty($children) && null === $parent_content_id ) {
      $children = $content->getRowsMatching('parent_content_id', 0);
    }

    // Get struct
    $struct = array();
    $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
    $ignoreColums = array();
    if( $coreSettingsApi->getSetting('sitepwa.layout.left', 1) ) {
      $ignoreColums[] = 'left';
    }
    if( $coreSettingsApi->getSetting('sitepwa.layout.right', 1) ) {
      $ignoreColums[] = 'right';
    }
    foreach( $children as $child ) {
      if( ($child->type == 'container' && $ignoreColums && in_array($child->name, $ignoreColums) ) ) {
        $elStruct = $this->createElementParams($child);
        if( $elStruct ) {
          $elements = $this->preparePWAContentArea($content, $child);
          $forcedElements = array();
          foreach( $elements as $element ) {
            if( strpos($element['name'], 'search') ) {
              $forcedElements[] = $element;
            }
          }
          if( $forcedElements ) {
            $elStruct['elements'] = $forcedElements;
            $struct[] = $elStruct;
          }
        }
        continue;
      }
      if( (strpos($child->name, '.browse-menu') || strpos($child->name, '.navigation') || strpos($child->name, '.browsenevigation')) && strpos($child->name, '.browse-menu-quick') == FALSE ) {
        $params = $child->params;
        $params['overwrite_widget'] = $child->name;
        $child->name = 'sitepwa.menu-navigation';
        $child->params = $params;
      }
      $elStruct = $this->createElementParams($child);
      if( $elStruct ) {
        $elStruct['elements'] = $this->preparePWAContentArea($content, $child);
        $struct[] = $elStruct;
      }
    }

    return $struct;
  }

  public function createElementParams($row)
  {
    $data = parent::createElementParams($row);
    //if( $row->type === 'widget' ) {
    $data['id'] = 'layout_container_' . $row->type . '_' . $row->content_id;
    $class = !empty($data['class']) ? $data['class'] . ' ' : '';
    $class .= 'sitepwa_layout_' . $row->type . ' ';
    $data['class'] = $class . 'generic_sitepwa_layout_container';
    //}

    return $data;
  }

  public function select($withFromPart = self::SELECT_WITHOUT_FROM_PART)
  {
    $select = parent::select($withFromPart);
    $mapTable = Engine_Api::_()->getDbtable('PageMaps', 'sitepwa');

    $select->from($this->info('name'), '*')->setIntegrityCheck(false)->joinLeft($mapTable->info('name'), $this->info('name') . '.page_id = ' . $mapTable->info('name') . '.page_map_id');
    return $select;
  }

  public function update(array $data, $where)
  {
    $updateData = array();
    foreach( $this->_getCols() as $col ) {
      if( isset($data[$col]) ) {
        $updateData[$col] = $data[$col];
      }
    }
    if( empty($updateData) ) {
      return;
    }
    return parent::update($updateData, $where);
  }

}