<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Manifest.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_Form_Admin_Manifest extends Engine_Form
{

  public function init()
  {
    $view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
    $url = $view->baseUrl() . "/admin/sitepwa/settings/faq/faq/faq_9";
    $description = 'The web app manifest is a simple JSON file that tells the browser about your web application and how it should behave when \'installed\' on the user’s mobile device or desktop. Having a manifest is required for your website to act like a PWA. To know more about Manifest Settings, ';
    $description .= vsprintf('<a href="%1$s" target="_blank"> Click here</a>.', array(
            $url,
        ));
    $this
      ->setTitle('Manifest Settings')
      ->setDescription($description);
    $this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOption('escape', false);


    $manifest = array(
      'name' => '',
      'short_name' => '',
      'start_url' => '',
      'description' => '',
      'background_color' => '#45b2f0',
      'theme_color' => '#ffffff',
      'app_icon' => 'application/modules/Sitepwa/externals/images/app.png'
    );
    $filename = APPLICATION_PATH . '/public/sitepwa/manifest.json';
    if( file_exists($filename) ) {
      $string = file_get_contents($filename);
      $manifest = json_decode($string, true);
    }
    // init site title
    $this->addElement('Text', 'name', array(
      'label' => 'App Name',
      'description' => 'Enter the name of the application that is displayed to the user. This name is used in the app install prompt.',
      'value' => $manifest['name']
    ));

    $this->addElement('Text', 'short_name', array(
      'label' => 'App Short Name',
      'description' => 'Enter the name to be displayed when there is insufficient space to display the full name of the application, like user\'s home screen, launcher, and other places.',
      'value' => $manifest['short_name']
    ));
    $this->addElement('Textarea', 'description', array(
      'label' => 'App Description',
      'description' => 'Enter a general description of what the application does.',
      'value' => $manifest['description']
    ));
    $this->addElement('Text', 'start_url', array(
      'label' => 'Start Url',
      'description' => 'Enter the URL that loads when a user launches the application from a device.',
      'value' => $manifest['start_url']
    ));



    $this->addElement('Text', 'theme_color', array(
      'decorators' => array(array('ViewScript', array(
            'viewScript' => '_formImagerainbowThemeColor.tpl',
            'class' => 'form element',
            'theme_color' => $manifest['theme_color']
          )))
    ));
    $this->addElement('Text', 'background_color', array(
      'decorators' => array(array('ViewScript', array(
            'viewScript' => '_formImagerainbowBackgroundColor.tpl',
            'class' => 'form element',
            'background_color' => $manifest['background_color']
          )))
    ));

    $this->addElement('Select', 'display', array(
      'label' => 'Display',
      'description' => "Select the default display mode for the web application, i.e. what browser UI is shown when your app is launched.<br>Note - In order to show the Add to Home Screen Prompt, display must be set to standalone.",
      'multiOptions' => array(
        'standalone' => 'Standalone App',
        'fullscreen' => 'Fullscreen App',
        'browser' => 'Web Page',
        'minimal-ui' => 'Minimal UI',
      ),
      'value' => $manifest['display']
    ));

$this->getElement('display')->getDecorator('Description')->setOptions(array('placement' =>
        'PREPEND', 'escape' => false));
    $this->addElement('Select', 'orientation', array(
      'label' => 'Screen Orientation',
      'description' => "Select the default screen orientation of the application. You can enforce a specific orientation, which is advantageous for apps that work in only one orientation, such as games.",
      'multiOptions' => array(
        'any' => 'Any',
        'natural' => 'Fullscreen App',
        'landscape' => 'Landscape',
        'portrait' => 'Portrait',
      ),
      'value' => $manifest['orientation']
    ));


    // Get available files (Icon for activity Feed).
    $logoOptions = array('application/modules/Sitepwa/externals/images/app.png' => 'Default Icon');
    $imageExtensions = array('png');

    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $logoOptions['public/admin/' . $basename] = $basename;
    }

    $description = "<div class='tip'><span>" . Zend_Registry::get('Zend_Translate')->_("You have not
                           uploaded an image for site logo. Please upload an image.") . "</span></div>";


    $URL = $view->baseUrl() . "/admin/files";
    $click = '<a href="' . $URL . '" target="_blank">over here</a>';
    $customBlocks = sprintf("The icon of the application that will be displayed on the user’s home screen.", $click);

    $selectedFile = !empty($manifest['app_icon']) && in_array($manifest['app_icon'], array_keys($logoOptions)) ? $manifest['app_icon'] : 'application/modules/Sitepwa/externals/images/app.png';
    if( !empty($logoOptions) ) {
      $this->addElement('Select', 'app_icon', array(
        'label' => 'App Icon',
        'description' => 'Select the icon of the application that will be displayed on the user’s home screen.',
        'multiOptions' => $logoOptions,
        'onchange' => "updateTextFields(this.value)",
        'value' => $selectedFile
      ));
      $this->getElement('app_icon')->getDecorator('Description')->setOptions(array('placement' =>
        'PREPEND', 'escape' => false));
    }
    $photoName = end($manifest['icons'])['src'];

    $description = '<div>';
    $description .= "<img src='$photoName' width='128' height='128'/>";
    $description .= '</div>';
    //VALUE FOR LOGO PREVIEW.
    $this->addElement('Dummy', 'app_photo_preview', array(
      'label' => 'App Icons Preview',
      'description' => $description,
    ));
    $this->app_photo_preview
      ->addDecorator('Description', array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'escape' => false));
    $this->addElement('Hidden', 'gcm_sender_id', array(
      'value' => '103953800507',
      'order' => time()
    ));
    // Add submit button
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}