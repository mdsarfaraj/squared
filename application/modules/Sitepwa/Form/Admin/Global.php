<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Global.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_Form_Admin_Global extends Engine_Form
{

  public function init()
  {
    $this
      ->setTitle('Global Settings')
      ->setDescription('These settings affect all members in your community.');


    $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
    // init site title
    $this->addElement('Text', 'sitepwa_site_title', array(
      'label' => 'Site Title',
      'description' => 'Set the name of your website that would appear in the header throughout on your website, when viewed in a mobile / tablet.',
      'value' => $coreSettingsApi->getSetting('sitepwa.site.title', $coreSettingsApi->getSetting('core_general_site_title'))
    ));
    // Show layout to only logged in users
    $this->addElement('Select', 'sitepwa_layout_left', array(
      'label' => 'Hide Left Column',
      'description' => "Do you want to hide left column from pages which are not added in PWA Layout Editor? This applies to the default pages on the website where you have left column, but have not been added to the PWA Layout Editor.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettingsApi->getSetting('sitepwa.layout.left', 1),
    ));
    // Show layout to only logged in users
    $this->addElement('Select', 'sitepwa_layout_right', array(
      'label' => 'Hide Right Column',
      'description' => "Do you want to hide right column from pages which are not added in PWA Layout Editor? This applies to the default pages on the website where you have right column, but have not been added to the PWA Layout Editor.",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettingsApi->getSetting('sitepwa.layout.right', 1),
    ));

    $this->addElement('Dummy', 'sitepwa_layout_right_note', array(
        'content' => '[Hiding Left and Right columns from your PWA pages improves the page speed instead of hiding elements as in the case of mobile sites, it simply does not load them (ike, css, images, etc) thus improving the speed.]',
      ));

    // Show layout to only logged in users
    $this->addElement('Select', 'sitepwa_layout_footer', array(
      'label' => 'Show footer',
      'description' => "Do you want to show the Site Footer in your PWA?",
      'multiOptions' => array(
        1 => 'Yes',
        0 => 'No'
      ),
      'value' => $coreSettingsApi->getSetting('sitepwa.layout.footer', 0),
    ));

    $imageOptions = array('0' => 'Text-only (No logo)');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');

    $imageOrgOptions = array();
    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $imageOrgOptions['public/admin/' . $basename] = $basename;
    }
    asort($imageOrgOptions);
    foreach( $imageOrgOptions as $key => $value ) {
      $imageOptions[$key] = $value;
    }
    $this->addElement('Select', 'sitepwa_banner_logo', array(
      'label' => 'Site Logo for Banner Widget',
      'multiOptions' => $imageOptions,
      'escape' => false,
      'value' => $coreSettingsApi->getSetting('sitepwa.banner.logo'),
    ));
    // Show layout to only logged in users
    $url = $this->getView()->serverUrl($this->getView()->url(array(), 'default', true)) . '?pwa=1';
    $this->addElement('Select', 'sitepwa_mode_enable', array(
      'label' => 'Auto Enable',
      'description' => "Do you want to enabled the pwa / mobile layout for your site automatically? [Note: If you select the 'Allow only for test users' settings then you can be able to enable PWA mode using this URL: <a href='$url'>$url</a> . ]",
      'multiOptions' => array(
        1 => 'Yes, Allow for all users',
        0 => 'No, Allow only for test users.'
      ),
      'value' => $coreSettingsApi->getSetting('sitepwa.mode.enable', 1),
    ));
    $this->sitepwa_mode_enable->getDecorator('Description')->setOption('escape', false);
    // Add submit button
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}