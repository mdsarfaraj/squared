<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: NavigationMenu.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Form_Admin_Widget_NavigationMenu extends Core_Form_Admin_Widget_Standard
{

  public function init()
  {
    parent::init();

    // Set form attributes
    $this
      ->setTitle('PWA: Navigation Menu')
      ->setDescription('Shows a selected menu. You can edit its contents in your menu editor.')
    ;

    $menuMultiOption = array('' => '');
    $table = Engine_Api::_()->getDbtable('menus', 'core');
    $rName = $table->info('name');

    $select = $table->select()->where("name LIKE ?", '%_main%')->orWhere("name LIKE ?", '%_main_%')->query()
      ->fetchAll();
    foreach( $select as $menu ) {
      if (in_array($menu['name'], array('core_main'))) {
        continue;
      }
      $menuMultiOption[$menu['name']] = $menu['title'];
    }
    $this->addElement('Select', 'sitepwa_menu', array(
      'label' => 'Please choose a menu.',
      'multiOptions' => $menuMultiOption,
      'escape' => false,
    ));
    $this->addElement('Hidden', 'nomobile', array('order' => 808000));
    $this->addElement('Hidden', 'title', array('order' => 808070));
  }

}