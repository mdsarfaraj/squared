<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Banner.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Form_Admin_Widget_Banner extends Core_Form_Admin_Widget_Standard
{

  public function init()
  {
    parent::init();

    // Set form attributes
    $this
      ->setTitle('Site Logo')
      ->setDescription('Shows your site-wide main logo or title.  Images are uploaded via the File Media Manager.')
    ;
    // Get available files
    $logoOptions = array('0' => '');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');

    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $logoOptions['public/admin/' . $basename] = $basename;
    }
    asort($logoOptions);
    $logoOptions[0] = 'Default Image';
    $this->addElement('Select', 'banner', array(
      'label' => 'Banner Image',
      'multiOptions' => $logoOptions,
    ));
     $this->addElement('Text', 'tagline', array(
      'label' => 'Banner Tagline',
      'multiOptions' => $logoOptions,
    ));

    $this->addElement('Hidden', 'nomobile', array('order' => 808000));
    $this->addElement('Hidden', 'title', array('order' => 808070));
  }

}