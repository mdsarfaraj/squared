<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Logo.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Form_Admin_Widget_Logo extends Core_Form_Admin_Widget_Standard
{

  public function init()
  {
    parent::init();

    // Set form attributes
    $this
      ->setTitle('Site Logo')
      ->setDescription('Shows your site-wide main logo or title.  Images are uploaded via the File Media Manager.')
    ;

   
    $this->addElement('Hidden', 'nomobile', array('order' => 808000));
    $this->addElement('Hidden', 'title', array('order' => 808070));
  }

}