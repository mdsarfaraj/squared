<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Header.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Form_Admin_Settings_Header extends Engine_Form
{

  public function init()
  {
    $this->setTitle("Manage Header");
    $this->setDescription("Use this area to manage your Header and Main Navigation Menu.");

    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    // Get available files
    $imageOptions = array('0' => 'Text-only (No logo)');
    $imageExtensions = array('gif', 'jpg', 'jpeg', 'png');

    $imageOrgOptions = array();
    $it = new DirectoryIterator(APPLICATION_PATH . '/public/admin/');
    foreach( $it as $file ) {
      if( $file->isDot() || !$file->isFile() )
        continue;
      $basename = basename($file->getFilename());
      if( !($pos = strrpos($basename, '.')) )
        continue;
      $ext = strtolower(ltrim(substr($basename, $pos), '.'));
      if( !in_array($ext, $imageExtensions) )
        continue;
      $imageOrgOptions['public/admin/' . $basename] = $basename;
    }
    asort($imageOrgOptions);
    foreach( $imageOrgOptions as $key => $value ) {
      $imageOptions[$key] = $value;
    }

    $this->addElement('Select', 'sitepwa_header_large_logo', array(
      'label' => 'Site Logo For Non Logged In Users',
      'description' => 'Select the logo that would be displayed to the non-logged in users of your website. The size of the logo displayed to the non-logged in users would be larger as compared to the logo displayed to the logged in users. [Note: Logo must be uploaded prior from Admin Panel → Appearance → File & Media Manager.]',
      'multiOptions' => $imageOptions,
      'escape' => false,
      'value' => $coreSettings->getSetting('sitepwa.header.large.logo'),
    ));
    $imageOptions[0] = 'No logo & Text';
    $this->addElement('Select', 'sitepwa_header_small_logo', array(
      'label' => 'Site Logo For Logged In Users',
      'description' => 'Select the logo that would be displayed to the logged in users of your website. [Note: Logo must be uploaded prior from Admin Panel → Appearance → File & Media Manager.]',
      'multiOptions' => $imageOptions,
      'escape' => false,
      'value' => $coreSettings->getSetting('sitepwa.header.small.logo'),
    ));


    $headerOptions = array(
      'logo' => 'Site Logo',
      'mini_menu' => 'Mini Menu',
      'main_menu' => 'Main Menu ',
      'search_box' => 'Search Box',
    );

    $deafultHeaderOptions = array(
      'logo',
      'mini_menu',
      'main_menu',
      'search_box',
    );

    $this->addElement('MultiCheckbox', 'sitepwa_header_loggedin_widgets', array(
      'label' => 'Header Elements for Logged In Members',
      'description' => 'Select the elements you want to display in the header for logged in members.',
      'multiOptions' => $headerOptions,
      'value' => $coreSettings->getSetting('sitepwa.header.loggedin.widgets', $deafultHeaderOptions),
    ));

    $this->addElement('MultiCheckbox', 'sitepwa_header_loggedout_widgets', array(
      'label' => 'Header Elements for Non-Logged In Members',
      'description' => 'Select the elements you want to display in the header for non-logged in / guest members.',
      'multiOptions' => $headerOptions,
      'value' => $coreSettings->getSetting('sitepwa.header.loggedout.widgets', $deafultHeaderOptions),
    ));

    $this->addElement('Select', 'sitepwa_header_mainmenu_moduleNavigations', array(
      'label' => 'Main Menu Panel Nested Navigations',
      'description' => 'Do you want to show nested navigations in the Main Menu?',
      'multiOptions' => array(
        '1' => 'Yes',
        '0' => 'No'
      ),
      'value' => $coreSettings->getSetting('sitepwa.header.mainmenu.moduleNavigations', 1),
    ));
    $this->addElement('Select', 'sitepwa_header_mainmenu_settingNavigations', array(
      'label' => 'Show Account Settings options in Main Menu Panel',
      'description' => 'Do you want to show Account Settings navigations in the Main Menu Panel?',
      'multiOptions' => array(
        '1' => 'Yes',
        '0' => 'No'
      ),
      'value' => $coreSettings->getSetting('sitepwa.header.mainmenu.settingNavigations', 1),
    ));
    $this->addElement('Select', 'sitepwa_header_mainmenu_footerSection', array(
      'label' => 'Show Footer Menu and Settings in Main Menu Panel',
      'description' => 'Do you want to show Footer Menu and Settings in Main Menu Panel?',
      'multiOptions' => array(
        '1' => 'Yes',
        '0' => 'No'
      ),
      'value' => $coreSettings->getSetting('sitepwa.header.mainmenu.footerSection', 1),
    ));

    $imageOptions[0] = 'Text-only (No logo)';
    $this->addElement('Select', 'sitepwa_panel_logo', array(
      'label' => 'Set the Site Logo for Main Menu Panel',
      'multiOptions' => $imageOptions,
      'escape' => false,
      'value' => $coreSettings->getSetting('sitepwa.panel.logo'),
    ));
    $imageOptions[0] = 'Default';
    $this->addElement('Select', 'sitepwa_panel_cover', array(
      'label' => 'Background Image for Main Menu Panel',
      'description' => 'Set the Background Image for Sign-in Section in Main Menu Panel',
      'multiOptions' => $imageOptions,
      'escape' => false,
      'value' => $coreSettings->getSetting('sitepwa.panel.cover'),
    ));

    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

}