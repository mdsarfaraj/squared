<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: EditColors.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

# Load the libraries. Do it manually if you don't like this way.
include APPLICATION_PATH . '/application/libraries/Scaffold/libraries/Bootstrap.php';
error_reporting(E_ALL);
ini_set('display_errors', true);

class Sitepwa_Form_Admin_Themes_EditColors extends Engine_Form
{

  protected $_theme;

  public function setTheme($theme)
  {
    $this->_theme = $theme;
  }

  public function init()
  {
    $this
      ->setTitle('Update Color Scheme')
      ->setDescription('Here you can manage color scheme for your website.');
    // Show layout to only logged in users
    $this->addElement('Select', 'sitepwa_update_method', array(
      'label' => 'Color Updation Method',
      'description' => "Select the method which you want to use to update color scheme.",
      'multiOptions' => array(
        'single' => 'Update the colors for specific elements solely',
        'group' => 'Update the colors wherever they are being used',
      ),
      'value' => 'group',
      'onchange' => 'spwThemeUpdateMethod(this.value)'
    ));
    $this->addElement('Dummy', 'sitepwa_header_constants_label', array(
      'label' => 'Heading Color',
    ));
    $this->addElement('Dummy', 'sitepwa_footer_constants_label', array(
      'label' => 'Footer Colors',
    ));
    $this->addElement('Dummy', 'sitepwa_body_constants_label', array(
      'label' => 'Body Colors',
    ));
    //ADD HERE THE LEBEL REPLACEMENT FOR THE CONSTANTS
    $constantLebels = array('sitepwa_body_header_color' => 'Heading Font Color',
      'sitepwa_body_font_color_light' => 'Body Font Color Light',
      'sitepwa_body_input_background_color' => 'Input Box Background Color',
      'sitepwa_body_input_font_color' => 'Input Box Font Color',
      'sitepwa_body_input_border_color' => 'Input Box Border Color',
      'sitepwa_body_comments_background_color' => 'Comments Box Background Color',
      'sitepwa_body_list_background_color' => 'Listview Background Color',
      'sitepwa_body_list_background_color_alt' => 'Listview Background Color Alt',
      'sitepwa_body_list_background_color_on_hover' => 'Listview Background Color On Hover',
      'sitepwa_header_minimenu_items_background_color' => 'Mini Menu Items Background Color',
      'sitepwa_header_minimenu_items_background_color_on_hover' => 'Mini Menu Items Background Color On Hover',
    );
    $colors = array();
    $headerConstants = array();
    $footerConstants = array();
    $bodyConstants = array();
    foreach( $this->getColorConstants() as $name => $value ) {
      $colors[$value] = $value;
      $constantType = explode('_', $name)[1];
      $labelString = $name;
      if( $constantType == 'header' ) {
        $headerConstants[] = $name;
        $labelString = str_replace('sitepwa_header_', '', $name);
      } else if( $constantType == 'footer' ) {
        $footerConstants[] = $name;
        $labelString = str_replace('sitepwa_footer_', '', $name);
      } else if( $constantType == 'body' ) {
        $bodyConstants[] = $name;
        $labelString = str_replace('sitepwa_body_', '', $name);
      }
      if( array_key_exists($name, $constantLebels) ) {
        $labelString = $constantLebels[$name];
      } else {
        $labelString = ucwords(preg_replace('/[^a-z-0-9]/', ' ', $labelString));
      }

      $this->addElement('Text', $name, array(
        'label' => ucwords(preg_replace('/[^a-z-0-9]/', ' ', $name)),
        'decorators' => array(array('ViewScript', array(
              'viewScript' => '_formColor.tpl',
              'class' => 'constant_color_single_element form element',
              'name' => $name,
              'value' => $value,
              'label' => $labelString
            )))
      ));
    }
    $this->addDisplayGroup(array_merge(array('sitepwa_header_constants_label'), $headerConstants), 'sitepwa_header_constants');
    $this->addDisplayGroup(array_merge(array('sitepwa_footer_constants_label'), $footerConstants), 'sitepwa_footer_constants');
    $this->addDisplayGroup(array_merge(array('sitepwa_body_constants_label'), $bodyConstants), 'sitepwa_body_constants');

    foreach( $colors as $name => $value ) {
      $name = 'spwgroupcolor-' . md5($name);
      $name = $this->addElement('Text', $name, array(
        'label' => "Color: " . $value,
        'decorators' => array(array('ViewScript', array(
              'viewScript' => '_formColor.tpl',
              'class' => 'constant_color_group_element form element',
              'name' => $name,
              'value' => $value,
              'label' => "Color: " . $value,
            )))
      ));
    }
    // Add submit button
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
  }

  public function getColorConstants()
  {
    $path = APPLICATION_PATH . '/application/themes/sitepwa/' . $this->_theme . '/colorConstants.css';
    $elements = array();
    if( file_exists($path) ) {
      $css = new Scaffold_CSS($path);
      $css->string = '@constants {' . $css->string . '}';
      $found = $css->find_at_group('constants');
      $elements = $found['values'];
    }
    return $elements;
  }

}