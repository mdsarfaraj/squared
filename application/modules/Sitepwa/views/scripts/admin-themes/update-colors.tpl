<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: update-colors.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<h2><?php echo $this->translate("Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface") ?></h2>

<?php if( count($this->navigation) ): ?>
	<div class='seaocore_admin_tabs tabs clr'>
		<?php
		// Render the menu
		//->setUlClass()
		echo $this->navigation()->menu()->setContainer($this->navigation)->render()
		?>
	</div>
<?php endif; ?>

<div class='clear'>
  <div class='settings'>

		<?php echo $this->form->render($this); ?>

  </div>
</div>
<script type="text/javascript">
  function spwThemeUpdateMethod(showElement) {
    var hideElement = showElement == 'group' ? 'single' : 'group';
    $$('.constant_color_' + hideElement + '_element').hide();
    $$('.constant_color_' + showElement + '_element').show();
    if(showElement == 'group') {
      $('sitepwa_header_constants-wrapper').hide();
      $('sitepwa_footer_constants-wrapper').hide();
      $('sitepwa_body_constants-wrapper').hide();
    } else {
      $('sitepwa_header_constants-wrapper').show();
      $('sitepwa_footer_constants-wrapper').show();
      $('sitepwa_body_constants-wrapper').show();
    }
  }
  spwThemeUpdateMethod($('sitepwa_update_method').value);
</script>