<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: faq.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<h2><?php echo $this->translate("Mobile / Tablet Plugin - Progressive Web Apps") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='seaocore_admin_tabs tabs clr'>
    <?php
    // Render the menu
    //->setUlClass()
    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<div class="admin_seaocore_files_wrapper">
  <ul class="admin_seaocore_files sitepwa_faq">

    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_1');"><?php echo 'What are the requirements I need to fulfill before setting up PWA for my website?'; ?></a>
      <div class='faq' style='display: none;' id='faq_1'>
        <?php echo 'Ans: To setup PWA for your website, it must be running under HTTPS and also you need a Web App Manifest (this is provided by this plugin). You simply need to fill in all the information about your app in the Manifest settings section of this plugin.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_2');"><?php echo 'My website is hosted on HTTP, will this plugin work for me?'; ?></a>
      <div class='faq' style='display: none;' id='faq_2'>
        <?php echo 'Ans: For PWA plugin to work for your site, the website must be https enabled. However, using this plugin your website will still be responsive when opened in the mobile browser.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_3');"><?php echo 'How can I set up a different layout for the PWA when compared to the website?'; ?></a>
      <div class='faq' style='display: none;' id='faq_3'>
        <?php echo 'Ans: You can set up the layout of your PWA from the Layout Editor section of your plugin. You can simply drag and drop the widgets to arrange the content you want on each page. Also, pages in PWA have a single column, unlike your website where pages have multiple columns.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_4');"><?php echo 'How can I set the default theme color for the PWA?'; ?></a>
      <div class='faq' style='display: none;' id='faq_4'>
        <?php echo 'Ans: You can set the default theme color of your application from the Manifest settings section of this plugin.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_5');"><?php echo 'I do not want some sections that appear on the website to appear in my PWA. Where can I remove them?'; ?></a>
      <div class='faq' style='display: none;' id='faq_5'>
        <?php echo 'Ans: To disable any section from your PWA pages you will have to remove the related widget from the widgetized page from Layout editor section of the Progressive Web App plugin.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_6');"><?php echo 'The Add to Home Screen Popup is not coming when I open the site in my mobile browser. What could be the reason?'; ?></a>
      <div class='faq' style='display: none;' id='faq_6'>
        <?php echo 'Ans: In order to show the Add to Home Screen Prompt, Display must be set to Standalone in the Manifest settings of this plugin. The Add to Home Screen popup will not appear when the display is set to Full Screen, Web Page or Minimal UI.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_7');"><?php echo 'Why are the left and the right columns of my website not appearing in this plugin’s Layout Editor section?'; ?></a>
      <div class='faq' style='display: none;' id='faq_7'>
        <?php echo 'Ans: Pages in PWA contain only the middle column of Content Layout unlike the default pages of the website where pages have multiple columns. The purpose of having a single column is to increase the speed of the Progressive Web App as with a single column, it does not load the left and right columns, images, css, etc. thus improving the speed.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_8');"><?php echo 'Why are Push notifications not working with PWA?'; ?></a>
      <div class='faq' style='display: none;' id='faq_8'>
        <?php echo 'Ans: Push notifications are not provided by default with the PWA plugin. You need to have our Browser Push Notifications plugin for push notifications to work with PWA.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_9');"><?php echo 'What are the Manifest Settings?'; ?></a>
      <div class='faq' style='display: none;' id='faq_9'>
        <?php echo 'Ans: The web app manifest is a simple JSON file that tells the browser about your web application and how it should behave when \'installed\' on the user’s mobile device or desktop. The Manifest settings affect the manifest.json file of your web application. Having a manifest is required for Progressive Web App to work for your website. <br/>
          The manifest file is required to declare the following:<br/>
          <b>App Name:</b> The name of the application that is displayed to the user. This name is used in the app install prompt. <br/>
          <b>App Short Name:</b> The name to be displayed when there is insufficient space to display the full name of the application, like user\'s home screen, launcher, and other places.<br/>
          <b>App Description:</b> A general description of what the application does.<br>
          <b>PWA Launch Url:</b> The URL that loads when a user launches the application from a device.<br>
          <b>Theme Color:</b> The default theme color for the application.<br>
          <b>Background Color:</b> The background color of the screen that is displayed between launching the application and loading its contents.<br>
          <b>Display:</b> The default display mode for the web application, i.e. what browser UI is shown when your app is launched. There are four different display types available -<br>
          <p style="text-indent: 50px;">
          <b>Fullscreen:</b> All of the available display area is used.<br>
          </p>
          <p style="text-indent: 50px;">
          <b>Standalone:</b> Opens the application to look and feel like a standalone native application with its own icon in the application launcher.<br>
          </p>
          <p style="text-indent: 50px;">
          <b>Minimal-ui:</b> The application will look and feel like a standalone application, but will have a minimal set of UI elements for controlling navigation. The elements will vary with browser.<br>
          </p>
          <p style="text-indent: 50px;">
          <b>Browser:</b> This is the default display type. The application opens in a conventional browser tab or new window, depending on the browser and platform.<br>
          </p>
          <b>Note -</b> In order to show the Add to Home Screen Prompt, display must be set to standalone.<br>
          <b>Screen Orientation:</b> The default screen orientation of the application. You can enforce a specific orientation, which is advantageous for apps that work in only one orientation, such as games.<br>
          <b>App Icon:</b> The icon of the application that will be displayed on the user’s home screen.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_10');"><?php echo 'How does Offline mode work?'; ?></a>
      <div class='faq' style='display: none;' id='faq_10'>
        <?php echo 'Ans: The website will work even when there is no or poor internet connectivity, this means that though you will not able to load new content, the site will continue to load the pages which have been previously visited.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_11');"><?php echo 'Will Progressive Web Apps work on iOS devices?'; ?></a>
      <div class='faq' style='display: none;' id='faq_11'>
        <?php echo 'Ans: Yes, iOS supports Progressive Web Apps from version 11.3. However, there are a few limitations in iOS, like the display: fullscreen and display: minimal-ui won’t work on iOS; fullscreen will trigger standalone, and minimal-ui will be just a shortcut to Safari. The app can store no more than 50MB of offline files. Splash screen will not work.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_12');"><?php echo 'Do I need to integrate any other plugin of my website with this plugin to make it work?'; ?></a>
      <div class='faq' style='display: none;' id='faq_12'>
        <?php echo 'Ans: You don’t need to integrate any plugin with the Progressive Web App plugin for it to work, however, if you wish to have push notifications available for your app, you need to have our Browser Push Notifications plugin for push notifications to work with PWA.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_13');"><?php echo 'How can I add my custom CSS in this theme so that my changes will not be lost in case of theme up-gradations?'; ?></a>
      <div class='faq' style='display: none;' id='faq_13'>
        <?php echo 'Ans: Yes, you can add your custom CSS in this theme. We have created a new file \'customization.css\' for you in this theme, which enables you to add your customized changes for your website. You can write your CSS code over here and make your site look just the way you want it to. Also, It will not be lost in case of theme upgradation.You can find this file by following the below steps :<br>
        1. Go to the \'Theme Editor\' section of this plugin from the admin panel.<br>
        2. Now choose \'custom.css\' from the \'editing file\' dropdown. You may add the changes here which you want to do for your website.' ?>
      </div>
    </li>
    <li>
      <a href="javascript:void(0);" onClick="faq_show('faq_14');"><?php echo 'Does this plugin make every plugin work like an app?'; ?></a>
      <div class='faq' style='display: none;' id='faq_14'>
        <?php echo 'Ans: This plugin will work in a way the core responsive functionality of the other plugins works. This plugin is compatible with every plugin as it uses the core responsive built of every plugin.' ?>
      </div>
    </li>
  </ul>
</div>


<script type="text/javascript">
  function faq_show(id) {
    if ($(id)) {
      if ($(id).style.display == 'block') {
        $(id).style.display = 'none';
      } else {
        $(id).style.display = 'block';
      }
    }
  }
<?php if( $this->faq_id ): ?>
    faq_show('<?php echo $this->faq_id; ?>');
<?php endif; ?>
</script>
