<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<script type="text/javascript">

  var SortablesInstance;

  window.addEvent('domready', function () {
    $$('.item_label').addEvents({
      mouseover: showPreview,
      mouseout: showPreview
    });
  });

  var showPreview = function (event) {
    try {
      element = $(event.target);
      element = element.getParent('.admin_menus_item').getElement('.item_url');
      if (event.type == 'mouseover') {
        element.setStyle('display', 'block');
      } else if (event.type == 'mouseout') {
        element.setStyle('display', 'none');
      }
    } catch (e) {
      //alert(e);
    }
  }


  window.addEvent('load', function () {
    SortablesInstance = new Sortables('menu_list', {
      clone: true,
      constrain: false,
      handle: '.item_label',
      onComplete: function (e) {
        reorder(e);
      }
    });
  });

  var reorder = function (e) {
    var menuitems = e.parentNode.childNodes;
    var ordering = {};
    var i = 1;
    for (var menuitem in menuitems)
    {
      var child_id = menuitems[menuitem].id;

      if ((child_id != undefined) && (child_id.substr(0, 5) == 'admin'))
      {
        ordering[child_id] = i;
        i++;
      }
    }
    ordering['menu'] = '<?php echo $this->selectedMenu->name; ?>';
    ordering['format'] = 'json';

    // Send request
    var url = '<?php echo $this->url(array('action' => 'order')) ?>';
    var request = new Request.JSON({
      'url': url,
      'method': 'POST',
      'data': ordering,
      onSuccess: function (responseJSON) {
      }
    });

    request.send();
  }

  function ignoreDrag()
  {
    event.stopPropagation();
    return false;
  }

</script>
<h2><?php echo $this->translate("Mobile / Tablet Plugin - Progressive Web Apps") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='seaocore_admin_tabs tabs clr'>
    <?php
    // Render the menu
    //->setUlClass()
    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<h3>
  <?php echo $this->translate('Menu Editor') ?>
</h3>
<p>
  Use this area to manage the various navigation menus that appear on your PWA. This section allows you to manage and edit the existing menus on your site. To add new menu or delete existing menu, please navigate to Appearance > Menu Editor section in the admin panel. <br><br>

  When you select the menu you wish to edit, a list of the menu items it contains will be shown. You can also add new menu items here, however, they would not appear for your main website or be available in the Appearance > Menu Editor section.<br>
  Note: You cannot the change the sequence of menu items using this section. Menu items will appear in the same sequence in which they are appearing in the website.

</p>

<br />

<div class="admin_menus_filter">
  <form action="<?php echo $this->url() ?>" method="get">
    <b><?php echo $this->translate("Editing:") ?></b>
    <?php echo $this->formSelect('name', $this->selectedMenu->name, array('onchange' => '$(this).getParent(\'form\').submit();', 'style' => 'display:inline-block'), $this->menuList) ?>
  </form>
</div>

<br />

<div class="admin_menus_options">
  <?php echo $this->htmlLink(array('reset' => false, 'action' => 'create', 'name' => $this->selectedMenu->name), $this->translate('Add Item'), array('class' => 'buttonlink admin_menus_additem smoothbox')) ?>
</div>

<br />

<ul class="admin_menus_items" id='menu_list'>
  <?php foreach( $this->menuItems as $menuItem ): ?>
    <li class="admin_menus_item<?php if( (isset($menuItem->params['pwa-enabled']) && !$menuItem->params['pwa-enabled']) || (isset($menuItem->enabled) && !$menuItem->enabled) ) echo ' disabled' ?>" id="admin_menus_item_<?php echo $menuItem->name ?>">
      <span class="item_wrapper">
        <span class="item_options">
          <?php echo $this->htmlLink(array('reset' => false, 'action' => 'edit', 'name' => $menuItem->name), $this->translate('edit'), array('class' => 'smoothbox')) ?>
          <?php if( $menuItem->custom && strpos($menuItem->name, 'custom_') === 0 ): ?>
            | <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete', 'name' => $menuItem->name), $this->translate('delete'), array('class' => 'smoothbox')) ?>
          <?php endif; ?>
        </span>
        <span class="item_label">
          <?php echo $this->translate($menuItem->label) ?>
        </span>
        <span class="item_url">
          <?php
          $href = '';
          if( isset($menuItem->params['uri']) ) {
            echo $this->htmlLink($menuItem->params['uri'], $menuItem->params['uri']);
          } else if( !empty($menuItem->plugin) ) {
            echo '<a>(' . $this->translate('variable') . ')</a>';
          } else {
            echo $this->htmlLink($this->htmlLink()->url($menuItem->params), $this->htmlLink()->url($menuItem->params));
          }
          ?>
        </span>
      </span>
    </li>
  <?php endforeach; ?>
</ul>
