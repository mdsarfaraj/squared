<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: _formImagerainbowThemeColor.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sitepwa/externals/scripts/mooRainbow.js');
$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sitepwa/externals/styles/mooRainbow.css');
?>
<script type="text/javascript">
  window.addEvent('domready', function () {
    var theme_color_s = new MooRainbow('theme_color', {
      id: 'myDemo3',
      'startColor': hexcolorTonumbercolor("<?php echo $this->theme_color ?>"),
      'onChange': function (color) {
        $('theme_color').value = color.hex;
        $('theme_color').setStyle('backgroundColor', color.hex);
        theme_color_s.okButton.click();
      }
    });
  });
</script>

<?php
echo '
  <div id="theme_color-wrapper" class="form-wrapper">
    <div id="theme_color-label" class="form-label">
      <label for="theme_color" class="optional">
        ' . $this->translate('Theme Color') . '
      </label>
    </div>
    <div id="theme_color-element" class="form-element">
      <p class="description">' . $this->translate('Set the default theme color for the application... (Click on the rainbow below to choose your color.') . '</p>
      <input name="theme_color" id="theme_color" style="background-color:' . $this->theme_color . '" value=' . $this->theme_color . ' type="text">
    </div>
  </div>
'
?>
