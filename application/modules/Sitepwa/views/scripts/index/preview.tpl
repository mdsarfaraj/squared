<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: preview.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<div class="sitepwa-mobile-preivew-showcase-wapper">
  <div class="sitepwa-mobile-preivew-showcase-bg">
    <iframe width="292" height="525" frameborder="0" src="<?php echo $this->absoluteUrl($this->url(array(), 'default', true)) ?>?pwa=1">
    </iframe>
  </div>
</div>