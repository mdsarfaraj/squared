<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: offline.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style type="text/css">
  #global_content_simple {
    display: block;
  }
  #offline-errors {
    position: relative;
    margin: 10px;
  }
  #offline-errors .tip>span {
    width: 100%;
  }
</style>
<div class="_offline">
  <div id="offline-errors">
    <div id="offline-error-msg" class="tip">
      <span class="seaocore_box_sizing" style="text-align: center">
        <?php echo $this->translate("You're currently offline. Please check your internet connection."); ?>
      </span>
    </div>
  </div>
</div>
<script type="text/javascript">
  window.addEventListener('online', function () {
    if (window.location.href.indexOf('/pwa/offline')) {
      window.location.href = window.location.href.replace('/pwa/offline', '');
    } else {
      window.location.reload(false);
    }
  });
</script>