<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: _formImagerainbowBackgroundColor.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sitepwa/externals/scripts/mooRainbow.js');
$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sitepwa/externals/styles/mooRainbow.css');
?>
<script type="text/javascript">
  function hexcolorTonumbercolor(hexcolor) {
    var hexcolorAlphabets = "0123456789ABCDEF";
    var valueNumber = new Array(3);
    var j = 0;
    if (hexcolor.charAt(0) == "#")
      hexcolor = hexcolor.slice(1);
    hexcolor = hexcolor.toUpperCase();
    for (var i = 0; i < 6; i += 2) {
      valueNumber[j] = (hexcolorAlphabets.indexOf(hexcolor.charAt(i)) * 16) + hexcolorAlphabets.indexOf(hexcolor.charAt(i + 1));
      j++;
    }
    return(valueNumber);
  }

  window.addEvent('domready', function () {
    var background_color_s = new MooRainbow('background_color', {
      id: 'myDemo2',
      'startColor': hexcolorTonumbercolor("<?php echo $this->background_color ?>"),
      'onChange': function (color) {
        $('background_color').value = color.hex;
        $('background_color').setStyle('backgroundColor', color.hex);
        background_color_s.okButton.click();
      }
    });
  });
</script>

<?php
echo '
  <div id="background_color-wrapper" class="form-wrapper">
    <div id="background_color-label" class="form-label">
      <label for="background_color" class="optional">
        ' . $this->translate('Background Color') . '
      </label>
    </div>
    <div id="background_color-element" class="form-element">
      <p class="description">' . $this->translate('Set the background color of screen that will be displayed between launching the application and loading its contents. (Click on the rainbow below to choose your color.)') . '</p>
      <input name="background_color" id="background_color" style="background-color:' . $this->background_color . '" value=' . $this->background_color . ' type="text">
    </div>
  </div>
'
?>
