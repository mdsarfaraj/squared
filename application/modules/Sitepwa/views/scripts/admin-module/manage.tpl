<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: manage.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<h2><?php echo $this->translate("Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='seaocore_admin_tabs tabs clr'>
    <?php
    // Render the menu
    //->setUlClass()
    echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<h3>
  <?php echo $this->translate("Manage Modules") ?>
</h3>
<p>
  <?php echo $this->translate('Below, you can manage the modules to be displayed in mobiles and tablets.'); ?>
</p>
<br/>

<table class='admin_table' width="100%">
  <thead>
    <tr>
      <th><?php echo $this->translate("Module Name") ?></th>
      <th align="center"><?php echo $this->translate("Integrate") ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $this->modulesList as $item ): ?>
      <tr>
        <?php if( $item->name == 'sitereview' && Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitereview') && Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitereviewlistingtype') ): ?>
          <td><?php echo $this->translate("Reviews & Ratings - Multiple Listing Types Plugin"); ?></td> 
        <?php else: ?>
          <td><?php echo ucfirst($item->title); ?></td>
        <?php endif; ?>

        <!--        If module enable then display disable and vice-versa.-->
        <td class="admin_table_centered">
          <?php echo ( ($item->integrated) ? 'Yes' : $this->htmlLink(array('reset' => false, 'action' => 'integrate', 'name' => $item->name, 'integrated' => $item->integrated), 'Integrate')) ?>

        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>