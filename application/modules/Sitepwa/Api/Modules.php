<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Modules.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_Api_Modules extends Core_Api_Abstract
{

  public $_pagesTable = 'engine4_core_pages';
  public $_contentTable = 'engine4_sitepwa_content';

  public function onInstall()
  {
    $this->addDefault();
    $this->addModules();
  }

  public function addModules()
  {

    $modulestable = Engine_Api::_()->getDbtable('modules', 'sitepwa');
    $this->_enabledModuleNames = $modulestable->getEnabledModuleNames();
    foreach( $this->_enabledModuleNames as $moduleName ) {
      $this->addModuleStart($moduleName);
    }
  }

  public function preAddModule($moduleName)
  {
    if( in_array($moduleName, array('album', 'video', 'poll', 'user', 'core', 'music', 'forum', 'group', 'event', 'messages', 'activity', 'blog', 'birthday', 'advancedactivity', 'fields', 'payment')) ) {
      return;
    }
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->query("UPDATE `engine4_sitepwa_modules` SET `integrated` = '0' WHERE `name` LIKE  '$moduleName%';");

    $db->query("DELETE FROM `engine4_sitepwa_content` WHERE `name` LIKE  '%$moduleName%';");

  }

  public function addDefault()
  {
    $this->addDefaultPage();
  }

  public function addDefaultPage()
  {
    //pages 
    $this->addMainPages();
    $this->addCorePages();
    $this->addUserPages();
    $this->addSettingsPages();
  }

  public function addModuleStart($moduleName)
  {


    if( !in_array($moduleName, array('core', 'user', 'sitepwa')) ) {
      $this->preAddModule($moduleName);
    }
    $this->addModule($moduleName);

    $moduleDir = $this->inflictModule($moduleName);

    $sqlFile = APPLICATION_PATH . "/application/modules/Sitepwa/Modules/$moduleDir/settings/my.sql";
    if( file_exists($sqlFile) ) {
      $contents = file_get_contents($sqlFile);
      if( $contents ) {
        $db = Engine_Db_Table::getDefaultAdapter();
        foreach( Engine_Package_Utilities::sqlSplit($contents) as $sqlFragment ) {
          $db->query($sqlFragment);
        }
      }
    }
  }

  public function inflictModule($moduleName)
  {
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $moduleName)));
  }

  public function addModule($moduleName)
  {

    $moduleDir = $this->inflictModule($moduleName);
    $modulestable = Engine_Api::_()->getDbtable('modules', 'sitepwa');
    $file = APPLICATION_PATH . '/application/modules/Sitepwa/Modules/' . $moduleDir . '/settings/install.php';
    if( file_exists($file) ) {
      try {
        $pluginName = $moduleDir . "_Install_Sitepwa";
        $plugin = Engine_Api::_()->loadClass($pluginName);
      } catch( Exception $e ) {
        //Silence exceptions
        //continue;
      }
      if( method_exists($plugin, 'onIntegrated') ) {
        $plugin->onIntegrated();
      }
    } else {
      $addModuleNamePages = 'add' . $moduleDir . 'Pages';
      if( method_exists($this, $addModuleNamePages) ) {
        $this->$addModuleNamePages();
      }
    }

    $existModuleName = $modulestable->select()->from($modulestable->info('name'), array('name'))->where('name =?', $moduleName)->query()->fetchColumn();
    if( $existModuleName ) {
      $modulestable->update(array(
        'integrated' => 1
        ), array('name = ?' => $moduleName));
    } else {
      $modulestable->insert(array(
        'name' => $moduleName,
        'integrated' => 1
      ));
    }
  }

  public function hasIntegratePage($pageId)
  {
    // Get all content
    $contentTable = Engine_Api::_()->getDbtable('content', 'sitepwa');
    $select = $contentTable->select()
      ->where('page_id = ?', $pageId)
      ->limit(1);
    return !empty($contentTable->fetchRow($select));
  }

  public function addMainPages()
  {
    $this->addSiteHeader();
    $this->addSiteFooter();
    $this->addHomePage();
    $this->addUserHomePage();
    $this->addUserProfilePage();
  }

  public function addCorePages()
  {

    //Contact,privacy & terms of services pages.
//    $this->addContactPage();
//    $this->addPrivacyPage();
//    $this->addTermsOfServicePage();
//    $this->addMemberBrowsePage();
//    $this->addNotificationPage();
//    $this->addGenericPage('core_error_requireuser', 'Sign-in Required', 'Sign-in Required Page', '');
//    $this->addGenericPage('core_search_index', 'Search', 'Search Page', '');
  }

  public function addUserPages()
  {
    //Sign up & sign in pages
    $this->addGenericPage('user_auth_login', 'Sign-in', 'Sign-in Page', 'This is the site sign-in page.');
    $this->addGenericPage('user_signup_index', 'Sign-up', 'Sign-up Page', 'This is the site sign-up page.');
    $this->addGenericPage('user_auth_forgot', 'Reset Password', 'Forgot Password Page', 'This is the site forgot password page.');
  }

  public function addSettingsPages()
  {
    //Setting pages
    $this->addGeneral();
    $this->addPrivacy();
    $this->addNetworks();
    $this->addNotifications();
    $this->addChangePassword();
    $this->addDeleteAccount();
  }

  public function addBlogPages()
  {
    //Blog pages
    $this->addBlogUserProfileContent();
    $this->addBlogListPage();
    $this->addBlogViewPage();
    $this->addBlogBrowsePage();
    $this->addBlogCreatePage();
    $this->addBlogManagePage();
  }

  public function addAlbumPages()
  {
    //Album pages
    $this->addAlbumUserProfileContent();
    $this->addAlbumPhotoViewPage();
    $this->addAlbumViewPage();
    $this->addAlbumBrowsePage();
    $this->addAlbumCreatePage();
    $this->addAlbumManagePage();
    $this->addAlbumPhotoPage();
  }

  public function addEventPages()
  {
    //Events pages
    $this->addEventUserProfileContent();
    $this->addEventPhotoViewPage();
    $this->addEventViewPage();
    $this->addEventBrowsePage();
    $this->addEventCreatePage();
    $this->addEventManagePage();

    //UPDATE - Menus change   
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->query("UPDATE `engine4_sitepwa_menuitems` SET `plugin` = 'Sitemobile_Plugin_eventMenus' WHERE `engine4_sitepwa_menuitems`.`name` = 'event_profile_invite'");
  }

  public function addGroupPages()
  {
    //Group pages
    $this->addGroupUserProfileContent();
    $this->addGroupPhotoViewPage();
    $this->addGroupViewPage();
    $this->addGroupBrowsePage();
    $this->addGroupCreatePage();
    $this->addGroupManagePage();
  }

  public function addVideoPages()
  {
    //Videos pages
    $this->addVideoUserProfileContent();
    $this->addVideoViewPage();
    $this->addVideoBrowsePage();
    $this->addVideoCreatePage();
    $this->addVideoManagePage();
  }

  public function addMessagesPages()
  {
    //Message Pages
    $this->addMessageInboxPage();
    $this->addMessageOutboxPage();
    $this->addMessageComposePage();
    $this->addMessageViewPage();
    $this->addMessageSearchPage();
  }

  public function addBirthdayPages()
  {
    //Birthday Pages
    $this->addBirthdayPage();

    //UPDATE - order change   
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->query("UPDATE `engine4_sitepwa_menuitems` SET `order` = '10' WHERE `engine4_sitepwa_menuitems`.`name` = 'event_main_birthday'");
  }

  public function addMusicPages()
  {
    // Music Pages
    $this->addMusicUserProfileContent();
    $this->addMusicBrowsePage();
    $this->addMusicCreatePage();
    $this->addMusicViewPage();
    $this->addMusicManagePage();
  }

  protected function addPollPages()
  {
    //polls pages
    $this->addPollUserProfileContent();
    $this->addPollBrowsePage();
    $this->addPollViewPage();
    $this->addPollCreatePage();
    $this->addPollManagePage();
  }

  protected function addForumPages()
  {
    $this->addForumUserProfileContent();
    $this->addForumIndexPage();
    $this->addForumViewPage();

    $this->addGenericPage('forum_topic_view', 'Forum Topic View', 'Forum Topic View Page', 'This is the forum topic view page.');
    $this->addGenericPage('forum_forum_topic-create', 'Post Topic', 'Forum Topic Create Page', 'This is the forum topic create page.');
  }

  //Get page id of pages from "sitepwa_pages" table.
  public function getPageId($page_name)
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $db->select()
      ->from($this->_pagesTable, 'page_id')
      ->where('name = ?', $page_name)
      ->limit(1)
      ->query()
      ->fetchColumn();
    return $page_id;
  }

  public function addGenericPage($page)
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId($page);
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'module' => 'core'
      ));
    }

    return $page_id;
  }

  //Mobile pages.
  public function addSiteHeader()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('header');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.startup',
        'parent_content_id' => $container_id,
        'order' => 1,
        'params' => '',
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.header',
        'parent_content_id' => $container_id,
        'order' => 4,
        'params' => '',
        'module' => 'sitepwa'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.menu-main',
        'parent_content_id' => $container_id,
        'order' => 6,
        'params' => '',
        'module' => 'sitepwa'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'user.login-or-signup-popup',
        'parent_content_id' => $container_id,
        'order' => 3,
        'params' => '',
        'module' => 'sitepwa'
      ));
    }
  }

  public function addSiteFooter()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('footer');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'core.menu-social-sites',
        'parent_content_id' => $container_id,
        'order' => 2,
        'module' => 'core'
      ));
    }
  }

  public function addHomePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('core_index_index');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'middle',
        'parent_content_id' => $container_id,
        'order' => 2,
        'params' => '',
      ));
      $middle_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'user.login-or-signup',
        'parent_content_id' => $middle_id,
        'order' => 3,
        'params' => '',
        'module' => 'user'
      ));
    }
  }

  public function addUserHomePage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('user_index_home');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

//      // containers
//      $db->insert($this->_contentTable, array(
//        'page_id' => $page_id,
//        'type' => 'container',
//        'name' => 'main',
//        'parent_content_id' => null,
//        'order' => 1,
//        'params' => '',
//      ));
//      $container_id = $db->lastInsertId($this->_contentTable);
//
//      $db->insert($this->_contentTable, array(
//        'page_id' => $page_id,
//        'type' => 'container',
//        'name' => 'middle',
//        'parent_content_id' => $container_id,
//        'order' => 2,
//        'params' => '',
//      ));
//      $middle_id = $db->lastInsertId($this->_contentTable);
    }
  }

  public function addUserProfilePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('user_profile_index');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
    }
  }

  //Blog pages
  public function addBlogManagePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('blog_index_manage');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'blog_index_manage',
        'displayname' => 'Blog Manage Page',
        'title' => 'My Entries',
        'description' => 'This page lists a user\'s blog entries.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));


      // Insert Advance search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));
      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addBlogCreatePage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('blog_index_create');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'blog_index_create',
        'displayname' => 'Blog Create Page',
        'title' => 'Write New Entry',
        'description' => 'This page is the blog create page.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addBlogBrowsePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page_id = $this->getPageId('blog_index_index');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'blog_index_index',
        'displayname' => 'Blog Browse Page',
        'title' => 'Blog Browse',
        'description' => 'This page lists blog entries.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert Advance search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 3,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addBlogListPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('blog_index_list');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'blog_index_list',
        'displayname' => 'Blog List Page',
        'title' => 'Blog List',
        'description' => 'This page lists a member\'s blog entries.',
        'provides' => 'subject=user',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addBlogViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('blog_index_view');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'blog_index_view',
        'displayname' => 'Blog View Page',
        'title' => 'Blog View',
        'description' => 'This page displays a blog entry.',
        'provides' => 'subject=blog',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-headingtitle',
        'parent_content_id' => $middle_id,
        'order' => 1,
        'params' => '{"title":"","nonloggedin":"1","loggedin":"1","nomobile":"0","notablet":"0","name":"sitepwa.sitepwa-headingtitle"}',
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 3,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addBlogUserProfileContent()
  {


    // install content areas

    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    // profile page
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'user_profile_index')
      ->limit(1);
    $page_id = $select->query()->fetchObject()->page_id;


    // sitepwa.blog-profile-blogs
    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from($this->_contentTable)
      ->where('page_id = ?', $page_id)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'sitepwa.blog-profile-blogs')
    ;
    $info = $select->query()->fetch();

    if( empty($info) ) {

      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('page_id = ?', $page_id)
        ->where('type = ?', 'container')
        ->limit(1);
      $container_id = $select->query()->fetchObject()->content_id;

      // middle_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('parent_content_id = ?', $container_id)
        ->where('type = ?', 'container')
        ->where('name = ?', 'middle')
        ->limit(1);
      $middle_id = $select->query()->fetchObject()->content_id;

      // tab_id (tab container) may not always be there
      $select
        ->reset('where')
        ->where('type = ?', 'widget')
        ->where('name = ?', 'sitepwa.container-tabs-columns')
        ->where('page_id = ?', $page_id)
        ->limit(1);
      $tab_id = $select->query()->fetchObject();
      if( $tab_id && @$tab_id->content_id ) {
        $tab_id = $tab_id->content_id;
      } else {
        $tab_id = null;
      }

      // tab on profile
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.blog-profile-blogs',
        'parent_content_id' => ($tab_id ? $tab_id : $middle_id),
        'order' => 10,
        'params' => '{"title":"Blogs","titleCount":true}',
        'module' => 'blog'
      ));
    }
  }

  //Album pages

  public function addAlbumPhotoPage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_index_photos');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_index_photos',
        'displayname' => 'Album Browse Photo Page',
        'title' => 'Browse Photos',
        'description' => 'This page lists photo entries.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));


      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.album-browse-photos',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addAlbumManagePage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_index_manage');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_index_manage',
        'displayname' => 'Album Manage Page',
        'title' => 'My Albums',
        'description' => 'This page lists album a user\'s albums.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));
      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addAlbumCreatePage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_index_upload');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_index_upload',
        'displayname' => 'Album Create Page',
        'title' => 'Add New Photos',
        'description' => 'This page is the album create page.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      //WE WILL NOT ADD THE NAVIGATION TAB ON APP AND TABLET APP.
      if( $this->_pagesTable == 'engine4_sitepwa_pages' || $this->_pagesTable == 'engine4_sitepwa_tablet_pages' )
      // Insert menu
        $db->insert($this->_contentTable, array(
          'type' => 'widget',
          'name' => 'sitepwa.sitepwa-navigation',
          'page_id' => $page_id,
          'parent_content_id' => $main_middle_id,
          'order' => 1,
          'module' => 'sitepwa'
        ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addAlbumPhotoViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_photo_view');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_photo_view',
        'displayname' => 'Album Photo View Page',
        'title' => 'Album Photo View',
        'description' => 'This page displays an album\'s photo.',
        'provides' => 'subject=album_photo',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addAlbumViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_album_view');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_album_view',
        'displayname' => 'Album View Page',
        'title' => 'Album View',
        'description' => 'This page displays an album\'s photos.',
        'provides' => 'subject=album',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-headingtitle',
        'parent_content_id' => $middle_id,
        'order' => 1,
        'params' => '{"title":"","nonloggedin":"1","loggedin":"1","nomobile":"0","notablet":"0","name":"sitepwa.sitepwa-headingtitle"}',
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 3,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addAlbumBrowsePage()
  {

    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('album_index_browse');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'album_index_browse',
        'displayname' => 'Album Browse Page',
        'title' => 'Album Browse',
        'description' => 'This page lists album entries.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

//      
      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addAlbumUserProfileContent()
  {
    // install content areas
    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    // profile page
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'user_profile_index')
      ->limit(1);
    $page_id = $select->query()->fetchObject()->page_id;


    // album.profile-albums
    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from($this->_contentTable)
      ->where('page_id = ?', $page_id)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'sitepwa.album-profile-albums')
    ;

    $info = $select->query()->fetch();

    if( empty($info) ) {
      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('page_id = ?', $page_id)
        ->where('type = ?', 'container')
        ->limit(1);
      $container_id = $select->query()->fetchObject()->content_id;

      // middle_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('parent_content_id = ?', $container_id)
        ->where('type = ?', 'container')
        ->where('name = ?', 'middle')
        ->limit(1);
      $middle_id = $select->query()->fetchObject()->content_id;

      // tab_id (tab container) may not always be there
      $select
        ->reset('where')
        ->where('type = ?', 'widget')
        ->where('name = ?', 'sitepwa.container-tabs-columns')
        ->where('page_id = ?', $page_id)
        ->limit(1);
      $tab_id = $select->query()->fetchObject();
      if( $tab_id && @$tab_id->content_id ) {
        $tab_id = $tab_id->content_id;
      } else {
        $tab_id = null;
      }

      // tab on profile
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.album-profile-albums',
        'parent_content_id' => ($tab_id ? $tab_id : $middle_id),
        'order' => 9,
        'params' => '{"title":"Albums","titleCount":true}',
        'module' => 'album'
      ));

      return $this;
    }
  }

  //Event pages
  public function addEventManagePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('event_index_manage');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'event_index_manage',
        'displayname' => 'Event Manage Page',
        'title' => 'My Events',
        'description' => 'This page lists a user\'s events.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));


      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));
      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addEventCreatePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('event_index_create');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'event_index_create',
        'displayname' => 'Event Create Page',
        'title' => 'Event Create',
        'description' => 'This page allows users to create events.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addEventViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();
    // profile page
    $page_id = $this->getPageId('event_profile_index');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {


      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'middle',
        'parent_content_id' => $container_id,
        'order' => 2,
        'params' => '',
      ));
      $middle_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.profile-photo-status',
        'parent_content_id' => $middle_id,
        'order' => 3,
        'params' => '',
        'module' => 'sitepwa'
      ));


      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.event-profile-info',
        'parent_content_id' => $middle_id,
        'order' => 5,
        'params' => '',
        'module' => 'event'
      ));
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.event-profile-rsvp',
        'parent_content_id' => $middle_id,
        'order' => 6,
        'params' => '',
        'module' => 'event'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.container-tabs-columns',
        'parent_content_id' => $middle_id,
        'order' => 7,
        'params' => '{"max":6}',
        'module' => 'sitepwa'
      ));
      $tab_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advfeed',
        'parent_content_id' => $tab_id,
        'order' => 8,
        'params' => '{"title":"Updates"}',
        'module' => 'advancedactivity'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.event-profile-members',
        'parent_content_id' => $tab_id,
        'order' => 10,
        'params' => '{"title":"Guests","titleCount":true}',
        'module' => 'event'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.event-profile-photos',
        'parent_content_id' => $tab_id,
        'order' => 11,
        'params' => '{"title":"Photos","titleCount":true}',
        'module' => 'event'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.event-profile-discussions',
        'parent_content_id' => $tab_id,
        'order' => 11,
        'params' => '{"title":"Discussions","titleCount":true}',
        'module' => 'event'
      ));
    }
  }

  public function addEventPhotoViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('event_photo_view');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'event_photo_view',
        'displayname' => 'Event Photo View Page',
        'title' => 'Event Photo View',
        'description' => 'This page displays an event\'s photo.',
        'provides' => 'subject=event_photo',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addEventBrowsePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('event_index_browse');

    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'event_index_browse',
        'displayname' => 'Event Browse Page',
        'title' => 'Event Browse',
        'description' => 'This page lists events.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));
      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addEventUserProfileContent()
  {
    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    $page_id = $this->getPageId('user_profile_index');

    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $hasProfileEvents = $select
      ->from($this->_contentTable, new Zend_Db_Expr('TRUE'))
      ->where('page_id = ?', $page_id)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'sitepwa.event-profile-events')
      ->query()
      ->fetchColumn()
    ;

    // Add it
    if( !$hasProfileEvents ) {

      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $container_id = $select
        ->from($this->_contentTable, 'content_id')
        ->where('page_id = ?', $page_id)
        ->where('type = ?', 'container')
        ->limit(1)
        ->query()
        ->fetchColumn()
      ;

      // middle_id (will always be there)
      $select = new Zend_Db_Select($db);
      $middle_id = $select
        ->from($this->_contentTable, 'content_id')
        ->where('parent_content_id = ?', $container_id)
        ->where('type = ?', 'container')
        ->where('name = ?', 'middle')
        ->limit(1)
        ->query()
        ->fetchColumn()
      ;

      // tab_id (tab container) may not always be there
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable, 'content_id')
        ->where('type = ?', 'widget')
        ->where('name = ?', 'sitepwa.container-tabs-columns')
        ->where('page_id = ?', $page_id)
        ->limit(1);
      $tab_id = $select->query()->fetchObject();
      if( $tab_id && @$tab_id->content_id ) {
        $tab_id = $tab_id->content_id;
      } else {
        $tab_id = $middle_id;
      }

      // insert
      if( $tab_id ) {
        $db->insert($this->_contentTable, array(
          'page_id' => $page_id,
          'type' => 'widget',
          'name' => 'sitepwa.event-profile-events',
          'parent_content_id' => $tab_id,
          'order' => 14,
          'params' => '{"title":"Events","titleCount":true}',
          'module' => 'event'
        ));
      }
    }
  }

  public function addBirthdayPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('birthday_index_view');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'birthday_index_view',
        'displayname' => 'Birthday Page',
        'title' => 'Birthdays',
        'description' => 'This page lists friends birthday.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  //Group pages
  public function addGroupManagePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('group_index_manage');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'group_index_manage',
        'displayname' => 'Group Manage Page',
        'title' => 'My Groups',
        'description' => 'This page lists a user\'s groups.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));


      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addGroupCreatePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('group_index_create');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'group_index_create',
        'displayname' => 'Group Create Page',
        'title' => 'Group Create',
        'description' => 'This page allows users to create groups.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      //WE WILL NOT ADD THE NAVIGATION TAB ON APP AND TABLET APP.
      if( $this->_pagesTable == 'engine4_sitepwa_pages' || $this->_pagesTable == 'engine4_sitepwa_tablet_pages' )
      // Insert menu
        $db->insert($this->_contentTable, array(
          'type' => 'widget',
          'name' => 'sitepwa.sitepwa-navigation',
          'page_id' => $page_id,
          'parent_content_id' => $main_middle_id,
          'order' => 1,
          'module' => 'sitepwa'
        ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addGroupUserProfileContent()
  {
    //
    // install content areas
    //
  $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    // profile page
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'user_profile_index')
      ->limit(1);
    $page_id = $select->query()->fetchObject()->page_id;

    // sitepwa.group-profile-groups
    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from($this->_contentTable)
      ->where('page_id = ?', $page_id)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'sitepwa.group-profile-groups')
    ;
    $info = $select->query()->fetch();

    if( empty($info) ) {

      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('page_id = ?', $page_id)
        ->where('type = ?', 'container')
        ->limit(1);
      $container_id = $select->query()->fetchObject()->content_id;

      // middle_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('parent_content_id = ?', $container_id)
        ->where('type = ?', 'container')
        ->where('name = ?', 'middle')
        ->limit(1);
      $middle_id = $select->query()->fetchObject()->content_id;

      // tab_id (tab container) may not always be there
      $select
        ->reset('where')
        ->where('type = ?', 'widget')
        ->where('name = ?', 'sitepwa.container-tabs-columns')
        ->where('page_id = ?', $page_id)
        ->limit(1);
      $tab_id = $select->query()->fetchObject();
      if( $tab_id && @$tab_id->content_id ) {
        $tab_id = $tab_id->content_id;
      } else {
        $tab_id = $middle_id;
      }

      // tab on profile
      if( $tab_id ) {
        $db->insert($this->_contentTable, array(
          'page_id' => $page_id,
          'type' => 'widget',
          'name' => 'sitepwa.group-profile-groups',
          'parent_content_id' => $tab_id,
          'order' => 12,
          'params' => '{"title":"Groups","titleCount":true}',
          'module' => 'group'
        ));
      }
    }
  }

  public function addGroupPhotoViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('group_photo_view');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'group_photo_view',
        'displayname' => 'Group Photo View Page',
        'title' => 'Group Photo View',
        'description' => 'This page displays an group\'s photo.',
        'provides' => 'subject=group_photo',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  public function addGroupViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    $select
      ->from('engine4_core_modules')
      ->where('name = ?', 'group')
      ->limit(1);
    ;
    $group_module = $select->query()->fetch();

    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'group_profile_index')
      ->limit(1);
    ;
    $info = $select->query()->fetch();

    if( empty($info) && !empty($group_module) ) {
      $db->insert($this->_pagesTable, array(
        'name' => 'group_profile_index',
        'displayname' => 'Group Profile',
        'title' => 'Group Profile',
        'description' => 'This is the mobile verison of a group profile.',
        'custom' => 0
      ));
      $page_id = $db->lastInsertId($this->_pagesTable);

      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'middle',
        'parent_content_id' => $container_id,
        'order' => 2,
        'params' => '',
      ));
      $middle_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.profile-photo-status',
        'parent_content_id' => $middle_id,
        'order' => 3,
        'params' => '',
        'module' => 'sitepwa'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-options',
        'parent_content_id' => $middle_id,
        'order' => 4,
        'params' => '',
        'module' => 'group'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-info',
        'parent_content_id' => $middle_id,
        'order' => 5,
        'params' => '',
        'module' => 'group'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.container-tabs-columns',
        'parent_content_id' => $middle_id,
        'order' => 6,
        'params' => '{"max":6}',
        'module' => 'sitepwa'
      ));
      $tab_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advfeed',
        'parent_content_id' => $tab_id,
        'order' => 7,
        'params' => '{"title":"Updates"}',
        'module' => 'advancedactivity'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-members',
        'parent_content_id' => $tab_id,
        'order' => 8,
        'params' => '{"title":"Members","titleCount":true}',
        'module' => 'group'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-photos',
        'parent_content_id' => $tab_id,
        'order' => 9,
        'params' => '{"title":"Photos","titleCount":true}',
        'module' => 'group'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-discussions',
        'parent_content_id' => $tab_id,
        'order' => 10,
        'params' => '{"title":"Discussions","titleCount":true}',
        'module' => 'group'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.group-profile-events',
        'parent_content_id' => $tab_id,
        'order' => 11,
        'params' => '{"title":"Events","titleCount":true}',
        'module' => 'group'
      ));
    }
  }

  public function addGroupBrowsePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('group_index_browse');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'group_index_browse',
        'displayname' => 'Group Browse Page',
        'title' => 'Group Browse',
        'description' => 'This page lists groups.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));


      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  //Videos pages
  public function addVideoManagePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('video_index_manage');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'video_index_manage',
        'displayname' => 'Video Manage Page',
        'title' => 'My Videos',
        'description' => 'This page lists a user\'s videos.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addVideoCreatePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // create page
    $page_id = $this->getPageId('video_index_create');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'video_index_create',
        'displayname' => 'Video Create Page',
        'title' => 'Video Create',
        'description' => 'This page allows video to be added.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addVideoBrowsePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('video_index_browse');
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'video_index_browse',
        'displayname' => 'Video Browse Page',
        'title' => 'Video Browse',
        'description' => 'This page lists videos.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();


      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 3,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 4,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addVideoUserProfileContent()
  {
    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);


    // profile page
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'user_profile_index')
      ->limit(1);
    $page_id = $select->query()->fetchObject()->page_id;

    if( $page_id ) {
      // video.profile-videos
      // Check if it's already been placed
      $select = new Zend_Db_Select($db);
      $select
        ->from($this->_contentTable)
        ->where('page_id = ?', $page_id)
        ->where('type = ?', 'widget')
        ->where('name = ?', 'sitepwa.video-profile-videos')
      ;
      $info = $select->query()->fetch();

      if( empty($info) ) {

        // container_id (will always be there)
        $select = new Zend_Db_Select($db);
        $select
          ->from($this->_contentTable)
          ->where('page_id = ?', $page_id)
          ->where('type = ?', 'container')
          ->limit(1);
        $container_id = $select->query()->fetchObject()->content_id;

        // middle_id (will always be there)
        $select = new Zend_Db_Select($db);
        $select
          ->from($this->_contentTable)
          ->where('parent_content_id = ?', $container_id)
          ->where('type = ?', 'container')
          ->where('name = ?', 'middle')
          ->limit(1);
        $middle_id = $select->query()->fetchObject()->content_id;

        // tab_id (tab container) may not always be there
        $select
          ->reset('where')
          ->where('type = ?', 'widget')
          ->where('name = ?', 'sitepwa.container-tabs-columns')
          ->where('page_id = ?', $page_id)
          ->limit(1);
        $tab_id = $select->query()->fetchObject();
        if( $tab_id && @$tab_id->content_id ) {
          $tab_id = $tab_id->content_id;
        } else {
          $tab_id = null;
        }

        // tab on profile
        $db->insert($this->_contentTable, array(
          'page_id' => $page_id,
          'type' => 'widget',
          'name' => 'sitepwa.video-profile-videos',
          'parent_content_id' => ($tab_id ? $tab_id : $middle_id),
          'order' => 18,
          'params' => '{"title":"Videos","titleCount":true}',
          'module' => 'video'
        ));
      }
    }
  }

  public function addVideoViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();
    $select = new Zend_Db_Select($db);

    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from($this->_pagesTable)
      ->where('name = ?', 'video_index_view')
      ->limit(1);
    ;
    $info = $select->query()->fetch();

    if( empty($info) ) {
      $db->insert($this->_pagesTable, array(
        'name' => 'video_index_view',
        'displayname' => 'Video View Page',
        'title' => 'View Video',
        'description' => 'This is the view page for a video.',
        'custom' => 0,
        'provides' => 'subject=video',
      ));
      $page_id = $db->lastInsertId($this->_pagesTable);

      // containers
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'main',
        'parent_content_id' => null,
        'order' => 1,
        'params' => '',
      ));
      $container_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'container',
        'name' => 'middle',
        'parent_content_id' => $container_id,
        'order' => 3,
        'params' => '',
      ));
      $middle_id = $db->lastInsertId($this->_contentTable);

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-headingtitle',
        'parent_content_id' => $middle_id,
        'order' => 1,
        'params' => '{"title":"","nonloggedin":"1","loggedin":"1","nomobile":"0","notablet":"0","name":"sitepwa.sitepwa-headingtitle"}',
        'module' => 'sitepwa'
      ));

      // middle column content
      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'core.content',
        'parent_content_id' => $middle_id,
        'order' => 1,
        'params' => '',
        'module' => 'sitepwa'
      ));

      $db->insert($this->_contentTable, array(
        'page_id' => $page_id,
        'type' => 'widget',
        'name' => 'sitepwa.comments',
        'parent_content_id' => $middle_id,
        'order' => 2,
        'params' => '',
        'module' => 'sitepwa'
      ));
    }
  }

  //Message Page
  public function addMessageInboxPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page = 'messages_messages_inbox';
    $displayname = 'Messages Inbox Page';
    $title = 'Inbox';
    $description = '';

    // check page
    $page_id = $this->getPageId($page);
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => $page,
        'displayname' => $displayname,
        'title' => $title,
        'description' => $description,
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Extra
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addMessageOutboxPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page = 'messages_messages_outbox';
    $displayname = 'Messages Outbox Page';
    $title = 'Inbox';
    $description = '';

    // check page
    $page_id = $this->getPageId($page);
    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => $page,
        'displayname' => $displayname,
        'title' => $title,
        'description' => $description,
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Extra
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addMessageViewPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page = 'messages_messages_view';
    $displayname = 'Messages View Page';
    $title = 'My Message';
    $description = '';

    // check page
    $page_id = $db->select()
      ->from($this->_pagesTable, 'page_id')
      ->where('name = ?', $page)
      ->limit(1)
      ->query()
      ->fetchColumn();

    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => $page,
        'displayname' => $displayname,
        'title' => $title,
        'description' => $description,
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Extra
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
  }

  protected function addMessageSearchPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page = 'messages_messages_search';
    $displayname = 'Messages Search Page';
    $title = 'Search';
    $description = '';

    // check page
    $page_id = $db->select()
      ->from($this->_pagesTable, 'page_id')
      ->where('name = ?', $page)
      ->limit(1)
      ->query()
      ->fetchColumn();

    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => $page,
        'displayname' => $displayname,
        'title' => $title,
        'description' => $description,
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Extra
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addMessageComposePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    $page = 'messages_messages_compose';
    $displayname = 'Messages Compose Page';
    $title = 'Compose';
    $description = '';

    // check page
    $page_id = $db->select()
      ->from($this->_pagesTable, 'page_id')
      ->where('name = ?', $page)
      ->limit(1)
      ->query()
      ->fetchColumn();

    // insert if it doesn't exist yet
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => $page,
        'displayname' => $displayname,
        'title' => $title,
        'description' => $description,
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
//WE WILL NOT ADD THE NAVIGATION TAB ON APP AND TABLET APP.
      if( $this->_pagesTable == 'engine4_sitepwa_pages' || $this->_pagesTable == 'engine4_sitepwa_tablet_pages' ) {
        // Extra
        $db->insert($this->_contentTable, array(
          'type' => 'widget',
          'name' => 'sitepwa.sitepwa-navigation',
          'page_id' => $page_id,
          'parent_content_id' => $middle_id,
          'order' => 1,
          'module' => 'sitepwa'
        ));
      }
    }
  }

  //Privacy Page 
  public function addPrivacyPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('core_help_privacy');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'core_help_privacy',
        'displayname' => 'Privacy Page',
        'title' => 'Privacy Policy',
        'description' => 'This is the privacy policy page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  //Terms of service page
  public function addTermsOfServicePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page
    $page_id = $this->getPageId('core_help_terms');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'core_help_terms',
        'displayname' => 'Terms of Service Page',
        'title' => 'Terms of Service',
        'description' => 'This is the terms of service page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }

    return $this;
  }

  //Contact page
  public function addContactPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('core_help_contact');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'core_help_contact',
        'displayname' => 'Contact Page',
        'title' => 'Contact Us',
        'description' => 'This is the contact page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Startup Page
  public function addStartupPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('sitepwa_browse_startup');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'sitepwa_browse_startup',
        'displayname' => 'Startup Page',
        'title' => 'Startup Page',
        'description' => 'This is the Startup page',
        'provides' => 'no-viewer;no-subject',
        'layout' => 'default-simple',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.startup',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Dashboard page
  public function addDashboardPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('sitepwa_browse_browse');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'sitepwa_browse_browse',
        'displayname' => 'Dashboard Page',
        'title' => 'Dashboard',
        'description' => 'This is the Dashboard Page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Member page
  public function addMemberBrowsePage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('user_index_browse');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'user_index_browse',
        'displayname' => 'Member Browse Page',
        'title' => 'Member Browse',
        'description' => 'This is the Member Page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert Advance search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 3,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Member page
  public function addFriendsPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('user_index_friends');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'user_index_friends',
        'displayname' => 'Friends Page',
        'title' => 'Friends',
        'description' => 'This is the Friends Page',
        'provides' => 'no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
      // Insert Advance search
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-advancedsearch',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'params' => '{"search":"2","title":"","nomobile":"0","name":"sitepwa.sitepwa-advancedsearch"}',
        'order' => 2,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 3,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Notification & Request page
  public function addNotificationPage()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('activity_notifications_index');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {
      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'activity_notifications_index',
        'displayname' => 'Notification Page',
        'title' => 'Notification',
        'description' => 'This is the Notification Page',
        'provides' => 'no-viewer;no-subject',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
      ));
      $main_id = $db->lastInsertId();

      // Insert middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
        'order' => 2,
      ));
      $middle_id = $db->lastInsertId();

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));
    }
    return $this;
  }

  //Setting pages
  public function addGeneral()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('user_settings_general');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'user_settings_general',
        'displayname' => 'User General Settings Page',
        'title' => 'General',
        'description' => 'This page is the user general settings page.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addPrivacy()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('user_settings_privacy');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'user_settings_privacy',
        'displayname' => 'User Privacy Settings Page',
        'title' => 'Privacy',
        'description' => 'This page is the user privacy settings page.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }

  public function addNetworks()
  {
    $db = Engine_Db_Table::getDefaultAdapter();

    // profile page

    $page_id = $this->getPageId('user_settings_network');
    if( $page_id && !$this->hasIntegratePage($page_id) ) {

      // Insert page
      $db->insert($this->_pagesTable, array(
        'name' => 'user_settings_network',
        'displayname' => 'User Networks Settings Page',
        'title' => 'Networks',
        'description' => 'This page is the user networks settings page.',
        'custom' => 0,
      ));
      $page_id = $db->lastInsertId();

      // Insert main
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'main',
        'page_id' => $page_id,
        'order' => 1,
      ));
      $main_id = $db->lastInsertId();

      // Insert main-middle
      $db->insert($this->_contentTable, array(
        'type' => 'container',
        'name' => 'middle',
        'page_id' => $page_id,
        'parent_content_id' => $main_id,
      ));
      $main_middle_id = $db->lastInsertId();

      // Insert menu
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'sitepwa.sitepwa-navigation',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 1,
        'module' => 'sitepwa'
      ));

      // Insert content
      $db->insert($this->_contentTable, array(
        'type' => 'widget',
        'name' => 'core.content',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => 2,
        'module' => 'sitepwa'
      ));
    }
  }





}