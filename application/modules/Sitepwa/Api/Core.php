<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Core.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_Api_Core extends Core_Api_Abstract
{

  public function isPWApp()
  {
    $session = new Zend_Session_Namespace('sitepwa');
    return $this->hasEnabledPWAmode() && !empty($session->pwaApp);
  }

  public function hasEnabledPWAmode()
  {
    $session = new Zend_Session_Namespace('sitepwa');
    return $session->pwa;
  }

  public function isMobileDevice()
  {
    // No UA defined?
    if( !isset($_SERVER['HTTP_USER_AGENT']) ) {
      return false;
    }

    // Windows is (generally) not a mobile OS
    if( false !== stripos($_SERVER['HTTP_USER_AGENT'], 'windows') &&
      false === stripos($_SERVER['HTTP_USER_AGENT'], 'windows phone') ) {
      return false;
    }
//Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 720)
    // Sends a WAP profile header
    if( isset($_SERVER['HTTP_PROFILE']) ||
      isset($_SERVER['HTTP_X_WAP_PROFILE']) ) {
      return true;
    }

    // Accepts WAP as a valid type
    if( isset($_SERVER['HTTP_ACCEPT']) &&
      false !== stripos($_SERVER['HTTP_ACCEPT'], 'application/vnd.wap.xhtml+xml') ) {
      return true;
    }

    // Is Opera Mini
    if( isset($_SERVER['ALL_HTTP']) &&
      false !== stripos($_SERVER['ALL_HTTP'], 'OperaMini') ) {
      return true;
    }

    $userAgent = $_SERVER['HTTP_USER_AGENT'];

    if( preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', $userAgent) ) {
      return true;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
      'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird',
      'blac', 'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric',
      'hipt', 'inno', 'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c',
      'lg-d', 'lg-g', 'lge-', 'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi',
      'mot-', 'moto', 'mwbp', 'nec-', 'newt', 'noki', 'oper', 'palm', 'pana',
      'pant', 'phil', 'play', 'port', 'prox', 'qwap', 'sage', 'sams', 'sany',
      'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar', 'sie-', 'siem', 'smal',
      'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-', 'tosh', 'tsm-',
      'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp', 'wapr',
      'webc', 'winw', 'winw', 'xda ', 'xda-', 'BB10'
    );

    if( in_array($mobile_ua, $mobile_agents) ) {
      return true;
    }

    return false;
  }

  public function isTabletDevice()
  {
    // No UA defined?
    if( !isset($_SERVER['HTTP_USER_AGENT']) ) {
      return false;
    }
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    if( preg_match('/' . 'Nexus 5' . '/i', $userAgent) )
      return false;
    if( preg_match('/' . 'iPad|Nexus|GT-P1000|SGH-T849|SHW-M180S' . '/i', $userAgent) )
      return true;
    else
      return false;
  }

  /**
   * Get content listing according to requirement
   *
   * @param array $params
   * @return $items
   */
  public function getCoreSearchData($params = array())
  {
    $SearchTable = Engine_Api::_()->getDbtable('search', 'core');
    $searchTableName = $SearchTable->info('name');
    $items = array();
    $text = trim($params['text']);
    if( !empty($text) ) {
      $enable_item_types = Engine_Api::_()->getItemTypes();
      $select = $SearchTable->select()
        ->setIntegrityCheck(false)
        ->from($SearchTable->info('name'), array('type', 'id', 'description', 'keywords'));
      $select->where("(`title` LIKE  ? OR `description` LIKE  ? OR `keywords` LIKE  ? OR `hidden` LIKE  ?)", "%$text%");

      if( !empty($params['resource_type']) && $params['resource_type'] != 'all' ) {
        $select->where('type = ?', $params['resource_type']);
      } else {
        $select->where('type IN (?)', $enable_item_types);
      }

      $select->group('id');

      if( !empty($params['pagination']) ) {
        $items = Zend_Paginator::factory($select);
        $items->setItemCountPerPage($params['limit']);
      } else {
        $select = $select->limit($params['limit']);
        $items = $SearchTable->fetchAll($select);
      }
    }
    return $items;
  }

  /**
   * Get Widgetized Page Layout Value
   * @param $params
   */
  public function getWidgetizedPageLayoutValue($params = array())
  {
    //GET CORE CONTENT TABLE
    $tableNamePages = Engine_Api::_()->getDbtable('pages', 'core');
    $select = $tableNamePages->select()
      ->from($tableNamePages->info('name'), 'layout');

    if( isset($params['name']) ) {
      $select->where('name =?', $params['name']);
    }
    if( isset($params['page_id']) ) {
      $select->where('page_id =?', $params['page_id']);
    }
    $layout = $select->query()
      ->fetchColumn();
    return $layout;
  }

}