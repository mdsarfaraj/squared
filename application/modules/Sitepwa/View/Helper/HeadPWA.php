<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: HeadPWA.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_View_Helper_HeadPWA extends Zend_View_Helper_Placeholder_Container_Standalone
{

  protected $_homescreenIcon = array(
    '16' => array('x' => '16', 'y' => '16'),
    '32' => array('x' => '32', 'y' => '32'),
    '57' => array('x' => '57', 'y' => '57'),
    '72' => array('x' => '72', 'y' => '72'),
    '76' => array('x' => '76', 'y' => '76'),
    '96' => array('x' => '96', 'y' => '96'),
    '114' => array('x' => '114', 'y' => '114'),
    '120' => array('x' => '120', 'y' => '120'),
    '128' => array('x' => '128', 'y' => '128'),
    '144' => array('x' => '144', 'y' => '144'),
    '152' => array('x' => '152', 'y' => '152'),
    '158' => array('x' => '158', 'y' => '158'),
    '168' => array('x' => '168', 'y' => '168'),
    '180' => array('x' => '180', 'y' => '180'),
    '192' => array('x' => '192', 'y' => '192'),
  );

  //Create home screen icon's links and add those links on web page.
  public function headPWA()
  {
    $view = $this->view;
    $view->headLink(array(
      'rel' => 'manifest',
      'href' => $view->baseUrl('/public/sitepwa/manifest.json')
    ), Zend_View_Helper_Placeholder_Container_Abstract::PREPEND);

    $view->headScript()->prependFile(Zend_Registry::get('StaticBaseUrl') .
      'application/modules/Sitepwa/externals/scripts/core.js');
    
    $appIconPath = $view->serverUrl($view->baseUrl('/' . $view->settings('sitepwa.manifest.app_icon')));

    $view->headMeta()->appendName('apple-mobile-web-app-status-bar-style', 'default');
    $view->headMeta()->appendName('apple-mobile-web-app-capable', 'yes');
    $view->headMeta()->appendName('apple-mobile-web-app-title', $view->settings('sitepwa.manifest.name'));
    $view->headMeta()->appendName('msapplication-TileColor', $view->settings('sitepwa.manifest.theme.color'));
    $view->headMeta()->appendName('theme-color', $view->settings('sitepwa.manifest.theme.color'));
    $view->headMeta()->appendName('msapplication-TileImage', $appIconPath);
    $view->headMeta()->appendName('mobile-web-app-capable', 'yes');

//		$view->headMeta()->appendName('viewport', 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no');

    foreach( $this->_homescreenIcon as $k => $value ) {
      $key = $value['x'] . 'x' . $value['y'];
      if( in_array($k, array('16', '32', '128', '192')) ) {
        $view->headLink(array('rel' => 'icon', 'sizes' => $key, 'href' => $appIconPath));
      }
      if( in_array($k, array('57', '72', '76', '114', '120', '144', '152', '180')) ) {
        $view->headLink(array('rel' => 'apple-touch-icon', 'sizes' => $key, 'href' => $appIconPath));
      }
      if( $k == '128' ) {
        $view->headLink(array('rel' => 'apple-touch-icon-precomposed', 'sizes' => $key, 'href' => $appIconPath));
        $view->headLink(array('rel' => 'apple-touch-icon-precomposed', 'href' => $appIconPath));
      }
    }
    $view->headLink(array('rel' => 'apple-touch-startup-image', 'media' => '(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)', 'href' => $appIconPath));

    return $this;
  }

  protected function isIOSDevice()
  {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    return ((false !== stripos($useragent, 'iphone')) || (false !== stripos($useragent, 'ipod')) ||
      (false !== stripos($useragent, 'ipad')) );
  }

}