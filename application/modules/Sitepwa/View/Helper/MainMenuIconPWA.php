<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: MainMenuIconPWA.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_View_Helper_MainMenuIconPWA extends Zend_View_Helper_Navigation_Menu
{  //Create home screen icon's links and add those links on web page.

  /**
   * View helper entry point:
   * Retrieves helper and optionally sets container to operate on
   *
   * @param  Zend_Navigation_Container $container  [optional] container to
   *                                               operate on
   * @return Zend_View_Helper_Navigation_Menu      fluent interface,
   *                                               returns self
   */

  public function mainMenuIconPWA(Zend_Navigation_Container $container = null)
  {
    if( null !== $container ) {
      $this->setContainer($container);
    }
    return $this;
  }

  /**
   * Returns an HTML string containing an 'a' element for the given page if
   * the page's href is not empty, and a 'span' element if it is empty
   *
   * Overrides {@link Zend_View_Helper_Navigation_Abstract::htmlify()}.
   *
   * @param  Zend_Navigation_Page $page  page to generate HTML for
   * @return string                      HTML string for the given page
   */
  public function htmlify(Zend_Navigation_Page $page)
  {
    // get label and title for translating
    $label = $page->getLabel();
    $title = $page->getTitle();

    // translate label and title?
    if( $this->getUseTranslator() && $t = $this->getTranslator() ) {
      if( is_string($label) && !empty($label) ) {
        $label = $t->translate($label);
      }
      if( is_string($title) && !empty($title) ) {
        $title = $t->translate($title);
      }
    }
    $label = $this->view->escape($label);

    $icon = $page->get('icon') ?: 'fa-star';
    if( Zend_Uri::check($icon) ) {
      $icon = '<i class="background_icon" style="background-image: url(' . $icon . ')"></i>';
    } else {
      $icon = '<i class="fa ' . $icon . '"></i>';
    }
    $label = $icon . '<span>' . $label . '</span>';
    // get attribs for element
    $attribs = array(
      'id' => $page->getId(),
      'title' => $title,
    );

    if( false === $this->getAddPageClassToLi() ) {
      $attribs['class'] = $page->getClass();
    }

    // does page have a href?
    if( $href = $page->getHref() ) {
      $element = 'a';
      $attribs['href'] = $href;
      $attribs['target'] = $page->getTarget();
      $attribs['accesskey'] = $page->getAccessKey();
    } else {
      $element = 'span';
    }

    // Add custom HTML attributes
    $attribs = array_merge($attribs, $page->getCustomHtmlAttribs());

    return '<' . $element . $this->_htmlAttribs($attribs) . '>'
      . $label
      . '</' . $element . '>'
      . ($page->hasChildren() ? '<span class="collapse_icon"></span>' : '');
  }

}