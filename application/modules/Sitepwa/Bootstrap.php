<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Bootstrap.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepwa_Bootstrap extends Engine_Application_Bootstrap_Abstract
{

  public function __construct($application)
  {
    parent::__construct($application);
  }

  public function _bootstrap($resource = null)
  {
    $front = Zend_Controller_Front::getInstance();
    $front->registerPlugin(new Sitepwa_Plugin_Core);

    $headScript = new Zend_View_Helper_HeadScript();
	$headScript->appendFile('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')
				   ->appendFile(Zend_Registry::get('StaticBaseUrl') . 'application/modules/Intechcore/externals/scripts/core.js');
  }

}
