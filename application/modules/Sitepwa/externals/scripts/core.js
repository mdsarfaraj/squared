/* $Id: core.js SocialEngineAddOns $ */






(function () { // START NAMESPACE

  en4.sitepwa = {
    layout: {
      setTabs: function () {
        $(document).getElements('.layout_core_container_tabs .tabs_alt > ul ').each(function (el) {
          el.getElement('li ul').getElements('li').each(function (liEl) {
            var a = new Element('a', {
              onclick: liEl.get('onclick'),
              href: 'javascript:void(0);',
              html: liEl.get('html')
            });
            liEl.set('html', '');
            liEl.set('onclick', false);
            a.inject(liEl);
            liEl.inject(el);
          });
          el.getElement('li ul').getParent('li').destroy();
        });
      },
      setNavigationMenu: function () {
        $(document).getElements('.navigation').each(function (el) {
          if (el.getParent().hasClass('tabs') && el.getElement('li ul')) {
            el.getElement('li ul').getElements('li').each(function (liEl) {
              liEl.inject(el);
            });
            el.getElement('li ul').getParent('li').destroy();
          }
        });
        $(document).getElements('.layout_siteadvsearch_search_contents ul.advsearch').each(function (el) {
          if (el.getElement('li ul')) {
            el.getElement('li ul').getElements('li').each(function (liEl) {
              liEl.removeClass('fleft').inject(el);
            });
            el.getElement('li ul').getParent('li').destroy();
          }
        });
      },
      setLeftPannelMenu: function (type) {
        var pannelElement = $(document).getElement('body').addClass('global_sitepwa_left_panel panel-collapsed global_sitepwa_left_panel_' + type);
//        var button = $(document).getElement('.layout_sitepwa_menu_main .panel-toggle');
        var headerEl = pannelElement.getElement('.layout_page_header .layout_main .generic_layout_container');
        if (headerEl.hasClass('layout_sitepwa_startup')) {
          headerEl = headerEl.getNext();
        }
        new Element('div', {
          'class': 'sitepwa_main_menu_toggle fa header-panel-toggle'
        }).inject(headerEl, 'top');
        var navigationElement = pannelElement.getElement('.layout_sitepwa_menu_main .main_menu_navigation');
        setTimeout(function () {
          new Element('div', {
            'class': 'sitepwa_main_menu_body_backdrop'
          }).inject($(document.body));
        }, 2000);
        var hasSetOffset = 0;
        pannelElement.getElements('.sitepwa_main_menu_toggle').addEvent('click', function () {
          pannelElement.toggleClass('panel-collapsed').toggleClass('panel-open');
          hasSetOffset = window.getScrollTop();;
        });
        $(document).getElement('body').addEvent('click', function (event) {
          $el = $(event.target);
          if (!$el.getParent('.layout_sitepwa_menu_main') && !$el.getParent('.sitepwa_main_menu_toggle') && !$el.hasClass('sitepwa_main_menu_toggle')) {
            pannelElement.addClass('panel-collapsed').removeClass('panel-open');
          }

        });
        navigationElement.getElements('.sitepwa_main_menu_parent .collapse_icon').addEvent('click', function (event) {
          var hasOpen = $(event.target).getParent('.sitepwa_main_menu_parent').hasClass('submenu_expand');
          navigationElement.getElements('.sitepwa_main_menu_parent').removeClass('submenu_expand');
          if (!hasOpen) {
            $(event.target).getParent('.sitepwa_main_menu_parent').addClass('submenu_expand');
          }
        });

        function headerScrolling() {
          var scrollTop = window.getScrollTop();
          var diff = scrollTop - hasSetOffset ;
          diff = diff < 0 ? diff * -1 : diff;
          if (diff > 300 && pannelElement.hasClass('panel-open')) {
           pannelElement.addClass('panel-collapsed').removeClass('panel-open');
          }
        }
        window.addEvent('scroll', headerScrolling);
      }
    },
    loader: function (event) {
      new Element('div', {
        'class': 'sitepwa_loader',
      }).inject($(document.body));
    },
    onLaod: function () {
      if (en4.user.viewer.id > 0) {
        $(document.body).addClass('sitepwa_logged_in_member');
      } else {
        en4.sitepwa.login();
        en4.sitepwa.signup();
      }
      en4.sitepwa.layout.setNavigationMenu();
      en4.sitepwa.layout.setTabs();
      window.onbeforeunload = en4.sitepwa.loader;
      if (!('serviceWorker' in navigator)) {
        return;
      }
      var userId = en4.user.viewer.id ? en4.user.viewer.id : 0;
      if (localStorage) {
        if (navigator.serviceWorker.controller != null && localStorage.getItem("userId") != userId && userId == 0) {
          navigator.serviceWorker.controller.postMessage({'command': 'clearPageCache'});
        }
        localStorage.setItem("userId", userId);
      }

      navigator.serviceWorker.register(en4.core.baseUrl + 'sitepwa-service-worker.js').then(function (registration) {
        en4.sitepwa.log('ServiceWorker registration successful with scope: ', registration.scope);
      }, function (err) {

        en4.sitepwa.log('ServiceWorker registration failed: ', err);
      });

      navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        window.addEventListener('beforeinstallprompt', function (e) {
          // beforeinstallprompt Event fired: before prompting the add home screen popup
          e.userChoice.then(function (choiceResult) {
            if (choiceResult.outcome === 'dismissed') {
              en4.sitepwa.log('User cancelled home screen install');
            } else {
              en4.sitepwa.log('User added to home screen');
            }
          });
        });
      });
    },
    log: function (str) {
      if (en4.core.environment == 'production') {
        return;
      }
      console.log(str);
    },
    login: function () {
      $(document.body).getElements("[id='user_form_login']").each(function (el) {
        if (el.hasClass('sitepwa_user_form_login')) {
          return;
        }
        el.addClass('sitepwa_user_form_login');
        var handelerOnFocus = function (event) {
          $(event.target).getParent('.form-wrapper').addClass('form-wapper-focus');
        };
        var handelerOnBlur = function (event) {
          $(event.target).getParent('.form-wrapper').removeClass('form-wapper-focus');
        };
        if (el.getElementById("twitter-wrapper") || el.getElementById("facebook-wrapper")) {
          var wrapperDiv = document.createElement("div");
          wrapperDiv.id = "sitepwa_loginform_sociallinks";
          wrapperDiv.inject(el);
          if (el.getElementById("facebook-wrapper")) {
            el.getElementById("facebook-element").title = en4.core.language.translate("Login with Facebook");
            el.getElementById("facebook-wrapper").inject(wrapperDiv);
          }

          if (el.getElementById("twitter-wrapper")) {
            el.getElementById("twitter-element").title = en4.core.language.translate("Login with Twitter");
            el.getElementById("twitter-wrapper").inject(wrapperDiv);
          }
        }

        el.getElements('input').each(function (el) {
          var type = el.get('type');
          if (type == 'email') {
            el.getParent('.form-wrapper').addClass('form-email-wrapper');
          }
          if (el.get('id') == 'password') {
            var showHideEl = new Element('div', {
              'id': 'show-hide-password-element',
              'class': 'show-hide-password-form-element fa fa-eye'
            }).inject(el.getParent('.form-element'));
            showHideEl.addEvent('click', function () {
              if (el.get('type') == 'password') {
                showHideEl.addClass('fa-eye-slash').removeClass('fa-eye');
                el.set('type', 'text');
              } else {
                showHideEl.removeClass('fa-eye-slash').addClass('fa-eye');
                el.set('type', 'password');
              }
            });
            $("user_form_login").addEvent('submit', function () {
              el.set('type', 'password');
              showHideEl.removeClass('fa-eye-slash').addClass('fa-eye');
            });
          }
          if ((type == 'text' || type == 'email' || type == 'password') && el.getParent('.form-wrapper').getElement('label').get('html')) {
            el.set('placeholder', el.getParent('.form-wrapper').getElement('label').get('html'));
            el.getParent('.form-wrapper').addClass('sitepwa_popup_form_field');
            el.addEvent('focus', handelerOnFocus);
            el.addEvent('blur', handelerOnBlur);
          }
        });
      });
    },
    signup: function () {
      $(document.body).getElements("[id='signup_account_form']").each(function (formEl) {

        formEl.getParent('.sitepwa_layout_widget').addClass('sitepwa-signup-account-form');
        formEl.addClass('sitepwa_signup_account_form');
        var handelerOnFocus = function (event) {
          $(event.target).getParent('.form-wrapper').addClass('form-wapper-focus');
        };
        var handelerOnBlur = function (event) {
          $(event.target).getParent('.form-wrapper').removeClass('form-wapper-focus');
        };
        if (formEl.getElementById("twitter-wrapper") || formEl.getElementById("facebook-wrapper")) {
          var wrapperDiv = document.createElement("div");
          wrapperDiv.id = "sitepwa_signupform_sociallinks";
          wrapperDiv.inject(formEl, 'top');
          if (formEl.getElementById("facebook-wrapper")) {
            var wrapperDiv = document.createElement("span");
            wrapperDiv.id = "facebook";
            wrapperDiv.innerHTML = "<div id='facebook-wrapper'><div id='facebook-element'><a href='" + en4.core.baseUrl + "user/auth/facebook'><img border='0' alt='Connect with Facebook' title = 'Login with Facebook' src='" + en4.core.baseUrl + "application/modules/User/externals/images/facebook-sign-in.gif'></a></div></div>";
            wrapperDiv.inject(wrapperDiv);
          }

          if (formEl.getElementById("twitter-wrapper")) {
            var wrapperDiv = document.createElement("span");
            wrapperDiv.id = "twitter";
            wrapperDiv.innerHTML = "<div id='twitter-wrapper'><div id='twitter-element'><a href='" + en4.core.baseUrl + "user/auth/twitter'><img border='0' alt='Connect with Twitter' title = 'Login with Twitter' src='" + en4.core.baseUrl + "application/modules/User/externals/images/twitter-sign-in.gif'></a></div></div>";
            wrapperDiv.inject(wrapperDiv);
          }
        }

        var className = 'sitepwa_seaolightbox_signup';
        if (wrapperDiv && wrapperDiv.getElement('.plan_subscriptions_container')) {
          className = className + ' sitepwa_seaolightbox_plan_subscriptions';
        }

        formEl.getElements('input').each(function (el) {
          var type = el.get('type');
          if (type == 'email') {
            el.getParent('.form-wrapper').addClass('form-email-wrapper');
          }
          if ((type == 'text' || type == 'email' || type == 'password') && el.getParent('.form-wrapper').getElement('label').get('html')) {
            el.set('placeholder', el.getParent('.form-wrapper').getElement('label').get('html'));
            el.getParent('.form-wrapper').addClass('sitepwa_signup_popup_form_field');
            el.addEvent('focus', handelerOnFocus);
            el.addEvent('blur', handelerOnBlur);
          }
        });
        if (formEl.getElementById('password-element') && formEl.getElementById('passconf-element')) {
          formEl.getElementById('password-element').getParent('.form-wrapper').addClass('sitepwa_popup_form_half_field');
          formEl.getElementById('passconf-element').getParent('.form-wrapper').addClass('sitepwa_popup_form_half_field');
        }
        var canMakeSmallFileds = !!formEl.getElementById('language-element') && !!formEl.getElementById('timezone-element');
        if (formEl.getElementById('timezone-element') && !formEl.getElementById('timezone-option-label')) {
          $('signup_account_form').getElementById('timezone-wrapper').addClass('sitepwa_popup_form_half_field');
          var el = formEl.getElementById('timezone');
          var options = new Element('option', {
            'id': 'timezone-option-label',
            'disabled': 'disabled',
            'class': 'sitepwa_signup_popup_form_field_option_label',
            'html': el.getParent('.form-wrapper').getElement('label').get('html')

          });
          options.inject(el, 'top');
          el.getParent('.form-wrapper').addClass('sitepwa_signup_popup_form_field');
          if (canMakeSmallFileds) {
            el.getParent('.form-wrapper').addClass('sitepwa_popup_form_half_field');
          }
        }
        if (formEl.getElementById('language-element') && !formEl.getElementById('language-option-label')) {
          $('signup_account_form').getElementById('language-wrapper').addClass('sitepwa_popup_form_half_field')
          var el = formEl.getElementById('language');
          var options = new Element('option', {
            'id': 'language-option-label',
            'class': 'sitepwa_signup_popup_form_field_option_label',
            'disabled': 'disabled',
            'html': el.getParent('.form-wrapper').getElement('label').get('html')

          });
          options.inject(el, 'top');
          el.getParent('.form-wrapper').addClass('sitepwa_signup_popup_form_field');
          if (canMakeSmallFileds) {
            el.getParent('.form-wrapper').addClass('sitepwa_popup_form_half_field');
          }
        }
        if (formEl.getElementById('profile_type') && formEl.getElementById('profile_type').get('type') != 'hidden' && !formEl.getElementById('profile_type-option-label')) {
          var el = $('signup_account_form').getElementById('profile_type');
          var addedFields = false;
          el.getElements('option').each(function (el) {
            if (el.get('value') == '') {
              el.set('html', el.getParent('.form-wrapper').getElement('label').get('html')).addClass('sitepwa_signup_popup_form_field_option_label');
              addedFields = true;
            }
          });
          if (!addedFields) {
            var options = new Element('option', {
              'id': 'profile_type-option-label',
              'class': 'sitepwa_signup_popup_form_field_option_label',
              'disabled': 'disabled',
              'html': el.getParent('.form-wrapper').getElement('label').get('html')

            });
            options.inject(el, 'top');
          }
          el.getParent('.form-wrapper').addClass('sitepwa_signup_popup_form_field sitepwa_popup_profile_type_form_field');
        }

      });
    }
  };
  en4.core.runonce.add(en4.sitepwa.onLaod);
})(); // END NAMESPACE

