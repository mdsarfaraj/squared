<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
$menu = $this->navigation()
  ->mainMenuIconPWA()
  ->setContainer($this->navigation)
  ->setRenderParentClass(true)
  ->setParentClass('sitepwa_main_menu_parent')
  //->setPartial(array('_navFontIcons.tpl', 'sitepwa'))
  ->render();
?>
<div class="main_menu_navigation_fixed">
  <?php if( $this->viewer->getIdentity() ): ?>
    <div class="main_menu-links-user">
      <a href="<?php echo $this->viewer()->getHref() ?>" >
        <span class="image">
          <?php echo $this->itemPhoto($this->viewer(), 'thumb.icon') ?>
        </span>
        <span class="user">
          <?php echo $this->viewer()->getTitle(); ?>
        </span>
      </a>
    </div>
  <?php else: ?>
    <?php $coverURl = $this->settings('sitepwa.panel.cover') ?>
    <div class="_cover_wapper" style="<?php if( $coverURl ): ?> background: url(<?php echo $coverURl ?>) <?php endif; ?>">
      <div class="_logo_banner">
        <?php
        $title = $this->settings('sitepwa.site.title', $this->settings('core_general_site_title', $this->translate('_SITE_TITLE')));
        $title = '<div class="sitepwa_logo_title">' . $this->translate($title) . '</div>';
        $logo = $this->settings('sitepwa.panel.logo');
        $route = array('route' => 'default');
        echo ($logo) ? $this->htmlLink($route, $this->htmlImage($logo, array('alt' => $title))) : $this->htmlLink($route, $title);
        ?>
      </div>
      <div class="_login_buttons">
        <?php echo $this->htmlLink(array('route' => 'user_login', 'return_url' => '64-' . base64_encode($_SERVER['REQUEST_URI'])), $this->translate('Sign In'), array('class' => 'button')); ?>
      </div>
    </div>
  <?php endif; ?>
</div>
<div class="main_menu_navigation scrollbars">
  <?php echo $menu; ?>
  <?php if( $this->userSettingsNavigation ): ?>
    <div class="_settings_menus">
      <h4><?php echo $this->translate('Account Settings'); ?></h4>
      <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->userSettingsNavigation)
        ->render();
      ?>
    </div>
  <?php endif; ?>
  <?php if( $this->footerSection ): ?>
    <div class="footer_menus">
      <h4><?php echo $this->translate('Help & Settings'); ?></h4>
      <?php echo $this->content()->renderWidget("sitepwa.menu-footer"); ?>
    </div>
  <?php endif; ?>
</div>
<script type="text/javascript">
  if (en4.user.viewer.id > 0) {
    $(document.body).addClass('sitepwa_logged_in_member');
  }
  en4.sitepwa.layout.setLeftPannelMenu('<?php echo $this->menuPannelType ?>');
</script>