<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_MenuMainController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $coreMenuApi = Engine_Api::_()->getApi('menus', 'core');
    $this->view->menuPannelType = $this->_getParam('menuType', 'overlay');
    $pages = $coreMenuApi
      ->getMenuParams('core_main');
    if( $this->_getParam('mobuleNavigations', 1) ) {
      foreach( $pages as $key => $page ) {
        $menuReference = explode(' ', $page['class']);
        $menuName = end($menuReference);
        $moduleName = str_replace('core_main_', '', $menuName);
        if( strpos($moduleName, 'custom_') !== false ) {
          continue;
        }
        if( strpos($moduleName, 'sitereview_listtype_') !== false ) {
          $mainMenu = str_replace('sitereview_', 'sitereview_main_', $moduleName);
        } else {
          $mainMenu = $moduleName . '_main';
        }
        $subPages = $coreMenuApi->getMenuParams($mainMenu);
        if( empty($subPages) ) {
          continue;
        }
        $page['pages'] = $subPages;
        $pages[$key] = $page;
      }
    }
    $navigation = new Zend_Navigation();
    $navigation->addPages($pages);
    $this->view->navigation = $navigation;
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    $requireCheck = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
    if( !$requireCheck && !$viewer->getIdentity() ) {
      $navigation->removePage($navigation->findOneBy('route', 'user_general'));
    }

    if( $viewer && $viewer->getIdentity() && $this->_getParam('settingNavigations', 1) ) {
      // Set up navigation
      $this->view->userSettingsNavigation = Engine_Api::_()
        ->getApi('menus', 'core')
        ->getNavigation('user_settings');
      // Check last super admin
      if( 1 === count(Engine_Api::_()->user()->getSuperAdmins()) && 1 === $viewer->level_id ) {
        foreach( $this->view->userSettingsNavigation as $page ) {
          if( $page instanceof Zend_Navigation_Page_Mvc &&
            $page->getAction() == 'delete' ) {
            $this->view->userSettingsNavigation->removePage($page);
          }
        }
      }
    }
    $this->view->footerSection = $this->_getParam('footerSection', 1);
  }

  public function getCacheKey()
  {
    //return Engine_Api::_()->user()->getViewer()->getIdentity();
  }

}