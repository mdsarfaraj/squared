<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<?php
echo $this->navigation()
  ->menu()
  ->setContainer($this->navigation)
  //->setPartial(array('_navFontIcons.tpl', 'sitepwa'))
  ->render();
?>
<?php if( count($this->socialMenusNavigation) > 0 ): ?>
  <div class="_menu_social_sites">
    <?php
    echo $this->navigation()
      ->menu()
      ->setContainer($this->socialMenusNavigation)
      ->setPartial(array('_navFontIcons.tpl', 'core'))
      ->render()
    ?>
  </div>
<?php endif; ?>
<div class="_footer_bottom">
  <?php if( 1 !== count($this->languageNameList) ): ?>

    <form method="post" action="<?php echo $this->url(array('controller' => 'utility', 'action' => 'locale'), 'default', true) ?>" class="language_form">
      <?php $selectedLanguage = $this->translate()->getLocale() ?>
      <?php echo $this->formSelect('language', $selectedLanguage, array('onchange' => '$(this).getParent(\'form\').submit();'), $this->languageNameList) ?>
      <?php echo $this->formHidden('return', $this->url()) ?>
    </form>
  <?php endif; ?>
  <?php if( $this->viewer()->getIdentity() ): ?>
  <div class="_signout">
    <a href="<?php echo $this->url(array(), 'user_logout', true); ?>" class="button"><?php echo $this->translate('Sign Out') ?></a>
  </div>
  <?php endif; ?>
  <div class="copyright">
    <?php echo $this->translate('Copyright &copy;%s', date('Y')) ?>
  </div>
  <?php if( !empty($this->affiliateCode) ): ?>
    <div class="affiliate_banner">
      <?php
      echo $this->translate('Powered by %1$s', $this->htmlLink('http://www.socialengine.com/?source=v4&aff=' . urlencode($this->affiliateCode), $this->translate('SocialEngine Community Software'), array('target' => '_blank')))
      ?>
    </div>
  <?php endif; ?>
</div>
