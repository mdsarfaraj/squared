<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style type="text/css">
  /*========Startup Widget CSS=======*/

  .sitepwa_startup_loader ._spinner > div {
    width: 16px;
    height: 16px;
    background-color: #fff;
	border: 2px solid $theme_button_background_color;
	box-sizing: border-box;
    border-radius: 100%;
    display: inline-block;
    -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
    animation: bouncedelay 1.4s infinite ease-in-out;
    /* Prevent first frame from flickering when animation starts */
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
  }
  .sitepwa_startup_loader ._spinner ._bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }
  .sitepwa_startup_loader ._spinner ._bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }
  @-webkit-keyframes bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0.0) }
    40% { -webkit-transform: scale(1.0) }
  }
  @keyframes bouncedelay {
    0%, 80%, 100% {
      transform: scale(0.0);
      -webkit-transform: scale(0.0);
    } 40% {
      transform: scale(1.0);
      -webkit-transform: scale(1.0);
    }
  }
</style>
<script type="text/javascript">
  window.addEvent('domready', function () {
    setTimeout(function () {
      $(document.body).getElements('.layout_sitepwa_startup').destroy();
    }, 2000);
    
  });
</script>
<div class="sitepwa_splash_screen">
  <?php if( $this->logo && $this->image_type == 'full' ): ?>
    <?php echo $this->htmlImage($this->logo, array('alt' => '')); ?>
  <?php else: ?>
    <div class="sitepwa_startup_cont">
      <div class="_sitetitle">
        <?php
        if( $this->logo ):
          echo $this->htmlImage($this->logo, array('alt' => ''), array('height' => $this->height, 'width' => $this->width));
        else :
          ?>
          <?php
          $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
          echo $this->translate($coreSettingsApi->getSetting('sitepwa.site.title', $coreSettingsApi->getSetting('core_general_site_title')));
          ?>
        <?php endif; ?>
      </div>
      <div class="_copyrighttxt">
        <?php echo $this->translate('Copyright &copy;%s', date('Y')) ?>
      </div>
    </div>
    <div class="sitepwa_startup_loader">
      <div class="_spinner">
        <div class="_bounce1"></div>
        <div class="_bounce2"></div>
        <div class="_bounce3"></div>
      </div>
    </div>
  <?php endif; ?>
</div>
