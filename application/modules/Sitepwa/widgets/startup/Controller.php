<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_StartupController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    //FOR APP DEFAULT WILL BE 1
    $request = Zend_Controller_Front::getInstance()->getRequest();
    $moduleName = $request->getModuleName();
    $controllerName = $request->getControllerName();
    $actionName = $request->getActionName();
    if( (Engine_Api::_()->sitepwa()->isPWApp() && $this->_getParam('utm_source') != 'sitepwa') || join('-', array($moduleName, $controllerName, $actionName)) !== 'core-index-index' ) {
      return $this->setNoRender();
    }
    $this->view->logo = $this->_getParam('logo');
    $this->view->image_type = $this->_getParam('image_type');
    $this->view->height = $this->_getParam('height');
    $this->view->width = $this->_getParam('width');
  }

  public function getCacheKey()
  {
    //return true;
  }

}