<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_MenuNavigationController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $name = $this->_getParam('sitepwa_menu');
    if( !$name ) {
      $overwrite_widget = $this->_getParam('overwrite_widget');
      if( $overwrite_widget ) {
        $module = explode('.', $overwrite_widget)[0];
      } else {
        $front = Zend_Controller_Front::getInstance();
        $module = $front->getRequest()->getModuleName();
      }
      $name = $module . '_main';
    }
    if( $name === 'core_main' ) {
      return $this->setNoRender();
    }
    $this->view->navigation = $navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation($name);
    if( count($navigation) <= 0 ) {
      return $this->setNoRender();
    }
  }

}