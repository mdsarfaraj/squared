<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<div id="connection-errors" class="dnone">
  <div id="connection-error-msg" class="tip">
    <span style="width: 100%;text-align: center">
      <?php echo $this->translate("You're currently offline. Please check your internet connection."); ?>
      <a id="connection-error-dismiss" href="javascript::void(0)">DISMISS</a>
    </span>
  </div>
</div>
<script type="text/javascript">
  function pwaUpdateOnlineConnectionStatus() {
    if (navigator.onLine) {
      $('connection-errors').addClass('dnone');
    } else {
      $('connection-errors').removeClass('dnone');
    }
  }
  $('connection-error-dismiss').addEvent('click', function () {
    $('connection-errors').addClass('dnone');
  });
  window.addEventListener('online', pwaUpdateOnlineConnectionStatus);
  window.addEventListener('offline', pwaUpdateOnlineConnectionStatus);
  pwaUpdateOnlineConnectionStatus();

</script>