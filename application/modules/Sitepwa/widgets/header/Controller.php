<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_HeaderController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $this->view->searchBoxType = $this->_getParam('searchType', 'box');
    $this->view->isSitemenuEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitemenu');
    $viewer = Engine_Api::_()->user()->getViewer();
    $require_check = Engine_Api::_()->getApi('settings', 'core')->core_general_search;
    $search_check = $require_check || $viewer->getIdentity();
    $this->view->searchBoxType = $search_check ? $this->view->searchBoxType : false;
    $optionSettings = ($viewer && $viewer->getIdentity() ) ? 'sitepwa.header.loggedin.widgets' : 'sitepwa.header.loggedout.widgets';
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $this->view->options = $coreSettings->getSetting($optionSettings, array(
      'logo',
      'mini_menu',
      'main_menu',
      'search_box',
    ));
    $location = $coreSettings->getSetting('sitepwa.header.minimenu.display.location', 0);
    $showIcons = $coreSettings->getSetting('sitepwa.header.minimenu.design', 1);
    $this->view->miniMenuParams = array(
      'changeMyLocation' => 0,
      'sitemenu_show_icon' => 1,
      'sitemenu_show_in_mini_options' => 6, //IN CASE OF 6 ADV MENU SEARCH BAR WILL NOT BE SHOWN IN HEADER
      'sitemenu_location_box_width' => '', //set the width blank to override width of Sitemenu/widgets/menu-mini/index.tpl line 447 
      'sitemenu_enable_login_lightbox' => 0,
    );
    $this->view->isSiteadvsearchEnable = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('siteadvsearch');
  }

  public function getCacheKey()
  {
    //return true;
  }

}