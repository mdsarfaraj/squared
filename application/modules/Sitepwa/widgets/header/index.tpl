<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style>
  .layout_sitepwa_header .sitepwa_search {
    width: calc(100% - 28px);
  }
  .layout_sitepwa_header .sitepwa_main_menu_toggle {
    float: left;
  }
</style>
<?php if( in_array('main_menu', $this->options) ): ?>
  <div class="sitepwa_main_menu">
	  <div class="sitepwa_main_menu_toggle _pannel_close panel-toggle"></div>
    <?php
    echo $this->content()->renderWidget("sitepwa.menu-main", array(
      'mobuleNavigations' => $this->settings('sitepwa.header.mainmenu.moduleNavigations', 1),
      'settingNavigations' => $this->settings('sitepwa.header.mainmenu.settingNavigations', 1),
      'footerSection' => $this->settings('sitepwa.header.mainmenu.footerSection', 1),
    ));
    ?>
  </div>
<?php endif; ?>
<?php if( in_array('logo', $this->options) ): ?>
  <div class="sitepwa_logo">
    <?php echo $this->content()->renderWidget("sitepwa.menu-logo"); ?>
  </div>
<?php endif; ?>
<?php if( in_array('mini_menu', $this->options) ): ?>
  <div class="sitepwa_minimenu">
    <?php if( in_array('search_box', $this->options) && $this->searchBoxType ) : ?>
      <a href="javascript:void(0);" class="sitepwa_header_minimenu_search" onclick="show_pwa_header_search()"><i class="sitepwa_minimenu-search fa fa-search"></i></a>
    <?php endif; ?>
    <?php if( $this->isSitemenuEnable && !empty($this->miniMenuParams) ) : ?>
      <?php echo $this->content()->renderWidget("sitemenu.menu-mini", $this->miniMenuParams); ?>
    <?php else: ?>
      <?php echo $this->content()->renderWidget("core.menu-mini", array());
      ?>
    <?php endif; ?>
  </div>
<?php endif; ?>
<?php if( in_array('search_box', $this->options) && $this->searchBoxType ) : ?>
  <div class="sitepwa_search">
    <a href="javascript:void(0);" onclick="hide_pwa_header_search()"><i class="fa fa-close"></i></a>
      <?php if( !empty($this->isSiteadvsearchEnable) ) : ?>
        <?php echo $this->content()->renderWidget("siteadvsearch.search-box", array("widgetName" => "advmenu_mini_menu",)); ?>
      <?php else: ?>
        <?php echo $this->content()->renderWidget("sitepwa.search-box", $this->searchParams); ?>
      <?php endif; ?>
  </div>

  <script type="text/javascript">
    function show_pwa_header_search() {
      $$('.layout_sitepwa_header').addClass('_show_search');
    }
    function hide_pwa_header_search() {
      $$('.layout_sitepwa_header').removeClass('_show_search');
    }
    document.getElements('.sitepwa_minimenu .menu_core_mini').each(function (el) {
      var menuClass = '';
      el.get('class').split(' ').each(function (className) {
        if (className.indexOf('core_mini_') === 0) {
          menuClass = className;
        }
      });
      el.getParent('li').addClass('sitepwa_' + menuClass);
    });
    en4.core.runonce.add(function () {
      var element = document.getElement('.layout_sitepwa_header').getParent('.sitepwa_layout_page_header');
      if (!element) {
        return;
      }
      function headerScrolling() {
        var element = $(document.body);
        var scrollTop = window.getScrollTop();
        if (scrollTop > 100) {
          element.addClass('sitepwa_header_fixed');
        } else if (element.hasClass('sitepwa_header_fixed')) {
          element.removeClass('sitepwa_header_fixed');
        }
      }
      window.addEvent('scroll', headerScrolling);
    });
  </script>
<?php endif; ?>
