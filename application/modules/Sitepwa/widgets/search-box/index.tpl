<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
$this->headScript()
  ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
  ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
  ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
  ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<?php $content = $this->identity; ?>
<?php $containerId = $content . '_titleAjax'; ?>
<?php $loadingImageId = 'main-search-loading_' . $content; ?>
<?php $defaultContentId = $content . '_default_content'; ?>
<?php $showDefaultContentId = $content . '_show_default_content'; ?>
<?php $hideContentId = $content . '_hide_content'; ?>
<?php $functionName = 'showDefaultContent' . $content; ?>
<?php $hidefunctionName = 'hideContent' . $content; ?>
<?php $buttonId = 'search_button_' . $content; ?>
<?php $pageResultFunctionName = 'getPageResults' . $content; ?>
<?php $seeMoreFunctionName = 'seeMoreSearch' . $content; ?>

<div class="<?php if( $this->searchbox_width == 0 ): ?>sitepwa_search_box_wrap <?php else: ?>layout_sitepwa_search_wrapper<?php endif; ?> ">
  <div id='global_search_form_container'>
    <form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get"> 
      <?php echo $div; ?>
      <input type='text' class='text suggested' onclick="<?php echo $functionName; ?>('click');"  name='query' id='<?php echo $containerId; ?>' size='20' maxlength='130' placeholder="<?php echo $this->translate('Search'); ?>"  autocomplete="off"/>
<!--          <button><i class="fa fa-search"></i></button>-->
      <span style="display: none;" class="fright" id="<?php echo $loadingImageId; ?>">
        <img alt="Loading" src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Sitepwa/externals/images/loading.gif" align="middle" />
      </span>
    </form> 
  </div>
</div>

<script type='text/javascript'>
  function <?php echo $hidefunctionName; ?>() {
    if ($('<?php echo $hideContentId; ?>'))
      $('<?php echo $hideContentId; ?>').style.opacity = '0';
    if ($('<?php echo $showDefaultContentId; ?>'))
      $('<?php echo $showDefaultContentId; ?>').style.display = 'none';
    $('<?php echo $loadingImageId; ?>').style.display = 'none';
  }

  function sitepwa_search_hide(classname) {

    var node = document.getElementsByTagName("body")[0];
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for (var i = 0, j = els.length; i < j; i++)
      if (re.test(els[i].className))
        a.push(els[i]);
    return a;
  }

  function <?php echo $functionName; ?>(event, key) {

    if ($$('.sitepwa-search-stoprequest').hasClass('sitepwa-search-remove')) {
      $$('.sitepwa-search-stoprequest').removeClass('sitepwa-search-remove')
    }

    if (event == 'enter' && $('<?php echo $containerId; ?>').value == '') {
      $('<?php echo $showDefaultContentId; ?>').style.display = 'none';
      $('<?php echo $loadingImageId; ?>').style.display = 'none';
      var elements = new Array();
      var z = 1;
      elements = sitepwa_search_hide('sitepwa-search-stoprequest');
      for (i in elements) {
        if (z == 1 && elements.length > 1)
          elements[i].addClass('sitepwa-search-remove');
        z++;
      }
      if ($('<?php echo $defaultContentId; ?>')) {
        var explodedValue = $('<?php echo $defaultContentId; ?>').value.split(',label:');
        $('<?php echo $containerId; ?>').value = explodedValue[1];
<?php echo $pageResultFunctionName; ?>(explodedValue[0], explodedValue[1])
        return;
      }
    } else if (event == 'enter' && $('<?php echo $containerId; ?>').value != '') {
<?php echo $pageResultFunctionName; ?>('seeMoreLink', '');
    }
    if ($('<?php echo $containerId; ?>').value == '') {
      if ($('<?php echo $hideContentId; ?>'))
        $('<?php echo $hideContentId; ?>').style.opacity = '1';
      if ($('<?php echo $showDefaultContentId; ?>'))
        $('<?php echo $showDefaultContentId; ?>').style.display = 'block';
      $('<?php echo $loadingImageId; ?>').style.display = 'none';
      var elements = new Array();
      var z = 1;
      elements = sitepwa_search_hide('sitepwa-search-stoprequest');
      for (i in elements) {
        if (z == 1 && elements.length > 1)
          elements[i].addClass('sitepwa-search-remove');
        z++;
      }
      return;
    } else {
      if (event == 'keyup' && key != 'down' && key != 'up') {
        $('<?php echo $loadingImageId; ?>').style.display = 'inline-block';
      }
      if ($('<?php echo $showDefaultContentId; ?>'))
        $('<?php echo $showDefaultContentId; ?>').style.display = 'none';
    }
  }

  $('<?php echo $containerId; ?>').addEvent('keyup', function (e) {

    if ($('advmenu_mini_menu_titleAjax')) {
      var OriginalString = $('advmenu_mini_menu_titleAjax').value;
      $('advmenu_mini_menu_titleAjax').value = OriginalString.replace(/(<([^>]+)>)/ig, "");
    }

    if (e.key == 'enter')
<?php echo $functionName; ?>('enter');
    else
<?php echo $functionName; ?>('keyup', e.key);
  });

  en4.core.runonce.add(function ()
  {
    $(document.body).addEvent('click', function (event) {
      var str = event.target.className;
      if (str.trim() == "text suggested") {
        return;
      }
      var elements = new Array();
      var z = 1;
      elements = sitepwa_search_hide('sitepwa-search-stoprequest');
      for (i in elements) {
        if (z == 1 && elements.length > 1)
          elements[i].addClass('sitepwa-search-remove');
        z++;
      }
      if (event.target.id != '<?php echo $buttonId ?>' && event.target.id != '<?php echo $containerId ?>') {
<?php echo $hidefunctionName; ?>();
      }
    });

    var contentLimit = '<?php echo $this->totalLimit ?>';
    var requestURL = '<?php echo $this->url(array('module' => 'sitepwa', 'controller' => 'index', 'action' => 'get-search-content', 'limit' => $this->limit), "default", true) ?>';

    var sitepwaSearchAutocomplete = new Autocompleter.Request.JSON('<?php echo $containerId; ?>', requestURL, {
      'postVar': 'text',
      'cache': false,
      'minLength': 1,
      'selectFirst': false,
      'selectMode': 'pick',
      'autocompleteType': 'tag',
      'className': 'sitepwa-tag-autosuggest',
      'maxChoices': contentLimit,
      'indicatorClass': 'checkin-loading',
      'customChoices': true,
      'filterSubset': true,
      'multiple': false,
      'injectChoice': function (token) {
        if (token.type == 'no_resuld_found') {
          var choice = new Element('li', {'class': 'autocompleter-choices', 'id': 'sitepwa_search_' + token.type});
          new Element('div', {'html': token.label, 'class': 'autocompleter-choicess'}).inject(choice);
          choice.inject(this.choices);
          choice.store('autocompleteChoice', token);
          return;
        }
        if (typeof token.label != 'undefined') {
          var seeMoreText = '<?php echo $this->string()->escapeJavascript($this->translate('See more results for') . ' '); ?>';

          if (token.item_url != 'seeMoreLink') {
            var choice = new Element('li', {'class': 'autocompleter-choices', 'html': token.photo, 'item_url': token.item_url, onclick: 'javascript:<?php echo $pageResultFunctionName; ?>("' + token.item_url + '")'});
            var divEl = new Element('div', {
              'html': token.type ? this.options.markQueryValueCustom.call(this, (token.label)) : token.label,
              'class': 'autocompleter-choice'
            });

            new Element('div', {
              'html': token.type,
              'class': 'seaocore_txt_light f_small'
            }).inject(divEl);

            divEl.inject(choice);
            new Element('input', {
              'type': 'hidden',
              'value': JSON.encode(token)
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          if (token.item_url == 'seeMoreLink') {
            var titleAjax1 = encodeURIComponent($('<?php echo $containerId; ?>').value);
            var choice = new Element('li', {'class': 'autocompleter-choices', 'html': '', 'id': 'sitepwa_search_choice_stop_event', 'item_url': ''});
            new Element('div', {'html': seeMoreText + '"' + titleAjax1 + '"', 'class': 'autocompleter-choicess', onclick: 'javascript:<?php echo $seeMoreFunctionName ?>()'}).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
        }
      },
      onShow: function () {
        $('<?php echo $loadingImageId; ?>').style.display = 'none';
      },
      markQueryValueCustom: function (str) {
        return (!this.options.markQuery || !this.queryValue) ? str
          : str.replace(new RegExp('(' + ((this.options.filterSubset) ? '' : '^') + this.queryValue.escapeRegExp() + ')', (this.options.filterCase) ? '' : 'i'), '<b>$1</b>');
      },
    });

    sitepwaSearchAutocomplete.addEvent('onSelection', function (element, selected, value, input) {
      if ($('<?php echo $containerId; ?>').value != '') {
        window.addEvent('keyup', function (e) {
          if (e.key == 'enter') {
            if (selected.retrieve('autocompleteChoice') != 'null') {
              var url = selected.retrieve('autocompleteChoice').item_url;
              if (url == 'seeMoreLink') {
<?php echo $seeMoreFunctionName ?>();
              } else {
                window.location.href = url;
              }
            }
          }
        });
      }
    });
    sitepwaSearchAutocomplete.addEvent('onComplete', function () {
      $('<?php echo $loadingImageId; ?>').style.display = 'none';
    });
  });

  function <?php echo $pageResultFunctionName; ?>(url, label) {

    if (label != '') {
      $('<?php echo $containerId; ?>').value = label;
      if ($('<?php echo $showDefaultContentId; ?>'))
        $('<?php echo $showDefaultContentId; ?>').style.display = 'none';
    }
    if (url != 'null') {
      if (url == 'seeMoreLink') {
<?php echo $seeMoreFunctionName ?>();
      } else {
        window.location.href = url;
      }
    }
  }

  function <?php echo $seeMoreFunctionName ?>() {

    $('sitepwa_search_choice_stop_event').removeEvents('click');
    var url = '<?php echo $this->url(array('controller' => 'search', 'action' => 'index'), 'default', true); ?>' + '?query=' + encodeURIComponent($('<?php echo $containerId; ?>').value) + '&type=' + 'all';

    var searchLocationElement = $('searchLocation');
    if (searchLocationElement && searchLocationElement.value != 0 && searchLocationElement.value != '') {
      url = url + '&searchLocation=' + $('searchLocation').value;
    }

    window.location.href = url;
  }
</script>
