<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_SearchBoxController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {

    // check public settings
    $require_check = Engine_Api::_()->getApi('settings', 'core')->core_general_search;
    if( !$require_check && !Zend_Controller_Action_HelperBroker::getStaticHelper('RequireUser')->checkRequire() ) {
      $this->setNoRender();
      return;
    }
    $this->view->widgetName = null;

    //ENABLED CONTENT TYPES WILL BE SHOWING IN MAIN SEARCH BOX WHEN CLICK IN BOX
    $this->view->defaultContents = $defaultContents = NULL;
    //LIMIT OF CONTENT TYPES WHICH WILL COME ON SEARCHING OF TEXT IN SEARCH BOX 
    $this->view->limit = $limit = 5;

    $this->view->searchbox_width = '';
    $this->view->advsearch_search_box_width_for_nonloggedin = '';
    $this->view->totalLimit = $limit + 1;
  }

}