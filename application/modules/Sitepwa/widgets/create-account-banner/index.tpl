<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<div class="sitepwa_create_account_banner">
  <?php
  $title = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepwa.site.title', $this->settings('core_general_site_title', $this->translate('_SITE_TITLE')));
  ?>

  <p><b><?php echo $this->translate('New to %s?', $this->translate($title)); ?></b></p>
  <div>
    <a class="_signup-btn button user_signup_link" href="<?php echo $this->url(array(), "user_signup") ?>"><?php echo $this->translate('Create New Account'); ?></a>
  </div>
  <div class="ui-login-page-btm-links dnone" id="create-account-banner-links-<?php echo $this->identity ?>">
    <a href='<?php echo $this->url(array('module' => 'user', 'controller' => 'auth', 'action' => 'forgot'), 'default', true) ?>'><?php echo $this->translate('Forgot Password?') ?></a>
    <span>&#183;</span>
    <?php if( Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitefaq') ): ?>
      <a href='<?php echo $this->url(array('action' => 'home'), 'sitefaq_general', true) ?>'><?php echo $this->translate('Help') ?></a>
    <?php else: ?>
      <a href='<?php echo $this->url(array('module' => 'core', 'controller' => 'help', 'action' => 'contact'), 'default', true) ?>'><?php echo $this->translate('Help') ?></a>
    <?php endif; ?>
  </div>
  
</div>
<script>
  en4.core.runonce.add(function () {
    var el = $(document.body).getElement('.layout_sitepwa_create_account_banner').getPrevious().getElement("[id='user_form_login']");
    if (el) {
      
      $(document.body).getElement('.layout_sitepwa_create_account_banner .sitepwa_create_account_banner').inject($(document.body).getElement('.layout_sitepwa_create_account_banner').getPrevious());
      $(document.body).getElement('.layout_sitepwa_create_account_banner').destroy();
      $('create-account-banner-links-<?php echo $this->identity ?>').removeClass('dnone');
    }
  });
</script>