<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_SearchContentController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $zendInstance = Zend_Controller_Front::getInstance();
    $request = $zendInstance->getRequest();
    $this->view->params = $p = $request->getParams();
    $this->view->action = $action = $zendInstance->getRouter()->assemble(array());
    if( $p['module'] == 'ynforum' ) {
      $p['module'] = 'forum';
    }
    $this->view->pageName = $pageName = $module_controller_action = $p['module'] . '_' . $p['controller'] . '_' . $p['action'];
  }

}