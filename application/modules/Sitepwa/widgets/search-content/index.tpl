<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php $filter = 'filter' ?>
<!--	DISPLAY LINKS TO TOGGLE ADVANCED SEARCH (HIDE / SHOW)-->


<a href="javascript:void(0)" id="hide_advanced_search_<?php echo $this->identity ?>_<?php echo $filter ?>" class="sitepwa_search_from_hide_link"><?php echo $this->translate("Collapsed"); ?></a>
<div class="search-field" id="simple_search_<?php echo $this->identity ?>_<?php echo $filter ?>">         
  <form class="global_form_box filter_form pwa-search-form" >
    <div class="_input_search">
      <a href="javascript:void(0)" id="show_advanced_search_<?php echo $this->identity ?>_<?php echo $filter ?>" class="sitepwa_search_from_show_link">
      </a>
      <input placeholder="<?php echo $this->translate("Search"); ?>" type="text" data-type="search"  id="" name="" value=""  class="_search"/>
    </div>
    <?php if( $this->pageName == 'sitereview_review_browse' && isset($this->params['listingtype_id']) && $this->params['listingtype_id'] > 0 ): ?>
      <input type="hidden" name="listingtype_id" value='<?php echo $this->params['listingtype_id'] ?>' />
    <?php elseif( $this->pageName == 'forum_index_index' ): ?>
      <input type="hidden" name="type" value="forum_topic" />
    <?php endif; ?>
    <?php if( $this->widgetParams['search'] == 2 ): ?>
      <input type="hidden" name="quickSearch" value="true" />
    <?php endif; ?>
  </form>
</div>
<script type="text/javascript">
  $('simple_search_<?php echo $this->identity ?>_<?php echo $filter ?>').getParent('.sitepwa_layout_widget').hide();
  en4.core.runonce.add(function () {
    $('global_wrapper').getElements('form').each(function (el) {
      if ((el.get('id') != 'filter_form' && !el.hasClass('field_search_criteria')) || el.hasClass('sitenews_advanced_search_form') || el.getParent('.sitepwa_layout_widget').getElement('.sitepwa_search_from_hide_link')) {
        return;
      }
      var form = $('simple_search_<?php echo $this->identity ?>_<?php echo $filter ?>').getElement('form');
      form.set('action', el.get('action'));
      form.set('method', el.get('method'));
      orgText = el.getElement('input[type="text"]');
      advText = (form.getElement('input[data-type="search"]'));
      advText.set('name', orgText.get('name'));
      advText.set('value', orgText.get('value'));
      $('hide_advanced_search_<?php echo $this->identity ?>_<?php echo $filter ?>').inject(el, 'before');
      var hideAdvForm, showAdvForm;
      if (el.getParent('.sitepwa_layout_widget').hasClass('layout_core_content')) {
        form.getParent('.sitepwa_layout_widget').inject(el.getParent('div').addClass('sitepwa_layout_widget'), 'before');
        
      } else {
        form.getParent('.sitepwa_layout_widget').inject(el.getParent('.sitepwa_layout_widget'), 'before');
      }
      hideAdvForm = function () {
          el.getParent('.sitepwa_layout_widget').hide();
          form.getParent('.sitepwa_layout_widget').show();
        };
        showAdvForm = function () {
          form.getParent('.sitepwa_layout_widget').hide();
          el.getParent('.sitepwa_layout_widget').show();
        };
      hideAdvForm();
      $("hide_advanced_search_<?php echo $this->identity ?>_<?php echo $filter ?>").addEvent('click', function () {
        el.getParent('.sitepwa_layout_widget').hide();
        form.getParent('.sitepwa_layout_widget').show();
      });
      $("show_advanced_search_<?php echo $this->identity ?>_<?php echo $filter ?>").addEvent('click', showAdvForm);
    });
  });
</script>