<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<div class="_banner" <?php if( $this->banner ): ?> style="background-image: url(<?php echo $this->banner; ?>)" <?php endif; ?>>
  <div class="sitepwa_logo_banner">
    <div class="sitepwa_sitebanner_logo">
      <div class="sitepwa_sitebanner_logo_img">
        <?php
        $title = $this->settings('sitepwa.site.title', $this->settings('core_general_site_title', $this->translate('_SITE_TITLE')));
        $title = '<div class="_logo_title">' . $this->translate($title) . '</div>';
        $logo = $this->settings('sitepwa.banner.logo');
        $route = array('route' => 'default');
        echo ($logo) ? $this->htmlLink($route, $this->htmlImage($logo, array('alt' => $title))) : $this->htmlLink($route, $title);
        ?>
      </div>
      <?php if( $this->tagline ): ?>
        <p><?php
          echo $this->translate($this->tagline);
          ?></p>
      <?php endif; ?>
    </div>
  </div>
</div>

