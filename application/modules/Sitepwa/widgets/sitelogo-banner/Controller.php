<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_SitelogoBannerController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $this->view->banner = $this->_getParam('banner');
    $title = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepwa.site.title', Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->view->translate('_SITE_TITLE')));
    $this->view->tagline = $this->_getParam('tagline', 'Welcome to ' . $title);
  }

  public function getCacheKey()
  {
    //return true;
  }

}