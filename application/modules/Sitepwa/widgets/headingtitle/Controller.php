<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_HeadingtitleController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {

    // Don't render this if not logged in
    $viewer = Engine_Api::_()->user()->getViewer();
    $this->view->pageHeaderTitle = $pageHeaderTitle = null;
    $this->view->displayNonLoggedInPages = $this->_getParam('nonloggedin', 1);
    //FOR APP DEFAULT WILL BE 1
    $request = Zend_Controller_Front::getInstance()->getRequest();
    $this->view->moduleName = $moduleName = $request->getModuleName();
    $this->view->controllerName = $controllerName = $request->getControllerName();
    $this->view->actionName = $actionName = $request->getActionName();
    $pagename = $moduleName . '_' . $controllerName . '_' . $actionName;
    $this->view->displayLoggedInPages = $this->_getParam('loggedin', 0);

    if( $pagename == 'user_index_home' ) {
      $this->view->displayLoggedInPages = 0;
    }
    if( !$viewer->getIdentity() && !$this->view->displayNonLoggedInPages ) {
      return $this->setNoRender();
    }
    if( $viewer->getIdentity() && !$this->view->displayLoggedInPages ) {
      return $this->setNoRender();
    }
    if( Zend_Registry::isRegistered('sitemapPageHeaderTitle') ) {
      $pageHeaderTitle = Zend_Registry::get('sitemapPageHeaderTitle');
    }

    if( $moduleName == "sitealbum" ) {
      $pageHeaderTitle = $this->view->translate("Albums");
    } else if( $moduleName == "sitemember" ) {
      $pageHeaderTitle = $this->view->translate("Members");
    }
    if( empty($pageHeaderTitle) ) {
      $subject = Engine_Api::_()->core()->hasSubject() ? Engine_Api::_()->core()->getSubject() : null;
      $pageTitleKey = 'mobilepagetitle-' . $moduleName . '-' . $actionName
        . '-' . $controllerName;
      $pageTitle = $this->view->translate($pageTitleKey);
      $spageTitleKey = 'pagetitle-' . $moduleName . '-' . $actionName
        . '-' . $controllerName;
      if( $request->getParam('listingtype_id') ) {
        $pageTitleKey = $pageTitleKey . '_LISTTYPE_' . $request->getParam('listingtype_id');
        $spageTitleKey = $spageTitleKey . '_LISTTYPE_' . $request->getParam('listingtype_id');
      }
      $spageTitle = $this->view->translate($spageTitleKey);
      if( ($pageTitle && $pageTitle != $pageTitleKey ) ) {
        $pageHeaderTitle = $pageTitle;
      } elseif( ($spageTitle && $spageTitle != $spageTitleKey ) ) {
        $pageHeaderTitle = $spageTitle;
      }

      if( ($subject && $subject->getIdentity()) && $subject->getTitle() ) {
        $pageHeaderTitle .= ($pageHeaderTitle ? " - " : '') . $subject->getTitle();
      }
    }

     $this->view->pageHeaderTitle = $pageHeaderTitle;
    if( !$pageHeaderTitle ) {
      return $this->setNoRender();
    }
  }

}