<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_Widget_MenuLogoController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $coreSettingsApi = Engine_Api::_()->getApi('settings', 'core');
    $this->view->logo = $viewer->getIdentity() ? $coreSettingsApi->getSetting('sitepwa.header.small.logo') : $coreSettingsApi->getSetting('sitepwa.header.large.logo');
    if( $viewer->getIdentity() && !$this->view->logo ) {
      return $this->setNoRender();
    }
  }

  public function getCacheKey()
  {
    //return true;
  }

}