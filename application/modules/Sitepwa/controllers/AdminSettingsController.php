<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminSettingsController.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_AdminSettingsController extends Core_Controller_Action_Admin
{

  public function indexAction()
  {
    $this->_createCustomizationThemeFile();
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('sitepwa_admin_main', array(), 'sitepwa_admin_main_settings');

    $this->view->form = $form = new Sitepwa_Form_Admin_Global();

    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
      $values = $form->getValues();

      foreach( $values as $key => $value ) {
        if( in_array($key, array('sitepwa_layout_right_note')) ) {
          continue;
        }
        Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
      }

      $form->addNotice('Your changes have been saved.');
    }
  }

  public function manifestAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('sitepwa_admin_main', array(), 'sitepwa_admin_main_manifest');

    $this->view->form = $form = new Sitepwa_Form_Admin_Manifest();

    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
      $values = $form->getValues();
      $settings = $values;
      $sizes = array('48', '72', '96', '128', '144', '168', '192', '512');
      $photo = $values['app_icon'];
      if( function_exists('getimagesize') && ($imageinfo = getimagesize($photo)) && ($imageinfo[0] !==$imageinfo[1] || $imageinfo[0] < 500)) {
        return $form->addError('Please select a valid image. Width and Height of image should be greather than or equal to 500px and ratio should be 1:1.');
      }
      $file = $photo;
      $fileName = $photo;
      $extension = ltrim(strrchr(basename($fileName), '.'), '.');
      $path = '/public/sitepwa/';
      chmod(APPLICATION_PATH . '/public/sitepwa', 0777);
      foreach( $sizes as $size ) {

        // Resize image (main)
        $iconPath = $path . 'app_icon-' . $size . '.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
          ->resize($size, $size)
          ->write(APPLICATION_PATH . $iconPath)
          ->destroy();
        chmod(APPLICATION_PATH . $iconPath, 0777);
        $settings['icons'][$size] = $values['icons'][] = array(
          'src' => $this->view->serverUrl($this->view->baseUrl($iconPath)),
          'sizes' => $size . 'x' . $size,
          "type" => "image/png",
        );
      }
      $filename = APPLICATION_PATH . '/public/sitepwa/manifest.json';
      file_put_contents($filename, json_encode($values));
      $settingsApi = Engine_Api::_()->getApi('settings', 'core');
      if( $settingsApi->hasSetting('sitepwa.manifest') ) {
        $settingsApi->removeSetting('sitepwa.manifest');
      }
      foreach( $settings as $key => $value ) {
        if( $value == null ) {
          continue;
        }
        $settingsApi->setSetting('sitepwa.manifest.' . $key, $value);
      }
      $this->view->form = $form = new Sitepwa_Form_Admin_Manifest();
      $form->addNotice('Your changes have been saved.');
    }
  }

  public function headerAction()
  {

    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('sitepwa_admin_main', array(), 'sitepwa_admin_main_header');

    //MAKE FORM
    $this->view->form = $form = new Sitepwa_Form_Admin_Settings_Header();

    // Check method/data
    if( !$this->getRequest()->isPost() ) {
      return;
    }
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }
    $coreSettings = Engine_Api::_()->getApi('settings', 'core');
    $values = $form->getValues();
    foreach( $values as $key => $value ) {
      if( $coreSettings->hasSetting($key, $value) ) {
        $coreSettings->removeSetting($key);
      }
      $coreSettings->setSetting($key, $value);
    }
    $form->addNotice('Your changes have been saved.');
  }

  public function faqAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('sitepwa_admin_main', array(), 'sitepwa_admin_main_faq');
    $this->view->faq_id = $faq_id = $this->_getParam('faq', 'faq_1');
  }

  private function _createCustomizationThemeFile()
  {
    foreach( glob(APPLICATION_PATH . '/application/themes/sitepwa/*', GLOB_ONLYDIR) as $dir ) {
      if( file_exists("$dir/manifest.php") && is_readable("$dir/manifest.php") && file_exists("$dir/theme.css") ) {
        $filename = $dir . '/custom.css';
        if( !file_exists($filename) ) {
          file_put_contents($filename, '/* Add your custom css here which are not overwrite during a plugin upgradation */');
          chmod($filename, 0777);
        }
      }
    }
  }

}