<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: IndexController.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_IndexController extends Core_Controller_Action_Standard
{

  public function indexAction()
  {
    $this->view->someVar = 'someVal';
  }

  public function offlineAction()
  {
    $this->_helper->layout->setLayout('default-simple');
  }

  public function previewAction() {
    $this->_helper->layout->setLayout('default-simple');
  }

  //ACTION FOR GET THE SEARCH RESULT BASED ON CORE SEARCH TABLE
  public function getSearchContentAction()
  {

    //GET SEARCHABLE TEXT FROM GLOBAL SEARCH BOX
    $text = $this->_getParam('text', null);
    $pos = strpos($text, '#');
    $data = array();
    $dataSearchable = array();
    if( !empty($text) ) {
      $values = array();
      $values['text'] = $text;
      $values['pagination'] = '';
      $values['resource_type'] = '';
      $values['limit'] = $this->_getParam('limit');

      $items = Engine_Api::_()->sitepwa()->getCoreSearchData($values);
      $count = $countSearchableResult = count($items);

      //START PRIVACY WORK FOR SHOWING CONTENT TYPE TO NON-LOGGED IN USERS
      $i = 0;
      foreach( $items as $item ) {

        $type = $item->type;
        if( !Engine_Api::_()->hasItemType($type) ) {
          continue;
        }
        $item = $this->view->item($type, $item->id);
        if( $item ) {
          if( $item->getPhotoUrl() != '' )
            $content_photo = $this->view->itemPhoto($item, 'thumb.icon');
          else
            $content_photo = "<img src='" . $this->view->layout()->staticBaseUrl . "application/modules/Sitepwa/externals/images/nophoto_icon.png' alt='' />";

          $i++;

          $resourceTitle = $item->getShortType();
          if( $item->getShortType() == 'user' ) {
            $resourceTitle = 'member';
          }
          $iType = $this->view->translate(ucfirst($resourceTitle));
          if( is_array($iType) && isset($iType[0]) ) {
            $iType = $iType[0];
          }
          $dataSearchable[] = array(
            'label' => $item->getTitle(),
            'type' => $iType,
            'photo' => $content_photo,
            'item_url' => $item->getHref(),
            'total_count' => $count,
            'count' => $i
          );
        }
      }

      $realCount = $i;
      $data = $dataSearchable;
      if( empty($dataSearchable) ) {
        $data[] = array(
          'label' => $this->view->translate('No result found for "%s".', $this->_getParam('text')),
          'type' => 'no_resuld_found',
          'item_url' => 'no_resuld_found',
          'search_text' => $text,
        );
      } else {
        $data[] = array(
          'id' => 'sitepwa_search_choice_stop_event',
          'label' => $this->_getParam('text'),
          'item_url' => 'seeMoreLink',
          'total_count' => ++$realCount
        );
      }
    }
    return $this->_helper->json($data);
  }

}