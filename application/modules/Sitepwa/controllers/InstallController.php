<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: InstallController.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_InstallController extends Core_Controller_Action_Standard
{

  protected $_token;

  public function init()
  {
    $this->_helper->contextSwitch->initContext();
//    $viewer = Engine_Api::_()->user()->getViewer();
    $settingsTable = Engine_Api::_()->getDbtable('settings', 'core');
    $row = $settingsTable->fetchRow($settingsTable->select()
        ->where('name = ?', 'sitepwa.install.ssotoken'));
    $this->_token = $row ? $row->value : null;
    $token = $this->_getParam('key', false);
    if( $token != $this->_token ) {
      exit(0);
    }
  }

  public function postInstallAction()
  {
    $clonePages = array('user_index_home', 'user_profile_index');
    foreach( $clonePages as $page ) {
      $this->addPWALayoutPage($page);
    }
    $widgets = array(
      'user_auth_login' => array(
        array('name' => 'sitepwa.sitelogo-banner', 'order' => 1),
        array('name' => 'core.content'),
      ),
      'user_signup_index' => array(
        array('name' => 'sitepwa.sitelogo-banner', 'order' => 1),
        array('name' => 'core.content'),
      ),
      'core_error_requireuser' => array(
        array('name' => 'sitepwa.sitelogo-banner', 'order' => 1),
        array('name' => 'core.content'),
      )
    );

    foreach( $widgets as $page => $widgets ) {
      $this->addWidgets($page, $widgets, true);
    }
    //CHANGE LAYOUT OF HEADER,FOOTER,SIGNIN,SIGNUP AND LANDING PAGE
    $settingsTable = Engine_Api::_()->getDbtable('settings', 'core');
    $settingsTable->delete(array('name = ?' => 'sitepwa.install.ssotoken'));
    exit(0);
  }

  private function hasIntegratePage($pageId)
  {
    // Get all content
    $contentTable = Engine_Api::_()->getDbtable('content', 'sitepwa');
    $select = $contentTable->select()
      ->where('page_id = ?', $pageId)
      ->limit(1);
    return !!$contentTable->fetchRow($select);
  }

  private function addWidgets($pageName, $widgets = array(), $fresh = false)
  {
    $contentTable = Engine_Api::_()->getDbtable('content', 'sitepwa');
    $pageTable = Engine_Api::_()->getDbtable('pages', 'sitepwa');
    $corePageObject = $pageTable->fetchRow(array('name = ?' => $pageName));
    if( empty($corePageObject) ) {
      return;
    }
    $page_id = $corePageObject->page_id;
    if( !$this->hasIntegratePage($page_id) ) {
      $this->addPWALayoutPage($pageName);
    }
    $order = 1000;
    $mainContainer = $contentTable->fetchRow(array('type = ?' => 'container', 'name = ?' => 'main', 'page_id = ?' => $page_id));
    $middleContainer = $contentTable->fetchRow(array('type = ?' => 'container', 'name = ?' => 'middle', 'page_id = ?' => $page_id, 'parent_content_id' => $mainContainer->content_id));
    if( $fresh ) {
      $contentTable->delete(array('type = ?' => 'widget', 'page_id = ?' => $page_id));
    }
    // Insert search
    $main_middle_id = $middleContainer->content_id;
    foreach( $widgets as $widget ) {
      $contentRow = $contentTable->createRow();
      $contentRow->setFromArray(array_merge(array(
        'type' => 'widget',
        'page_id' => $page_id,
        'parent_content_id' => $main_middle_id,
        'order' => $order++,
          ), $widget));
      $contentRow->save();
    }
  }

  private function addPWALayoutPage($pageName)
  {
    $pageTable = Engine_Api::_()->getDbtable('pages', 'sitepwa');
    $contentTable = Engine_Api::_()->getDbtable('content', 'sitepwa');

    // Make new page
    $corePageObject = $pageTable->fetchRow(array('name = ?' => $pageName));
    if( empty($corePageObject) ) {
      return;
    }
    $page_id = $corePageObject->page_id;
    if( $this->hasIntegratePage($page_id) ) {
      return;
    }
    $pageObject = $corePageObject;
    $pageObject->sitepwa_layout = $corePageObject->layout;
    $pageObject->save();
    $db = Engine_Db_Table::getDefaultAdapter();
    $core_page_content = $db->select()
      ->from('engine4_core_content')
      ->where('`page_id` = ?', $page_id)
      ->order(array('type', 'content_id'))
      ->query()
      ->fetchAll();

    $content_count = count($core_page_content);
    $ignore_parent_ids = array();
    for( $i = 0; $i < $content_count; $i++ ) {
      if( ($core_page_content[$i]['type'] == 'container' && in_array($core_page_content[$i]['name'], array('left', 'right'))) || in_array($core_page_content[$i]['parent_content_id'], $ignore_parent_ids) ) {
        $ignore_parent_ids[] = $core_page_content[$i]['content_id'];
        continue;
      }
      $contentRow = $contentTable->createRow();
      $contentRow->page_id = $page_id;
      $contentRow->type = $core_page_content[$i]['type'];
      $contentRow->name = $core_page_content[$i]['name'];
      if( $core_page_content[$i]['parent_content_id'] != null ) {
        $contentRow->parent_content_id = $content_id_array[$core_page_content[$i]['parent_content_id']];
      } else {
        $contentRow->parent_content_id = $core_page_content[$i]['parent_content_id'];
      }
      $contentRow->order = 20 + $core_page_content[$i]['order'];
      $contentRow->params = $core_page_content[$i]['params'];
      $contentRow->attribs = $core_page_content[$i]['attribs'];
      $contentRow->save();
      $content_id_array[$core_page_content[$i]['content_id']] = $contentRow->content_id;
    }
  }

}