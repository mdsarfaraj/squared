<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: AdminModuleController.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitepwa_AdminModuleController extends Core_Controller_Action_Admin
{

  protected $_modulestable;

  public function init()
  {
    $this->_modulestable = Engine_Api::_()->getDbtable('modules', 'sitepwa');
  }

  public function manageAction()
  {

    //GET NAVIGATION
    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('sitepwa_admin_main', array(), 'sitepwa_admin_main_module');

    // Get list of all modules from sitepwa module table which are enabled in core module table.
    $this->view->modulesList = $modules = $this->_modulestable->getManageModulesList();
  }

  //TO DISABLE/ENABLE ANY MODULE.
  public function integrateAction()
  {
    set_time_limit(0);
    $this->view->name = $moduleName = $this->_getParam('name');
    Engine_Api::_()->getApi('modules', 'sitepwa')->addModuleStart($moduleName);
    $modulesList = $this->_modulestable->getExtensionsList(array('modulename' => $moduleName));
    //Disable all the Page plugin extension which are enabled.
    foreach( $modulesList as $module ) {
      $moduleName = $module->name;
      Engine_Api::_()->getApi('modules', 'sitepwa')->addModuleStart($moduleName);
    }
    $redirect = $this->_getParam('redirect', false);
    if( !$redirect ) {
      $this->_redirect('admin/sitepwa/module/manage');
    } else {
      $this->_redirect('install/manage');
    }
  }

}