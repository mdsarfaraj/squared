<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: sitemodule_install.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class SiteModule_Installer extends Engine_Package_Installer_Module
{

  protected $_urls = array();

  /**
   * Internal construct hook
   *
   * @return void
   */
  public function init()
  {
    $this->_urls = array(
      'after_install_configure' => $this->_name . '/install/post-install',
    );
  }

  public function onPreInstall()
  {
    parent::onPreInstall();
  }

  public function onInstall()
  {
    parent::onInstall();
  }

  public function onPostInstall()
  {
    $this->configureAfterInstall();
  }

  protected function configureAfterInstall()
  {
    if( $this->_databaseOperationType != 'install' || empty($this->_urls['after_install_configure']) ) {
      return;
    }

    $token = md5(Engine_String::str_random(15));
    $db = $this->getDb();
    $name = $this->_name . ".install.ssotoken";
    $db->query("DELETE FROM `engine4_core_settings` WHERE `engine4_core_settings`.`name` = '$name'");
    $db->query("INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('$name', '$token');");
    $this->_triggerCurlPostRequest($this->_urls['after_install_configure'], array('key' => $token));
  }

  public function _triggerCurlPostRequest($uri, $postOptions)
  {
    // get general config
    $generalConfig = array();
    if( file_exists(APPLICATION_PATH . DS . 'application' . DS . 'settings' . DS . 'general.php') ) {
      $generalConfig = include APPLICATION_PATH . DS . 'application' . DS . 'settings' . DS . 'general.php';
    }
    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
    $request = Zend_Controller_Front::getInstance()->getRequest();
    $prefix = str_replace('/install', '', $baseUrl);
    $url = $request->getScheme() . '://' . $request->getHttpHost() . $prefix . '/' . $uri;
    $curloptions = array();
    if( !empty($generalConfig['maintenance']['code']) ) {
      $curloptions[CURLOPT_COOKIE] = 'en4_maint_code=' . $generalConfig['maintenance']['code'];
    }
    // Try to handle basic htauth
    if( !empty($request->getServer('PHP_AUTH_USER')) && !empty($request->getServer('PHP_AUTH_PW')) ) {
      $curloptions[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
      $curloptions[CURLOPT_USERPWD] = $request->getServer('PHP_AUTH_USER') . ':' . $request->getServer('PHP_AUTH_PW');
    }
    // If SSL is enabled
    if( $request->getScheme() == 'https://' ) {
      $curloptions[CURLOPT_VERBOSE] = true;
      $curloptions[CURLOPT_SSL_VERIFYPEER] = false;
    }
    $client = new Zend_Http_Client();
    $client->setConfig(array('timeout' => 6000, 'adapter' => 'Zend_Http_Client_Adapter_Curl', 'curloptions' => $curloptions))
      ->setUri($url)
      ->setMethod(Zend_Http_Client::POST)
      ->setParameterPost($postOptions);
    $response = $client->request();
    $responseData = $response->getBody();
  }

}