<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: sitepwa_content.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
return array(
  array(
    'title' => 'PWA: Main Menu',
    'description' => 'Shows the site-wide main menu. You can edit its contents in your menu editor.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.menu-main',
    'requirements' => array(
      'header-footer',
    ),
    'autoEdit' => true,
    'adminForm' => array(
      'elements' => array(
        array(
          'Select',
          'mobuleNavigations',
          array(
            'label' => 'Menu Nested Navigatinos',
            'description' => 'Do you want to show nested navigatinos.',
            'multiOptions' => array(
              '1' => 'Yes',
              '0' => 'No'
            ),
            'value' => '1'
          )),
        array(
          'Select',
          'settingNavigations',
          array(
            'label' => 'Settigns Navigatinos',
            'description' => 'Do you want to show account settings navigatinos.',
            'multiOptions' => array(
              '1' => 'Yes',
              '0' => 'No'
            ),
            'value' => '1'
          )),
//        array(
//          'Select',
//          'menuType',
//          array(
//            'label' => 'Menu Pannel Type',
//            'description'=> 'Select the type of pannel of menu.',
//            'multiOptions' => array(
//                'slide' => 'Slide Pannel',
//                'overlay' => 'Overlay Pannel'
//            ),
//            'value' => 'overlay'
//        )),
      )
    ),
  ),
  array(
    'title' => 'PWA: Startup Image',
    'description' => 'Shows the Startup Image that will appear while your website is being loaded. You can add a Startup Image from the “Layout” >> “File and Media Manager” section in the Admin Panel. This widget should be placed on the Landing Page.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.startup',
    'adminForm' => 'Sitepwa_Form_Admin_Widget_Startup',
    'requirements' => array(
      'header-footer',
    ),
  ),
  array(
    'title' => 'PWA: Header',
    'description' => 'Displays your Progressive Web App Header. This should be placed on the Site header. You can edit its contents in the Manage Header section of this plugin.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.header',
    'adminForm' => array(
    ),
    'defaultParams' => array(
      'title' => '',
      'titleCount' => true,
    ),
  ),
    array(
    'title' => 'PWA: Create Account Banner',
    'description' => 'Displays the Create New Account Banner. This should be placed on the Landing Page.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.create-account-banner',
    'adminForm' => array(
    ),
    'defaultParams' => array(
      'title' => '',
      'titleCount' => true,
    ),
  ),
  array(
    'title' => 'Show Offline mode message',
    'description' => 'Shows a tip with an offline mode message when the network is not available.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.check-online-mode',
  ),
  array(
    'title' => 'PWA: Site Logo',
    'description' => 'Shows your site-wide main logo or title.  Images are uploaded via the <a href="admin/files" target="_parent">File Media Manager</a>.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.menu-logo',
    'adminForm' => 'Sitepwa_Form_Admin_Widget_Logo',
    'requirements' => array(
      'header-footer',
    ),
  ),
  array(
    'title' => 'PWA: Site Banner Logo',
    'description' => 'Displays the selected Banner image.  Images are uploaded via the <a href="admin/files" target="_parent">File Media Manager</a>.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.sitelogo-banner',
    'adminForm' => 'Sitepwa_Form_Admin_Widget_Banner'
  ),
  array(
    'title' => 'PWA: Search Box',
    'description' => 'Displays PWA Search Box. This widget can be placed anywhere on the site.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.search-box',
    'adminForm' => array(
    ),
    'defaultParams' => array(
      'title' => '',
      'titleCount' => true,
    ),
  ),
//  array(
//    'title' => 'PWA: Navigation Tabs',
//    'description' => "Displays the site wide navigation menus of your website. This widget should be placed in header.",
//    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
//    'type' => 'widget',
//    'name' => 'sitepwa.navigation',
//    'defaultParams' => array(
//      'title' => '',
//      'titleCount' => true,
//    ),
//    'adminForm' => array(
//    ),
//  ),
  array(
    'title' => 'PWA: Navigation Menu',
    'description' => 'Displays the selected Navigation Menu. The content of different navigation menus can be edited from the Menu Editor Section of the Progressive Web App plugin.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.menu-navigation',
    'autoEdit' => true,
    'defaultParams' => array(
      'title' => '',
    ),
    'adminForm' => 'Sitepwa_Form_Admin_Widget_NavigationMenu',
  ),
  array(
    'title' => 'PWA: Page Title',
    'description' => 'Shows the page title. This widget will work for all pages except Member Home Page.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.headingtitle',
    'isPaginated' => false,
    'autoEdit' => true,
    'defaultParams' => array(
      'title' => '',
    ),
    'adminForm' => array(
      'elements' => array(
        array(
          'Radio',
          'nonloggedin',
          array(
            'label' => 'Show Page Title to users (non-logged in users) of your site.',
            'multiOptions' => array(
              1 => 'Yes',
              0 => 'No'
            ),
            'value' => 1,
          )
        ),
        array(
          'Radio',
          'loggedin',
          array(
            'label' => 'Show Page Title to members (logged-in users) of your site.',
            'multiOptions' => array(
              1 => "Yes, show titles on all the pages except Member Home Page",
              0 => "No, do not show titles"
            ),
            'value' => 0,
          )
        )
      )
    )
  ),
  array(
    'title' => 'PWA: Advanced Search',
    'description' => 'Display the Advanced Search box to allow search your site’s content. This should be placed on the site header.',
    'category' => 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface',
    'type' => 'widget',
    'name' => 'sitepwa.search-content',
    'autoEdit' => true,
    'adminForm' => array(
      'elements' => array(
        array(
          'Radio',
          'search',
          array(
            'label' => 'Select the display type for Search.',
            'multiOptions' => array(
              1 => 'Only Search Text field',
              2 => 'Search Text field with expandable Advanced Search options',
            ),
            'value' => 2,
          )
        ),
      )
    )
  ),
);
?>