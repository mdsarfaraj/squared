<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: manifest.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
return array(
  'package' =>
  array(
    'type' => 'module',
    'name' => 'sitepwa',
    'version' => '4.10.5',
    'path' => 'application/modules/Sitepwa',
    'title' => 'Mobile / Tablet Plugin - Progressive Web Apps',
    'description' => 'Mobile / Tablet Plugin - Progressive Web Apps',
    'author' => '<a href="http://www.socialengineaddons.com" style="text-decoration:underline;" target="_blank">SocialEngineAddOns</a>',
    'seao-sku' => 'seao-sitepwa',
    'callback' =>
    array(
      'path' => 'application/modules/Sitepwa/settings/install.php',
      'class' => 'Sitepwa_Installer',
    ),
    'dependencies' => array(
      array(
        'type' => 'module',
        'name' => 'core',
        'minVersion' => '4.10.3',
      ),
    ),
    'actions' =>
    array(
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' =>
    array(
      0 => 'application/modules/Sitepwa',
      1 => 'application/themes/sitepwa',
    ),
    'files' =>
    array(
      2 => 'application/languages/en/sitepwa.csv',
      0 => 'sitepwa-service-worker.js',
      1 => 'public/sitepwa/index.html'
    ),
  ),
  // Routes --------------------------------------------------------------------
  'routes' => array(
    'sitepwa_general' => array(
        'route' => 'pwa/:action/*',
          'defaults' => array(
              'module' => 'sitepwa',
              'controller' => 'index',
              'action' => 'index',
            ),
            'reqs' => array(
            ),
    ),
  ),
  // Hooks ---------------------------------------------------------------------
  'hooks' => array(
    array(
      'event' => 'onRenderLayoutDefault',
      'resource' => 'Sitepwa_Plugin_Core',
    ),
    array(
      'event' => 'onRenderLayoutDefaultSimple',
      'resource' => 'Sitepwa_Plugin_Core',
    ),
    array(
      'event' => 'onCorePageUpdateAfter',
      'resource' => 'Sitepwa_Plugin_Core',
    ),
    array(
      'event' => 'onCorePageDeleteBefore',
      'resource' => 'Sitepwa_Plugin_Core',
    ),
  ),
);
?>