<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepwa
 * @copyright  Copyright 2018-2019 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: install.php 2018-11-20 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
require_once realpath(dirname(__FILE__)) . '/sitemodule_install.php';

class Sitepwa_Installer extends SiteModule_Installer
{

  protected $_name = 'sitepwa';

  protected $_installConfig = array(
    'sku' => 'sitepwa',
  );

  function onInstall()
  {
    $filename = APPLICATION_PATH . '/public/sitepwa/manifest.json';
    chmod(APPLICATION_PATH . '/public/sitepwa', 0777);
    if( !file_exists($filename) && !is_writable(APPLICATION_PATH . '/public/sitepwa')) {
      return $this->_error(sprintf("<span style='color:red'>Note: You do not have writable permission on the path below, please give 'chmod 777 recursive permission' on it to continue with the installation process : <br /> 
  Path Name: %s </span>", APPLICATION_PATH . '/public/sitepwa'));
    }
    $this->_writeManifest();
    $this->_createCustomizationThemeFile();
    parent::onInstall();
    $this->_updateLogo();
  }

  private function _updateLogo()
  {

    if( $this->_databaseOperationType != 'install' ) {
      return;
    }
    $db = $this->getDb();
    $select = new Zend_Db_Select($db);
    $select
      ->from('engine4_core_content')
      ->where('name = ?', 'core.menu-logo')
      ->where('page_id = ?', 1);
    $contentObject = $select->query()->fetchObject();
    if( !$contentObject || !$contentObject->params ) {
      return;
    }
    $params = json_decode($contentObject->params, true);
    if( empty($params['logo']) ) {
      return;
    }
    $db->insert('engine4_core_settings', array('name' => 'sitepwa.header.large.logo', 'value' => $params['logo']));
  }

  private function _createCustomizationThemeFile()
  {
    chmod(APPLICATION_PATH . '/application/themes/sitepwa', 0777);
    foreach( glob(APPLICATION_PATH . '/application/themes/sitepwa/*', GLOB_ONLYDIR) as $dir ) {
      if( file_exists("$dir/manifest.php") && is_readable("$dir/manifest.php") && file_exists("$dir/theme.css") ) {
        $filename = $dir . '/custom.css';
        if( !file_exists($filename) ) {
          chmod($dir, 0777);
          file_put_contents($filename, '/* Add your custom css here which are not overwrite during a plugin upgradation */');
          chmod($filename, 0777);
        }
      }
    }
  }

  private function _writeManifest()
  {
    $filename = APPLICATION_PATH . '/public/sitepwa/manifest.json';
    if( file_exists($filename) ) {
      return;
    }

    chmod(APPLICATION_PATH . '/public/sitepwa', 0777);
    $db = $this->getDb();
    $name = $db->query("SELECT `value` FROM `engine4_core_settings` WHERE `name` = 'core.general.site.title' LIMIT 1")->fetchColumn();
    $description = $db->query("SELECT `value` FROM `engine4_core_settings` WHERE `name` = 'core.general.site.description' LIMIT 1")->fetchColumn();
    $serverUrl = (new Zend_View_Helper_ServerUrl)->serverUrl();
    $baseUrlArray = explode('/install', Zend_Controller_Front::getInstance()->getBaseUrl());
    $base = array_shift($baseUrlArray);
    $start_url = $serverUrl . $base . '/?utm_source=sitepwa';
    $siteUrl = rtrim($serverUrl . $base, '/');
    $sampleFilename = APPLICATION_PATH . '/application/modules/Sitepwa/settings/manifest-sample.json';
    $string = str_replace('https:\/\/example.com', str_replace('/', '\/', $siteUrl), file_get_contents($sampleFilename));
    $manifest = json_decode($string, true);
    $manifestData = array(
      'name' => $name,
      'short_name' => $name,
      'start_url' => $start_url,
      'description' => $description ?: sprintf('Enjoy the Progressive Web Apps (PWA) of %s', $name),
    );
    $sizes = array('48', '72', '96', '128', '144', '168', '192', '512');
    $photo = APPLICATION_PATH . '/application/modules/Sitepwa/externals/images/app.png';
    $file = $photo;
    $fileName = $photo;
    $extension = ltrim(strrchr(basename($fileName), '.'), '.');
    $path = '/public/sitepwa/';
    $icons = array();
    foreach( $sizes as $size ) {

      // Resize image (main)
      $iconPath = $path . 'app_icon-' . $size . '.' . $extension;
      $image = Engine_Image::factory();
      $image->open($file)
        ->resize($size, $size)
        ->write(APPLICATION_PATH . $iconPath)
        ->destroy();
      chmod(APPLICATION_PATH . $iconPath, 0777);
      $icons[] = array(
        'src' => $siteUrl . $iconPath,
        'sizes' => $size . 'x' . $size,
        "type" => "image/png",
      );
    }
    $manifestData['icons'] = $icons;
    chmod(APPLICATION_PATH . '/public/sitepwa', 0777);
    $filename = APPLICATION_PATH . '/public/sitepwa/manifest.json';
    file_put_contents($filename, json_encode(array_merge($manifest, $manifestData)));
    chmod($filename, 0777);
  }

}
?>
