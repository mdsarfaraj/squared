INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('sitepwa', 'Progressive Web Apps (PWA) Plugin', 'Progressive Web Apps (PWA) Plugin – Interactive Mobile Interface', '4.10.3p2', 1, 'extra') ;


-- --------------------------------------------------------

--
-- Table structure for table `engine4_sitepwa_content`
--

DROP TABLE IF EXISTS `engine4_sitepwa_content`;
CREATE TABLE IF NOT EXISTS `engine4_sitepwa_content` (
  `content_id` int(11) unsigned NOT NULL auto_increment,
  `page_id` int(11) unsigned NOT NULL,
  /* Rendering */
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL default 'widget',
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  /* Placement */
  `parent_content_id` int(11) unsigned NULL,
  `order` int(11) NOT NULL default '1',
  /* Misc */
  `params` text NULL,
  `attribs` text NULL,
  PRIMARY KEY  (`content_id`),
  KEY (`page_id`, `order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

--
-- Dumping data for table `engine4_sitepwa_content`
--

INSERT INTO `engine4_sitepwa_content` (`content_id`, `page_id`, `type`, `name`, `parent_content_id`, `order`, `params`) VALUES
/* Header */
(100, 1, 'container', 'main', NULL, 1, ''),
(114, 1, 'widget', 'sitepwa.startup', 100, 1, ''),
(110, 1, 'widget', 'sitepwa.header', 100, 2, ''),
(116, 1, 'widget', 'sitepwa.search-content', 100, 99, ''),
/* Footer */
(200, 2, 'container', 'main', NULL, 1, ''),

(210, 2, 'widget', 'core.menu-footer', 200, 2, ''),
(215, 2, 'widget', 'user.login-or-signup-popup', 200, 999, ''),
(211, 2, 'widget', 'core.menu-social-sites', 200, 3, ''),
/* Home */
(300, 3, 'container', 'main', NULL, 1, ''),

(312, 3, 'container', 'middle', 300, 3, ''),

(320, 3, 'widget', 'sitepwa.sitelogo-banner', 312, 1, ''),
(322, 3, 'widget', 'user.login-or-signup', 312, 2, ''),
(324, 3, 'widget', 'sitepwa.create-account-banner', 312, 3, '');


-- --------------------------------------------------------

--
-- Table structure for table `engine4_sitepwa_content`
--

DROP TABLE IF EXISTS `engine4_sitepwa_page_maps`;
CREATE TABLE IF NOT EXISTS `engine4_sitepwa_page_maps` (
  `page_map_id` int(11) unsigned NOT NULL,
  `sitepwa_layout` VARCHAR( 32 ) NOT NULL DEFAULT '',
  PRIMARY KEY  (`page_map_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;
-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_menuitems`
--

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES

('core_admin_main_plugins_sitepwa', 'sitepwa', 'SEAO - Progressive Web Apps', '', '{"route":"admin_default","module":"sitepwa","controller":"settings"}', 'core_admin_main_plugins', '', 999),

('sitepwa_admin_main_settings', 'sitepwa', 'Global Settings', '', '{"route":"admin_default","module":"sitepwa","controller":"settings"}', 'sitepwa_admin_main', '', 2),
('sitepwa_admin_main_manifest', 'sitepwa', 'Manifest Settings', '', '{"route":"admin_default","module":"sitepwa","controller":"settings","action": "manifest"}', 'sitepwa_admin_main', '', 3),
('sitepwa_admin_main_header', 'sitepwa', 'Manage Header', '', '{"route":"admin_default","module":"sitepwa","controller":"settings","action": "header"}', 'sitepwa_admin_main', '', 30),
('sitepwa_admin_main_content', 'sitepwa', 'Layout Editor', '', '{"route":"admin_default","module":"sitepwa","controller":"content"}', 'sitepwa_admin_main', '', 40),
('sitepwa_admin_main_themes', 'sitepwa', 'Theme Editor', '', '{"route":"admin_default","module":"sitepwa","controller":"themes"}', 'sitepwa_admin_main', '', 50),
('sitepwa_admin_main_menus', 'sitepwa', 'Menu Editor', '', '{"route":"admin_default","module":"sitepwa","controller":"menus"}', 'sitepwa_admin_main', '', 60),

('sitepwa_admin_main_faq', 'sitepwa', 'FAQ', '', '{"route":"admin_default","module":"sitepwa","controller":"settings","action": "faq"}', 'sitepwa_admin_main', '', 999);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_themes`
--
-- --------------------------------------------------------
DROP TABLE IF EXISTS `engine4_sitepwa_themes`;
CREATE TABLE IF NOT EXISTS `engine4_sitepwa_themes` (
  `theme_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`theme_id`),
  UNIQUE KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

--
-- Dumping data for table `engine4_allure_themes`
--

INSERT IGNORE INTO `engine4_sitepwa_themes` ( `name`, `title`, `description`, `active`, `default`) VALUES
('skyblue', 'Sky Blue', '', 1, 1),
('pink', 'Pink', '', 0, 1),
('green', 'Green', '', 0, 1),
('orange', 'Orange', '', 0, 1),
('yellow', 'Yellow', '', 0, 1),
('red', 'Red', '', 0, 1),
('darkskyblue', 'Dark Sky Blue', '', 0, 2),
('darkpink', 'Dark Pink', '', 0, 2),
('darkgreen', 'Dark Green', '', 0, 2),
('darkorange', 'Dark Orange', '', 0, 2),
('darkyellow', 'Dark Yellow', '', 0, 2),
('darkred', 'Dark Red', '', 0, 2);

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
("core_admin_main_layout_sitepwa_content", "sitepwa", "PWA Plugin: Layout Editor", "", '{"route":"admin_default","module":"sitepwa","controller":"content"}', "core_admin_main_layout", "",  900),
("core_admin_main_layout_sitepwa_themes", "sitepwa", "PWA Plugin: Theme Editor", "", '{"route":"admin_default","module":"sitepwa","controller":"themes"}', "core_admin_main_layout", "", 950),
("core_admin_main_layout_sitepwa_menus", "sitepwa", "PWA Plugin: Menu Editor", "", '{"route":"admin_default","module":"sitepwa","controller":"menus"}', "core_admin_main_layout", "", 999);
