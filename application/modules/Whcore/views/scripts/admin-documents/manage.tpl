<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whcore/externals/scripts/pdfobject.js'); ?>

<h2>
    <?php echo $this->translate("Manage Documents") ?>
</h2>

<?php if (count($this->navigation)): ?>
    <div class='tabs'>
        <?php
        echo $this->navigation()->menu()->setContainer($this->navigation)->render()
        ?>
    </div>
<?php endif; ?>

<br />
<br />

<div class='admin_search'>
    <?php echo $this->formFilter->render($this) ?>
</div>

<br />

<div class='admin_results'>
    <div>
        <?php $count = $this->paginator->getTotalItemCount() ?>
        <?php echo $this->translate(array("%s documents found", "%s documents found", $count), $this->locale()->toNumber($count))
        ?>
    </div>
    <div>
        <?php
        echo $this->paginationControl($this->paginator, null, null, array(
            'pageAsQuery' => true,
                //'query'       => $this->formValues,
                //'params' => $this->formValues,
        ));
        ?>
    </div>
</div>

<br />

<div class="admin_table_form">
    <table class='admin_table'>
        <thead>
            <tr>
                <th style='width: 1%;'><?php echo $this->translate("ID") ?></th>
                <th><?php echo $this->translate("Title") ?></th>
                <th><?php echo $this->translate("Owner") ?></th>
                <th style='width: 1%;'><?php echo $this->translate("Page") ?></th>
                <th style='width: 1%;'><?php echo $this->translate("Creation Date") ?></a></th>
                <th style='width: 1%;' class='admin_table_options'><?php echo $this->translate("Options") ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($this->paginator)): ?>
                <?php foreach ($this->paginator as $item): ?>
                    <tr>
                        <td><?php echo $item->document_id ?></td>
                        <td class='admin_table_bold'>
                            <?php
                            echo $this->htmlLink('javascript:void(0)', $item->getTitle(), array('onclick' => 'embedPDF(\'' . $item->getDocument()->storage_path . '\', \'' . $item->getIdentity() . '\')'))
                            ?>
                        </td>
                        <td class='admin_table_user'>
                            <?php echo $this->htmlLink($this->item('user', $item->user_id)->getHref(), $this->item('user', $item->user_id)->username, array('target' => '_blank')) ?>
                        </td>
                        <td class='admin_table_email'>
                            <?php //if ($this->item('sitepage_page', $item->page_id)) : ?>
                                <?php //echo $this->htmlLink($this->item('sitepage_page', $item->page_id)->getHref(), $this->item('sitepage_page', $item->page_id)->getTitle(), array('target' => '_blank')) ?>
                            <?php //else : ?>
                                Deleted
                            <?php //endif ?>
                        </td>
                        <td class="admin_table_centered nowrap">
                            <?php echo $this->locale()->toDateTime($item->creation_date) ?>
                        </td>
                        <td class='admin_table_options'>
                            <a class='smoothbox' href='<?php echo $this->url(array('action' => 'delete', 'id' => $item->document_id)); ?>'>
                                <?php echo $this->translate("delete") ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function embedPDF(file, id) {
        var iframe = new Element('iframe', {
            src: file,
            width: '98%',
            height: '100%',
            onload: 'Smoothbox.instance.doAutoResize($("global_content"))'
        });
        Smoothbox.open(iframe);
    }
</script>