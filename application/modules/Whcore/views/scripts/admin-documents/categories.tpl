<div class="clear">
    <div class="settings">
        <form class="global_form">
            <div>
                <h3>Documents Categories</h3>          
                <table class="admin_table">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($this->categories as $cat) : ?>
                        <tr>
                            <td><?php echo $cat->title ?></td>
                            <td>
                                <a class="smoothbox" href="<?php echo $this->url(array('action' => 'edit-cat', 'id' => $cat->category_id)) ?>">edit</a>                        |
                                <a class="smoothbox" href="<?php echo $this->url(array('action' => 'delete-cat', 'id' => $cat->category_id)) ?>">delete</a>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <br>

                <a style="background-image: url(/application/modules/Core/externals/images/admin/new_category.png);" class="smoothbox buttonlink" href="<?php echo $this->url(array('action' => 'create-cat')) ?>">Add New Category</a>
            </div>
        </form>
    </div>
</div>