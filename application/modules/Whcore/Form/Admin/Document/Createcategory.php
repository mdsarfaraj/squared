<?php

class Whcore_Form_Admin_Document_Createcategory extends Engine_Form {

    public function init() {
        $this->setTitle('Create Category')
                ->setAttrib('class', 'global_form_popup');

        $this->addElement('text', 'title', array(
            'label'      => 'Category Title',
            'allowEmpty' => false,
            'required'   => true,
            'filters'    => array(
                'stringTrim',
                'stripTags'
            ),
        ));

        $this->addElement('Button', 'submit', array(
            'type'       => 'submit',
            'label'      => 'Create Category',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label'       => 'cancel',
            'link'        => true,
            'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
            'href'        => '',
            'onclick'     => 'parent.Smoothbox.close();',
            'decorators'  => array(
                'ViewHelper'
            )
        ));
        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    }

}
