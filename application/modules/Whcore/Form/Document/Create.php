<?php

class Whcore_Form_Document_Create extends Engine_Form {

    public function init() {
        $this->setTitle('Add Document')
                ->setAttrib('class', 'global_form_popup');

        $this->addElement('text', 'title', array(
            'label'      => 'Title',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => array(
                'stringTrim',
                'stripTags'
            )
        ));

        $this->addElement('textarea', 'description', array(
            'label'   => 'Description',
            'filters' => array(
                'stringTrim',
                'stripTags'
            )
        ));
        
        $this->addELement('select', 'category_id', array(
            'label' => 'Category',
            'required' => true,
            'allowEmpty' => false
        ));
        
        $this->category_id->addMultiOption('', '');
        foreach (Engine_Api::_()->getDbtable('categories', 'whcore')->fetchAll() as $cat) {
            $this->category_id->addMultiOption($cat->category_id, $cat->title);
        }

        $this->addElement('file', 'document', array(
            'label'      => 'Document',
            'required'   => true,
            'allowEmpty' => false,
            'validators' => array(
                array('MimeType', false, array('application/pdf')),
                array('Extension', false, array('pdf'))
            )
        ));

        $this->addElement('Button', 'submit', array(
            'type'       => 'submit',
            'label'      => 'Add Document',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label'       => 'cancel',
            'link'        => true,
            'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
            'href'        => '',
            'onclick'     => 'parent.Smoothbox.close();',
            'decorators'  => array(
                'ViewHelper'
            )
        ));
        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    }

}
