<?php

class Whcore_Plugin_Task_MassMessages extends Core_Plugin_Task_Abstract {

    public function execute() {

        $viewer = Engine_Api::_()->user()->getViewer();
        $table = Engine_Api::_()->getDbtable('massmessages', 'whcore');
        $result = $table->fetchAll($table->select()->limit(25));

        $db = Engine_Api::_()->getDbtable('messages', 'messages')->getAdapter();
        $db->beginTransaction();

        try {

            foreach ($result as $ind => $data) {

                $attachmentData = unserialize($data->attachment);
                $attachment = null;

                if (!empty($attachmentData) && !empty($attachmentData['type'])) {
                    $type = $attachmentData['type'];
                    $config = null;
                    foreach (Zend_Registry::get('Engine_Manifest') as $d) {
                        if (!empty($d['composer'][$type])) {
                            $config = $d['composer'][$type];
                        }
                    }
                    if ($config) {
                        $plugin = Engine_Api::_()->loadClass($config['plugin']);
                        $method = 'onAttach' . ucfirst($type);
                        $attachment = $plugin->$method($attachmentData);
                        $parent = $attachment->getParent();
                        if ($parent->getType() === 'user') {
                            $attachment->search = 0;
                            $attachment->save();
                        } else {
                            $parent->search = 0;
                            $parent->save();
                        }
                    }
                }

                $viewer = Engine_Api::_()->user()->getViewer();
                $user = Engine_Api::_()->user()->getUser($data->recipient_id);

                $conversation = null;
                $conversation = Engine_Api::_()->getItemTable('messages_conversation')->send(
                        $viewer, $user, $data->subject, $data->body, $attachment
                );

                Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                        $user, $viewer, $conversation, 'massmessage_new', array('object_body' => $data->notification_body)
                );

                // Increment messages counter
                Engine_Api::_()->getDbtable('statistics', 'core')->increment('messages.creations');

                // delete mass messages
                $data->delete();
            }

            // Commit
            $db->commit();
            echo 'true';
        } catch (Exception $e) {
            $db->rollBack();
            //throw $e;
        }
    }

}
