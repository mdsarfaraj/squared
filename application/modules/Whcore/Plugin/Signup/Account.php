<?php

class Whcore_Plugin_Signup_Account extends User_Plugin_Signup_Account {

    protected $_adminFormClass = 'Whcore_Form_Admin_Signup_Account';

    public function __construct() {
        $this->_formClass = 'User_Form_Signup_Account';

        $enabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
        if (in_array('honeypot', $enabledModuleNames)) {
            $this->_script = array('account.tpl', 'honeypot');
            $this->_formClass = 'Honeypot_Form_Signup_Account';
        }
    }

    public function getForm() {
        $form = parent::getForm();

        $settings = Engine_Api::_()->getApi('settings', 'core');
        if ($settings->getSetting('user.signup.random', 0) != 0 ||
                !empty($_SESSION['facebook_signup']) ||
                !empty($_SESSION['janrain_signup']) ||
                !empty($_SESSION['wh_signup'])) {

            //$form->removeElement('password');
            //$form->removeElement('passconf');
        }

        $this->_form = $form;
        return $form;
    }

    public function onView() {
        parent::onView();

        // Attempt to preload information
        if( !empty($_SESSION['facebook_signup']) ) {
          try {
            $facebookTable = Engine_Api::_()->getDbtable('facebook', 'user');
            $facebook = $facebookTable->getApi();
            $settings = Engine_Api::_()->getDbtable('settings', 'core');
            if( $facebook && $settings->core_facebook_enable ) {
              // Get email address
              $apiInfo = $facebook->api('/me?fields=name,email'); // @TODO: Temporarily store FB user data session
              // General
              $form = $this->getForm();

              // email prefill
                if (Engine_Api::_()->hasModuleBootstrap('honeypot'))
                    $emailElement = $form->{$_SESSION['hpem']};
                elseif($form->getElement('email') && !$form->getValue()){
                    $emailElement = $form->getElement('email');
                }

                if ($emailElement && !$emailElement->getValue()){
                    $emailElement->setValue($apiInfo['email']);
                }
              if( ($usernameEl = $form->getElement('username')) && !$usernameEl->getValue() ) {
                $usernameEl->setValue(preg_replace('/[^A-Za-z]/', '', $apiInfo['name']));
              }
            }
          } catch( Exception $e ) {
            // Silence?
          }
        }

        if (!empty($_SESSION['wh_signup']) && Engine_Api::_()->hasModuleBootstrap($_SESSION['wh_signup']['type'])) {
            try {
                $data = $_SESSION['wh_signup']['data'];
                $form = $this->getForm();

                if (isset($data['email']) && !empty($data['email'])) {
                    if (Engine_Api::_()->hasModuleBootstrap('honeypot'))
                        $emailElement = $form->{$_SESSION['hpem']};
                    else
                        $emailElement = $form->email;

                    if ($emailElement && !$emailElement->getValue())
                        $emailElement->setValue($data['email']);
                }

                if ((isset($data['username']) && !empty($data['username'])) && $usernameElement = $form->getElement('username')) {

                    $username = preg_replace("/[^a-zA-Z0-9]/", "", $data['username']);
                    $usernameElement->setValue($username);
                }
            } catch (Exception $e) {
                // Silence?
            }
        }
    }

    public function onSubmit(Zend_Controller_Request_Abstract $request) {
        if ($this->_formClass == 'Honeypot_Form_Signup_Account') {
            $honeypot = $_SESSION['hpem'];
            $postData = $request->getPost();

            if (isset($postData['pc_field']) && $postData['pc_field'] == 'off') {
                $postData['passconf'] = $postData['password'];
            }

            $honeypot_fields = array('email', 'first', 'fname', 'firstname', 'middleinitial', 'middle name', 'middlename', 'middle', 'last',
                'lname', 'lastname', 'surname', 'birthday', 'born', 'jobtitle', 'street', 'streetaddress', 'address1', 'address', 'city', 'state', 'zip', 'zipcode',
                'postalcode', 'country', 'homephone', 'eveningphone', 'evening phone', 'homeareacode', 'eveningareacode', 'workphone', 'work phone', 'dayphone',
                'daytimephone', 'companyphone', 'businessphone', 'workareacode', 'dayareacode', 'companyareacode', 'businessareacode', 'mobilephone', 'cellphone',
                'mobileareacode', 'cellareacode', 'pagerphone', 'pagerareacode', 'areacode', 'phone', 'fax', 'organization', 'company'); // 50 items

            $table = Engine_Api::_()->getDbTable('stat', 'honeypot');
            $db = $table->getAdapter();

            $ipObj = new Engine_IP();
            $ip = $ipObj->toIPv4();

            foreach ($honeypot_fields as $value) {
                if (isset($_POST[$value]) && !empty($_POST[$value])) {
                    $fields[$value] = $_POST[$value];
                }
            }


            if (!empty($fields)) {

                $db->beginTransaction();

                try {
                    $row = $table->createRow();
                    $row->fields = serialize($fields);
                    $row->ip = $ip;
                    $row->date = date('Y-m-d H:00:00');
                    $row->type = 'signup';
                    $row->save();
                    $db->commit();
                } catch (Exception $e) {

                }

                die(Zend_Registry::get('Zend_Log')->log('Bot signup was prevented by Honeypot plugin (' . serialize($fields) . ')', Zend_Log::INFO));
            }

            // Form was valid
            if ($this->getForm()->isValid($postData)) {
                $values = $this->getForm()->getValues();
                $values['email'] = $values[$honeypot];
                unset($values[$honeypot]);

                $this->getSession()->data = $values;

                //profile type hack
                $accountSession = new Zend_Session_Namespace('User_Plugin_Signup_Account');
                $accountSession->data = $values;

                $this->setActive(false);
                $this->onSubmitIsValid();
                return true;
            }
            // Form was not valid
            else {
                $this->getSession()->active = true;
                $this->onSubmitNotIsValid();
                return false;
            }
        } else {
            if ($this->getForm()->isValid($request->getPost())) {
                $values = $this->getForm()->getValues();

                $this->getSession()->data = $values;

                //profile type hack
                $accountSession = new Zend_Session_Namespace('User_Plugin_Signup_Account');
                $accountSession->data = $values;
            }

            parent::onSubmit($request);
            parent::onSubmit($request);
        }
    }

}
