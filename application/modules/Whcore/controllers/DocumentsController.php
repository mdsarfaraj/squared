<?php

class Whcore_DocumentsController extends Core_Controller_Action_User {

    public function createAction() {
        $this->view->form = $form = new Whcore_Form_Document_Create();

        if (!$this->getRequest()->isPost())
            return;

        if (!$form->isValid($this->getRequest()->getPost()))
            return;

        $values = $form->getValues();

        $form->document->receive();
        $mainName = $form->document->getFileName();
        $file = array(
            'tmp_name' => $mainName,
            'name'     => basename($mainName)
        );

        $table = Engine_Api::_()->getItemTable('whcore_document');
        $document = $table->createRow();
        $document->setFromArray(array(
            'title'       => $values['title'],
            'description' => $values['description'],
            'user_id'     => Engine_Api::_()->user()->getViewer()->getIdentity(),
                //'page_id' => $page->getIdentity(),
            'category_id' => $values['category_id']
        ));
        $document->save();

        $params = array(
            'parent_id'   => $document->getIdentity(),
            'parent_type' => 'whcore_document',
        );

        $documentFile = Engine_Api::_()->getDbtable('files', 'storage')->createFile($mainName, $params);
        $documentFile->mime_major = 'pdf';
        $documentFile->mime_minor = 'pdf';
        $documentFile->save();
        
        $document->file_id = $documentFile->file_id;
        $document->save();

        if (class_exists('Imagick', false)) {
            $file_tmp = $documentFile->temporary();
            $file_tmp_thumb = $file_tmp . '.jpg';
            $im = new imagick($file_tmp . '[0]');
            $im->setImageFormat("jpg");
            $im->writeImage($file_tmp_thumb);
            $im->destroy();
            $image = Engine_Image::factory(array('quality' => 100));
            $image->open($file_tmp_thumb)
                    ->resize('720', 900)
                    ->write($file_tmp_thumb)
                    ->destroy();
            $thumbFileRow = Engine_Api::_()->storage()->create($file_tmp_thumb, array(
                'parent_id'      => $document->getIdentity(),
                'parent_type'    => 'whcore_document',
                'type'           => 'thumb.etalon',
                'parent_file_id' => $documentFile->file_id));

            file_exists($file_tmp_thumb) && unlink($file_tmp_thumb);
            file_exists($file_tmp) && unlink($file_tmp);
        }

        @unlink($file['tmp_name']);
        @unlink($mainName);
        
        return $this->_forward('success', 'utility', 'core', array(
            'messages' => array('Document has been uploaded.'),
            'parentRefresh' => 1500
        ));
    }

}
