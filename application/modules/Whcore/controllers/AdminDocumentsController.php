<?php

class Whcore_AdminDocumentsController extends Core_Controller_Action_Admin {

    public function manageAction() {
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('whcore_admin_documents', array(), 'whcore_admin_documents_manage');

        $this->view->formFilter = new Whcore_Form_Admin_Document_Filter();
        $params = $this->_getAllParams();

        $table = Engine_Api::_()->getItemTable('whcore_document');
        $select = $table->select();
        $select->from('engine4_whcore_documents');

        if (isset($params['title']) && $params['title'] != '')
            $select->where('engine4_whcore_documents.title like ?', '%' . $params['title'] . '%');

        if (isset($params['owner']) && $params['owner'] != '')
            $select->setIntegrityCheck(false)
                    ->joinLeftUsing('engine4_users', 'user_id')
                    ->where('engine4_users.displayname like ?', '%' . $params['owner'] . '%');

        if (isset($params['page']) && $params['page'] != '')
            $select->setIntegrityCheck(false)
                    ->joinLeft('engine4_page_pages', 'engine4_page_pages.owner_id = engine4_whcore_documents.user_id')
                    ->where('engine4_page_pages.title like ?', '%' . $params['page'] . '%');

        $select->order('engine4_whcore_documents.document_id DESC');

        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage(20);
    }

    public function deleteAction() {
        $this->view->form = $form = new Whcore_Form_Admin_Document_Delete();
        $form->setAction($this->view->url(array('format' => 'smoothbox')));

        $id = $this->_getParam('id');
        if (!$id)
            return;

        $document = Engine_Api::_()->getItemTable('whcore_document')->find($id)->current();

        if (!$document)
            return;

        if ($this->getRequest()->isPost()) {
            $document->delete();

            $this->_forward('success', 'utility', 'core', array(
                'messages'       => array('Document has been deleted.'),
                'parentRefresh'  => 1500,
                'smoothboxClose' => 1500
            ));
        }
    }

    public function categoriesAction() {
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('whcore_admin_documents', array(), 'whcore_admin_documents_categories');

        $table = Engine_Api::_()->getDbtable('categories', 'whcore');
        $select = $table->select();
        $select->order('category_id ASC');

        $this->view->categories = $table->fetchAll($select);
    }

    public function createCatAction() {
        $this->view->form = $form = new Whcore_Form_Admin_Document_Createcategory();

        if (!$this->getRequest()->isPost())
            return;

        if (!$form->isValid($this->getRequest()->getPost()))
            return;

        $values = $form->getValues();

        $table = Engine_Api::_()->getDbtable('categories', 'whcore');
        $row = $table->createRow();
        $row->title = $values['title'];
        $row->save();

        $this->_forward('success', 'utility', 'core', array(
            'messages'       => array('Category has been created.'),
            'parentRefresh'  => 1500,
            'smoothboxClose' => 1500
        ));
    }

    public function editCatAction() {
        $this->view->form = $form = new Whcore_Form_Admin_Document_Createcategory();
        $form->setTitle('Edit Category');
        $form->submit->setLabel('Save Changes');
        $form->setAction($this->view->url(array('format' => 'smoothbox')));

        $id = $this->_getParam('id');
        if (!$id)
            return;

        $category = Engine_Api::_()->getDbtable('categories', 'whcore')->find($id)->current();
        $form->populate($category->toArray());

        if (!$this->getRequest()->isPost())
            return;

        if (!$form->isValid($this->getRequest()->getPost()))
            return;

        $values = $form->getValues();
        $category->setFromArray($values);
        $category->save();

        $this->_forward('success', 'utility', 'core', array(
            'messages'       => array('Changes have been saved.'),
            'parentRefresh'  => 1500,
            'smoothboxClose' => 1500
        ));
    }

    public function deleteCatAction() {
        $this->view->form = $form = new Whcore_Form_Admin_Document_Delete();
        $form->setTitle('Delete Category');
        $form->setDescription('Are you sure you want to delete this category?');
        $form->submit->setLabel('Delete Category');
        $form->setAction($this->view->url(array('format' => 'smoothbox')));

        $id = $this->_getParam('id');
        if (!$id)
            return;

        $category = Engine_Api::_()->getDbtable('categories', 'whcore')->find($id)->current();

        if (!$this->getRequest()->isPost())
            return;

        if (!$form->isValid($this->getRequest()->getPost()))
            return;

        $category->delete();

        $this->_forward('success', 'utility', 'core', array(
            'messages'       => array('Category has been deleted.'),
            'parentRefresh'  => 1500,
            'smoothboxClose' => 1500
        ));
    }

}
