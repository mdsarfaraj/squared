ALTER TABLE  `engine4_users` ADD  `unsubscribe` BOOLEAN NOT NULL ;
ALTER TABLE  `engine4_users` ADD  `subscribe_hash` VARCHAR( 64 ) NOT NULL ;

INSERT INTO `engine4_activity_notificationtypes` (`type` ,`module` ,`body` ,`is_request` ,`handler` ,`default`) VALUES (
'massmessage_new',  'whcore',  '{item:$subject} has sent you a {item:$object:message}. {var:$object_body}',  '0',  '',  '1');

INSERT INTO  `engine4_core_mailtemplates` (`type` ,`module` ,`vars`)
VALUES ('notify_massmessage_new',  'whcore', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]');

INSERT INTO  `engine4_core_tasks` (`title` ,`module` ,`plugin`,`timeout`,`processes`,`semaphore`,`started_last`,`started_count`,`completed_last`,`completed_count`,`failure_last`,`failure_count`,`success_last`,`success_count`)
VALUES ('Send Mass Messages',  'whcore', 'Whcore_Plugin_Task_MassMessages', 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DROP TABLE IF EXISTS `engine4_whcore_massmessages`;
CREATE TABLE IF NOT EXISTS `engine4_whcore_massmessages` (
  `massmessage_id` int(11) unsigned NOT NULL auto_increment,
  `recipient_id` int(11) unsigned NOT NULL,
  `subject` varchar(225) NOT NULL,
  `body` text NOT NULL,
  `notification_body` longtext NOT NULL,
  `attachment` longtext NOT NULL,
  PRIMARY KEY  (`massmessage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;