<?php

return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'whcore',
        'version' => '4.10.3',
        'path' => 'application/modules/Whcore',
        'title' => 'WebHive Core',
        'description' => '',
        'author' => 'WebHive Team',
        'sku' => 'wh-core',
        'callback' => array(
            'path' => 'application/modules/Whcore/settings/install.php',
            'class' => 'Whcore_Installer',
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'directories' =>
        array(
            0 => 'application/modules/Whcore',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/whcore.csv',
        ),
    ),
    // Hooks ---------------------------------------------------------------------
    'hooks' => array(
        array(
            'event' => 'onRenderLayoutDefault',
            'resource' => 'Whcore_Plugin_Hooks',
        )
    ),
    // Routes ---------------------------------------------------------------------
    'routes' => array(
        'whcore_phpthumb' => array(
            'route' => 'whcore/thumb/*',
            'defaults' => array(
                'module' => 'whcore',
                'controller' => 'thumb',
                'action' => 'index'
            )
        )
    )
);
?>