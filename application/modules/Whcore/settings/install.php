<?php

class Whcore_Installer extends Engine_Package_Installer_Module {

    public function onInstall() {
        $db = $this->getDb();

        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Account'), array('class = "Honeypot_Plugin_Signup_Account" OR class = "User_Plugin_Signup_Account" OR class = "Linkedin_Plugin_Signup_Account" OR class = "Vklogin_Plugin_Signup_Account"'));
        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Fields'), array('class = "Linkedin_Plugin_Signup_Fields" OR class = "User_Plugin_Signup_Fields" OR class = "Vklogin_Plugin_Signup_Fields"'));
        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Photo'), array('class = "Linkedin_Plugin_Signup_Photo" OR class = "User_Plugin_Signup_Photo" OR class = "Vklogin_Plugin_Signup_Photo" OR class = "Avatar_Plugin_Signup_Photo"'));

        parent::onInstall();
    }

    public function onEnable() {
        $db = $this->getDb();

        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Account'), array('class = "Honeypot_Plugin_Signup_Account" OR class = "User_Plugin_Signup_Account" OR class = "Linkedin_Plugin_Signup_Account" OR class = "Vklogin_Plugin_Signup_Account"'));
        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Fields'), array('class = "Linkedin_Plugin_Signup_Fields" OR class = "User_Plugin_Signup_Fields" OR class = "Vklogin_Plugin_Signup_Fields"'));
        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Photo'), array('class = "Linkedin_Plugin_Signup_Photo" OR class = "User_Plugin_Signup_Photo" OR class = "Vklogin_Plugin_Signup_Photo" OR class = "Avatar_Plugin_Signup_Photo"'));

        parent::onEnable();
    }

    public function onDisable() {
        $db = $this->getDb();

        $db->update('engine4_user_signup', array('class' => 'User_Plugin_Signup_Account'), array('class = ?' => 'Whcore_Plugin_Signup_Account'));
        $db->update('engine4_user_signup', array('class' => 'User_Plugin_Signup_Fields'), array('class = ?' => 'Whcore_Plugin_Signup_Fields'));
        $db->update('engine4_user_signup', array('class' => 'User_Plugin_Signup_Photo'), array('class = ?' => 'Whcore_Plugin_Signup_Photo'));

        parent::onDisable();
    }

}
