<?php

class Whcore_Widget_WebhiveDocumentsController extends Engine_Content_Widget_Abstract {
    
    public function indexAction() {
        $table = Engine_Api::_()->getItemTable('whcore_document');
        $select = $table->select();
        $select->where('user_id = ?', $this->view->subject()->getIdentity());
        
        $this->view->documents = $table->fetchAll($select);
    }
}

