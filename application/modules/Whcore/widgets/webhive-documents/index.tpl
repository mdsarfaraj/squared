<?php
$this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whcore/externals/scripts/pdfobject.js');
echo $this->htmlLink(array(
    'route'      => 'default',
    'module'     => 'whcore',
    'controller' => 'documents',
    'action'     => 'create'
        ), $this->translate('Add Document'), array('class' => 'add_document buttonlink smoothbox'));
?>

<div class="documents_wrapper">
    <ul>
        <?php foreach ($this->documents as $document) : ?>
            <li>
                <?php $thumb = $document->getDocument()->getChildren()->getRowMatching('type', 'thumb.etalon')->storage_path ?>
                <span class="document_thumb">
                    <a href="javascript:void(0)" onclick="embedPDF('<?php echo $document->getDocument()->storage_path ?>', '<?php echo $document->getIdentity() ?>')">
                        <img src="<?php
                        echo $this->url(array(), 'whcore_phpthumb', true) . '?' . http_build_query(array(
                            'src' => $thumb,
                            'w'   => 120,
                            'h'   => 120,
                            'cz'  => 1
                        ))
                        ?>" >
                    </a>
                </span>
                <span class="document_title"><a href="javascript:void(0)" onclick="embedPDF('<?php echo $document->getDocument()->storage_path ?>', '<?php echo $document->getIdentity() ?>')"><?php echo $document->getTitle() ?></a></span>
                <?php if (!empty($document->description)) : ?>
                    <span class="document_desc"><?php echo $this->viewMore($document->getDescription()) ?></span>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>
</div>

<script type="text/javascript">
    function embedPDF(file, id) {
        var options = {
            height: "700px",
            pdfOpenParams: {
                pagemode: "none",
                navpanes: 0,
                toolbar: 0,
                statusbar: 0,
                view: "FitV"
            }
        };
        var iframe = new Element('iframe', {
            src: file,
            width: '98%',
            height: '100%',
            onload: 'Smoothbox.instance.doAutoResize($("global_content"))'
        });
        Smoothbox.open(iframe);
    }
</script>




<ul id="sitebusinessdocument_search" class="sitebusiness_profile_list">
    <?php foreach ($this->documents as $document) : ?>
    <li>
        <a title="<?php echo $document->getTitle() ?>" href="javascript:void(0)" onclick="embedPDF('<?php echo $document->getDocument()->storage_path ?>', '<?php echo $document->getIdentity() ?>')">
            <?php $thumb = $document->getDocument()->getChildren()->getRowMatching('type', 'thumb.etalon')->storage_path ?>
            <img src="<?php
            echo $this->url(array(), 'whcore_phpthumb', true) . '?' . http_build_query(array(
                'src' => $thumb,
                'w'   => 120,
                'h'   => 120,
                'cz'  => 1
            ))
            ?>" >
        </a>								
        <div class="sitebusiness_profile_list_options">
            delete
        </div>

        <div class="sitebusiness_profile_list_info">
            <div class="sitebusiness_profile_list_title">
                <span>
                </span>
                <span>
                </span>
                <a title="<?php echo $document->getTitle() ?>" href="javascript:void(0)" onclick="embedPDF('<?php echo $document->getDocument()->storage_path ?>', '<?php echo $document->getIdentity() ?>')"><?php echo $document->getTitle() ?></a>
            </div>
            <div class="clear"></div>
            <div class="sitebusiness_profile_list_info_date">
                <?php if ($document->creation_date != '') : ?>Created <?php echo $this->timestamp($document->creation_date) ?> <?php endif ?> by <?php echo $this->item('user', $document->user_id)->toString() ?>											
            </div>

            <div class="sitebusiness_profile_list_info_des">
                <?php echo $this->viewMore($document->getDescription()) ?>
            </div>
        </div>
    </li>
    <?php endforeach ?>
</ul>