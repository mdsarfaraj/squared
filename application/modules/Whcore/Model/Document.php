<?php

class Whcore_Model_Document extends Core_Model_Item_Abstract {
    
    public function getDocument() {        
        return Engine_Api::_()->getItemTable('storage_file')->getFile($this->file_id);
    }
}
