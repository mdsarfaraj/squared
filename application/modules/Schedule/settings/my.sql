-- --------------------------------------------------------

--
-- Dumping data for table `engine4_activity_modules`
--

INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES
('schedule', 'Schedule', 'Schedule', '4.5.0', 1, 'extra');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_activity_actiontypes`
--

INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`) VALUES
('schedule_create', 'schedule', '{item:$subject} created a new schedule event:', 1, 5, 1, 3, 1, 1);

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_authorization_permissions`
--

-- ALL
-- auth_view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'auth_view' as `name`,
    5 as `value`,
    '["everyone","owner_network","owner_member_member","owner_member","owner"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN('public');
-- PUBLIC
-- view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'view' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('public');
-- USER
-- create, delete, edit, view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'create' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'delete' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'edit' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'view' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
-- ADMIN, MODERATOR
-- create, delete, edit, view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'create' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'delete' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'edit' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'schedule' as `type`,
    'view' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_menuitems`
--

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_admin_main_plugins_schedule', 'schedule', 'Schedule', '', '{"route":"admin_default","module":"schedule","controller":"settings"}', 'core_admin_main_plugins', '', 999),
('schedule_admin_main_settings', 'schedule', 'General Settings', '', '{"route":"admin_default","module":"schedule","controller":"settings"}', 'schedule_admin_main', '', 1),
('schedule_admin_main_level', 'schedule', 'Level Settings', '', '{"route":"admin_default","module":"schedule","controller":"level"}', 'schedule_admin_main', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_schedule_schedules`
--

DROP TABLE IF EXISTS `engine4_schedule_schedules`;
CREATE TABLE  `engine4_schedule_schedules` (
  `schedule_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `owner_type` varchar(255) NOT NULL DEFAULT 'user',
  `owner_id` int(11) unsigned NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `date` date NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`schedule_id`),
  KEY `owner_id` (`owner_id`) USING BTREE,
  KEY `owner_type` (`owner_type`) USING BTREE,
  KEY `date` (`date`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_schedule_settings`
--

CREATE TABLE IF NOT EXISTS `engine4_schedule_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_schedule_reminders`
--

CREATE TABLE IF NOT EXISTS `engine4_schedule_reminders` (
  `reminder_id` INT(11)  NOT NULL AUTO_INCREMENT,
  `schedule_id` INT(11)  NOT NULL,
  `user_id` INT(11)  NOT NULL,
  `date` DATETIME  NOT NULL,
  `time` INT(11)  NOT NULL,
  `executed` TINYINT(1)  NOT NULL DEFAULT 0,
  PRIMARY KEY (`reminder_id`),
  KEY `executed` (`executed`)
)
ENGINE = InnoDB;

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_settings`
--

INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES
('schedule.time.start', '08:00'),
('schedule.time.end', '22:00'),
('schedule.ampm', '0'),
('schedule.event', '0');

INSERT IGNORE INTO `engine4_core_tasks` (`title`, `module`, `plugin`, `timeout`) VALUES
('Schedule Reminder', 'schedule', 'Schedule_Plugin_Task_Reminder', 60);

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_activity_notificationtypes`
--

INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`) VALUES
('schedule_invite', 'schedule', '{item:$subject} has invited you to the event {item:$object}.', 1, 'schedule.schedule.request-event'),
('schedule_accept', 'schedule', '{item:$subject} joined the event {item:$object}', 0, ''),
('schedule_reminder', 'schedule', '{var:$object:$label} event will start in {var:$reminder}.', 0, ''),
('schedule_reminder_now', 'schedule', '{var:$object:$label} event has been started.', 0, ''),
('schedule_event_deleted', 'schedule', 'Event {var:$object:$label} associated with schedule was deleted.', 0, ''),
('schedule_event_edited', 'schedule', 'Event {var:$object:$label} associated with schedule was edited.', 0, '');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_mailtemplates`
--

INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`, `module`, `vars`) VALUES
('notify_schedule_reminder', 'schedule', '[host],[event_link],[event_remind_period],[event_time],[event_title]'),
('notify_schedule_reminder_now', 'schedule', '[host],[event_link],[event_time],[event_title]'),
('notify_schedule_invite', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]'),
('notify_schedule_accept', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]'),
('notify_schedule_event_deleted', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]'),
('notify_schedule_event_edited', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]');


CREATE TABLE `engine4_schedule_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request','schedule','{item:$subject} has added a request in your {item:$object:schedule}.',1,'schedule.schedule.request',1);
INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request_accept','schedule','{item:$subject} has confirmed your request in the {item:$object:schedule}.',0,'',1);
INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request_reject','schedule','{item:$subject} has rejected your request in the {item:$object:schedule}.',0,'',1);
