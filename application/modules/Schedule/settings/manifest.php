<?php

return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'schedule',
        'version' => '4.9.3',
        'path' => 'application/modules/Schedule',
        'title' => 'Schedule',
        'description' => 'Schedule',
        'author' => 'WebHive Team',
        'callback' => array(
            'path' => 'application/modules/Schedule/settings/install.php',
            'class' => 'Schedule_Installer',
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'directories' =>
        array(
            0 => 'application/modules/Schedule',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/schedule.csv',
        ),
    ),
    // Items ---------------------------------------------------------------------
    'items' => array(
        'schedule',
        'setting',
        'reminder',
        'request',
        'schedule_request'
    ),
    //Hooks
    'hooks' => array(
        array(
            'event' => 'onUserDeleteBefore',
            'resource' => 'Schedule_Plugin_Core',
        ),
        array(
            'event' => 'onItemDeleteBefore',
            'resource' => 'Schedule_Plugin_Core',
        ),
    ),
    // Routes
    'routes' => array(
        'schedule_specific' => array(
            'route' => 'schedule/:action/:schedule_id/*',
            'defaults' => array(
                'module' => 'schedule',
                'controller' => 'schedule',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '(edit|delete|invite|accept|reject|users|request-accept|request-reject)',
                'schedule_id' => '\d+'
            )
        ),
        'schedule_general' => array(
            'route' => 'schedule/:action/*',
            'defaults' => array(
                'module' => 'schedule',
                'controller' => 'index',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '(index|create|settings|request)',
            ),
        ),
        'schedule_view' => array(
            'route' => 'schedule/:user_id/:date/*',
            'defaults' => array(
                'module' => 'schedule',
                'controller' => 'index',
                'action' => 'list',
            ),
            'reqs' => array(
                'user_id' => '\d+',
                'date' => '(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)',
            ),
        ),
        'schedule_reminder' => array(
            'route' => 'schedule/reminder/*',
            'defaults' => array(
                'module' => 'schedule',
                'controller' => 'schedule',
                'action' => 'reminder',
            ),
        ),
        'schedule_test' => array(
            'route' => 'schedule/test/*',
            'defaults' => array(
                'module' => 'schedule',
                'controller' => 'index',
                'action' => 'test',
            ),
        ),
    ),
);
?>