CREATE TABLE `engine4_schedule_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request','schedule','{item:$subject} has added a request in your {item:$object:schedule}.',1,'schedule.schedule.request',1);
INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request_accept','schedule','{item:$subject} has confirmed your request in the {item:$object:schedule}.',0,'',1);
INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`,`module`,`body`,`is_request`,`handler`,`default`) VALUES ('schedule_request_reject','schedule','{item:$subject} has rejected your request in the {item:$object:schedule}.',0,'',1);
