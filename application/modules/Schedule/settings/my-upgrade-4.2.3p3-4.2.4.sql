INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('schedule.ampm', '0');
INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('schedule.event', '0');
INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('schedule.time.start', '08:00');
INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('schedule.time.end', '22:00');

DELETE FROM `engine4_core_settings` WHERE `name` IN ('schedule.start_time', 'schedule.end_time');

UPDATE `engine4_core_menuitems` SET `params` = '{"route":"admin_default","module":"schedule","controller":"settings"}' WHERE `name` = 'core_admin_main_plugins_schedule';

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('schedule_admin_main_settings', 'schedule', 'General Settings', '', '{"route":"admin_default","module":"schedule","controller":"settings"}', 'schedule_admin_main', '', 1),
('schedule_admin_main_level', 'schedule', 'Level Settings', '', '{"route":"admin_default","module":"schedule","controller":"level"}', 'schedule_admin_main', '', 2);

INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`) VALUES
('schedule_invite', 'schedule', '{item:$subject} has invited you to the event {item:$object}.', 1, 'schedule.schedule.request-event'),
('schedule_accept', 'schedule', '{item:$subject} joined the event {item:$object}', 0, '');