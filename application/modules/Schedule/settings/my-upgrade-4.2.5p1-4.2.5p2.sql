INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`) VALUES
('schedule_reminder', 'schedule', '{var:$object:$label} event will start in {var:$reminder}.', 0, ''),
('schedule_reminder_now', 'schedule', '{var:$object:$label} event has been started.', 0, '');