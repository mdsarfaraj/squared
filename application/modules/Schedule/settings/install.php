<?php

class Schedule_Installer extends Engine_Package_Installer_Module {

    function onInstall() {
        //
        // install content areas
        //
        $db = $this->getDb();
        $select = new Zend_Db_Select($db);

        // profile page
        $select
                ->from('engine4_core_pages')
                ->where('name = ?', 'user_profile_index')
                ->limit(1);
        $page_id = $select->query()->fetchObject()->page_id;


        // schedule.profile-schedule
        // Check if it's already been placed
        $select = new Zend_Db_Select($db);
        $select
                ->from('engine4_core_content')
                ->where('page_id = ?', $page_id)
                ->where('type = ?', 'widget')
                ->where('name = ?', 'schedule.profile-schedule')
        ;
        $info = $select->query()->fetch();

        if (empty($info)) {

            // container_id (will always be there)
            $select = new Zend_Db_Select($db);
            $select
                    ->from('engine4_core_content')
                    ->where('page_id = ?', $page_id)
                    ->where('type = ?', 'container')
                    ->limit(1);
            $container_id = $select->query()->fetchObject()->content_id;

            // middle_id (will always be there)
            $select = new Zend_Db_Select($db);
            $select
                    ->from('engine4_core_content')
                    ->where('parent_content_id = ?', $container_id)
                    ->where('type = ?', 'container')
                    ->where('name = ?', 'middle')
                    ->limit(1);
            $middle_id = $select->query()->fetchObject()->content_id;

            // tab_id (tab container) may not always be there
            $select
                    ->reset('where')
                    ->where('type = ?', 'widget')
                    ->where('name = ?', 'core.container-tabs')
                    ->where('page_id = ?', $page_id)
                    ->limit(1);
            $tab_id = $select->query()->fetchObject();
            if ($tab_id && @$tab_id->content_id) {
                $tab_id = $tab_id->content_id;
            } else {
                $tab_id = null;
            }

            // tab on profile
            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => 'schedule.profile-schedule',
                'parent_content_id' => ($tab_id ? $tab_id : $middle_id),
                'order' => 6,
                'params' => '{"title":"Schedule","nomobile":"1"}',
            ));
        }

        $tables = $db->listTables();
        if (in_array('engine4_schedule_settings', $tables)) {
            $settingsTable = $db->describeTable('engine4_schedule_settings');
            $columns = array_keys($settingsTable);

            if (in_array('start_time', $columns)) {
                $sql = "CREATE TABLE IF NOT EXISTS `engine4_schedule_settings_tmp` (
                     `setting_id` int(11) NOT NULL AUTO_INCREMENT,
                     `name` varchar(255) DEFAULT NULL,
                     `value` longtext DEFAULT NULL,
                     PRIMARY KEY (`setting_id`)
                   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
                $db->query($sql);

                $select = new Zend_Db_Select($db);
                $select->from('engine4_schedule_settings');
                $settings = $select->query()->fetchAll();
                if (count($settings)) {
                    foreach ($settings as $setting) {
                        $db->insert('engine4_schedule_settings_tmp', array(
                            'name' => (int) $setting['user_id'] . '.time.start',
                            'value' => $setting['start_time'],
                        ));
                        $db->insert('engine4_schedule_settings_tmp', array(
                            'name' => (int) $setting['user_id'] . '.time.end',
                            'value' => $setting['end_time'],
                        ));
                    }
                }
                $db->query("DROP TABLE IF EXISTS `engine4_schedule_settings`");
                $db->query("ALTER TABLE `engine4_schedule_settings_tmp` RENAME TO `engine4_schedule_settings`");
            }
        }

        parent::onInstall();
    }

}

?>