INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`, `module`, `vars`) VALUES
('notify_schedule_invite', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]'),
('notify_schedule_accept', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]');