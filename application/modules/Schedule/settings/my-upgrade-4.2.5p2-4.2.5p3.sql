INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`, `module`, `vars`) VALUES
('notify_schedule_reminder', 'schedule', '[host],[event_link],[event_remind_period],[event_time],[event_title]'),
('notify_schedule_reminder_now', 'schedule', '[host],[event_link],[event_time],[event_title]');