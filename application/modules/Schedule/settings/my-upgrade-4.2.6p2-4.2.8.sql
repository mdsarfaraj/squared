ALTER TABLE `engine4_schedule_schedules` ADD COLUMN `event_id` INT(11)  NOT NULL AFTER `date`;

INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`) VALUES
('schedule_event_deleted', 'schedule', 'Event {var:$object:$label} associated with schedule was deleted.', 0, ''),
('schedule_event_edited', 'schedule', 'Event {var:$object:$label} associated with schedule was edited.', 0, '');

INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`, `module`, `vars`) VALUES
('notify_schedule_event_deleted', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]'),
('notify_schedule_event_edited', 'schedule', '[host],[user_title], [user_link],[event_title],[event_link]');