ALTER TABLE `engine4_schedule_schedules` DROP COLUMN `reminder`;
ALTER TABLE `engine4_schedule_schedules` DROP COLUMN `notified`;

ALTER TABLE `engine4_schedule_schedules` ADD `owner_type` VARCHAR(255) DEFAULT 'user' AFTER `event_id`;
ALTER TABLE `engine4_schedule_schedules` ADD `user_id` INT(11) AFTER `owner_type`;

ALTER TABLE `engine4_schedule_schedules` DROP INDEX `start_time` ;

ALTER TABLE `engine4_schedule_schedules` ADD INDEX (`owner_type`);
ALTER TABLE `engine4_schedule_schedules` ADD INDEX `event_id` (`event_id`);
ALTER TABLE `engine4_schedule_schedules` ADD INDEX `user_id` (`user_id`);


ALTER TABLE `engine4_schedule_reminders` ADD INDEX `executed` (`executed`);
