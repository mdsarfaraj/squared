<?php

return array(
    array(
        'title' => 'Member Schedule',
        'description' => 'Displays member schedule on profile.',
        'category' => 'Schedule',
        'type' => 'widget',
        'name' => 'schedule.profile-schedule',
        'defaultParams' => array(
            'title' => 'Schedule',
        ),
        'requirements' => array(
            'subject' => 'user',
        ),
    ),
    array(
        'title' => 'Custom Schedule',
        'description' => 'Displays schedule of any member chosen by admin. If used on member home - will show owner\'s schedule by default',
        'category' => 'Schedule',
        'type' => 'widget',
        'name' => 'schedule.mini-calendar',
        'adminForm' => 'Schedule_Form_Admin_Widget_ScheduleBlock',
        'autoEdit' => true
    ),
    array(
        'title' => 'Group Schedule',
        'description' => 'Display several users schedule',
        'category' => 'Schedule',
        'type' => 'widget',
        'name' => 'schedule.multi-calendar',
        'adminForm' => 'Schedule_Form_Admin_Widget_ScheduleMultiBlock',
    ),
        )
?>