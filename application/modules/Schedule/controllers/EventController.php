<?php

Engine_Loader::autoload('application_modules_Event_controllers_IndexController');

class Schedule_EventController extends Event_IndexController {

    public function createAction() {
        if (!$this->_helper->requireUser->isValid())
            return;
        if (!$this->_helper->requireAuth()->setAuthParams('event', null, 'create')->isValid())
            return;

        // Get navigation
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('event_main');

        $viewer = Engine_Api::_()->user()->getViewer();
        $parent_type = $this->_getParam('parent_type');
        $parent_id = $this->_getParam('parent_id', $this->_getParam('subject_id'));

        if ($parent_type == 'group' && Engine_Api::_()->hasItemType('group')) {
            $this->view->group = $group = Engine_Api::_()->getItem('group', $parent_id);
            if (!$this->_helper->requireAuth()->setAuthParams($group, null, 'event')->isValid()) {
                return;
            }
        } else {
            $parent_type = 'user';
            $parent_id = $viewer->getIdentity();
        }

        // Create form
        $this->view->parent_type = $parent_type;
        $this->view->form = $form = new Event_Form_Create(array(
                    'parent_type' => $parent_type,
                    'parent_id' => $parent_id
                ));

        // Populate with categories
        $categories = Engine_Api::_()->getDbtable('categories', 'event')->getCategoriesAssoc();
        asort($categories, SORT_LOCALE_STRING);
        $categoryOptions = array('0' => '');
        foreach ($categories as $k => $v) {
            $categoryOptions[$k] = $v;
        }
        if (sizeof($categoryOptions) <= 1) {
            $form->removeElement('category_id');
        } else {
            $form->category_id->setMultiOptions($categoryOptions);
        }


        // Not post/invalid
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }


        // Process
        $values = $form->getValues();

        $values['user_id'] = $viewer->getIdentity();
        $values['parent_type'] = $parent_type;
        $values['parent_id'] = $parent_id;
        if ($parent_type == 'group' && Engine_Api::_()->hasItemType('group') && empty($values['host'])) {
            $values['host'] = $group->getTitle();
        }

        $start = strtotime($values['starttime']);
        $values['date'] = date('Y-m-d', $start);
        // Convert times
        $oldTz = date_default_timezone_get();
        date_default_timezone_set($viewer->timezone);
        $start = strtotime($values['starttime']);
        $end = strtotime($values['endtime']);
        date_default_timezone_set($oldTz);
        $values['starttime'] = date('Y-m-d H:i:s', $start);
        $values['endtime'] = date('Y-m-d H:i:s', $end);

        $db = Engine_Api::_()->getDbtable('events', 'event')->getAdapter();
        $db->beginTransaction();

        try {
            // Create event
            $table = Engine_Api::_()->getDbtable('events', 'event');
            $event = $table->createRow();

            $event->setFromArray($values);
            $event->save();
            
            $dt_elements = explode(' ',$values['starttime']);
            $date_elements = explode('-',$dt_elements[0]);
            $time_elements =  explode(':',$dt_elements[1]);
            $end_time = date('Y-m-d H:i:s', mktime($time_elements[0]+1, $time_elements[1],$time_elements[2], $date_elements[1],$date_elements[2], $date_elements[0]));
            
            $scheduleTale = Engine_Api::_()->getDbtable('schedules', 'schedule');
            $schedule = $scheduleTale->createRow();
            $schedule->owner_id = $viewer->getIdentity();
            $schedule->title = $values['title'];
            $schedule->description = $values['description'];
            $schedule->start_time = $values['starttime'];
            $schedule->end_time = $end_time;
            $schedule->date = $values['date'];
            $schedule->event_id = $event->getIdentity();
            $schedule->save();

            // Add owner as member
            $event->membership()->addMember($viewer)
                    ->setUserApproved($viewer)
                    ->setResourceApproved($viewer);

            // Add owner rsvp
            $event->membership()
                    ->getMemberInfo($viewer)
                    ->setFromArray(array('rsvp' => 2))
                    ->save();

            // Add photo
            if (!empty($values['photo'])) {
                $event->setPhoto($form->photo);
            }

            // Set auth
            $auth = Engine_Api::_()->authorization()->context;

            if ($values['parent_type'] == 'group') {
                $roles = array('owner', 'member', 'parent_member', 'registered', 'everyone');
            } else {
                $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
            }

            if (empty($values['auth_view'])) {
                $values['auth_view'] = 'everyone';
            }

            if (empty($values['auth_comment'])) {
                $values['auth_comment'] = 'everyone';
            }

            $viewMax = array_search($values['auth_view'], $roles);
            $commentMax = array_search($values['auth_comment'], $roles);
            $photoMax = array_search($values['auth_photo'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
                $auth->setAllowed($event, $role, 'comment', ($i <= $commentMax));
                $auth->setAllowed($event, $role, 'photo', ($i <= $photoMax));
            }

            $auth->setAllowed($event, 'member', 'invite', $values['auth_invite']);

            // Add an entry for member_requested
            $auth->setAllowed($event, 'member_requested', 'view', 1);

            // Add action
            $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');

            $action = $activityApi->addActivity($viewer, $event, 'event_create');

            if ($action) {
                $activityApi->attachActivity($action, $event);
            }
            // Commit
            $db->commit();

            // Redirect
            return $this->_helper->redirector->gotoRoute(array('id' => $event->getIdentity()), 'event_profile', true);
        } catch (Engine_Image_Exception $e) {
            $db->rollBack();
            $form->addError(Zend_Registry::get('Zend_Translate')->_('The image you selected was too large.'));
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

}