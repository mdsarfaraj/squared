<?php

Engine_Loader::autoload('application_modules_Event_controllers_WidgetController');

class Schedule_WidgetController extends Event_WidgetController {

    public function profileRsvpAction() {
        $this->view->form = new Event_Form_Rsvp();
        $event = Engine_Api::_()->core()->getSubject();
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$event->membership()->isMember($viewer, true)) {
            return;
        }
        $row = $event->membership()->getRow($viewer);
        $this->view->viewer_id = $viewer->getIdentity();
        if ($row) {
            $this->view->rsvp = $row->rsvp;
        } else {
            return $this->_helper->viewRenderer->setNoRender(true);
        }
        if ($this->getRequest()->isPost()) {
            $option_id = $this->getRequest()->getParam('option_id');

            $row->rsvp = $option_id;
            $row->save();

            $dt_elements = explode(' ', $event->starttime);
            $date_elements = explode('-', $dt_elements[0]);
            $time_elements = explode(':', $dt_elements[1]);
            $end_time = date('Y-m-d H:i:s', mktime($time_elements[0] + 1, $time_elements[1], $time_elements[2], $date_elements[1], $date_elements[2], $date_elements[0]));

            $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
            
            if ($option_id == 2) {
                //attending
                $select = $scheduleTable->select();
                $select->where('owner_id = ?', $viewer->getIdentity())
                        ->where('start_time = ?', $event->starttime)
                        ->where('end_time = ?', $end_time)
                        ->where('date = ?', $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2]);
                if (count($scheduleTable->fetchRow($select)) == 0) {
                    $schedule = $scheduleTable->createRow();
                    $schedule->owner_id = $viewer->getIdentity();
                    $schedule->title = $event->title;
                    $schedule->description = $event->description;
                    $schedule->start_time = $event->starttime;
                    $schedule->end_time = $end_time;
                    $schedule->date = $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2];
                    $schedule->save();
                }
            } elseif ($option_id == 0 || $option_id == 1) {
                $select = $scheduleTable->select();
                $select->where('owner_id = ?', $viewer->getIdentity())
                        ->where('start_time = ?', $event->starttime)
                        ->where('end_time = ?', $end_time)
                        ->where('date = ?', $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2]);
                if (count($schedule = $scheduleTable->fetchRow($select)) == 1) {
                    $schedule->delete();
                }
            }
        }
    }

}
