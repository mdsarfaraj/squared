<?php

class Schedule_IndexController extends Core_Controller_Action_Standard
{

    public function init()
    {
        // only show to member_level if authorized
        if (!$this->_helper->requireAuth()->setAuthParams('schedule', null, 'view')->isValid())
            return;
    }

    public function listAction()
    {
        $this->view->format = $this->getParam('format', 'html');

        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

        $timezone = $viewer->getIdentity() > 0 ? $viewer->timezone : $this->view->locale()->getTimezone();

        $owner = Engine_Api::_()->getItem('user', (int)$this->_getParam('user_id'));

        if (!Engine_Api::_()->core()->hasSubject())
            Engine_Api::_()->core()->setSubject($owner);


        if (!$this->_helper->requireSubject()->isValid())
            return;

        $date = $this->_getParam('date');

        try {
            $date = new DateTime($date, new DateTimeZone($timezone));
        } catch (Exception $exc) {
            return $this->setNoRender();
        }

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $viewDate = array('year' => $year, 'month' => $month, 'day' => $day);

        $tmp_date = clone $date;
        $tmp_date->modify('-1 day');
        $prev = $tmp_date->format('Y-m-d');

        $tmp_date = clone $date;
        $tmp_date->modify('+1 day');
        $next = $tmp_date->format('Y-m-d');

        $setting = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($owner->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));
        $step = (int)substr($setting['time_start'], 0, 2) * 6;

        $tmp_date = new DateTime($date->format('Y-m-d') . ' ' . $setting['time_start'], new DateTimeZone($timezone));
        $user_start_time = $tmp_date->format('Y-m-d H:i:s');

        $tmp_date = new DateTime($date->format('Y-m-d') . ' ' . $setting['time_end'], new DateTimeZone($timezone));
        $user_end_time = $tmp_date->format('Y-m-d H:i:s');
        
        $schedule = Engine_Api::_()->getItemTable('schedule')->getDaySchedule(array(
            'date' => $date->format('Y-m-d'),
            'owner_id' => $owner->getIdentity(),
            'owner_type' => 'user'
        ));

        list($totalStartTimeH, $totalStartTimeM) = explode(':', $setting['time_start']);
        list($totalEndTimeH, $totalEndTimeM) = explode(':', $setting['time_end']);

        $hours = array();
        for ($i = (int)$totalStartTimeH; $i <= (int)$totalEndTimeH - 1; $i++) {
            $hours[$i] = $i;
        }

        $tmp = array();

        foreach ($schedule as $scheduleItem) {
            $item = $scheduleItem->toArray();

            $item['is_request'] = false;

            if ($item['owner_id'] != $viewer->getIdentity()) {
                $notificationsTable = Engine_Api::_()->getDbTable('notifications', 'activity');
                $select = $notificationsTable->select();
                $select->where('object_type = ?', 'schedule')
                    ->where('object_id = ?', $item['schedule_id'])
                    ->where('user_id =?', $viewer->getIdentity())
                    ->where('mitigated = ?', 0);
                $invite = $notificationsTable->fetchRow($select);

                $item['has_invite'] = false;
                if ($invite != null && count($invite) == 1) {
                    $item['has_invite'] = true;
                    $item['invite'] = $invite;
                }
            }

            $item['private'] = !$this->_helper->requireAuth()->setAuthParams($scheduleItem, $viewer, 'view')->setNoForward()->isValid();

            // Convert times
            $item['start_time'] = $this->view->locale()->toTime($item['start_time'], array('format' => 'H:mm'));
            $item['end_time'] = $this->view->locale()->toTime($item['end_time'], array('format' => 'H:mm'));


            $rowspan = explode(':', $item['end_time']);
            $key = explode(':', $item['start_time']);

            if ($key[0] == 0)
                $key[0] = 24;


            //counting params
            $item['start'] = self::_toMin($item['start_time']) - $step;
            $item['end'] = (self::_toMin($item['end_time']) - $step) - ($this->_toMin($item['start_time']) - $step);

            for ($i = $rowspan[0] - 1; $i >= $key[0]; $i--) {
                unset($hours[$i]);
            }

            if ($rowspan[1] != 00) {
                unset($hours[$rowspan[0]]);
            }

            $item['short'] = false;
            $item['s'] = strtotime($item['start_time']);
            $item['e'] = $item['end_time'];

            $end_time = self::_toMin($item['end_time']);
            $start_time = self::_toMin($item['start_time']);

            if (($end_time - $start_time) <= 2) {
                $item['short'] = true;
            }
            if (strtotime($item['start_time']) >= strtotime($setting['time_start']) && strtotime($item['end_time']) <= strtotime($setting['time_end']))
                $tmp[] = $item;
            else
                $notInRange[] = $item;
        }

        $requests = Engine_Api::_()->getDbTable('requests', 'schedule')->getDayRequests($date->format('Y-m-d'));
        foreach ($requests as $request) {
            $item = $request->toArray();

            $item['is_request'] = true;
            $item['private'] = false;

            // Convert times
            $item['start_time'] = $this->view->locale()->toTime($item['start_time'], array('format' => 'H:mm'));
            $item['end_time'] = $this->view->locale()->toTime($item['end_time'], array('format' => 'H:mm'));


            $rowspan = explode(':', $item['end_time']);
            $key = explode(':', $item['start_time']);

            if ($key[0] == 0)
                $key[0] = 24;


            //counting params
            $item['start'] = self::_toMin($item['start_time']) - $step;
            $item['end'] = (self::_toMin($item['end_time']) - $step) - ($this->_toMin($item['start_time']) - $step);

            for ($i = $rowspan[0] - 1; $i >= $key[0]; $i--) {
                unset($hours[$i]);
            }

            if ($rowspan[1] != 00) {
                unset($hours[$rowspan[0]]);
            }

            $item['short'] = false;

            $end_time = self::_toMin($item['end_time']);
            $start_time = self::_toMin($item['start_time']);

            if (($end_time - $start_time) <= 2) {
                $item['short'] = true;
            }
            if (strtotime($item['start_time']) >= strtotime($setting['time_start']) && strtotime($item['end_time']) <= strtotime($setting['time_end']))
                $tmp[] = $item;
            else
                $notInRange[] = $item;
        }

        usort($tmp, array($this, 'overlappingSort'));

        $groups = array();

        foreach ($tmp as $item) {
            $found = false;

            foreach ($groups as &$group) {
                if ((strtotime($item['start_time']) >= $group['start_time'] && strtotime($item['start_time']) <= $group['end_time']) ||
                    (strtotime($item['end_time']) >= $group['start_time'] && strtotime($item['end_time']) <= $group['end_time'])
                ) {
                    $group['items'][] = $item;
                    $found = true;
                }
            }

            if (!$found) {
                $groups[] = array(
                    'start_time' => strtotime($item['start_time']),
                    'end_time' => strtotime($item['end_time']),
                    'items' => array($item),
                );
            }
        }


        $isSelf = $viewer->isSelf($owner);

        $this->view->data = array(
            'viewer' => $viewer,
            'owner' => $owner,
            'date' => $viewDate,
            'prev' => $prev,
            'next' => $next,
            'setting' => $setting,
            'hours' => $hours,
            'isSelf' => $isSelf,
            'schedule' => $groups,
            'notinrange' => isset($notInRange) ? $notInRange : null,
            'step' => $step,
        );
    }

    static function overlappingSort($a, $b)
    {
        if ($a['start'] == $b['start']) {
            $aTS = $a['end'];
            $bTS = $b['end'];
        } else {
            $aTS = $a['start'];
            $bTS = $b['start'];
        }
        if ($aTS == $bTS)
            return 0;
        return ($aTS < $bTS) ? -1 : 1;
    }

    public function createAction()
    {
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireAuth()->setAuthParams('schedule', null, 'create')->isValid())
            return;

        //prepare data
        $viewer = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($viewer);

        //getting correct date wich depends on timezone
        //$oldTz = date_default_timezone_get();
        //date_default_timezone_set($viewer->timezone);
        //$this->view->cur_day = date('d-m-Y H:i', strtotime('now'));
        //date_default_timezone_set($oldTz);

        $now = new DateTime('now', new DateTimeZone($viewer->timezone));
        $this->view->cur_day = $now->format('d-m-Y H:i');

        $data = $this->_getParam('data', '');
        $date = explode(' ', $data);

        // Create form
        $this->view->form = $form = new Schedule_Form_Create();

        $type = $this->_getParam('type', 'schedule');


        if ($date[0] != '') {
            $form->populate(array(
                'date' => $date[0],
                'start_time' => $date[1] . ':00',
                'end_time' => $date[1] + 1 . ':00'
            ));
        } else {
            $form->populate(array(
                'start_time' => '8:00',
                'end_time' => '9:00',
                'date' => $now->format('Y-m-d')
            ));
            $form->cancel->setAttrib('onclick', 'parent.Smoothbox.close();');
        }

        if ($type == 'request') {
            $schedule = Engine_Api::_()->getDbTable('requests', 'schedule')->find($this->_getParam('schedule_id'))->current();
            if (!$this->getRequest()->isPost()) {
                $form->populate($schedule->toArray());
                return;
            }
        }

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Process
        $values = $form->getValues();


        if ($type == 'request') {
            $values['owner_id'] = $schedule->owner_id;
        } else{
            $values['owner_id'] = $viewer->getIdentity();
        }


        $values['start_time'] = $values['date'] . ' ' . $values['start_time'];
        $values['end_time'] = $values['date'] . ' ' . $values['end_time'];

        if (!in_array($values['reminder'], array('0', '30', '60', '90', '120', '150', '180', '1440')))
            $values['reminder'] = '-1';

        // Convert times
        $oldTz = date_default_timezone_get();
        date_default_timezone_set($viewer->timezone);
        $start_time = strtotime($values['start_time']);
        $end_time = strtotime($values['end_time']);
        date_default_timezone_set($oldTz);
        $values['start_time'] = date('Y-m-d H:i:s', $start_time);
        $values['end_time'] = date('Y-m-d H:i:s', $end_time);

        $db = Engine_Api::_()->getDbtable('schedules', 'schedule')->getAdapter();
        $db->beginTransaction();

        try {
            // Create event
            $table = Engine_Api::_()->getDbtable('schedules', 'schedule');

//            $select = $table->select();
//            $select->where('owner_id = ?', $values['owner_id'])
//                    ->where('date = ?', $values['date'])
//                    ->where('start_time = ?', $values['start_time'])
//                    ->where('end_time = ?', $values['end_time']);
//
//            if (null != $table->fetchRow($select)) {
//                $form->start_time->addError('Event can\'t be added,  because this time field overlaps with other event.');
//                $form->end_time->addError('Event can\'t be added,  because this time field overlaps with other event.');
//                return;
//            }
//            if ($table->checkDateRange($values['start_time'], $values['end_time'], array(
//                        'date' => $values['date'],
//                        'user' => $viewer->getIdentity()
//                    )))
//                return $form->addError('This event\'s time overlaps with other event.');

            $event = $table->createRow();
            $event->setFromArray($values);
            $event->save();


            $url = $event->getHref();

            // Auth
            $auth = Engine_Api::_()->authorization()->context;
            $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'everyone');

            if (empty($values['auth_view'])) {
                $values['auth_view'] = 'everyone';
            }

            $viewMax = array_search($values['auth_view'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
            }

            // Add action
            $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');

            $action = $activityApi->addActivity($viewer, $event, 'schedule_create');

            if ($action) {
                $activityApi->attachActivity($action, $event);
            }

            if ($values['reminder'] != '-1') {
                $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
                $reminder = $reminderTable->createRow();


                if ($type == 'request') {
                    $reminder->user_id = $schedule->requester_id;
                } else {
                    $reminder->user_id = $viewer->getIdentity();
                }


                $reminder_date = strtotime($values['start_time']) - ($values['reminder'] * 60);
                $reminder->date = date('Y-m-d H:i:s', $reminder_date);
                $reminder->schedule_id = $event->getIdentity();
                $reminder->time = $values['reminder'];
                $reminder->save();
            }

            if ($values['recurring'] == 1) {
                if ($values['repeat'] <= 10) {
                    $period = 'day';
                    if (in_array($values['every'], array('wday', 'day', 'week', 'month', 'year')))
                        $period = $values['every'];

                    $skipWEnd = false;
                    if ($period == 'wday') {
                        $period = 'day';
                        $skipWEnd = true;
                    }

                    for ($c = 1; $c <= $values['repeat']; $c++) {
                        $date_tmp = new DateTime($values['start_time'], new DateTimeZone($viewer->timezone));

                        if ($values['every'] == 'month') {
                            $currentDay = $date_tmp->format('d');
                        }

                        $date_tmp->modify('+1 ' . $period);
                        $values['start_time'] = $toValidate = $date_tmp;

                        if ($values['every'] == 'month' && $date_tmp->format('d') != $currentDay) {
                            $db->rollback();
                            return $form->addError($this->view->translate('Event can\'t be added for all months'));
                        }

                        if ($skipWEnd && in_array($date_tmp->format('w'), array(6, 0))) {
                            switch ($date_tmp->format('w')) {
                                case 6:
                                    $date_tmp->modify('+2 days');
                                    $values['start_time'] = $toValidate = $date_tmp;
                                    break;
                                case 0:
                                    $date_tmp->modify('+1 ' . $period);
                                    $values['start_time'] = $toValidate = $date_tmp;
                                    break;

                                default:
                                    break;
                            }
                        }

                        $date_tmp = new DateTime($values['date'], new DateTimeZone($viewer->timezone));
                        $date_tmp->modify('+1 ' . $period);
                        $values['date'] = $date_tmp->format('Y-m-d');

                        $values['start_time'] = $values['start_time']->format('Y-m-d H:i:s');

                        $validator = new Schedule_Form_Validate_InRange();

                        if (!$validator->isValid($toValidate->format('H:i:s'), array('user' => $viewer->getIdentity(), 'date' => array('date' => $values['date'])))) {
                            $db->rollBack();
                            return $form->addError($this->view->translate('Recurring events can\'t be added because one of events overlaps with other event on %1$s', $this->view->locale()->toDate($values['date'])));
                        }

                        $date_tmp = new DateTime($values['end_time'], new DateTimeZone($viewer->timezone));
                        $date_tmp->modify('+1 ' . $period);
                        $values['end_time'] = $date_tmp->format('Y-m-d H:i:s');

                        $event = $table->createRow();

                        $event->setFromArray($values);
                        $event->save();

                        // Auth
                        $auth = Engine_Api::_()->authorization()->context;
                        $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

                        if (empty($values['auth_view'])) {
                            $values['auth_view'] = 'everyone';
                        }

                        $viewMax = array_search($values['auth_view'], $roles);

                        foreach ($roles as $i => $role) {
                            $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
                        }

                        // Add action
                        $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');

                        $action = $activityApi->addActivity($viewer, $event, 'schedule_create');

                        if ($action) {
                            $activityApi->attachActivity($action, $event);
                        }

                        if ($values['reminder'] != '-1') {
                            $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
                            $reminder = $reminderTable->createRow();

                            if ($type == 'request') {
                                $reminder->user_id = $schedule->requester_id;
                            } else {
                                $reminder->user_id = $viewer->getIdentity();
                            }


                            $reminder_date = strtotime($values['start_time']) - $values['reminder'];
                            $reminder->date = date('Y-m-d H:i:s', $reminder_date);
                            $reminder->schedule_id = $event->getIdentity();
                            $reminder->save();
                        }
                    }
                }
            }

            // Commit
            $db->commit();
            if($schedule != null){
                $schedule->delete();
            }
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }


        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => $url,
            'messages' => array(Zend_Registry::get('Zend_Translate')->_('Event has been successfully added.')),
        ));
    }

    public function requestAction()
    {
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireAuth()->setAuthParams('schedule', null, 'create')->isValid())
            return;

        //prepare data
        $viewer = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($viewer);

        $now = new DateTime('now', new DateTimeZone($viewer->timezone));
        $this->view->cur_day = $now->format('d-m-Y H:i');

        $data = $this->_getParam('data', '');
        $date = explode(' ', $data);

        $owner_id = $this->_getParam('owner');
        $owner = Engine_Api::_()->getItem('user', $owner_id);

        // Create form
        $this->view->form = $form = new Schedule_Form_Create();
        $form->removeElement('reminder');
        $form->removeElement('auth_view');
        $form->removeElement('recurring');
        $form->removeElement('repeat');
        $form->removeElement('every');
        $form->setTitle('Request Event');

        if ($date[0] != '')
            $form->populate(array(
                'date' => $date[0],
                'start_time' => $date[1] . ':00',
                'end_time' => $date[1] + 1 . ':00'
            ));
        else {
            $form->populate(array(
                'start_time' => '8:00',
                'end_time' => '9:00',
                'date' => $now->format('Y-m-d')
            ));
            $form->cancel->setAttrib('onclick', 'parent.Smoothbox.close();');
        }

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Process
        $values = $form->getValues();

        $values['owner_id'] = $owner->getIdentity();
        $values['requester_id'] = $viewer->getIdentity();
        $values['start_time'] = $values['date'] . ' ' . $values['start_time'];
        $values['end_time'] = $values['date'] . ' ' . $values['end_time'];

        // Convert times
        $oldTz = date_default_timezone_get();
        date_default_timezone_set($viewer->timezone);
        $start_time = strtotime($values['start_time']);
        $end_time = strtotime($values['end_time']);
        date_default_timezone_set($oldTz);
        $values['start_time'] = date('Y-m-d H:i:s', $start_time);
        $values['end_time'] = date('Y-m-d H:i:s', $end_time);

        $db = Engine_Api::_()->getDbtable('schedules', 'schedule')->getAdapter();
        $db->beginTransaction();

        try {
            // Create event
            $table = Engine_Api::_()->getDbtable('requests', 'schedule');

            $event = $table->createRow();

            $event->setFromArray($values);
            $event->save();

            $url = $event->getHref();

            //add notification
            $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
            $notifyApi->addNotification($owner, $viewer, $event, 'schedule_request', array(
                'user_title' => $viewer->getTitle(),
                'event_title' => $event->getTitle(),
                'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $event->getHref()
            ));

            // Commit
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }


        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => $url,
            'messages' => array(Zend_Registry::get('Zend_Translate')->_('Event has been successfully requested.')),
        ));
    }

    public function settingsAction()
    {
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireAuth()->setAuthParams('schedule', null, 'create')->isValid())
            return;

        //prepare data
        $viewer = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($viewer);

        $user_id = $viewer->getIdentity();

        $this->view->form = $form = new Schedule_Form_Settings(array(
            'parent_id' => $user_id,
        ));

        $setting = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($viewer->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));


        $form->populate($setting);

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $values = $form->getValues();
        //$values['user_id'] = $viewer->getIdentity();

        Engine_Api::_()->getApi('settings', 'schedule')->setFlatSetting((string)$viewer->getIdentity(), $values);


        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your options have been saved.');
        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => $viewer->getHref() . '/tab/schedule.profile-schedule',
            'messages' => Array($this->view->message),
            'tab' => 'schedule.profile-schedule'
        ));
    }

    protected function _toMin($val)
    {
        if (is_string($val)) {
            $val = explode(':', $val);
            return ((int)$val[0] * 6 + (int)$val[1] / 10);
        }

        if (isArray($val)) {
            return ((int)$val[0] * 6 + (int)$val[1] / 10);
        }
    }

}
