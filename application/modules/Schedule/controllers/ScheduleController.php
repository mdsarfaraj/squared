<?php

class Schedule_ScheduleController extends Core_Controller_Action_Standard
{

    public function init()
    {
        $id = $this->_getParam('schedule_id', $this->_getParam('id', null));
        if ($id) {
            $schedule = Engine_Api::_()->getItem('schedule', $id);
            if ($schedule) {
                Engine_Api::_()->core()->setSubject($schedule);
            }
        }
    }

    //delete
    public function deleteAction()
    {

        $viewer = Engine_Api::_()->user()->getViewer();

        $type = $this->_getParam('type', 'schedule');

        if($type == 'request'){
            $this->view->event = $schedule = Engine_Api::_()->getDbTable('requests', 'schedule')->find($this->_getParam('schedule_id'))->current();
        } else {
            $this->view->event = $schedule = Engine_Api::_()->getItem('schedule', $this->_getParam('schedule_id'));
        }

        $url = $schedule->getHref();
        $uid = $this->_getParam('uid', 0);
        $date = $this->_getParam('date', 0);

        $this->view->parentRefresh = false;

            if ($type != 'request' && !$this->_helper->requireAuth()->setAuthParams($schedule, null, 'delete')->isValid()){
                return;
            }

        $date = $this->_getParam('date');
        // In smoothbox
        $this->_helper->layout->setLayout('default-simple');

        // Make form
        $this->view->form = $form = new Schedule_Form_Delete();
        $form->params = array('uid' => $uid, 'date' => $date);

        if (!$schedule) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_("Event doesn't exists or not authorized to delete");
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }

        $this->view->parentRefresh = true;
        $this->view->owner_id = $schedule->owner_id;
        $this->view->date = $schedule->date;

        $db = $schedule->getTable()->getAdapter();
        $db->beginTransaction();

        try {
            $schedule->delete();
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    //edit
    public function editAction()
    {
        $schedule_id = $this->_getParam('schedule_id');
        $this->view->event = $schedule = Engine_Api::_()->getItem('schedule', $schedule_id);
        $url = $schedule->getHref();
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!($this->_helper->requireAuth()->setAuthParams($schedule, null, 'edit')->isValid() || $schedule->isOwner($viewer))) {
            return;
        }

        $this->view->parentRefresh = false;

        // Create form
        $this->view->form = $form = new Schedule_Form_Edit(array('owner_id' => $schedule->owner_id));

        $oldTz = date_default_timezone_get();

        if (!$this->getRequest()->isPost()) {

            //getting correct date wich depends on timezone
            date_default_timezone_set($viewer->timezone);
            $now = new DateTime('now', new DateTimeZone($viewer->timezone));
            $this->view->cur_day = $now->format('d-m-Y H:i');


            // Populate auth
            $auth = Engine_Api::_()->authorization()->context;
            $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

            foreach ($roles as $role) {
                if (isset($form->auth_view->options[$role]) && $auth->isAllowed($schedule, $role, 'view')) {
                    $form->auth_view->setValue($role);
                }
            }

            $form->populate($schedule->toArray());

            // Convert and re-populate times
            date_default_timezone_set($oldTz);
            $start = strtotime($schedule->start_time);
            $end = strtotime($schedule->end_time);
            date_default_timezone_set($viewer->timezone);
            $start = date('H:i', $start);
            $end = date('H:i', $end);

            $form->populate(array(
                'start_time' => $start,
                'end_time' => $end,
            ));
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $this->view->parentRefresh = true;

        // Process
        $values = $form->getValues();

        $values['start_time'] = $values['date'] . ' ' . $values['start_time'];
        $values['end_time'] = $values['date'] . ' ' . $values['end_time'];

        // Convert times
        date_default_timezone_set($viewer->timezone);
        $start_time = strtotime($values['start_time']);
        $end_time = strtotime($values['end_time']);
        date_default_timezone_set($oldTz);
        $values['start_time'] = date('Y-m-d H:i:s', $start_time);
        $values['end_time'] = date('Y-m-d H:i:s', $end_time);

        if (!in_array($values['reminder'], array('-1', '0', '30', '60', '90', '120', '150', '180', '1440')))
            $values['reminder'] = '-1';

        // Process
        $db = Engine_Api::_()->getItemTable('schedule')->getAdapter();
        $db->beginTransaction();

        try {

            $table = Engine_Api::_()->getDbtable('schedules', 'schedule');

            $select = $table->select();
            $select->where('owner_id = ?', Engine_Api::_()->user()->getViewer()->getIdentity())
                ->where('date = ?', $values['date'])
                ->where('start_time = ?', $values['start_time'])
                ->where('end_time = ?', $values['end_time']);

            // Set event info
            $schedule->setFromArray($values);
            $schedule->save();

            // Process privacy
            $auth = Engine_Api::_()->authorization()->context;

            $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

            $viewMax = array_search($values['auth_view'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($schedule, $role, 'view', ($i <= $viewMax));
            }

            if ($values['reminder'] != '-1') {
                $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
                $reminder = $reminderTable->createRow();
                $reminder->user_id = $viewer->getIdentity();

                $reminder_date = strtotime($values['start_time']) - ($values['reminder'] * 60);
                $reminder->date = date('Y-m-d H:i:s', $reminder_date);
                $reminder->schedule_id = $schedule->getIdentity();
                $reminder->time = $values['reminder'];
                $reminder->save();
            }

            // Commit
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }


        $db->beginTransaction();
        try {
            // Rebuild privacy
            $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
            foreach ($actionTable->getActionsByObject($schedule) as $action) {
                $actionTable->resetActivityBindings($action);
            }

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    public function reminderAction()
    {
        $viewer = Engine_Api::_()->user()->getViewer();

        $timezone = $this->view->locale()->getTimezone();

        if ($viewer->getIdentity() > 0) {
            $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
            $select = $scheduleTable->select();

            $select->where('owner_id = ?', $viewer->getIdentity())
                ->where('reminder != ?', -1)
                ->where('notified = ?', 0);
            $schedule = $scheduleTable->fetchAll($select);

            $oldTz = date_default_timezone_get();
            date_default_timezone_set($timezone);

            $now = strtotime('now');

            foreach ($schedule as $item) {
                $end_time = strtotime($item->end_time);

                if (($item->reminder * 60) >= ($end_time - $now)) {
                    switch ($item->reminder) {
                        case 30:
                            $time = '30 Minutes';
                            break;
                        case 60:
                            $time = '1 Hour';
                            break;
                        case 90:
                            $time = '1 Hour and 30 Minutes ';
                            break;
                        case 120:
                            $time = '2 Hour Before';
                            break;
                        case 150:
                            $time = '2 Hour and 30 Minutes';
                            break;
                        case 180:
                            $time = '3 Hour';
                            break;
                        case 1440:
                            $time = '1 Day';
                            break;
                    }

                    if (isset($time))
                        Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                            $viewer, $viewer, $item, 'schedule_reminder', array('reminder' => $time));
                    else
                        Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                            $viewer, $viewer, $item, 'schedule_reminder_now');

                    $item->notified = 1;
                    $item->save();
                }
            }

            date_default_timezone_set($oldTz);
        }
    }

    public function inviteAction()
    {

        $this->_helper->layout->setLayout('default-simple');

        $this->view->schedule_id = $schedule_id = $this->_getParam('schedule_id');
        $this->view->event = $schedule = Engine_Api::_()->getItem('schedule', $schedule_id);
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!($this->_helper->requireAuth()->setAuthParams($schedule, null, 'edit')->isValid() || $schedule->isOwner($viewer))) {
            return;
        }

        $this->view->parentRefresh = false;

        // Create form
        $this->view->form = $form = new Schedule_Form_Invite();

        // Check method/data
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $this->view->parentRefresh = true;

        $values = $form->getValues();
        $recipients = preg_split('/[,. ]+/', $values['toValues']);

        $recipients = array_unique($recipients);
        $recipients = array_slice($recipients, 0, 3);
        $recipientsUsers = Engine_Api::_()->getItemMulti('user', $recipients);

        $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');

        foreach ($recipientsUsers as $recipientUser) {
            $notifyApi->addNotification($recipientUser, $viewer, $schedule, 'schedule_invite', array(
                'user_title' => $viewer->getTitle(),
                'event_title' => $schedule->getTitle(),
                'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $schedule->getHref()
            ));
        }
    }

    public function requestEventAction()
    {
        $this->view->notification = $notification = $this->_getParam('notification');
    }

    public function requestAction()
    {
        $this->view->notification = $notification = $this->_getParam('notification');
    }

    public function rejectAction()
    {
        // Check auth
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireSubject('schedule')->isValid())
            return;


        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->core()->getSubject();

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = true;
            $this->view->message = Zend_Registry::get('Zend_Translate')->_('Invalid Method');
            return;
        }

        // Set the request as handled
        $notification = Engine_Api::_()->getDbtable('notifications', 'activity')->getNotificationByObjectAndType(
            $viewer, $subject, 'schedule_invite');
        if ($notification) {
            $notification->mitigated = true;
            $notification->save();
        }


        $this->view->status = true;
        $this->view->error = false;
        if ($this->_getParam('type', null) == 'fromSchedule')
            $message = Zend_Registry::get('Zend_Translate')->_('Invite was ignored');
        else {
            $message = Zend_Registry::get('Zend_Translate')->_('You have ignored the invite to the event %s');
            $message = sprintf($message, $subject->__toString());
        }
        $this->view->message = $message;
    }

    public function requestRejectAction() {
        // Check auth
        if (!$this->_helper->requireUser()->isValid())
            return;

        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->getItem('request', $this->_getParam('schedule_id'));
        $subject->confirmed = -1;
        $subject->save();

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = true;
            $this->view->message = Zend_Registry::get('Zend_Translate')->_('Invalid Method');
            return;
        }

        // Set the request as handled
        $notification = Engine_Api::_()->getDbtable('notifications', 'activity')->getNotificationByObjectAndType(
            $viewer, $subject, 'schedule_request');
        if ($notification) {
            $notification->mitigated = true;
            $notification->save();
        }

        $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
        $notifyApi->addNotification(Engine_Api::_()->user()->getUser($subject->requester_id), $viewer, $subject, 'schedule_request_reject', array(
            'user_title' => $viewer->getTitle(),
            'event_title' => $subject->getTitle(),
            'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $subject->getHref()
        ));


        $this->view->status = true;
        $this->view->error = false;

        $subject->delete();


        $this->view->redirect = $this->view->url(array('module' => 'user',
            'controller' => 'profile',
            'action' => 'index',
            'id' => $viewer->getIdentity(),
            'tab' => 'schedule.profile-schedule',
            'date' => $subject->date,
            'dayschedule' => 1), 'default', true);
    }

    public function requestAcceptAction()
    {
        // Check auth
        if (!$this->_helper->requireUser()->isValid())
            return;

        // Process form
        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = true;
            $this->view->message = Zend_Registry::get('Zend_Translate')->_('Invalid Method');
            return;
        }

        // Process form
        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->getItem('request', $this->_getParam('schedule_id'));
        $subject->confirmed = 1;
        $subject->save();

        // Set the request as handled
        $notification = Engine_Api::_()->getDbtable('notifications', 'activity')->getNotificationByObjectAndType(
            $viewer, $subject, 'schedule_request');
        if ($notification) {
            $notification->mitigated = true;
            $notification->save();
        }

        $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
        $notifyApi->addNotification(Engine_Api::_()->user()->getUser($subject->requester_id), $viewer, $subject, 'schedule_request_accept', array(
            'user_title' => $viewer->getTitle(),
            'event_title' => $subject->getTitle(),
            'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $subject->getHref()
        ));

        $this->view->status = true;
        $this->view->error = false;
        if ($this->_getParam('type', null) == 'fromSchedule')
            $message = Zend_Registry::get('Zend_Translate')->_('Request was accepted');
        else {
            $message = Zend_Registry::get('Zend_Translate')->_('You have accepted the request to the event %s');
            $message = sprintf($message, $subject->__toString());
        }
        $this->view->message = $message;

        $this->view->redirect = $this->view->url(array('module' => 'user',
            'controller' => 'profile',
            'action' => 'index',
            'id' => $viewer->getIdentity(),
            'tab' => 'schedule.profile-schedule',
            'date' => $subject->date,
            'dayschedule' => 1), 'default', true);

    }

    public function usersAction()
    {
        $value = $this->_getParam('value');
        if ($value) {
            $userTable = Engine_Api::_()->getItemTable('user');
            $select = $userTable->select();
            $select->where('displayname like ?', '%' . $value . '%');

            $data = array();
            foreach ($userTable->fetchAll($select) as $user) {
                $data[] = array(
                    'type' => 'user',
                    'id' => $user->getIdentity(),
                    'guid' => $user->getGuid(),
                    'label' => $user->getTitle(),
                    'photo' => $this->view->itemPhoto($user, 'thumb.icon'),
                    'url' => $user->getHref(),
                );
            }
            return $this->_helper->json($data);
        }
    }

}
