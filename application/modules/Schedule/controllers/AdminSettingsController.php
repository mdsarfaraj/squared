<?php

	class Schedule_AdminSettingsController extends Core_Controller_Action_Admin {

		public function indexAction() {

			$this->view->navigation = $navigation = Engine_Api::_()->getApi( 'menus', 'core' )
			                                                  ->getNavigation( 'schedule_admin_main', array(),
				                                                  'schedule_admin_main_settings' );

			$this->view->form = $form = new Schedule_Form_Admin_Settings();

			// Populate values
			$settings = Engine_Api::_()->getApi( 'settings', 'core' )->getFlatSetting( 'schedule' );
			if (!empty($settings)) {
				$form->populate( $settings );
			}
			// Check post
			if ( ! $this->getRequest()->isPost() ) {
				return;
			}

			// Check validitiy
			if ( ! $form->isValid( $this->getRequest()->getPost() ) ) {
				return;
			}

			// Process
			$values = $form->getValues();
			Engine_Api::_()->getApi( 'settings', 'core' )->setFlatSetting( 'schedule', $values );

			$form->addNotice( 'Your changes have been saved.' );
		}

	}