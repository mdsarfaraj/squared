<?php

Engine_Loader::autoload('application_modules_Event_controllers_EventController');

class Schedule_ManageEventController extends Event_EventController {

    public function editAction() {
        $event_id = $this->getRequest()->getParam('event_id');
        $event = Engine_Api::_()->getItem('event', $event_id);
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!($this->_helper->requireAuth()->setAuthParams(null, null, 'edit')->isValid() || $event->isOwner($viewer))) {
            return;
        }

        // Create form
        $event = Engine_Api::_()->core()->getSubject();
        $this->view->form = $form = new Event_Form_Edit(array('parent_type' => $event->parent_type, 'parent_id' => $event->parent_id));

        // Populate with categories
        $categories = Engine_Api::_()->getDbtable('categories', 'event')->getCategoriesAssoc();
        asort($categories, SORT_LOCALE_STRING);
        $categoryOptions = array('0' => '');
        foreach ($categories as $k => $v) {
            $categoryOptions[$k] = $v;
        }
        if (sizeof($categoryOptions) <= 1) {
            $form->removeElement('category_id');
        } else {
            $form->category_id->setMultiOptions($categoryOptions);
        }

        if (!$this->getRequest()->isPost()) {
            // Populate auth
            $auth = Engine_Api::_()->authorization()->context;

            if ($event->parent_type == 'group') {
                $roles = array('owner', 'member', 'parent_member', 'registered', 'everyone');
            } else {
                $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
            }

            foreach ($roles as $role) {
                if (isset($form->auth_view->options[$role]) && $auth->isAllowed($event, $role, 'view')) {
                    $form->auth_view->setValue($role);
                }
                if (isset($form->auth_comment->options[$role]) && $auth->isAllowed($event, $role, 'comment')) {
                    $form->auth_comment->setValue($role);
                }
                if (isset($form->auth_photo->options[$role]) && $auth->isAllowed($event, $role, 'photo')) {
                    $form->auth_photo->setValue($role);
                }
            }
            $form->auth_invite->setValue($auth->isAllowed($event, 'member', 'invite'));
            $form->populate($event->toArray());

            // Convert and re-populate times
            $start = strtotime($event->starttime);
            $end = strtotime($event->endtime);
            $oldTz = date_default_timezone_get();
            date_default_timezone_set($viewer->timezone);
            $start = date('Y-m-d H:i:s', $start);
            $end = date('Y-m-d H:i:s', $end);
            date_default_timezone_set($oldTz);

            $form->populate(array(
                'starttime' => $start,
                'endtime' => $end,
            ));
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }


        // Process
        $values = $form->getValues();

        // Convert times
        $oldTz = date_default_timezone_get();
        date_default_timezone_set($viewer->timezone);
        $start = strtotime($values['starttime']);
        $end = strtotime($values['endtime']);
        date_default_timezone_set($oldTz);
        $values['starttime'] = date('Y-m-d H:i:s', $start);
        $values['endtime'] = date('Y-m-d H:i:s', $end);

        // Check parent
        if (!isset($values['host']) && $event->parent_type == 'group' && Engine_Api::_()->hasItemType('group')) {
            $group = Engine_Api::_()->getItem('group', $event->parent_id);
            $values['host'] = $group->getTitle();
        }

        // Process
        $db = Engine_Api::_()->getItemTable('event')->getAdapter();
        $db->beginTransaction();

        try {
            // Set event info
            $event->setFromArray($values);
            $event->save();

            if (!empty($values['photo'])) {
                $event->setPhoto($form->photo);
            }


            // Process privacy
            $auth = Engine_Api::_()->authorization()->context;

            if ($event->parent_type == 'group') {
                $roles = array('owner', 'member', 'parent_member', 'registered', 'everyone');
            } else {
                $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
            }

            $viewMax = array_search($values['auth_view'], $roles);
            $commentMax = array_search($values['auth_comment'], $roles);
            $photoMax = array_search($values['auth_photo'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
                $auth->setAllowed($event, $role, 'comment', ($i <= $commentMax));
                $auth->setAllowed($event, $role, 'photo', ($i <= $photoMax));
            }

            $auth->setAllowed($event, 'member', 'invite', $values['auth_invite']);

            $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
            $select = $scheduleTable->select();
            $select->where('event_id = ?', $event->getIdentity());

            $schedule = $scheduleTable->fetchRow($select);
            if ($schedule) {
                $members = $event->membership()->getMembers();

                foreach ($members as $member) {
                    Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                            $member, $member, $schedule, 'schedule_event_edited', array());
                }
            }

            // Commit
            $db->commit();
        } catch (Engine_Image_Exception $e) {
            $db->rollBack();
            $form->addError(Zend_Registry::get('Zend_Translate')->_('The image you selected was too large.'));
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }


        $db->beginTransaction();
        try {
            // Rebuild privacy
            $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
            foreach ($actionTable->getActionsByObject($event) as $action) {
                $actionTable->resetActivityBindings($action);
            }

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        // Redirect
        if ($this->_getParam('ref') === 'profile') {
            //$this->_redirectCustom($event);
        } else {
            //$this->_redirectCustom(array('route' => 'event_general', 'action' => 'manage'));
        }
    }

    public function deleteAction() {

        $viewer = Engine_Api::_()->user()->getViewer();
        $event = Engine_Api::_()->getItem('event', $this->getRequest()->getParam('event_id'));
        if (!$this->_helper->requireAuth()->setAuthParams($event, null, 'delete')->isValid())
            return;

        // In smoothbox
        $this->_helper->layout->setLayout('default-simple');

        // Make form
        $this->view->form = $form = new Event_Form_Delete();

        if (!$event) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_("Event doesn't exists or not authorized to delete");
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }

        $db = $event->getTable()->getAdapter();
        $db->beginTransaction();

        try {
            $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
            $select = $scheduleTable->select();
            $select->where('event_id = ?', $event->getIdentity());

            $schedule = $scheduleTable->fetchRow($select);
            if ($schedule) {
                $members = $event->membership()->getMembers();

                foreach ($members as $member) {
                    Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                            $member, $member, $schedule, 'schedule_event_edited', array());
                }
            }

            $event->delete();
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('The selected event has been deleted.');
        return $this->_forward('success', 'utility', 'core', array(
                    'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'manage'), 'event_general', true),
                    'messages' => Array($this->view->message)
                ));
    }

}
