<?php

Engine_Loader::autoload('application_modules_Event_controllers_MemberController');

class Schedule_MemberController extends Event_MemberController {

    public function joinAction() {
        // Check auth
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireSubject()->isValid())
            return;

        // Check resource approval
        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->core()->getSubject();
        if ($subject->membership()->isResourceApprovalRequired()) {
            $row = $subject->membership()->getReceiver()
                    ->select()
                    ->where('resource_id = ?', $subject->getIdentity())
                    ->where('user_id = ?', $viewer->getIdentity())
                    ->query()
                    ->fetch(Zend_Db::FETCH_ASSOC, 0);
            ;
            if (empty($row)) {
                // has not yet requested an invite
                return $this->_helper->redirector->gotoRoute(array('action' => 'request', 'format' => 'smoothbox'));
            } elseif ($row['user_approved'] && !$row['resource_approved']) {
                // has requested an invite; show cancel invite page
                return $this->_helper->redirector->gotoRoute(array('action' => 'cancel', 'format' => 'smoothbox'));
            }
        }

        // Make form
        $this->view->form = $form = new Event_Form_Member_Join();

        // Process form
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
            $viewer = Engine_Api::_()->user()->getViewer();
            $subject = Engine_Api::_()->core()->getSubject();
            $db = $subject->membership()->getReceiver()->getTable()->getAdapter();
            $db->beginTransaction();

            try {
                $membership_status = $subject->membership()->getRow($viewer)->active;

                $subject->membership()
                        ->addMember($viewer)
                        ->setUserApproved($viewer)
                ;

                $row = $subject->membership()
                        ->getRow($viewer);

                $row->rsvp = $form->getValue('rsvp');
                $row->save();

                // Add activity if membership status was not valid from before
                if (!$membership_status) {
                    $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');
                    $action = $activityApi->addActivity($viewer, $subject, 'event_join');
                }

                if ($form->getValue('rsvp') == 2) {
                    $dt_elements = explode(' ', $subject->starttime);
                    $date_elements = explode('-', $dt_elements[0]);
                    $time_elements = explode(':', $dt_elements[1]);
                    $end_time = date('Y-m-d H:i:s', mktime($time_elements[0] + 1, $time_elements[1], $time_elements[2], $date_elements[1], $date_elements[2], $date_elements[0]));

                    $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
                    $select = $scheduleTable->select();
                    $select->where('owner_id = ?', $viewer->getIdentity())
                            ->where('start_time = ?', $subject->starttime)
                            ->where('end_time = ?', $end_time)
                            ->where('date = ?', $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2]);
                    if (count($scheduleTable->fetchRow($select)) == 0) {
                        $schedule = $scheduleTable->createRow();
                        $schedule->owner_id = $viewer->getIdentity();
                        $schedule->title = $subject->title;
                        $schedule->description = $subject->description;
                        $schedule->start_time = $subject->starttime;
                        $schedule->end_time = $end_time;
                        $schedule->date = $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2];
                        $schedule->save();
                    }
                }

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }

            return $this->_forward('success', 'utility', 'core', array(
                        'messages' => array(Zend_Registry::get('Zend_Translate')->_('Event joined')),
                        'layout' => 'default-simple',
                        'parentRefresh' => true,
                    ));
        }
    }

    public function leaveAction() {
        // Check auth
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireSubject()->isValid())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->core()->getSubject();

        if ($subject->isOwner($viewer))
            return;

        // Make form
        $this->view->form = $form = new Event_Form_Member_Leave();

        // Process form
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
            $db = $subject->membership()->getReceiver()->getTable()->getAdapter();
            $db->beginTransaction();

            try {
                $subject->membership()->removeMember($viewer);

                $dt_elements = explode(' ', $subject->starttime);
                $date_elements = explode('-', $dt_elements[0]);
                $time_elements = explode(':', $dt_elements[1]);
                $end_time = date('Y-m-d H:i:s', mktime($time_elements[0] + 1, $time_elements[1], $time_elements[2], $date_elements[1], $date_elements[2], $date_elements[0]));

                $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
                $select = $scheduleTable->select();
                $select->where('owner_id = ?', $viewer->getIdentity())
                        ->where('start_time = ?', $subject->starttime)
                        ->where('end_time = ?', $end_time)
                        ->where('date = ?', $date_elements[0] . '-' . $date_elements[1] . '-' . $date_elements[2]);
                if (count($schedule = $scheduleTable->fetchRow($select)) == 1) {
                    $schedule->delete();
                }

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }

            return $this->_forward('success', 'utility', 'core', array(
                        'messages' => array(Zend_Registry::get('Zend_Translate')->_('Event left')),
                        'layout' => 'default-simple',
                        'parentRefresh' => true,
                    ));
        }
    }

}
