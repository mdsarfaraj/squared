<?php

class Schedule_View_Helper_FormTime extends Zend_View_Helper_FormElement {

    public function formTime($name, $value = null, $attribs = null, $options = null, $listsep = "<br />\n") {
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, value, attribs, options, listsep, disable
        // Get use military time
        if (isset($attribs['useMilitaryTime'])) {
            $useMilitaryTime = $attribs['useMilitaryTime'];
        } else {
            $useMilitaryTime = true;
        }

        // Check value type
        if (is_string($value) && preg_match('/^(\d{1,2}):(\d{2})(:(\d{2}))?$/', $value, $m)) {
            $value = array();

            // Get time
            if (isset($m[0])) {
                $value['hour'] = $m[1];
                $value['minute'] = $m[2];
                if (!$useMilitaryTime) {
                    $value['ampm'] = ( $value['hour'] >= 12 ? 'PM' : 'AM' );
                    if (0 == (int) $value['hour']) {
                        $value['hour'] = 12;
                    } else if ($value['hour'] > 12) {
                        $value['hour'] -= 12;
                    }
                }
            }
        }

        if (!is_array($value)) {
            $value = array();
        }

        return
                '<div class="event_calendar_container" style="display:inline">' . $this->view->timeForm($name, $value, $attribs, $options) . '</div>';
    }

}