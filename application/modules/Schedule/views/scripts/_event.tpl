<?php if (!$this->event['private']) : ?>
    <?php
    if (isset($this->event['schedule_id']))
        $id = 'event_' . $this->event['schedule_id'];
    else
        $id = 'request_' . $this->event['request_id'];
    ?>
    <div
            id="<?php echo $id ?>"
            class="
        schedule_time 
        event_text 
        <?php echo $this->event['is_request'] ? 'event_request' : '' ?>
        <?php echo ($this->event['is_request'] && !$this->event['confirmed']) ? 'not_confirmed' : '' ?>
        <?php echo (($this->event['end'] * 9) == 9) ? 'small_event' : '' ?>
        "
            onmouseover="expand(this);"
            style="top:<?php echo $this->event['start'] * 10 ?>px; height:<?php echo (($this->event['end'] * 9) == 9) ? '' : $this->event['end'] * 10 ?>px; width: <?php echo $this->itemWidth ?>%; left: <?php echo $this->left ?>%"
    >
        <div class="pos_relative"
             style="height: <?php echo (($this->event['end'] * 9) == 9) ? '' : ($this->event['end'] * 10) - 10 ?>px;">
            <?php if ($this->format != 'smoothbox') : ?>
                <?php if (true === $this->data['isSelf'] && $this->event['is_request'] == false) : ?>
                    <div class="pulldown m_proj_settings">
                        <div class="pulldown_contents_wrapper">
                            <div class="pulldown_contents">
                                <ul>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'delete', 'schedule_id' => $this->event['schedule_id']), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_delete delete smoothbox', 'id' => 'event_' . $this->event['schedule_id']));
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'edit', 'schedule_id' => $this->event['schedule_id']), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_edit smoothbox'))
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'invite', 'schedule_id' => $this->event['schedule_id']), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_invite smoothbox'))
                                        ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="schedule_nav_block invite" id="invite_<?php echo $this->event['schedule_id'] ?>">
                        <script type="text/javascript">
                            var eventWidgetRequestSend = function (action, event_id, notification_id) {
                                var url = en4.core.baseUrl + 'schedule/' + action + '/' + event_id + '/type/fromSchedule';

                                (new Request.JSON({
                                    'url': url,
                                    'data': {
                                        'format': 'json'
                                    },
                                    'onSuccess': function (responseJSON) {

                                        if (responseJSON.redirect) {
                                            window.location.href = responseJSON.redirect;
                                        }

                                        if (!responseJSON.status) {
                                            $('invite_' + event_id).innerHTML = responseJSON.error;
                                        } else {
                                            $('invite_' + event_id).innerHTML = responseJSON.message;
                                        }
                                    }
                                })).send();
                            }
                        </script>
                        <?php if ((isset($this->event['has_invite']) && $this->event['has_invite']) || ($this->event['is_request'] && $this->event['owner_id'] == $this->viewer()->getIdentity() && $this->event['confirmed'] == 0)) : ?>

                            <?php
                            echo $this->htmlLink('javascript:void(0)', '&nbsp;', array(
                                'class' => 'buttonlink icon_schedule_accept accept', 'onclick' => 'eventWidgetRequestSend("request-accept", ' . $this->event['request_id'] . ')'));
                            ?>
                        <?php endif ?>

                        <?php
                        echo $this->htmlLink('javascript:void(0)', '&nbsp;', array(
                            'class' => 'buttonlink icon_schedule_reject reject', 'onclick' => 'eventWidgetRequestSend("request-reject", ' . $this->event['request_id'] . ')'));
                        ?>
                    </div>
                <?php endif ?>
            <?php endif ?>
            <div class="time_period">
                <?php $content = ($this->ampm == 0 ? date('g:i a', strtotime($this->event['start_time'])) : $this->event['start_time']) . ' - ' . ($this->ampm == 0 ? date('g:i a', strtotime($this->event['end_time'])) : $this->event['end_time']) ?>
                <?php if ($this->event['is_request']) : ?>
                    <?php if ($this->event['requester_id'] == $this->subject()->getIdentity()) : ?>
                        <?php $content .= ', ' . $this->translate('request to %s', $this->htmlLink(Engine_Api::_()->user()->getUser($this->event['owner_id'])->getHref(), Engine_Api::_()->user()->getUser($this->event['owner_id'])->getTitle(), array('target' => '_blank'))) ?>
                    <?php else : ?>
                        <?php $content .= ', ' . $this->translate('request from %s', $this->htmlLink(Engine_Api::_()->user()->getUser($this->event['requester_id'])->getHref(), Engine_Api::_()->user()->getUser($this->event['requester_id'])->getTitle(), array('target' => '_blank'))) ?>
                    <?php endif ?>
                <?php endif ?>
                <?php echo $content ?>
            </div>
            <div class="schelude_title"><?php echo $this->event['title'] ?></div>
            <div class="schelude_descr"><?php echo $this->event['description'] ?></div>
        </div>
    </div>
<?php else : ?>
    <div class="schedule_time event_text <?php echo (($this->event['end'] * 9) == 9) ? 'small_event' : '' ?>"
         onmouseover="expand(this);"
         style="top:<?php echo $this->event['start'] * 9 + $step ?>px; height:<?php echo (($this->event['end'] * 9) == 9) ? '' : $this->event['end'] * 9 ?>px; width: <?php echo $this->itemWidth ?>%; left: <?php echo $this->left ?>%">
        <div class="pos_relative">
            <?php if ($this->format != 'smoothbox') : ?>
                <?php if (true === $this->data['isSelf']) : ?>
                    <div class="pulldown m_proj_settings">
                        <div class="pulldown_contents_wrapper">
                            <div class="pulldown_contents">
                                <ul>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'delete', 'schedule_id' => $this->event['schedule_id'], 'format' => 'smoothbox'), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_delete delete', 'id' => 'event_' . $this->event['schedule_id'], 'onclick' => 'window.Smoothbox.open("' . $this->url(array('action' => 'delete', 'schedule_id' => $this->event['schedule_id']), 'schedule_specific', true) . '", {overlay : false, width : 100, height : 100, autoResize : false})'));
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'edit', 'schedule_id' => $this->event['schedule_id'], 'format' => 'smoothbox'), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_edit', 'onclick' => 'window.Smoothbox.open("' . $this->url(array('action' => 'edit', 'schedule_id' => $this->event['schedule_id']), 'schedule_specific', true) . '", {overlay : false, width : 100, height : 100, autoResize : false})'))
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        echo $this->htmlLink(array('route' => 'schedule_specific', 'action' => 'invite', 'schedule_id' => $this->event['schedule_id'], 'format' => 'smoothbox'), '&nbsp;', array(
                                            'class' => 'buttonlink icon_groupschedule_invite', 'onclick' => 'window.Smoothbox.open("' . $this->url(array('action' => 'invite', 'schedule_id' => $this->event['schedule_id']), 'schedule_specific', true) . '")'))
                                        ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <?php if (isset($this->event['has_invite']) && $this->event['has_invite']) : ?>
                        <div class="schedule_nav_block invite" id="invite_<?php echo $this->event['schedule_id'] ?>">
                            <script type="text/javascript">
                                var eventWidgetRequestSend = function (action, event_id, notification_id) {
                                    var url;
                                    if (action == 'accept') {
                                        url = '<?php echo $this->url(array('action' => 'accept', 'schedule_id' => $this->event['schedule_id'], 'type' => 'fromSchedule'), 'schedule_specific', true) ?>';
                                    } else if (action == 'reject') {
                                        url = '<?php echo $this->url(array('action' => 'reject', 'schedule_id' => $this->event['schedule_id'], 'type' => 'fromSchedule'), 'schedule_specific', true) ?>';
                                    } else {
                                        return false;
                                    }

                                    (new Request.JSON({
                                        'url': url,
                                        'data': {
                                            'format': 'json'
                                        },
                                        'onSuccess': function (responseJSON) {
                                            if (!responseJSON.status) {
                                                $('invite_<?php echo $this->event['schedule_id'] ?>').innerHTML = responseJSON.error;
                                            } else {
                                                $('invite_<?php echo $this->event['schedule_id'] ?>').innerHTML = responseJSON.message;
                                            }
                                        }
                                    })).send();
                                }
                            </script>
                            <?php
                            echo $this->htmlLink('javascript:void(0)', '&nbsp;', array(
                                'class' => 'buttonlink icon_schedule_accept accept', 'onclick' => 'eventWidgetRequestSend("accept", ' . $this->event['schedule_id'] . ', ' . $this->event['invite']->notification_id . ')'));
                            ?>
                            <?php
                            echo $this->htmlLink('javascript:void(0)', '&nbsp;', array(
                                'class' => 'buttonlink icon_schedule_reject reject', 'onclick' => 'eventWidgetRequestSend("reject", ' . $this->event['schedule_id'] . ', ' . $this->event['invite']->notification_id . ')'));
                            ?>
                        </div>
                    <?php endif ?>
                <?php endif ?>
            <?php endif ?>
            <div class="time_perion"><?php echo $this->ampm == 0 ? date('g:i a', strtotime($this->event['start_time'])) : $this->event['start_time'] ?>
                - <?php echo $this->ampm == 0 ? date('g:i a', strtotime($this->event['end_time'])) : $this->event['end_time'] ?></div>
            <div class="schelude_title"><?php echo $this->translate('Private Event') ?></div>
        </div>
    </div>
<?php endif ?>