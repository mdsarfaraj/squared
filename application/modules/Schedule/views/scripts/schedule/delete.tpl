<?php if (!$this->parentRefresh) : ?>
    <?php echo $this->form->render($this) ?>
<?php else : ?>
    <div>
        <div class="global_form_popup_message"><?php echo $this->translate('Selected event has been deleted.') ?></div>
    </div>
    <script type="text/javascript">
        en4.core.runonce.add(function() {
            var anchor = parent.$('schedule_list').getParent();

            en4.core.request.send(new Request.HTML({
                url: en4.core.baseUrl + 'schedule/<?php echo $this->owner_id ?>/<?php echo $this->date ?>',
                data: {
                    format: 'html',
                    identity: parent.identity
                },
                onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript) {
                    anchor.innerHTML = responseHTML;
                    parent.Smoothbox.bind();
                    parent.Smoothbox.close();
                }
            }));
        });
    </script>
<?php endif ?>