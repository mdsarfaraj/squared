<?php if (!$this->parentRefresh) : ?>
    <?php
    $this->headScript()
            ->appendFile($this->baseUrl() . '/externals/autocompleter/Observer.js')
            ->appendFile($this->baseUrl() . '/externals/autocompleter/Autocompleter.js')
            ->appendFile($this->baseUrl() . '/externals/autocompleter/Autocompleter.Local.js')
            ->appendFile($this->baseUrl() . '/externals/autocompleter/Autocompleter.Request.js');
    ?>

    <script type="text/javascript">

        // Populate data
        var maxRecipients = 3;
        var to = {
            id: false,
            type: false,
            guid: false,
            title: false
        };
        var isPopulated = false;

    <?php if (!empty($this->isPopulated) && !empty($this->toObject)): ?>
            isPopulated = true;
            to = {
                id: <?php echo sprintf("%d", $this->toObject->getIdentity()) ?>,
                type: '<?php echo $this->toObject->getType() ?>',
                guid: '<?php echo $this->toObject->getGuid() ?>',
                title: '<?php echo $this->string()->escapeJavascript($this->toObject->getTitle()) ?>'
            };
    <?php endif; ?>

        function removeFromToValue(id) {
            // code to change the values in the hidden field to have updated values
            // when recipients are removed.
            var toValues = $('toValues').value;
            var toValueArray = toValues.split(",");
            var toValueIndex = "";

            var checkMulti = id.search(/,/);

            // check if we are removing multiple recipients
            if (checkMulti != -1) {
                var recipientsArray = id.split(",");
                for (var i = 0; i < recipientsArray.length; i++) {
                    removeToValue(recipientsArray[i], toValueArray);
                }
            }
            else {
                removeToValue(id, toValueArray);
            }

            // hide the wrapper for usernames if it is empty
            if ($('toValues').value == "") {
                $('toValues-wrapper').setStyle('height', '0');
            }

            $('to').disabled = false;
        }

        function removeToValue(id, toValueArray) {
            for (var i = 0; i < toValueArray.length; i++) {
                if (toValueArray[i] == id)
                    toValueIndex = i;
            }

            toValueArray.splice(toValueIndex, 1);
            $('toValues').value = toValueArray.join();
        }

        en4.core.runonce.add(function() {
            if (!isPopulated) { // NOT POPULATED
                new Autocompleter.Request.JSON('to', '<?php echo $this->url(array('action' => 'users', 'schedule_id' => $this->schedule_id), 'schedule_specific', true) ?>', {
                    'minLength': 3,
                    'delay': 250,
                    'selectMode': 'pick',
                    'autocompleteType': 'message',
                    'multiple': false,
                    'className': 'message-autosuggest',
                    'filterSubset': true,
                    'tokenFormat': 'object',
                    'tokenValueKey': 'label',
                    'injectChoice': function(token) {
                        if (token.type == 'user') {
                            var choice = new Element('li', {
                                'class': 'autocompleter-choices',
                                'html': token.photo,
                                'id': token.label
                            });
                            new Element('div', {
                                'html': this.markQueryValue(token.label),
                                'class': 'autocompleter-choice'
                            }).inject(choice);
                            this.addChoiceEvents(choice).inject(this.choices);
                            choice.store('autocompleteChoice', token);
                        }
                        else {
                            var choice = new Element('li', {
                                'class': 'autocompleter-choices friendlist',
                                'id': token.label
                            });
                            new Element('div', {
                                'html': this.markQueryValue(token.label),
                                'class': 'autocompleter-choice'
                            }).inject(choice);
                            this.addChoiceEvents(choice).inject(this.choices);
                            choice.store('autocompleteChoice', token);
                        }

                    },
                    onPush: function() {
                        if ($('toValues').value.split(',').length >= maxRecipients) {
                            $('to').disabled = true;
                        }
                    }
                });

                new Composer.OverText($('to'), {
                    'textOverride': "<?php echo $this->translate("Start typing member's name...") ?>",
                    'element': 'label',
                    'isPlainText': true,
                    'positionOptions': {
                        position: (en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft'),
                        edge: (en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft'),
                        offset: {
                            x: (en4.orientation == 'rtl' ? -4 : 4),
                            y: 2
                        }
                    }
                });

            } else { // POPULATED

                var myElement = new Element("span", {
                    'id': 'tospan' + to.id,
                    'class': 'tag tag_' + to.type,
                    'html': to.title /* + ' <a href="javascript:void(0);" ' +
                     'onclick="this.parentNode.destroy();removeFromToValue("' + toID + '");">x</a>"' */
                });
                $('to-element').appendChild(myElement);
                $('to-wrapper').setStyle('height', 'auto');

                // Hide to input?
                $('to').setStyle('display', 'none');
                $('toValues-wrapper').setStyle('display', 'none');
            }
        });
    </script>

    <?php
    $this->headScript()
            ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Core/externals/scripts/composer.js');
    ?>

    <?php echo $this->form->render($this) ?>
<?php else : ?>
    <div>
        <div class="global_form_popup_message"><?php echo $this->translate('Members invited.') ?></div>
    </div>
    <script type="text/javascript">
        en4.core.runonce.add(function() {
            var anchor = parent.$('schedule_list').getParent();

            en4.core.request.send(new Request.HTML({
                url: en4.core.baseUrl + 'schedule/<?php echo $this->subject()->owner_id ?>/<?php echo $this->event->date ?>',
                data: {
                    format: 'html',
                    identity: parent.identity
                },
                onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript) {
                    anchor.innerHTML = responseHTML;
                    parent.Smoothbox.bind();
                    parent.Smoothbox.close();
                }
            }));
        });
    </script>
<?php endif; ?>


