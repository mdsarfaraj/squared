<script type="text/javascript">
    var eventWidgetRequestSend = function(action, event_id, notification_id, rsvp)
    {
        var url;
        if( action == 'accept' )
        {
            url = '<?php echo $this->url(array('action' => 'accept', 'schedule_id' => $this->notification->getObject()->getIdentity()), 'schedule_specific', true) ?>';
        }
        else if( action == 'reject' )
        {
            url = '<?php echo $this->url(array('action' => 'reject', 'schedule_id' => $this->notification->getObject()->getIdentity()), 'schedule_specific', true) ?>';
        }
        else
        {
            return false;
        }

        (new Request.JSON({
            'url' : url,
            'data' : {
                'format' : 'json'
            },
            'onSuccess' : function(responseJSON)
            {
                if( !responseJSON.status )
                {
                    $('event-widget-request-' + notification_id).innerHTML = responseJSON.error;
                }
                else
                {
                    $('event-widget-request-' + notification_id).innerHTML = responseJSON.message;
                }
            }
        })).send();
    }
</script>

<li id="event-widget-request-<?php echo $this->notification->notification_id ?>">
    <div>
        <div>
            <?php echo $this->translate('%1$s has invited you to the event %2$s', $this->htmlLink($this->notification->getSubject()->getHref(), $this->notification->getSubject()->getTitle()), $this->htmlLink($this->notification->getObject()->getHref(), $this->notification->getObject()->getTitle())); ?>
        </div>
        <div>
            <button type="submit" onclick='eventWidgetRequestSend("accept", <?php echo $this->string()->escapeJavascript($this->notification->getObject()->getIdentity()) ?>, <?php echo $this->notification->notification_id ?>, 2)'>
                <?php echo $this->translate('Attending'); ?>
            </button>
            <?php echo $this->translate('or'); ?>
            <a href="javascript:void(0);" onclick='eventWidgetRequestSend("reject", <?php echo $this->string()->escapeJavascript($this->notification->getObject()->getIdentity()) ?>, <?php echo $this->notification->notification_id ?>)'>
                <?php echo $this->translate('ignore request'); ?>
            </a>
        </div>
    </div>
</li>