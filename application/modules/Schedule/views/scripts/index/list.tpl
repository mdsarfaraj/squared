<?php echo $this->htmlLink('javascript:calendarView(\'' . $this->data['date']['month'] . '\', \'' . $this->data['date']['year'] . '\')', $this->translate('Back to Month View'), array('class' => 'buttonlink back_to_month'))
?>

<?php $this->ampm = $ampm = Engine_Api::_()->getApi('settings', 'schedule')->getSetting($this->viewer->getIdentity() . '.' . 'ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm')); ?>
<script type="text/javascript">

    var page = function (date) {
        var element = $('schedule_list').getParent();

        en4.core.request.send(new Request.HTML({
            url: en4.core.baseUrl + 'schedule/<?php echo $this->data['owner']->getIdentity() ?>/' + date,
            data: {
                format: 'html'
            }
        }), {
            'element': element
        });
    };

    function expand(elem) {
        var size = parseInt(elem.getStyle('height'));
        if (size < 60) {
            elem.setStyle('height', 'auto');
            elem.setStyle('z-index', '2');
            elem.addEvent('mouseout', function () {
                elem.setStyle('height', size + 'px');
                elem.setStyle('z-index', '1');
            });
        }

    }

    function calendarView(month, year) {
        var element = $('schedule_list').getParent();

        en4.core.request.send(new Request.HTML({
            url: en4.core.baseUrl + 'widget/index',
            data: {
                format: 'html',
                content_id: identity,
                subject: '<?php echo $this->subject()->getGuid() ?>',
                date: year + '-' + month
            }
        }), {
            'element': element
        });
    }

    en4.core.runonce.add(function () {
        if ($$('html')[0].get('id') == 'smoothbox_window') {
            $$('.back_to_month').hide();
        }
    });
</script>
<div class="schedule_list" id="schedule_list">
    <?php $now = new Zend_Date($this->data['date'], $this->locale()->getLocale()); ?>

    <div class="schedule_list_header">
        <div id="prev" class="prev"
             onclick="page('<?php echo $this->data['prev'] ?>');"><?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array('class' => 'buttonlink icon_previous')); ?></div>
        <div id="sch_month"><?php echo $now->get(Zend_Date::DATE_LONG) ?></div>
        <div id="next" class="next"
             onclick="page('<?php echo $this->data['next'] ?>');"><?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array('class' => 'buttonlink_right icon_next')); ?></div>
    </div>

    <?php if (true === $this->data['isSelf'] && $this->format != 'smoothbox') : ?>
        <?php if (isset($this->data['notinrange'])) : ?>
            <div class="tip">
                <span><?php echo $this->translate('You have events that are out of your schedule time range. You can check them below the schedule.') ?></span>
            </div>
        <?php endif ?>
    <?php endif ?>

    <?php
    list($sH, $sM) = explode(':', $this->data['setting']['time_start']);
    list($eH, $eM) = explode(':', $this->data['setting']['time_end']);

    $a = DateTime::createFromFormat('H:i', $this->data['setting']['time_end']);
    $b = DateTime::createFromFormat('H:i', $this->data['setting']['time_start']);

    $interval = $a->diff($b);
    $hours = ($interval->days * 24) + $interval->h + ($interval->i / 60) + ($interval->s / 3600);
    ?>

    <table cellspacing="0" cellpadding="0" id="schedule_table" class="schedule_table"
           style="height: <?php echo $hours * 60 + 60 ?>px">
        <tr height="1">
            <td style="width: 60px"></td>
            <td colspan="1">
                <div class="rel_events_wrapper">
                    <div class="abs_events_wrapper">
                        <?php for ($i = (int)$sH; $i <= (int)$eH; $i++) : ?>
                            <?php $hoursArr[] = $i ?>
                            <div class="hour" style="height:60px"></div>
                        <?php endfor ?>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="hours">
                <?php for ($i = (int)$sH; $i <= (int)$eH; $i++) : ?>
                    <div style="height: 60px">
                        <div style="height: 60px">
                            <?php echo $ampm == 0 ? date('g:i \\<\b\r\>A', strtotime($i . ':00:00')) : $i . ':00' ?>
                        </div>
                    </div>
                <?php endfor ?>
            </td>
            <td class="events">
                <div class="events_wrapper" style="position: relative; height: <?php echo $hours * 60 + 60 ?>px">
                    <?php if (isset($this->data['schedule'])) : ?>
                        <?php foreach ($this->data['schedule'] as $group) : ?>
                            <?php $this->left = 0 ?>
                            <?php foreach ($group['items'] as $count => $item) : ?>
                                <?php //@TODO: rework it to ViewHelper ?>
                                <?php $this->itemWidth = 100 / count($group['items']) ?>
                                <?php
                                if ($count > 0)
                                    $this->left += (100 / count($group['items']))
                                ?>
                                <?php $this->event = $item ?>
                                <?php echo $this->render('_event.tpl') ?>
                                <?php unset($this->event) ?>
                            <?php endforeach ?>
                        <?php endforeach ?>
                    <?php endif ?>

                    <?php $hoursArr = array_flip($hoursArr) ?>

                    <?php foreach ($this->data['hours'] as $hour) : ?>
                        <?php if ($hour != substr($this->data['setting']['time_end'], 0, 2)) : ?>

                            <div class="schedule_time empty"
                                 style="top:<?php echo $hoursArr[$hour] * 60 ?>px; height:60px">
                                <?php if ($this->format != 'smoothbox') : ?>
                                    <?php if (true === $this->data['isSelf']) : ?>
                                        <?php
                                        echo $this->htmlLink(array(
                                            'route' => 'schedule_general',
                                            'action' => 'create',
                                            'data' => $this->data['date']['year'] . '-' . $this->data['date']['month'] . '-' . $this->data['date']['day'] . ' ' . $hour
                                        ), $this->translate('Add Event'), array(
                                            'class' => 'buttonlink icon_schedule_new add_event smoothbox'))
                                        ?>
                                    <?php else : ?>
                                        <?php if ($this->viewer()->getIdentity() > 0) : ?>
                                            <?php
                                            echo $this->htmlLink(array(
                                                'route' => 'schedule_general',
                                                'action' => 'request',
                                                'owner' => $this->data['owner']->getIdentity(),
                                                'data' => $this->data['date']['year'] . '-' . $this->data['date']['month'] . '-' . $this->data['date']['day'] . ' ' . $hour
                                            ), $this->translate('Request Event'), array(
                                                'class' => 'buttonlink icon_schedule_request request_event smoothbox'))
                                            ?>
                                        <?php endif ?>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </td>
        </tr>
    </table>

    <?php if (true === $this->data['isSelf'] && $this->format != 'smoothbox') : ?>
        <div class="not_in_range">
            <?php if (isset($this->data['notinrange'])) : ?>
                <?php foreach ($this->data['notinrange'] as $event) :
                    if (isset($event['schedule_id'])) {
                        $type = 'schedule';
                        $route_edit = 'schedule_specific';
                    } else {
                        $type = 'request';
                        $route_edit = 'schedule_general';
                    }
                    ?>
                    <div class="schedule_time event_text"
                         style="height:60px; position: relative; left: 0px; width: 100%;" onmouseover="expand(this);">
                        <div class="pos_relative">
                            <?php if (true === $this->data['isSelf']) : ?>
                                <div class="schedule_nav_block">
                                    <div class="schedule_nav_block">
                                        <?php
                                        echo $this->htmlLink(array(
                                            'route' => 'schedule_specific',
                                            'action' => 'delete',
                                            'schedule_id' => isset($event['schedule_id']) ? $event['schedule_id'] : $event['request_id'],
                                            'type' => $type
                                        ), '&nbsp;', array(
                                            'class' => 'buttonlink icon_schedule_delete delete smoothbox',
                                            'id' => 'event_' . isset($event['schedule_id']) ? $event['schedule_id'] : $event['request_id']));
                                        ?>
                                        <?php
                                        echo $this->htmlLink(array(
                                             'route' => $route_edit,
                                            'action' => isset($event['schedule_id']) ? 'edit' : 'create',
                                            'schedule_id' => isset($event['schedule_id']) ? $event['schedule_id'] : $event['request_id'],
                                            'type' => $type),
                                            '&nbsp;', array(
                                            'class' => 'buttonlink icon_schedule_edit smoothbox',
                                        ))
                                        ?>
                                        <div id="confirm"></div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="time_perion"><?php echo $ampm == 0 ? date('g:i a', strtotime($event['start_time'])) : $event['start_time'] ?>
                                - <?php echo $ampm == 0 ? date('g:i a', strtotime($event['end_time'])) : $event['end_time'] ?></div>
                            <div class="schelude_title"><?php echo $event['title'] ?></div>
                            <div class="schelude_descr"><?php echo $event['description'] ?></div>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    <?php endif ?>
</div>