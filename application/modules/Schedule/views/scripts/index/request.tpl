<?php echo $this->form ?>
<script type="text/javascript">
    en4.core.runonce.add(function() {
        enable(window.$('recurring'));

        var date = new Date(Date.parse(window.cal_date.calendars[0].el.value));
        if (date) {
            var day_name = cal_date.options.days[date.format('%w')];
            var day_number = date.format('%e');
            var month = cal_date.options.months[parseInt(date.format('%m'))-1];

            window.$$('label[for=every-week]')[0].set('text', en4.core.language.translate('Week (every %s)', day_name));
            window.$$('label[for=every-month]')[0].set('text', en4.core.language.translate('Month (on %s each month)', day_number));
            window.$$('label[for=every-year]')[0].set('text', en4.core.language.translate('Year (on %s of %s each year)', day_number, month));
        }
    });

    function enable(elem) {
        if (elem.checked == true) {
            window.$('repeat-wrapper').show();
            window.$('every-wrapper').show();
        } else {
            window.$('repeat-wrapper').hide();
            window.$('every-wrapper').hide();
        }
        parent.window.Smoothbox.instance.doAutoResize();
    }

    function cal_date_onHideStart(cal) {
        if (cal.calendars[0].val) {            
            var day_name = cal.options.days[cal.calendars[0].val.format('%w')];
            var day_number = cal.calendars[0].val.format('%e');
            var month = cal.options.months[parseInt(cal.calendars[0].val.format('%m'))-1];
            
            window.$$('label[for=every-week]')[0].set('text', en4.core.language.translate('Week (every %s)', day_name));
            window.$$('label[for=every-month]')[0].set('text', en4.core.language.translate('Month (on %s each month)', day_number));
            window.$$('label[for=every-year]')[0].set('text', en4.core.language.translate('Year (on %s of %s each year)', day_number, month));
        }
    }
</script>

<?php
$this->headTranslate(array(
    'Week (every %s)',
    'Month (on %s each month)',
    'Year (on %s of %s each year)'
)); ?>