<?php

class Schedule_Form_Element_Time extends Zend_Form_Element_Xhtml {

    public $helper = 'formTime';
    public $ignoreValid;
    protected $_minuteOptions;
    protected $_hourOptions;
    protected $_useMilitaryTime;

    public function init() {
        $this->useMilitaryTime = $this->_useMilitaryTime();
    }

    public function loadDefaultDecorators() {
        if ($this->loadDefaultDecoratorsIsDisabled()) {
            return;
        }

        $decorators = $this->getDecorators();
        if (empty($decorators)) {
            $this->addDecorator('ViewHelper');
            Engine_Form::addDefaultDecorators($this);
        }
    }

    public function setMultiOptions($options) {
        if (isset($options['hour'])) {
            $this->_hourOptions = $options['hour'];
        }
        return $this;
    }

    public function getMultiOptions() {
        if (empty($this->options)) {
            $this->options = array(
                'minute' => $this->getMinuteOptions(),
                'hour' => $this->getHourOptions(),
                'ampm' => $this->getAMPMOptions()
            );
        }

        return $this->options;
    }

    // Hour
    public function getHourOptions() {
        if (null === $this->_hourOptions) {
            $this->_hourOptions = array();
            if ($this->getAllowEmpty()) {
                $this->_hourOptions[''] = '';
            }
            $isMilitary = $this->_useMilitaryTime();
            for ($i = ($isMilitary ? 0 : 1 ), $l = ( $isMilitary ? 23 : 12 ); $i <= $l; $i++) {
                $this->_hourOptions[$i] = $i;
            }
        }
        return $this->_hourOptions;
    }

    // Minute
    public function getMinuteOptions() {
        if (null === $this->_minuteOptions) {
            $this->_minuteOptions = array();
            if ($this->getAllowEmpty()) {
                $this->_minuteOptions[''] = '';
            }
            for ($i = 0; $i < 6; $i++) {
                $this->_minuteOptions[( $i * 10 )] = sprintf('%02d', ($i * 10));
            }
        }
        return $this->_minuteOptions;
    }

    // Value/valid
    public function setValue($value) {
        if (is_array($value)) {
            // Process time
            $hour = null;
            $minute = null;
            if (isset($value['hour']) && is_numeric($value['hour']) && in_array($value['hour'], $this->getHourOptions())) {
                $hour = $value['hour'];
            }
            if (isset($value['minute']) && is_numeric($value['minute']) && in_array($value['minute'], $this->getMinuteOptions())) {
                $minute = $value['minute'];
            }
            if (isset($value['ampm']) && in_array($value['ampm'], $this->getAMPMOptions())) {
                if ($value['ampm'] == 'PM' && $hour < 12 && null !== $hour) {
                    $hour += 12;
                } else if ($value['ampm'] == 'AM' && $hour == 12) {
                    $hour = 0;
                }
            }

            // Get values
            $formatString = '%1$01d:%2$02d';

            $valueString = sprintf($formatString, $hour, $minute);

            $value = $valueString;
        }

        return parent::setValue($value);
    }

    public function getValue() {
        return parent::getValue();
    }

    public function isValid($value, $context = null) {

        // Empty
        if ($this->getAllowEmpty() && (empty($value) || (is_array($value) && 0 == count(array_filter($value))))) {
            return parent::isValid($value, $context);
        }

        $this->setValue($value);
        $value = $this->getValue();

        // Normal processing    
        if (is_string($value)) {
            if (preg_match('/^(\d{1,2}):(\d{1,2})(:(\d{2}))?$/', $value, $m)) {
                $hour = $m[1];
                $minute = $m[2];
            } else {
                $this->addError('Please select a date from the calendar.');
                return false;
            }
        } else if (is_array($value)) {
            if (isset($value['hour']) && in_array($value['hour'], $this->getHourOptions())) {
                $hour = $value['hour'];
            }
            if (isset($value['minute']) && in_array($value['minute'], $this->getMinuteOptions())) {
                $minute = $value['minute'];
            }
            if (isset($value['ampm']) && in_array($value['ampm'], $this->getAMPMOptions())) {
                if ($value['ampm'] == 'pm' && $hour < 12 && null !== $hour) {
                    $hour += 12;
                } else if ($value['ampm'] == 'AM' && $hour == 12) {
                    $hour = 0;
                }
            }
        }

        // Check validity
        if (!isset($hour) || !$minute) {
            $this->addError('Please select a time from the dropdown.');
            return false;
        }

        if ($hour < 0 || $hour > 23) {
            $this->addError('Please select a time from the dropdown.');
            return false;
        }

        if ($minute < 0 || $minute >= 60) {
            $this->addError('Please select a time from the dropdown.');
            return false;
        }

        return parent::isValid($value, $context);
    }

    protected function _useMilitaryTime() {
        if (null === $this->_useMilitaryTime) {
            $viewer = Engine_Api::_()->user()->getViewer();

            $this->_useMilitaryTime = (bool) Engine_Api::_()->getApi('settings', 'schedule')->getSetting($viewer->getIdentity() . '.ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm'));
        }

        return $this->_useMilitaryTime;
    }

    public function getAMPMOptions() {
        if ($this->_useMilitaryTime()) {
            return array();
        } else if (!$this->getAllowEmpty()) {
            return array('AM' => 'AM', 'PM' => 'PM');
        } else {
            return array('' => '', 'AM' => 'AM', 'PM' => 'PM');
        }
    }

}