<?php

class Schedule_Form_Validate_Time extends Zend_Validate_Abstract {
    const IS_EARLIER = 'isEarlier';

    protected $_messageTemplates = array(
        self::IS_EARLIER => 'Incorrect End Time'
    );
    protected $_contextKey;

    public function __construct($key) {
        $this->_contextKey = $key;
    }

    public function isValid($value, $context = null) {
        if (is_array($context)) {
            if (isset($context[$this->_contextKey])) {
                $end_time = $this->toSec($value);
                $start_time = $this->toSec($context[$this->_contextKey]);
                if ($end_time > $start_time) {
                    return true;
                } else {
                    $this->_error(self::IS_EARLIER);
                    return false;
                }
            }
        }
        if ($value <= $context) {
            $this->_error(self::IS_EARLIER);
            return false;
        }
        $this->_error(self::IS_EARLIER);
        return false;
    }

    private function toSec($time) {
        if (is_array($time)) {
            $sec = ($time['hour'] * 60 + $time['minute']) * 60;
        } else {
            $tmp = explode(':', $time);
            $sec = ($tmp[0] * 60 + $tmp[1]) * 60;
        }

        return $sec;
    }

}