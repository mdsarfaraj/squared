<?php

class Schedule_Form_Validate_InRange extends Zend_Validate_Abstract {

    const IN_RANGE = 'InRange';

    protected $_messageTemplates = array(
        self::IN_RANGE => 'Event can\'t be added, because this time field overlaps with other event.',
    );

    public function isValid($value, $context = null) {
        $viewer = Engine_Api::_()->user()->getViewer();

        try {
            $value = new DateTime($context['date']['date'] . ' ' . $value, new DateTimeZone($viewer->timezone));
            $value->setTimeZone(new DateTimeZone('UTC'));
        } catch (Exception $exc) {
            return false;
        }

        $data['user'] = $viewer->getIdentity();
        $data['date'] = $context['date']['date'];

        $value = $value->format('H:i');

        $table = Engine_Api::_()->getDbTable('schedules', 'schedule');
        $inRange = $table->checkDate($value, $data);

        if ($inRange === true)
            $this->_error(self::IN_RANGE);

        return !$inRange;
    }

}
