<?php

class Schedule_Form_Admin_Widget_ScheduleMultiBlock extends Core_Form_Admin_Widget_Standard {

    public function init() {

        parent::init();

        // Set form attributes
        $this->setTitle('Mini Calendar')
                ->setDescription('Please enter user name');

        // Element: name
        $this->addElement('Text', 'user', array(
            'label' => 'Users separated by comma',
        ));
    }

}