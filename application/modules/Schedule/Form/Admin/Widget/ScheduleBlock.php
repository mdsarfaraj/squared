<?php

class Schedule_Form_Admin_Widget_ScheduleBlock extends Core_Form_Admin_Widget_Standard {

    public function init() {
        parent::init();

        // Set form attributes
        $this->setTitle('Mini Calendar');

        // Element: name
        $this->addElement('Text', 'user', array(
            'label' => 'User',
        ));
        $this->user->setDescription('');
    }

}