<?php

class Schedule_Form_Admin_Settings extends Engine_Form {

    public function init() {

        $this->setTitle('General Settings');
        
        //$viewer = Engine_Api::_()->user()->getViewer();
        //$ampm = Engine_Api::_()->getApi('settings', 'schedule')->getSetting($viewer->getIdentity() . '.' . 'ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm'));
        //if ($ampm == 1)
        //    $hours = 24;
        //else
        //    $hours = 12;
        //for ($i = 0; $i < $hours; $i++) {
        //    $opts[$i] = $i;
        //}
        //start time
        $start_time = new Schedule_Form_Element_Time('time_start');
        $start_time->setLabel("Start Time");
        $start_time->setAllowEmpty(false);
        $start_time->setAttrib('class', 'start_time');
        //$start_time->setMultiOptions(array('hour' => $opts));
        $this->addElement($start_time);

        //end time
        $end_time = new Schedule_Form_Element_Time('time_end');
        $end_time->setLabel("End Time");
        $end_time->setAllowEmpty(false);
        $end_time->setAttrib('class', 'end_time');
        $end_time->addValidator(new Schedule_Form_Validate_Time('time_start'));
        //$end_time->setMultiOptions(array('hour' => $opts));
        $this->addElement($end_time);

        $this->addElement('Select', 'ampm', array(
            'label' => 'Time Format',
            'allowEmpty' => false,
            'required' => true,
            'multioptions' => array(
                1 => '24 Hours',
                0 => '12 Hours'
            )
        ));

        $this->addElement('Select', 'event', array(
            'label' => 'Integration with Events module',
            'allowEmpty' => false,
            'required' => true,
            'multioptions' => array(
                0 => 'No',
                1 => 'Yes'
            )
        ));

        $this->addElement('Button', 'submit', array(
            'label' => 'Save Changes',
            'type' => 'submit',
            'ignore' => true,
        ));
    }

}