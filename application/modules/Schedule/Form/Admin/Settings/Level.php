<?php

class Schedule_Form_Admin_Settings_Level extends Authorization_Form_Admin_Level_Abstract
{
  public function init()
  {
    parent::init();

    // My stuff
    $this
      ->setTitle('Member Level Settings');

    // Element: view
    $this->addElement('Radio', 'view', array(
      'label' => 'Allow Viewing of Schedules?',
      'description' => 'Do you want to let members view schedules? If set to no, some other settings on this page may not apply.',
      'multiOptions' => array(
        2 => 'Yes, allow viewing of all schedules, even private ones.',
        1 => 'Yes, allow viewing of schedules.',
        0 => 'No, do not allow schedules to be viewed.',
      ),
      'value' => ( $this->isModerator() ? 2 : 1 ),
    ));
    if( !$this->isModerator() ) {
      unset($this->view->options[2]);
    }

    if( !$this->isPublic() ) {

      // Element: create
      $this->addElement('Radio', 'create', array(
        'label' => 'Allow Creation of Schedules?',
        'description' => 'Do you want to let members create schedules? If set to no, some other settings on this page may not apply. This is useful if you want members to be able to view schedules, but only want certain levels to be able to create schedules.',
        'multiOptions' => array(
          1 => 'Yes, allow creation of schedules.',
          0 => 'No, do not allow schedules to be created.'
        ),
        'value' => 1,
      ));

      // Element: edit
      $this->addElement('Radio', 'edit', array(
        'label' => 'Allow Editing of Schedules?',
        'description' => 'Do you want to let members edit schedules? If set to no, some other settings on this page may not apply.',
        'multiOptions' => array(
          2 => 'Yes, allow members to edit all schedules.',
          1 => 'Yes, allow members to edit their own schedules.',
          0 => 'No, do not allow members to edit their schedules.',
        ),
        'value' => ( $this->isModerator() ? 2 : 1 ),
      ));
      if( !$this->isModerator() ) {
        unset($this->edit->options[2]);
      }

      // Element: delete
      $this->addElement('Radio', 'delete', array(
        'label' => 'Allow Deletion of Schedules?',
        'description' => 'Do you want to let members delete schedules? If set to no, some other settings on this page may not apply.',
        'multiOptions' => array(
          2 => 'Yes, allow members to delete all schedules.',
          1 => 'Yes, allow members to delete their own schedules.',
          0 => 'No, do not allow members to delete their schedules.',
        ),
        'value' => ( $this->isModerator() ? 2 : 1 ),
      ));
      if( !$this->isModerator() ) {
        unset($this->delete->options[2]);
      }

      // Element: auth_view
      $this->addElement('MultiCheckbox', 'auth_view', array(
        'label' => 'Schedule Entry Privacy',
        'description' => "Your members can choose from any of the options checked below when they decide who can see their schedule entries. These options appear on your members 'Add Entry' and 'Edit Entry' pages. If you do not check any options, everyone will be allowed to view schedules.",
        'multiOptions' => array(
          'everyone'            => 'Everyone',
          'registered'          => 'All Registered Members',
          'owner_network'       => 'Friends and Networks',
          'owner_member_member' => 'Friends of Friends',
          'owner_member'        => 'Friends Only',
          'owner'               => 'Just Me'
        ),
        'value' => array('everyone', 'owner_network', 'owner_member_member', 'owner_member', 'owner'),
      ));
    }
  }
}