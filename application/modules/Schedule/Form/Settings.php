<?php

class Schedule_Form_Settings extends Engine_Form {

    protected $_owner_id;

    public function setOwner_id($value) {
        $this->_owner_id = $value;
    }

    public function init() {
        $this->setTitle('Schedule Settings');
        $this->setAttrib('class', 'global_form_popup');
        $user = Engine_Api::_()->user()->getViewer();

        //$viewer = Engine_Api::_()->user()->getViewer();
        //$ampm = Engine_Api::_()->getApi('settings', 'schedule')->getSetting($viewer->getIdentity() . '.' . 'ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm'));
        //if ($ampm == 1)
        //    $hours = 23;
        //else
        //    $hours = 12;
        //for ($i = $ampm == 1 ? 0 : 1; $i < $hours; $i++) {
        //    $opts[$i] = $i;
        //}
        //start time
        $start_time = new Schedule_Form_Element_Time('time_start');
        $start_time->setLabel("Start Time");
        $start_time->setAllowEmpty(false);
        $start_time->setAttrib('class', 'start_time');
        //$start_time->setMultiOptions(array('hour' => $opts));
        $this->addElement($start_time);

        //end time
        $end_time = new Schedule_Form_Element_Time('time_end');
        $end_time->setLabel("End Time");
        $end_time->setAllowEmpty(false);
        $end_time->setAttrib('class', 'end_time');
        $end_time->addValidator(new Schedule_Form_Validate_Time('time_start'));
        //$end_time->setMultiOptions(array('hour' => $opts));
        $this->addElement($end_time);

        $ampm = new Zend_Form_Element_Select('ampm');
        $ampm->setLabel('Time Format')
                ->setDecorators(array(
                    'ViewHelper',
                    array('HtmlTag', array('tag' => 'div')),
                    array('Label', array('tag' => 'div', 'placement' => 'PREPEND')),
                    array('HtmlTag2', array('tag' => 'div', 'class' => 'form-wrapper')),
                ))
                ->setMultiOptions(array(
                    1 => '24 Hours',
                    0 => '12 Hours'
                ));
        $this->addElement($ampm);

        // Buttons
        $this->addElement('Button', 'submit', array(
            'label' => 'Save Changes',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array(
                'ViewHelper',
            ),
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'link' => true,
            'prependText' => ' or ',
            'decorators' => array(
                'ViewHelper',
            ),
            'onclick' => 'parent.Smoothbox.close();'
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                'DivDivDivWrapper',
            ),
        ));
    }

}