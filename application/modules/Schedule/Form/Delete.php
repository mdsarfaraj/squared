<?php

class Schedule_Form_Delete extends Engine_Form {

    public $params;

    public function init() {

        $this->setTitle('Delete Event')
                ->setDescription('Are you sure you want to delete this event?')
                ->setAttrib('class', 'global_form_popup')
                ->setMethod('POST');
        ;

        // Buttons
        $this->addElement('Button', 'submit', array(
            'label'      => 'Delete Event',
            'type'       => 'submit',
            'ignore'     => true,
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label'       => 'cancel',
            'link'        => true,
            'onclick'     => 'parent.Smoothbox.close()',
            'prependText' => ' or ',
            'href'        => '',
            'decorators'  => array(
                'ViewHelper'
            )
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
        $button_group = $this->getDisplayGroup('buttons');
    }

}
