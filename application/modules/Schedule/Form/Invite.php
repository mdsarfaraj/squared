<?php

class Schedule_Form_Invite extends Engine_Form {

    public function init() {

        $this->setTitle('Invite Members')
                ->setAttrib('class', 'global_form_popup schedule_invite');

        // init to
        $this->addElement('Text', 'to', array(
            'label' => 'Send To',
            'autocomplete' => 'off'));

        Engine_Form::addDefaultDecorators($this->to);

        // Init to Values
        $this->addElement('Hidden', 'toValues', array(
            'label' => 'Send To',
            'required' => true,
            'allowEmpty' => false,
            'order' => 2,
            'validators' => array(
                'NotEmpty'
            ),
            'filters' => array(
                'HtmlEntities'
            ),
        ));
        Engine_Form::addDefaultDecorators($this->toValues);

        $this->addElement('Button', 'submit', array(
            'label' => 'Send Invite',
            'order' => 5,
            'type' => 'submit',
            'ignore' => true
        ));
        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'order' => 6,
            'link' => true,
            'onclick' => 'parent.Smoothbox.close()',
            'prependText' => ' or ',
            'decorators' => array(
                'ViewHelper',
            ),
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                'DivDivDivWrapper',
            ),
            'order' => 7
        ));
    }

}
