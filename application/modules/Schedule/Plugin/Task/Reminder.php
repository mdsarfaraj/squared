<?php

class Schedule_Plugin_Task_Reminder extends Core_Plugin_Task_Abstract {

    public function execute() {
        $mins = date('i');

        $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
        $select = $reminderTable->select();
        $select->where('executed = ?', '1');
        $reminded = $reminderTable->fetchAll($select);
        if (count($reminded)) {
            foreach ($reminded as $item) {
                $item->delete();
            }
        }
        unset($item);

        $select = $reminderTable->select();
        $select->where('executed = ?', '0');
        $select->where('DATE(date) = ?', date('Y-m-d'));

        $needRemind = $reminderTable->fetchAll($select);

        if (count($needRemind)) {
            foreach ($needRemind as $item) {
                $user = Engine_Api::_()->user()->getUser($item->user_id);
                if ($user->getIdentity() > 0) {
                    $now = strtotime('now');

                    if (strtotime($item->date) <= strtotime('now')) {
                        switch ($item->time) {
                            case 0:
                                $time = 0;
                                break;
                            case 30:
                                $time = '30 Minutes';
                                break;
                            case 60:
                                $time = '1 Hour';
                                break;
                            case 90:
                                $time = '1 Hour and 30 Minutes ';
                                break;
                            case 120:
                                $time = '2 Hour Before';
                                break;
                            case 150:
                                $time = '2 Hour and 30 Minutes';
                                break;
                            case 180:
                                $time = '3 Hour';
                                break;
                            case 1440:
                                $time = '1 Day';
                                break;
                        }

                        $value = Engine_Api::_()->getItem('schedule', $item->schedule_id);
                        if (count($value)) {
                            if (isset($time) && $time != 0) {
                                Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                                        $user, $user, $value, 'schedule_reminder', array(
                                    'reminder' => $time,
                                    'event_title' => $value->getTitle(),
                                    'event_time' => Zend_Registry::get('Zend_View')->locale()->toDateTime($value->start_time, array('timezone' => Zend_Registry::get('Zend_View')->locale()->getTimezone(), 'size' => 'short')),
                                    'event_remind_period' => $time,
                                    'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $value->getHref(),
                                ));
                            } elseif (isset($time) && $time == 0) {
                                Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification(
                                        $user, $user, $value, 'schedule_reminder_now', array(
                                    'event_title' => $value->getTitle(),
                                    'event_time' => Zend_Registry::get('Zend_View')->locale()->toDateTime($value->start_time, array('timezone' => Zend_Registry::get('Zend_View')->locale()->getTimezone(), 'size' => 'short')),
                                    'event_link' => 'http://' . $_SERVER['HTTP_HOST'] . $value->getHref(),
                                ));
                            }
                        }

                        $item->executed = 1;
                        $item->save();
                    }
                }
            }
        }
    }

}
