<?php

class Schedule_Plugin_Plugins extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $setting = Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.event', 0);
        if ($setting == 1) {
            if ($request->getModuleName() == 'event') {
                if ($request->getControllerName() == 'index' and $request->getActionName() == 'create') {
                    $request->setModuleName('schedule');
                    $request->setControllerName('event');
                    $request->setActionName('create');
                } else if ($request->getControllerName() == 'member' and $request->getActionName() == 'join') {
                    $request->setModuleName('schedule');
                } elseif ($request->getControllerName() == 'member' and $request->getActionName() == 'leave') {
                    $request->setModuleName('schedule');
                } elseif ($request->getControllerName() == 'widget' and $request->getActionName() == 'profile-rsvp') {
                    $request->setModuleName('schedule');
                } elseif ($request->getControllerName() == 'event' and $request->getActionName() == 'edit') {
                    $request->setModuleName('schedule');
                    $request->setControllerName('manage-event');
                } elseif ($request->getControllerName() == 'event' and $request->getActionName() == 'delete') {
                    $request->setModuleName('schedule');
                    $request->setControllerName('manage-event');
                }
            }
        }
    }

}
