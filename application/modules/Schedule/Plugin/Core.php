<?php

class Schedule_Plugin_Core {

    public function onUserDeleteBefore($event) {
        $payload = $event->getPayload();
        if ($payload instanceof User_Model_User) {
            $scheduleTable = Engine_Api::_()->getDbTable('schedules', 'schedule');
            $scheduleSelect = $scheduleTable->select()->where('owner_id = ?', $payload->getIdentity());
            foreach ($scheduleTable->fetchAll($scheduleSelect) as $schedule) {
                $schedule->delete();
            }

            $settingsTable = Engine_Api::_()->getDbTable('settings', 'schedule');
            $settingsSelect = $settingsTable->select()->where('name LIKE (?)', $payload->getIdentity() . '.%');
            foreach ($settingsTable->fetchAll($settingsSelect) as $setting) {
                $setting->delete();
            }
        }
    }

    public function onItemDeleteBefore($event) {
        $payload = $event->getPayload();

        if ($payload instanceof Schedule_Model_Schedule) {
            $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
            $remindersSelect = $reminderTable->select()->where('schedule_id = ?', $payload->getIdentity());
            foreach ($reminderTable->fetchAll($remindersSelect) as $reminder) {
                $reminder->delete();
            }
        }
    }

}