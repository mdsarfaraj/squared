<?php

class Schedule_Model_Request extends Core_Model_Item_Abstract {
    
    protected $_parent_is_owner = true;

    public function getHref() {
        //if ($this->owner_type == 'user') {
            $owner = Engine_Api::_()->user()->getUser($this->owner_id);
            return $owner->getHref() . '/tab/schedule.profile-schedule/date/' . $this->date . '/dayschedule/1';
        //}

//        if ($this->owner_type == 'group') {
//            $group = Engine_Api::_()->getItem('group', $this->owner_id);
//            if ($group)
//                return $group->getHref() . '/tab/scheduleaddon.schedule/date/' . $this->date;
//        }

        return 'javascript:void(0);';
    }

}
