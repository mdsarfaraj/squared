<?php

class Schedule_Model_DbTable_Schedules extends Engine_Db_Table {

    protected $_rowClass = "Schedule_Model_Schedule";

    public function getDaySchedule($params = array()) {
        $select = $this->select();
        if (isset($params['date']))
            $select->where('date = ?', $params['date']);
        if (isset($params['owner_id']))
            $select->where('owner_id = ?', $params['owner_id']);
        if (isset($params['start_time']))
            $select->where('start_time >= ?', $params['start_time']);
        if (isset($params['end_time']))
            $select->where('end_time <= ?', $params['end_time']);
        if (isset($params['owner_type']))
            $select->where('owner_type = ?', $params['owner_type']);

        $select->order('start_time ASC');

        return $this->fetchAll($select);
    }

    public function checkDate($value, array $params) {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        
        $select = $this->select();
        $select->from($this, array('start_time' => new Zend_Db_Expr('TIME(start_time)'), 'end_time' => new Zend_Db_Expr('TIME(end_time)')))
                ->where('owner_id = ?', $params['user'])
                ->where('date = ?', date('Y-m-d', strtotime($params['date'])))
                ->where('TIME(start_time) != ?', $value)
                ->where(New Zend_Db_Expr("TIME('$value') > TIME(start_time)"))
                ->where(New Zend_Db_Expr("TIME('$value') < TIME(end_time)"));
        if (Engine_Api::_()->core()->hasSubject())
            if ($request->getParam('action') != 'edit')
                $select->where('owner_id = ?', Engine_Api::_()->core()->getSubject()->getIdentity());
            else
                $select->where('schedule_id != ?', Engine_Api::_()->core()->getSubject()->getIdentity());

        return $this->fetchRow($select) === null ? false : true;
    }
    
    public function checkDateRange($start, $end, array $params) {
        $select = $this->select();
        $select->from($this, array('start_time' => new Zend_Db_Expr('TIME(start_time)'), 'end_time' => new Zend_Db_Expr('TIME(end_time)')))
                ->where('owner_id = ?', $params['user'])
                ->where('date = ?', date('Y-m-d', strtotime($params['date'])))
                ->where(New Zend_Db_Expr("'$start' < end_time"))
                ->where(New Zend_Db_Expr("'$end' > start_time"));
        if (isset($params['event_id']))
            $select->where('schedule_id != ?', $params['event_id']);

        return $this->fetchRow($select) === null ? false : true;
    }

}