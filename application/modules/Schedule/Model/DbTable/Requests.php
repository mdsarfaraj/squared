<?php

class Schedule_Model_DbTable_Requests extends Engine_Db_Table {
    
    protected $_rowClass = "Schedule_Model_Request";

    public function getDayRequests($date) {

        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->core()->getSubject();
        
        $select = $this->select();
        if (isset($date))
            $select->where('date = ?', $date);
        
        if ($viewer->isSelf($subject))
            $select->where(new Zend_Db_Expr('(owner_id = "' . $subject->getIdentity() . '" OR requester_id = "' . $subject->getIdentity() . '" )' ));
        else
            $select->where(new Zend_Db_Expr('(owner_id = "' . $subject->getIdentity() . '" AND requester_id = "' . $viewer->getIdentity() . '" )' ));
            
        $select->order('start_time ASC');

        return $this->fetchAll($select);
    }

}
