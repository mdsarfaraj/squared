<?php

class Schedule_Model_Schedule extends Core_Model_Item_Abstract {

    protected $_parent_is_owner = true;

    public function getHref() {
        if ($this->owner_type == 'user') {
            $owner = Engine_Api::_()->user()->getUser($this->owner_id);
            return $owner->getHref() . '/tab/schedule.profile-schedule/date/' . $this->date . '/dayschedule/1';
        }

        if ($this->owner_type == 'group') {
            $group = Engine_Api::_()->getItem('group', $this->owner_id);
            if ($group)
        	return $group->getHref() . '/tab/scheduleaddon.schedule/date/' . $this->date . '/dayschedule/1';
        }
        
        return 'javascript:void(0);';
    }

    public function comments() {
        return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('comments', 'core'));
    }

    public function likes() {
        return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('likes', 'core'));
    }

}