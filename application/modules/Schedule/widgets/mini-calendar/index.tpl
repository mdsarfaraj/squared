<script type="text/javascript">
    en4.core.runonce.add(function() {
        var anchor = $('calendar_<?php echo $this->identity ?>').getParent();

        $('prev_<?php echo $this->identity ?>').removeEvents('click').addEvent('click', function() {
            en4.core.request.send(new Request.HTML({
                url: '<?php echo $this->url(array('module' => 'core', 'controller' => 'widget', 'action' => 'index', 'content_id' => $this->identity), 'default', true) ?>',
                data: {
                    format: 'html',
                    subject: en4.core.subject.guid,
                    date: '<?php echo $this->data['prev'] ?>'
                }
            }), {
                'element': anchor
            })
        })

        $('next_<?php echo $this->identity ?>').removeEvents('click').addEvent('click', function() {
            en4.core.request.send(new Request.HTML({
                url: '<?php echo $this->url(array('module' => 'core', 'controller' => 'widget', 'action' => 'index', 'content_id' => $this->identity), 'default', true) ?>',
                data: {
                    format: 'html',
                    subject: en4.core.subject.guid,
                    date: '<?php echo $this->data['next'] ?>'
                }
            }), {
                'element': anchor
            })
        })
    });
</script>

<?php $month = new Zend_Date(array('year' => $this->data['year'], 'month' => $this->data['month'], 'day' => 1), Zend_Registry::get('Locale')); ?>
<div id="calendar_<?php echo $this->identity ?>" width="100%" class="calendar">
    <table id="days" width="95%" style="text-align: center;" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="7">
        <div id="prev_<?php echo $this->identity ?>" class="prev"><?php echo $this->htmlLink('javascript:void(0);', $this->translate(''), array('onclick' => '', 'class' => 'buttonlink icon_previous')); ?></div>
        <div id="sch_month"><?php echo $month->get(Zend_Date::MONTH_NAME) ?>, <?php echo $month->get(Zend_Date::YEAR) ?></div>
        <div id="next_<?php echo $this->identity ?>" class="next"><?php echo $this->htmlLink('javascript:void(0);', $this->translate(''), array('onclick' => '', 'class' => 'buttonlink_right icon_next')); ?></div>
        </th>
        </tr>
        <tr class="week_day">
            <th><?php echo $this->translate('Sun') ?></th>
            <th><?php echo $this->translate('Mon') ?></th>
            <th><?php echo $this->translate('Tue') ?></th>
            <th><?php echo $this->translate('Wed') ?></th>
            <th><?php echo $this->translate('Thu') ?></th>
            <th><?php echo $this->translate('Fri') ?></th>
            <th><?php echo $this->translate('Sat') ?></th>
        </tr>
        <?php $day = 1; ?>
        <?php $next_month_day = 1; ?>
        <?php for ($i = 0; $i < 6; $i++) : ?>
            <?php if ($i == 5 && $day >= $this->data['num_days']) : ?>
                <?php continue ?>
            <?php endif ?>
            <tr class="month_days">
                <?php for ($j = 0; $j <= 6; $j++) : ?>
                    <?php if ($this->data['skip'] > 0) : ?>
                        <td class="none"><span><?php echo $this->data['num_days_prev'] ?></span></td>
                        <?php $this->data['skip']--; ?>
                        <?php $this->data['num_days_prev']++; ?>
                    <?php elseif ($day > $this->data['num_days']) : ?>
                        <td class="none">
                            <span>
                                <?php
                                echo $next_month_day;
                                $next_month_day++;
                                ?>
                            </span>
                        </td>
                    <?php else : ?>
                        <?php if ($j == 0 || $j == 6) : ?>
                            <?php
                            $class[] = 'holiday';
                            ?>
                        <?php else : ?>
                            <?php
                            $class[] = 'day';
                            ?>
                        <?php endif ?>
                        <?php if (($this->data['cur_day'] == $day) && (date('m') == $this->data['month']) && (date('Y') == $this->data['year'] )) : ?>
                            <?php
                            $class[] = 'today';
                            ?>
                        <?php endif ?>
                        <?php if (isset($this->data['busydays'][$day]) && $this->data['busydays'][$day]['busy'] == 'partial') : ?>
                            <?php
                            $class[] = 'busy_partial';
                            ?>
                        <?php elseif (isset($this->data['busydays'][$day]) && $this->data['busydays'][$day]['busy'] == 'full') : ?>
                            <?php
                            $class[] = 'busy_full';
                            ?>
                        <?php endif ?>
                        <?php
                        $class_string = implode(' ', $class);
                        unset($class);
                        ?>
                        <?php $now = new Zend_Date(array('year' => $this->data['year'], 'month' => $this->data['month'], 'day' => $day), Zend_Registry::get('Locale')); ?>
                        <td class="<?php echo $class_string ?>">
                            <span class="day_info_min">
                                <?php echo $this->htmlLink($this->url(array('date' => $this->data['year'] . '-' . $this->data['month'] . '-' . ($day <= 9 ? '0' . $day : $day), 'user_id' => $this->user->getIdentity()), 'schedule_view'), $day, array('class' => 'smoothbox', 'style' => 'display: block;')) ?>
                                <?php echo isset($this->data['busydays'][$day]['total_count']) ? '<i class="total_events">' . $this->data['busydays'][$day]['total_count'] . '</i>' : '' ?>
                            </span>
                        </td>
                        <?php $day++; ?>
                    <?php endif ?>
                <?php endfor ?>
            </tr>
        <?php endfor ?>
    </table>

    <?php

        $viewer = Engine_Api::_()->user()->getViewer();
        $levelId = $viewer->level_id;

    ?>
    <?php if ($this->data['is_owner'] === true): ?>

        <div class="simon-wants">
            <?php echo $this->htmlLink($this->url(array('action' => 'create', 'format' => 'smoothbox'), 'schedule_general'), $this->translate('Add Event'), array('class' => 'buttonlink icon_schedule_new add_event smoothbox')) ?>

             <!--appointment link-->
            <?php if($levelId == 1 || $levelId == 6) { ?>
               <?php echo $this->htmlLink($this->url(array('module' => 'intechcore', 'controller'=>'appointment', 'action' => 'create'), 'appointment_general'), $this->translate('Appointment'), array('class' => 'buttonlink icon_schedule_new add_event')) ?>
            <?php } ?>
        
            <?php echo $this->htmlLink($this->url(array('action' => 'settings', 'format' => 'smoothbox'), 'schedule_general'), $this->translate('Settings'), array('class' => 'buttonlink icon_settings add_event smoothbox')) ?>
            <?php ?>
      

        </div>

    <?php endif ?>
</div>

<style type="text/css">
    .simon-wants{
        width: 100%;
    }
</style>