<?php

class Schedule_Widget_MiniCalendarController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        $viewer = Engine_Api::_()->user()->getViewer();

        $timezone = Zend_Registry::get('Zend_View')->locale()->getTimezone();

        $is_owner = false;
        //don't render if we don't have username
        $username = $this->_getParam('user');
        if (!$username) {
            if ($viewer->getIdentity() == 0)
                return $this->setNoRender();

            $this->view->user = $user = $viewer;
            $is_owner = true;
        }
        else
            $this->view->user = $user = Engine_Api::_()->user()->getUser($username);

        if (!$user)
            return $this->setNoRender();

        $date = Zend_Controller_Front::getInstance()->getRequest()->getParam('date');

        if (null == $date)
            $date = 'now';

        try {
            $date = new DateTime($date, new DateTimeZone($timezone));
            $now = new DateTime('now', new DateTimeZone($timezone));
        } catch (Exception $exc) {
            return $this->setNoRender();
        }

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $tmp_date = clone $date;
        $tmp_date->modify('-1 month');
        $prev = $tmp_date->format('Y-m');
        $num_days_prev = $tmp_date->format('t');

        $tmp_date = clone $date;
        $tmp_date->modify('+1 month');
        $next = $tmp_date->format('Y-m');

        $num_days = $date->format('t'); // counting days in month

        $tmp_date = clone $date;
        $tmp_date->modify('first day of');
        $skip = $tmp_date->format('w'); // first day of month
        
        $num_days_prev = $num_days_prev - $skip +1;

        $cur_day = $now->format('j');

        $table = Engine_Api::_()->getDbTable('schedules', 'schedule');
        $select = $table->select();
        $select->distinct()
                ->from($table, array('DAY(date) as day', 'start_time', 'end_time'))
                ->where('YEAR(date) = ?', $year)
                ->where('MONTH(date) = ?', $month)
                ->where('owner_id = ?', $user->getIdentity())
                ->where('owner_type = ?', 'user');
        $schedule = $table->fetchAll($select);

        $setting = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($user->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));

        list($totalStartTimeH, $totalStartTimeM) = explode(':', $setting['time_start']);
        list($totalEndTimeH, $totalEndTimeM) = explode(':', $setting['time_end']);

        $totalScheduleTime = (((int) $totalEndTimeH * 6) + ((int) $totalEndTimeM / 10)) - (((int) $totalStartTimeH * 6) + ((int) $totalStartTimeM / 10));

        if ($schedule) {
            foreach ($schedule as $day) {
                $day->start_time = $this->view->locale()->toTime($day->start_time, array('format' => 'H:mm'));
                $day->end_time = $this->view->locale()->toTime($day->end_time, array('format' => 'H:mm'));

                sscanf($day->start_time, "%d:%d", $sHours, $sMinutes);
                sscanf($day->end_time, "%d:%d", $eHours, $eMinutes);

                $sMins = ($sHours * 6) + ($sMinutes / 10);
                $eMins = ($eHours * 6) + ($eMinutes / 10);

                if (isset($total[$day->day]['busy'])) {
                    $total[$day->day]['busy'] += ($eMins - $sMins);
                } else {
                    $total[$day->day]['busy'] = ($eMins - $sMins);
                }

                if (isset($total[$day->day]['total_count'])) {
                    $total[$day->day]['total_count']++;
                } else {
                    $total[$day->day]['total_count'] = 1;
                }

                if ($total[$day->day]['busy'] >= $totalScheduleTime) {
                    $tmp[$day->day]['busy'] = 'full';
                } else {
                    $tmp[$day->day]['busy'] = 'partial';
                }
                $tmp[$day->day]['total_count'] = $total[$day->day]['total_count'];
            }
        }

        $this->view->data = array(
            'year' => $year,
            'month' => $month,
            'prev' => $prev,
            'next' => $next,
            'num_days' => $num_days,
            'skip' => $skip,
            'num_days_prev' => isset($num_days_prev) ? $num_days_prev : null,
            'cur_day' => $cur_day,
            'busydays' => isset($tmp) ? $tmp : null,
            'is_owner' => $is_owner
        );
    }

    public function getCacheKey() {
        return false;
    }

}