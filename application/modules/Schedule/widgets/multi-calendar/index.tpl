<?php
$localeObject = Zend_Registry::get('Locale');
$ampm = Engine_Api::_()->getApi('settings', 'schedule')->getSetting($this->viewer->getIdentity() . '.' . 'ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm'));

$months = Zend_Locale::getTranslationList('months', $localeObject);
if ($months['default'] == NULL) {
    $months['default'] = "wide";
}
$months = $months['format'][$months['default']];

$days = Zend_Locale::getTranslationList('days', $localeObject);
if ($days['default'] == NULL) {
    $days['default'] = "wide";
}
$days = $days['format'][$days['default']];
?>
<?php
if ($this->first == true) {
    $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Schedule/externals/calendar/calendar.compat.js');
    $this->headLink()->appendStylesheet($this->baseUrl() . '/application/modules/Schedule/externals/calendar/styles.css');
}

if ($this->first == true) :
    $this->headScript()->captureStart();
    ?>
    en4.core.runonce.add(function(){
    neCalendar=new Class({
    Implements: [Calendar],

    clicked: function(td, day, cal) {
    var anchor = $('schedule_list_<?php echo $this->identity ?>').getParent();
    cal.val = (this.value(cal) == day) ? null : new Date(cal.year, cal.month, day);
    var value = this.format(cal.val, 'Y-m-d');
    this.toggle(cal);
    en4.core.request.send(new Request.HTML({
    url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
    data : {
    format : 'html',
    subject : en4.core.subject.guid,
    date : value
    }
    }), {
    'element' : anchor
    })
    }
    });
    });

    <?php
    $this->headScript()->captureEnd();
endif;
?>
<script type="text/javascript">
    en4.core.runonce.add(function() {

        new neCalendar({'date-date': 'd/m/Y'}, {
            classes: ['event_calendar'],
            pad: 0,
            direction: 0,
            months: <?php echo Zend_Json::encode(array_values($months)) ?>,
            days: <?php echo Zend_Json::encode(array_values($days)) ?>,
            day_suffixes: ['', '', '', ''],
            onHideStart: function() {
                if (typeof cal_date_onHideStart == 'function')
                    cal_date_onHideStart();
            },
            onHideComplete: function() {
                if (typeof cal_date_onHideComplete == 'function')
                    cal_date_onHideComplete();
            },
            onShowStart: function() {
                if (typeof cal_date_onShowStart == 'function')
                    cal_date_onShowStart();
            },
            onShowComplete: function() {
                if (typeof cal_date_onShowComplete == 'function')
                    cal_date_onShowComplete();
            }
        })

        var i = 0;
        $$('button.event_calendar').each(function(el) {
            i++;
            if (i > 1) {
                el.destroy();
            }
        });
    });
</script>
<script type="text/javascript">
    function expand(elem) {
        var size = elem.getSize();
        if (size.y <= 56) {
            elem.setStyle('height', 'auto');
            elem.setStyle('z-index', '2');
            elem.addEvent('mouseout', function() {
                elem.setStyle('height', size.y - 2 + 'px');
                elem.setStyle('z-index', '0');
            });
        }
    }
    ;
</script>

<div class="schedule_list" id="schedule_list_<?php echo $this->identity ?>">
    <form method="post" action="<?php echo $this->url() ?>" class="global_form" enctype="application/x-www-form-urlencoded">
        <div>
            <div>
                <div class="form-elements">
                    <div class="form-wrapper" id="date-wrapper">
                        <div class="form-label" id="date-label">
                            <label class="optional" for="date"><?php echo $this->translate('Select date for schedule review') ?></label>
                        </div>
                        <div class="form-element" id="date-element">
                            <div class="event_calendar_container" style="display:inline">
                                <?php echo $this->formHidden('date[date]', $this->data['date']['day'] . '/' . $this->data['date']['month'] . '/' . $this->data['date']['year'], array_merge(array('class' => 'calendar', 'id' => 'date-date'), array())) ?>
                                <span class="calendar_output_span" id="calendar_output_span_date-date"><?php echo $this->data['date']['day'] . '/' . $this->data['date']['month'] . '/' . $this->data['date']['year'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php $now = new Zend_Date($this->data['date'], Zend_Registry::get('Locale')); ?>
    <?php
    if ($this->data['date']['day'] > 0 && $this->data['date']['day'] < 10)
        $this->data['date']['day'] = '0' . $this->data['date']['day'];
    ?>

    <?php if (isset($this->data['schedule'])) : ?>
        <?php foreach ($this->data['schedule'] as $userId => $schedule) : ?>
            <?php $user = Engine_Api::_()->user()->getUser($userId) ?>
            <div class="user">
                <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon')) ?>
                <div class="name"><?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?></div>
                <?php if ($schedule) : ?>
                    <?php foreach ($schedule as $group) : ?>
                        <?php $this->left = 0 ?>
                        <?php foreach ($group['items'] as $count => $event) : ?>
                            <?php //@TODO: rework it to ViewHelper ?>
                            <?php $this->itemWidth = 100 / count($group['items']) ?>
                            <?php
                            if ($count > 0)
                                $this->left += (100 / count($group['items']))
                                ?>
                            <div class="
                                 schedule_time 
                                 event_text 
                                 <?php echo (($event['end'] * 9) == 9) ? 'small_event' : '' ?>
                                 " 
                                 onmouseover="expand(this);" 
                                 style="top:<?php echo $event['start'] * 9 + 54 ?>px; height:<?php echo (($event['end'] * 9) == 9) ? '' : $event['end'] * 9 ?>px; width: <?php echo $this->itemWidth ?>%; left: <?php echo $this->left ?>%"
                                 >
                                <div class="pos_relative">
                                    <div class="time_perion"><?php echo $ampm == 0 ? date('g:i A', strtotime($event['start_time'])) : $event['start_time'] ?> - <?php echo $ampm == 0 ? date('g:i A', strtotime($event['end_time'])) : $event['end_time'] ?></div>
                                    <div class="schelude_title"><?php echo $event['private'] ? $this->translate('Private Event') : $event['title'] ?></div>
                                    <div class="schelude_descr"><?php echo $event['private'] ? '' : $event['description'] ?></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        <?php endforeach ?>
    <?php endif ?>
    <?php
    list($sH, $sM) = explode(':', $this->data['setting']['start_time']);
    list($eH, $eM) = explode(':', $this->data['setting']['end_time']);
    ?>
    <ul>
        <?php for ($i = (int) $sH; $i <= (int) $eH; $i++) : ?>
            <li><?php echo $ampm == 0 ? date('g:i \\<\b\r\>A', strtotime($i . ':00:00')) : $i . ':00' ?></li>
        <?php endfor ?>
    </ul>
</div>