<?php

class Schedule_Widget_MultiCalendarController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

        $timezone = Zend_Registry::get('Zend_View')->locale()->getTimezone();

        //don't render if we don't have username
        $usernames = $this->_getParam('user');

        if (!$usernames)
            return $this->setNoRender();

        $usernames_arr = explode(',', $usernames);

        $date = $this->_getParam('date', '');

        if ($date != '') {
            $this->view->first = false;
        } else {
            $this->view->first = true;
            $date = 'now';
        }

        try {
            $date = new DateTime($date, new DateTimeZone($timezone));
            $now = new DateTime('now', new DateTimeZone($timezone));
        } catch (Exception $exc) {
            return $this->setNoRender();
        }
        
        if ($this->_getParam('format') == 'html')
            $this->getElement()->removeDecorator('container');

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $viewDate = array('year' => $year, 'month' => $month, 'day' => $day);

        $i = 0;
        foreach ($usernames_arr as $username) {
            $i++;
            if ($i <= 3) {
                $user = Engine_Api::_()->user()->getUser($username);

                $user_settings = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($user->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));
                $settings_tmp['start'][] = $user_settings['time_start'];
                $settings_tmp['end'][] = $user_settings['time_end'];
            }
        }

        $settings['start_time'] = min($settings_tmp['start']);
        $settings['end_time'] = max($settings_tmp['end']);
        $settings['ampm'] = Engine_Api::_()->getApi('settings', 'schedule')->getSetting($viewer->getIdentity() . '.ampm', Engine_Api::_()->getApi('settings', 'core')->getSetting('schedule.ampm'));
        $step = (int) substr($settings['start_time'], 0, 2) * 6;

        $groups = array();
        
        foreach ($usernames_arr as $username) {
            $user = Engine_Api::_()->user()->getUser($username);

            if ($user->getIdentity()) {
                $tmp[$user->getIdentity()] = '';

                $user_settings = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($user->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));
                $settings_tmp['start'][] = $user_settings['time_start'];
                $settings_tmp['end'][] = $user_settings['time_end'];

                $oldTz = date_default_timezone_get();
                date_default_timezone_set($timezone);
                $start_time = strtotime($date->format('Y-m-d') . ' ' . $user_settings['time_start']);
                $end_time = strtotime($date->format('Y-m-d') . ' ' . $user_settings['time_end']);
                date_default_timezone_set($oldTz);

                $user_start_time = date('Y-m-d H:i:s', $start_time);
                $user_end_time = date('Y-m-d H:i:s', $end_time);

                $schedule = Engine_Api::_()->getItemTable('schedule')->getDaySchedule(array(
                    'date'       => $date->format('Y-m-d'),
                    'owner_id'   => $user->getIdentity(),
                    'start_time' => $user_start_time,
                    'end_time'   => $user_end_time,
                    'owner_type' => 'user',
                ));

                foreach ($schedule as $scheduleItem) {
                    $item = $scheduleItem->toArray();

                    $item['private'] = Zend_Controller_Action_HelperBroker::getStaticHelper('requireAuth')->setAuthParams($scheduleItem, $viewer, 'view')->setNoForward()->isValid();
                    //$item['private'] = Zend_Contoller_A  !$this->_helper->requireAuth()->setAuthParams($scheduleItem, $viewer, 'view')->setNoForward()->isValid();
                    // Convert times
                    $item['start_time'] = $this->view->locale()->toTime($item['start_time'], array('format' => 'H:mm'));
                    $item['end_time'] = $this->view->locale()->toTime($item['end_time'], array('format' => 'H:mm'));

                    //counting params
                    $item['start'] = self::_toMin($item['start_time']) - $step;
                    $item['end'] = (self::_toMin($item['end_time']) - $step) - ($this->_toMin($item['start_time']) - $step);


                    $item['short'] = false;
                    $item['s'] = strtotime($item['start_time']);
                    $item['e'] = $item['end_time'];

                    $end_time = self::_toMin($item['end_time']);
                    $start_time = self::_toMin($item['start_time']);

                    if (($end_time - $start_time) <= 2) {
                        $item['short'] = true;
                    }

                    $tmp[$user->getIdentity()][] = $item;
                }

                usort($tmp[$user->getIdentity()], array($this, 'overlappingSort'));

                foreach ($tmp[$user->getIdentity()] as $item) {
                    $found = false;

                    foreach ($groups[$user->getIdentity()] as &$group) {
                        if ((strtotime($item['start_time']) >= $group['start_time'] && strtotime($item['start_time']) <= $group['end_time']) ||
                                (strtotime($item['end_time']) >= $group['start_time'] && strtotime($item['end_time']) <= $group['end_time'])) {
                            $group['items'][] = $item;
                            $found = true;
                        }
                    }

                    if (!$found) {
                        $groups[$user->getIdentity()][] = array(
                            'start_time' => strtotime($item['start_time']),
                            'end_time'   => strtotime($item['end_time']),
                            'items'      => array($item),
                        );
                    }
                }
            }
        }

        $this->view->data = array(
            'setting'  => $settings,
            'date'     => $viewDate,
            'schedule' => $groups,
            'step'     => 'step'
        );
    }

    public function getCacheKey() {
        return false;
    }

    protected function _toMin($val) {
        if (is_string($val)) {
            $val = explode(':', $val);
            return ((int) $val[0] * 6 + (int) $val[1] / 10);
        }

        if (isArray($val)) {
            return ((int) $val[0] * 6 + (int) $val[1] / 10);
        }
    }

    static function overlappingSort($a, $b) {
        if ($a['start'] == $b['start']) {
            $aTS = $a['end'];
            $bTS = $b['end'];
        } else {
            $aTS = $a['start'];
            $bTS = $b['start'];
        }
        if ($aTS == $bTS)
            return 0;
        return ($aTS < $bTS) ? -1 : 1;
    }

}
