<?php

class Schedule_Widget_ProfileScheduleController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!Engine_Api::_()->core()->hasSubject()) {
            return $this->setNoRender();
        }

        $timezone = Zend_Registry::get('Zend_View')->locale()->getTimezone();

        $subject = Engine_Api::_()->core()->getSubject('user');
        if (!$subject->authorization()->isAllowed($viewer, 'view')) {
            return $this->setNoRender();
        }

        $this->getElement()->removeDecorator('Title');

        $date = Zend_Controller_Front::getInstance()->getRequest()->getParam('date');
        $this->view->dayschedule = $daySchedule = Zend_Controller_Front::getInstance()->getRequest()->getParam('dayschedule');

        if (null == $date)
            $date = 'now';

        try {
            $date = new DateTime($date, new DateTimeZone($timezone));
            $now = new DateTime('now', new DateTimeZone($timezone));
        } catch (Exception $exc) {
            return $this->setNoRender();
        }

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $tmp_date = clone $date;
        $tmp_date->modify('-1 month');
        $prev = $tmp_date->format('Y-m');
        $num_days_prev = $tmp_date->format('t');

        $tmp_date = clone $date;
        $tmp_date->modify('+1 month');
        $next = $tmp_date->format('Y-m');

        $num_days = $date->format('t'); // counting days in month

        $tmp_date = clone $date;
        $tmp_date->modify('first day of');
        $skip = $tmp_date->format('w'); // first day of month
        
        $num_days_prev = $num_days_prev - $skip +1;

        $cur_day = $now->format('j');

        $isSelf = $viewer->isSelf($subject);

        $table = Engine_Api::_()->getDbTable('schedules', 'schedule');
        $select = $table->select();
        $select->distinct()
                ->from($table, array('DAY(date) as day', 'start_time', 'end_time'))
                ->where('YEAR(date) = ?', $year)
                ->where('MONTH(date) = ?', $month)
                ->where('owner_id = ?', $subject->getIdentity())
                ->where('owner_type = ?', 'user');
        $schedule = $table->fetchAll($select);

        $setting = Engine_Api::_()->getApi('settings', 'schedule')->getFlatSetting($subject->getIdentity(), Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('schedule'));

        list($totalStartTimeH, $totalStartTimeM) = explode(':', $setting['time_start']);
        list($totalEndTimeH, $totalEndTimeM) = explode(':', $setting['time_end']);

        $totalScheduleTime = (((int) $totalEndTimeH * 6) + ((int) $totalEndTimeM / 10)) - (((int) $totalStartTimeH * 6) + ((int) $totalStartTimeM / 10));

        if ($schedule) {
            $events_count = $schedule->count();
            $total = array();
            foreach ($schedule as $day) {
                $day->start_time = $this->view->locale()->toTime($day->start_time, array('format' => 'H:mm'));
                $day->end_time = $this->view->locale()->toTime($day->end_time, array('format' => 'H:mm'));

                //list($sTime_h, $sTimeM) = explode(':', $day->start_time);
                sscanf($day->start_time, "%d:%d", $sHours, $sMinutes);
                sscanf($day->end_time, "%d:%d", $eHours, $eMinutes);
                //list($eTime_h, $eTimeM) = explode(':', $day->end_time);

                $sMins = ($sHours * 6) + ($sMinutes / 10);
                $eMins = ($eHours * 6) + ($eMinutes / 10);

                if (isset($total[$day->day]['busy']))
                    $total[$day->day]['busy'] += ($eMins - $sMins);
                else
                    $total[$day->day]['busy'] = ($eMins - $sMins);

                if (isset($total[$day->day]['total_count']))
                    $total[$day->day]['total_count']++;
                else
                    $total[$day->day]['total_count'] = 1;

                if ($total[$day->day]['busy'] >= $totalScheduleTime)
                    $tmp[$day->day]['busy'] = 'full';
                else
                    $tmp[$day->day]['busy'] = 'partial';

                $tmp[$day->day]['total_count'] = $total[$day->day]['total_count'];
            }
        }

        $this->view->data = array(
            'date' => Zend_Controller_Front::getInstance()->getRequest()->getParam('date') ? $date->format('Y-m-d') : null,
            'year' => $year,
            'month' => $month,
            'prev' => $prev,
            'next' => $next,
            'num_days' => $num_days,
            'skip' => $skip,
            'num_days_prev' => isset($num_days_prev) ? $num_days_prev : null,
            'cur_day' => $cur_day,
            'isSelf' => $isSelf,
            'busydays' => isset($tmp) ? $tmp : null,
            'events_count' => $events_count
        );
    }

}