<script type="text/javascript">
    var identity = '<?php echo $this->identity ?>';

    en4.core.runonce.add(function() {
<?php if (null != $this->dayschedule) : ?>
            daySchedule('<?php echo $this->data['date'] ?>', '<?php echo $this->subject()->getIdentity() ?>');
<?php endif ?>
        var anchor = $('calendar_<?php echo $this->identity ?>').getParent();
        $('prev_<?php echo $this->identity ?>').removeEvents('click').addEvent('click', function() {
            en4.core.request.send(new Request.HTML({
                url: en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
                data: {
                    format: 'html',
                    subject: en4.core.subject.guid,
                    date: '<?php echo $this->data['prev'] ?>'
                }
            }), {
                'element': anchor
            })
        });

        $('next_<?php echo $this->identity ?>').removeEvents('click').addEvent('click', function() {
            en4.core.request.send(new Request.HTML({
                url: en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
                data: {
                    format: 'html',
                    subject: en4.core.subject.guid,
                    date: '<?php echo $this->data['next'] ?>'
                }
            }), {
                'element': anchor
            });
        });
    });

    function isValidDate(date) {
        var matches = /^(\d{4})[-](\d{2})[-](\d{1,2})$/.exec(date);
        if (matches == null)
            return false;
        var d = matches[3];
        var m = matches[2] - 1;
        var y = matches[1];
        var composedDate = new Date(y, m, d);
        return composedDate.getDate() == d &&
                composedDate.getMonth() == m &&
                composedDate.getFullYear() == y;
    }

    function daySchedule(date, user_id) {
        var check = date.split('-');
        console.log(date);
        if (check.length == 3) {
            var anchor = $('calendar_<?php echo $this->identity ?>').getParent();
            en4.core.request.send(new Request.HTML({
                url: en4.core.baseUrl + 'schedule/' + user_id + '/' + date,
                data: {
                    format: 'html',
                    identity: identity,
                }
            }), {
                element: anchor,
                force: true
            });
        }
    }
</script>

<?php $month = new Zend_Date(array('year' => $this->data['year'], 'month' => $this->data['month'], 'day' => 1), Zend_Registry::get('Locale')); ?>
<div id="calendar_<?php echo $this->identity ?>" width="100%" class="calendar">
    <table id="days" width="100%" style="text-align: center;"  cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="7">
        <div class="prev" id="prev_<?php echo $this->identity ?>"><?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array('onclick' => '', 'class' => 'buttonlink icon_previous')); ?></div>
        <div id="sch_month"><?php echo $month->get(Zend_Date::MONTH_NAME) ?>, <?php echo $month->get(Zend_Date::YEAR) ?></div>
        <div class="next" id="next_<?php echo $this->identity ?>"><?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array('onclick' => '', 'class' => 'buttonlink_right icon_next')); ?></div>
        </th>
        </tr>
        <tr class="week_day">
            <th><?php echo $this->translate('Sunday') ?></th>
            <th><?php echo $this->translate('Monday') ?></th>
            <th><?php echo $this->translate('Tuesday') ?></th>
            <th><?php echo $this->translate('Wednesday') ?></th>
            <th><?php echo $this->translate('Thursday') ?></th>
            <th><?php echo $this->translate('Friday') ?></th>
            <th><?php echo $this->translate('Saturday') ?></th>
        </tr>
        <?php $day = 1; ?>
        <?php $next_month_day = 1; ?>
        <?php for ($i = 0; $i < 6; $i++) : ?>
            <?php if ($i == 5 && $day >= $this->data['num_days']) : ?>
                <?php continue ?>
            <?php endif ?>
            <tr class="month_days">
                <?php for ($j = 0; $j <= 6; $j++) : ?>
                    <?php if ($this->data['skip'] > 0) : ?>
                        <td class="none"><span><?php echo $this->data['num_days_prev'] ?></span></td>
                        <?php $this->data['skip'] --; ?>
                        <?php $this->data['num_days_prev'] ++; ?>
                    <?php elseif ($day > $this->data['num_days']) : ?>
                        <td class="none">
                            <span>
                                <?php
                                echo $next_month_day;
                                $next_month_day++;
                                ?>
                            </span>
                        </td>
                    <?php else : ?>
                        <?php if ($j == 0 || $j == 6) : ?>
                            <?php
                            $class[] = 'holiday';
                            ?>
                        <?php else : ?>
                            <?php
                            $class[] = 'day';
                            ?>
                        <?php endif ?>
                        <?php if (($this->data['cur_day'] == $day) && (date('m') == $this->data['month']) && (date('Y') == $this->data['year'] )) : ?>
                            <?php
                            $class[] = 'today';
                            ?>
                        <?php endif ?>
                        <?php if (isset($this->data['busydays'][$day]) && $this->data['busydays'][$day]['busy'] == 'partial') : ?>
                            <?php
                            $class[] = 'busy_partial';
                            ?>
                        <?php elseif (isset($this->data['busydays'][$day]) && $this->data['busydays'][$day]['busy'] == 'full') : ?>
                            <?php
                            $class[] = 'busy_full';
                            ?>
                        <?php endif ?>
                        <?php
                        $class_string = implode(' ', $class);
                        unset($class);
                        ?>
                        <?php $now = new Zend_Date(array('year' => $this->data['year'], 'month' => $this->data['month'], 'day' => $day), Zend_Registry::get('Locale')); ?>
                        <td class="<?php echo $class_string ?>">
                            <span class="day_info">
                                <?php $date = $this->data['year'] . '-' . $this->data['month'] . '-' . ($day <= 9 ? '0' . $day : $day) ?>
                                <?php $user_id = Engine_Api::_()->core()->getSubject()->getIdentity() ?>
                                <a href="javascript:void(0)" onclick="daySchedule('<?php echo $date ?>', '<?php echo $user_id ?>')">
                                    <?php echo $day ?>
                                    <?php echo isset($this->data['busydays'][$day]['total_count']) ? '<i class="total_events">' . $this->translate(array('%s event', '%s events', $this->data['busydays'][$day]['total_count']), $this->locale()->toNumber($this->data['busydays'][$day]['total_count'])) . '</i>' : '' ?>
                                </a>
                                <?php //echo $this->htmlLink($this->url(array('date' => $this->data['year'] . '-' . $this->data['month'] . '-' . ($day <= 9 ? '0' . $day : $day), 'user_id' => Engine_Api::_()->core()->getSubject()->getIdentity()), 'schedule_view'), $day, array('class' => 'smoothbox', 'style' => 'display: block;')) ?>
                                <?php //echo isset($this->data['busydays'][$day]['total_count']) ? '<i class="total_events">' . $this->translate(array('%s event', '%s events', $this->data['busydays'][$day]['total_count']), $this->locale()->toNumber($this->data['busydays'][$day]['total_count'])) . '</i>' : '' ?>
                            </span>
                        </td>
                        <?php $day++; ?>
                    <?php endif ?>
                <?php endfor ?>
            </tr>
        <?php endfor ?>
    </table>

    <?php

        $viewer = Engine_Api::_()->user()->getViewer();
        $levelId = $viewer->level_id;

    ?>
    
    <?php if ($this->data['isSelf'] === true) : ?>

        <?php echo $this->htmlLink($this->url(array('action' => 'create', 'format' => 'smoothbox'), 'schedule_general'), $this->translate('Add Event'), array('class' => 'buttonlink icon_schedule_new add_event smoothbox')) ?>
          <!--appointment link-->
            <?php if($levelId == 1 || $levelId == 6) { ?>
               <?php echo $this->htmlLink($this->url(array('module' => 'intechcore', 'controller'=>'appointment', 'action' => 'create'), 'appointment_general'), $this->translate('Appointment'), array('class' => 'buttonlink icon_schedule_new add_event')) ?>
            <?php } ?>

        <?php echo $this->htmlLink($this->url(array('action' => 'settings', 'format' => 'smoothbox'), 'schedule_general'), $this->translate('Settings'), array('class' => 'buttonlink icon_settings add_event smoothbox')) ?>

    <?php endif ?>
</div>