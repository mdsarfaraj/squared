<div>
  <button id="chat-btn"> <i class="fa fa-comment" aria-hidden="true"></i> </button>
</div>
<div class="l_c_h">
  <div class="c_h">
      <div class="left_c">
            <div class="left right_c left_icons">
                   <a href="#" class="mini" style="font-size:23px;">-</a>
            </div>
            <div class="left center_icons">
                 <p class="live-help-text">Squared Connect</p>
            </div>      
        </div>
        <div>
          <iframe scrolling="no" id="chat_container" src="<?php echo $this->meetingUrl; ?>" allow="camera; microphone; encrypted media" style="overflow: auto;" ></iframe>
        </div> 
</div>
</div>

<script type="text/javascript">
  
  jQuery(function(){


    function getCameraAccess(){
       navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;

      if (navigator.getUserMedia) {
         navigator.getUserMedia({ audio: true, video: true },
            function(stream) {
               console.log('success');
            },
            function(err) {
               console.log("The following error occurred: " + err.name);
            }
         );
      } else {
         console.log("getUserMedia not supported");
      }
    }
   
        console.log('On beforeonlog inisitead.');
    

          jQuery(window).on('beforeunload', function(){
              var iframe = jQuery('#chat_container');
              if(iframe.css('display') == 'inline' || iframe.css('display') == 'block' )
              {/*
                  jQuery.confirm({
                      title: 'Confirm!',
                      content: 'Simple confirm!',
                      buttons: {
                          confirm: function () {
                              jQuery.alert('Confirmed!');
                          },
                          cancel: function () {
                              jQuery.alert('Canceled!');
                          }
                      }
                  });*/

                var c = confirm();

                if(c){
                  return true;
                }
                else
                   return false;
              }
          });
    
    


   jQuery(".c_h").click(function(e) {
          jQuery(".l_c_h").css('display','none');
            jQuery("#chat-btn").show();
            jQuery("#chat_container").slideToggle("slow");
            return false
        });

   jQuery("#chat-btn").click(function(e) {
            getCameraAccess();
            jQuery(".l_c_h").css('display','block');

            jQuery("#chat-btn").hide();
            jQuery("#chat_container").slideToggle("slow");
            return false
        });

});
</script>

<style type="text/css">
  
    #nd_widget_visitors{
      display: none!important;
    }
    #chat-btn{
      background-color: #1aa3b3; /* Green */
      border: none;
      color: white;
      padding: 15px;
      text-align: center;
      cursor: pointer;
      bottom: 70px;
      z-index: 99999;
      border-radius: 50%;
      position: fixed;
      right: 20px;
      font-size: 1.8rem;
  }

  p.live-help-text {
    color: #fff;
    font-size: 14px;
    margin-top: 5px;
    font-weight: bolder;
  }

  #chat_container{
    height: 590px;
    border: none;
    width: 100%;
    overflow: hidden;
    display: none;
    padding-top: 5px;
  }
  .l_c_h {
    width:900px;
    position:fixed;
    bottom:0;
    right: 5px;
    background:#ffc712;
    z-index:9999999;
    display:none;
    }

  .l_c_h .c_h {
    cursor:pointer;
    border-radius:0px;
    background: #ffc712;
  }
  .l_c_h .left_c {
    width:200px;
  }
  .l_c_h .right_c {
    background:#ffc712;
    line-height: 36px;
  }
  .l_c_h .right_c  a {
   
    cursor: pointer !important;
    line-height: normal !important;
    margin-top: 0 !important;
    padding: 1px 0px !important;
    text-align: center !important;
    text-decoration:none;
    font-weight:600;
    color: #fff;
  }

 
  .left{
    float:left;
  }
  .right{
    float:right;
  }
  .left_icons{
    width:35px;
    height:auto;
    text-align:center;
    color:#999;
    background: #ffc712;
    font-size:15px;
  }
  .left_icons a{
    font-weight:normal;
  }
  .center_icons{
    text-align:center;
    padding:2px 0px 0px 5px;
  }
  
  @media only screen and (min-device-width : 768px) and (max-device-width : 1024px)  {

    #chat-btn{
      display: none!important;
    }

  }
  
</style>

