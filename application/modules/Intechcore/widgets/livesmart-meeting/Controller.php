<?php
/**
 
 */
class Intechcore_Widget_LivesmartMeetingController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {

  	$viewer = Engine_Api::_()->user()->getViewer();
  	if(!$viewer->getIdentity()){
  		return $this->setNoRender();
  	}


    //check the tablet and mobile device
    if(Engine_Api::_()->sitepwa()->isMobileDevice() || Engine_Api::_()->sitepwa()->isTabletDevice()){
        return $this->setNoRender();
    }

  	$id = $viewer->getIdentity();
  	$user = Engine_Api::_()->getItem("user",$id);
  	//$name = $user->getTitle();
    $email = $user->email;

    $levelId = $user->level_id;
    
    if($levelId == 1){
      $user_level = "admin";
    }else{
      $user_level = "client";
    }

    $table = Engine_Api::_()->getDbtable('logins','intechcore');
    $select = $table->select()->where('user_id = ?',$id);
    $passwordData = $table->fetchRow($select);
    
    $password = $passwordData->value;
    
  	$this->view->meetingUrl = "https://squaredconnect.conxt.net/app/index.html?email=$email&value=$password&role=$user_level";

  	
  }
}
