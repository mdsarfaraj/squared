<?php
/**
 
 */
class Intechcore_Widget_LiveSupportController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    if(!$viewer->getIdentity()){
       return $this->setNoRender();
    }

    header('Access-Control-Allow-Origin: *');  
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

    $user = Engine_Api::_()->getItem("user", $viewer->getIdentity());
    $name = $user->getTitle();
    $email = $user->email;

    $this->view->url = "https://helpdesk.intechnique.net/chat_widget.js?token=91378264&siteid=squared&name=$name&email=$email";
  }
}
