<?php

class Intechcore_Bootstrap extends Engine_Application_Bootstrap_Abstract
{
	protected function _bootstrap($resource = null) {

		require_once APPLICATION_PATH . '/application/modules/Intechcore/controllers/AuthController.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/controllers/SignupController.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/controllers/UserController.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/controllers/SettingsController.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/Plugin/Signup/Account.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/Form/Signup/Account.php';
		require_once APPLICATION_PATH . '/application/modules/Intechcore/Plugin/Signup/Photo.php';
		

		$headScript = new Zend_View_Helper_HeadScript();
		$headScript->appendFile('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')
				   ->appendFile(Zend_Registry::get('StaticBaseUrl') . 'application/modules/Intechcore/externals/scripts/core.js');
/*
	    $headScript->headLink()
            ->appendStylesheet($this->view->layout()->staticBaseUrl . 'https://squared.conxt.net/application/modules/Intechcore/externals/styles/font/proximanova-regular.otf');*/
				   
	}
}