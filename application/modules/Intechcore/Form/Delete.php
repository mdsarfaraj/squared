<?php
/**

 */
class Intechcore_Form_Delete extends Engine_Form
{

  public function init()
  {   
    $this->setTitle('Remove Background')
         ->setDescription('Are you sure want to remove background ?')
    ;

    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Remove',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => 'javascript:history.back();',
      'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');



  }

}
  