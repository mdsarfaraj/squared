<?php
/**

 */
class Intechcore_Form_Custumtheme extends Engine_Form
{

  public function init()
  {   
    $this->setTitle('Chnage Background');
    


    $this->addElement('Select', 'background', array(
      'label' => 'Background Type',
      'multiOptions' => array(
        'intech_color' => 'Background Color',
        'intech_img' => 'Background Image'
      ),
      'onchange' => 'jQuery(\'#intech_color-wrapper\').toggle(); jQuery(\'body#global_page_intech-custum-custumtheme #photo-wrapper\').toggle(); '
    ));



    $this->addElement('File', 'photo', array(
      'label' => 'Choose Website Background Photo',
    ));
    $this->photo->addValidator('Extension', false, 'jpg,png,gif,jpeg');

    $this->addElement('Text', 'intech_color', array(
      'label' => 'Color',
      'decorators' => array(array('ViewScript', array(
    'viewScript' => '_colorchoose.tpl',
    'class' => 'form element',
       )))
    ));


    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Set Background',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => 'javascript:history.back();',
      'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');


  }

}
  