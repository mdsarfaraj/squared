<?php
/**
	
*/
class Intechcore_Form_Appointment_Create extends Engine_Form {

    public function init() {
        $user = Engine_Api::_()->user()->getViewer();

        $this->setTitle('Create An Appointment')
                ->setMethod("POST");

        // date
        $date = new Schedule_Form_Element_Date('date');
        $date->setLabel("Date");
        $date->setAllowEmpty(false);
        $date->addValidator('date');
        $this->addElement($date);

        //start time
        $start_time = new Schedule_Form_Element_Time('start_time');
        $start_time->setLabel("Start Time");
        $start_time->setAllowEmpty(false);
        $start_time->setAttrib('class', 'start_time');
        //$start_time->addValidator(new Schedule_Form_Validate_InRange());
        $this->addElement($start_time);

        //end time
        $end_time = new Schedule_Form_Element_Time('end_time');
        $end_time->setLabel("End Time");
        $end_time->setAllowEmpty(false);
        $end_time->setAttrib('class', 'end_time');
        $end_time->addValidator(new Schedule_Form_Validate_Time('start_time'));
        //$end_time->addValidator(new Schedule_Form_Validate_InRange());
        $this->addElement($end_time);

        // Title
        $this->addElement('Text', 'title', array(
            'label' => 'Title',
            'allowEmpty' => false,
            'required' => true,
            'validators' => array(
                array('NotEmpty', true),
                array('StringLength', false, array(1, 64)),
            ),
            'filters' => array(
                'StripTags',
                new Engine_Filter_Censor(),
            ),
        ));
        $title = $this->getElement('title');

        // Description
        $this->addElement('Textarea', 'description', array(
            'label' => 'Details',
            'maxlength' => '10000',
            'filters' => array(
                'StripTags',
                new Engine_Filter_Censor(),
                new Engine_Filter_EnableLinks(),
                new Engine_Filter_StringLength(array('max' => 10000)),
            ),
        ));

        /*select username from auto-populate*/
        $this->addElement('Text', 'user_id', array(
            'label'=>'Username',
            'description' => 'You can type one or more username',
            'placeholder' => 'Type username',
            'id'=>'to',
            'autocomplete'=>'off'
        ));

	      Engine_Form::addDefaultDecorators($this->user_id);
	      $this->user_id->getDecorator("Description")->setOption("placement", "append");

	      // Init to Values
	      $this->addElement('Hidden', 'toValues', array(
	            'required' => true,
	            'allowEmpty' => false,
	            'order' => 6,
	            'validators' => array(
	              'NotEmpty'
	            ),
	            'filters' => array(
	              'HtmlEntities'
	            ),
	      ));
	      Engine_Form::addDefaultDecorators($this->toValues);

	     /*ended auto-populate*/

        $this->addElement('Select', 'reminder', array(
            'label' => 'Remind Me',
        ));
        $this->reminder->addMultiOptions(array(
            '-1' => 'Never',
            '0' => 'On Time',
            '30' => '30 Minutes Before',
            '60' => '1 Hour Before',
            '90' => '1 Hour and 30 Minutes Before',
            '120' => '2 Hours Before',
            '150' => '2 Hours and 30 Minutes Before',
            '180' => '3 Hours Before',
            '1440' => '1 Day Before'
        ));

        // Privacy
        $viewOptions = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('schedule', $user, 'auth_view');
        $availableLabels = array(
            'everyone' => 'Everyone',
            'registered' => 'All Registered Members',
            'owner_network' => 'Friends and Networks',
            'owner_member_member' => 'Friends of Friends',
            'owner_member' => 'Friends Only',
            'member' => 'Event Guests Only',
            'owner' => 'Just Me'
        );
        $viewOptions = array_intersect_key($availableLabels, array_flip($viewOptions));

        // View
        if (!empty($viewOptions) && count($viewOptions) >= 1) {
            // Make a hidden field
            if (count($viewOptions) == 1) {
                $this->addElement('hidden', 'auth_view', array('value' => key($viewOptions)));
                // Make select box
            } else {
                $this->addElement('Select', 'auth_view', array(
                    'label' => 'Privacy',
                    'description' => 'Who may see this event?',
                    'multiOptions' => $viewOptions,
                    'value' => key($viewOptions),
                ));
                $this->auth_view->getDecorator('Description')->setOption('placement', 'append');
            }
        }

        $this->addElement('Checkbox', 'recurring', array(
            'label' => 'Recurring event',
            'onclick' => 'enable(this)'
        ));

        $this->addElement('Select', 'repeat', array(
            'label' => 'Repeat',
            'description' => 'more times',
            'multioptions' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10),
            'decorators' => array(
                'ViewHelper',
                array('HtmlTag', array('tag' => 'div')),
                array('Description', array('placement' => 'APPEND')),
                array('Label', array('tag' => 'div', 'placement' => 'PREPEND')),
                array('HtmlTag2', array('tag' => 'div', 'id' => 'repeat-wrapper')),
        )));

        $this->addElement('Radio', 'every', array(
            'label' => 'Every',
        ));
        $this->every->addMultiOptions(array(
            'wday' => 'Working Day (Mon-Fri)',
            'day' => 'Day (Mon-Sun)',
            'week' => 'Week',
            'month' => 'Month',
            'year' => 'Year'
        ));

        // Buttons
        $this->addElement('Button', 'submit', array(
            'label' => 'Create',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array(
                'ViewHelper',
            ),
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'link' => true,
            'href' => 'javascript:history.back();',
            'prependText' => ' or ',
            'decorators' => array(
                'ViewHelper',
            ),
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                'DivDivDivWrapper',
            ),
        ));
    }

}