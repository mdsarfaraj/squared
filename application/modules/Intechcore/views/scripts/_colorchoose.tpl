<?php

/**

 */
?>

<style type="text/css">
  body#global_page_intech-custum-custumtheme #photo-wrapper {
    display: none;
  }
</style>
<?php 
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/mooRainbow.js');

$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/styles/mooRainbow.css'); 
?> 
<script type="text/javascript">
  window.addEvent('domready', function() {
    new MooRainbow('colorChooser', {
      id: 'colorChooser',
      'onChange': function(color) {
        $('intech_color').value = color.hex;
      }
    });
  });
</script>
<div id="intech_color-wrapper" class="form-wrapper">
  <div id="sesbasic_color-element" class="form-element">
    <p class="description"><?php echo $this->translate('Choose Color')?></p>
    <input name="intech_color" id="intech_color" value="#ffffff" type="text">
    <input name="colorChooser" id="colorChooser" src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Sesbasic/externals/images/colorpicker/rainbow.png" link="true" type="image">
  </div>
</div>