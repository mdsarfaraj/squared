<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<!--Show the form-->
<?php echo $this->form->render($this); ?>


<script type="text/javascript">

  // Populate data
  var maxRecipients = 5;
  var to = {
    id : false,
    type : false,
    guid : false,
    title : false
  };
  var isPopulated = false;

  <?php if( !empty($this->isPopulated) && !empty($this->toObject) ): ?>
    isPopulated = true;
    to = {
      id : <?php echo sprintf("%d", $this->toObject->getIdentity()) ?>,
      type : '<?php echo $this->toObject->getType() ?>',
      guid : '<?php echo $this->toObject->getGuid() ?>',
      title : '<?php echo $this->string()->escapeJavascript($this->toObject->getTitle()) ?>'
    };
  <?php endif; ?>
  
  function removeFromToValue(id) {
    // code to change the values in the hidden field to have updated values
    // when recipients are removed.
    var toValues = $('toValues').value;
    var toValueArray = toValues.split(",");
    var toValueIndex = "";

    var checkMulti = id.search(/,/);

    // check if we are removing multiple recipients
    if (checkMulti!=-1){
      var recipientsArray = id.split(",");
      for (var i = 0; i < recipientsArray.length; i++){
        removeToValue(recipientsArray[i], toValueArray);
      }
    }
    else{
      removeToValue(id, toValueArray);
    }

    // hide the wrapper for usernames if it is empty
    if ($('toValues').value==""){
      $('toValues-wrapper').setStyle('height', '0');
    }

    $('user_id').disabled = false;
  }

  function removeToValue(id, toValueArray){
    for (var i = 0; i < toValueArray.length; i++){
      if (toValueArray[i]==id) toValueIndex =i;
    }

    toValueArray.splice(toValueIndex, 1);
    $('toValues').value = toValueArray.join();
  }

en4.core.runonce.add(function() {
    if( !isPopulated ) {
      new Autocompleter.Request.JSON('to', '<?php echo $this->url(array('module' => 'intechcore', 'controller' => 'appointment', 'action' => 'suggest-user'), 'default', true) ?>', {
        'minLength': 1,
        'delay' : 250,
        'selectMode': 'pick',
        'autocompleteType': 'message',
        'multiple': false,
        'className': 'message-autosuggest',
        'filterSubset' : true,
        'tokenFormat' : 'object',
        'tokenValueKey' : 'label',
        'injectChoice': function(token){
          if(token.type == 'user'){
            var choice = new Element('li', {
              'class': 'autocompleter-choices',
              'html': token.photo,
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          else {
            var choice = new Element('li', {
              'class': 'autocompleter-choices friendlist',
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
        }
      , onPush : function(){
        }
      });
    }
  });
</script>

<script type="text/javascript">
    en4.core.runonce.add(function() {
        enable(window.$('recurring'));

        var date = new Date(Date.parse(window.cal_date.calendars[0].el.value));
        if (date) {
            var day_name = cal_date.options.days[date.format('%w')];
            var day_number = date.format('%e');
            var month = cal_date.options.months[parseInt(date.format('%m'))-1];

            window.$$('label[for=every-week]')[0].set('text', en4.core.language.translate('Week (every %s)', day_name));
            window.$$('label[for=every-month]')[0].set('text', en4.core.language.translate('Month (on %s each month)', day_number));
            window.$$('label[for=every-year]')[0].set('text', en4.core.language.translate('Year (on %s of %s each year)', day_number, month));
        }
    });

    function enable(elem) {
        if (elem.checked == true) {
            window.$('repeat-wrapper').show();
            window.$('every-wrapper').show();
        } else {
            window.$('repeat-wrapper').hide();
            window.$('every-wrapper').hide();
        }
        parent.window.Smoothbox.instance.doAutoResize();
    }

    function cal_date_onHideStart(cal) {
        if (cal.calendars[0].val) {            
            var day_name = cal.options.days[cal.calendars[0].val.format('%w')];
            var day_number = cal.calendars[0].val.format('%e');
            var month = cal.options.months[parseInt(cal.calendars[0].val.format('%m'))-1];
            
            window.$$('label[for=every-week]')[0].set('text', en4.core.language.translate('Week (every %s)', day_name));
            window.$$('label[for=every-month]')[0].set('text', en4.core.language.translate('Month (on %s each month)', day_number));
            window.$$('label[for=every-year]')[0].set('text', en4.core.language.translate('Year (on %s of %s each year)', day_number, month));
        }
    }
</script>

<?php
$this->headTranslate(array(
    'Week (every %s)',
    'Month (on %s each month)',
    'Year (on %s of %s each year)'
)); ?>