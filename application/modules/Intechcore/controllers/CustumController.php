<?php

class Intechcore_CustumController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
   
  }

  public function custumthemeAction(){
  	//get the viewer
  	$viewer = Engine_Api::_()->user()->getViewer();

  	//Make Form

  	$this->view->form = $form = new Intechcore_Form_Custumtheme();

  	/*$custumtable = Engine_Api::_()->getDbtable('custums','intech');*/

    $custumtable = Engine_Api::_()->getItemTable('intechcore_custum');


    $select = $custumtable->select()->where('user_id = ?',$viewer->getIdentity());
    $selectData = $custumtable->fetchRow($select);

    
    if( !$this->getRequest()->isPost() ) {
     	return;
   	}

   	if( !$form->isValid($this->getRequest()->getPost()) ) {
        return;
    }

    //fetch data
    $select = $custumtable->select()->where('user_id = ?',$viewer->getIdentity());
    $selectData = $custumtable->fetchRow($select);


    $db = $custumtable->getAdapter();
    $db->beginTransaction();

    

    try{
      
        $values = $form->getValues();     

        if($values['background'] == 'intech_color'){
          
            if($selectData){
              $selectData->delete();
              $db->commit();
            }

            //data params
            $params = array(
              'user_id' => $viewer->getIdentity(),
              'type' => $values['background'],
              'intech_color' => $values['intech_color'],
              'added_date' => date('Y-m-d H:i:s'),
              'updated_date' =>   date('Y-m-d H:i:s'),
            );

            //save
            $custum = $custumtable->createRow();
            $custum->setFromArray($params);
            $custum->save();

            $db->commit();


        }else{


            if($selectData){
              $selectData->delete();
              $db->commit();
            }

            //photo params
            $photoParams = array(
              'user_id' => $viewer->getIdentity(),
              'type' => $values['background'],
              'added_date' => date('Y-m-d H:i:s'),
            );

            //save
            $custumData = $custumtable->createRow();
            $custumData->setFromArray($photoParams);
            $custumData->save();

            if(!empty($values['photo'])){
              $custumData->setCustumBackground($form->photo);
            }

            $db->commit();

          
        }

        $this->view->message = Zend_Registry::get('Zend_Translate')->_('Background has changed successfully...');
         return $this->_forward('success' ,'utility', 'core', array(
          'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'home'), 'user_general', true),'messages' => Array($this->view->message)
        ));

    }catch(Exception $e){
    	$db->rollBack();
    	throw $e;
    }

  }


  public function deleteAction(){
    //Make Form
    $this->view->form = $from = new Intechcore_Form_Delete();

    $custumId = $this->getRequest()->getParam('custum_id',0);

    //table

    $custumTableData = Engine_Api::_()->getItem('intechcore_custum',$custumId);

    // Check post/form
    if( !$this->getRequest()->isPost() ) {
      return;
    }
    

    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    
    try{
       $custumTableData->delete();
       $db->commit();
    }catch(Exception $e){
      $db->rollBack();
      throw $e;
    }
    
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Background removed successfully...');
         return $this->_forward('success' ,'utility', 'core', array(
          'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'home'), 'user_general', true),'messages' => Array($this->view->message)
    ));


  }


}
