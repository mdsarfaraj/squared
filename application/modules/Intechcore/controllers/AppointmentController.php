<?php

class Intechcore_AppointmentController extends Core_Controller_Action_Standard
{

  public function init(){

  	$viewer = Engine_Api::_()->user()->getViewer();
  	
    if($viewer->getIdentity()){
        if($viewer->level_id == "6" || $viewer->isAdmin()){
          return true;
        }else{
          return $this->_forward('notfound', 'error', 'core'); 
        }
      }else{
        return $this->_forward('notfound', 'error', 'core'); 
      }

  }


  public function indexAction()
  {
     
  }


  public function createAction(){
  	   if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$this->_helper->requireAuth()->setAuthParams('schedule', null, 'create')->isValid())
            return;

        //prepare data
        $viewer = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($viewer);

        //getting correct date wich depends on timezone
        //$oldTz = date_default_timezone_get();
        //date_default_timezone_set($viewer->timezone);
        //$this->view->cur_day = date('d-m-Y H:i', strtotime('now'));
        //date_default_timezone_set($oldTz);

        $now = new DateTime('now', new DateTimeZone($viewer->timezone));
        $this->view->cur_day = $now->format('d-m-Y H:i');

        $data = $this->_getParam('data', '');
        $date = explode(' ', $data);

        // Create form
        $this->view->form = $form = new Intechcore_Form_Appointment_Create();

        $type = $this->_getParam('type', 'schedule');


        if ($date[0] != '') {
            $form->populate(array(
                'date' => $date[0],
                'start_time' => $date[1] . ':00',
                'end_time' => $date[1] + 1 . ':00'
            ));
        } else {
            $form->populate(array(
                'start_time' => '8:00',
                'end_time' => '9:00',
                'date' => $now->format('Y-m-d')
            ));
            $form->cancel->setAttrib('onclick', 'parent.Smoothbox.close();');
        }

        if ($type == 'request') {
            $schedule = Engine_Api::_()->getDbTable('requests', 'schedule')->find($this->_getParam('schedule_id'))->current();
            if (!$this->getRequest()->isPost()) {
                $form->populate($schedule->toArray());
                return;
            }
        }

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Process
        $values = $form->getValues();


        if ($type == 'request') {
            $values['owner_id'] = $schedule->owner_id;
        } else{
            $values['owner_id'] = $viewer->getIdentity();
        }


        $values['start_time'] = $values['date'] . ' ' . $values['start_time'];
        $values['end_time'] = $values['date'] . ' ' . $values['end_time'];

        if (!in_array($values['reminder'], array('0', '30', '60', '90', '120', '150', '180', '1440')))
            $values['reminder'] = '-1';

        // Convert times
        $oldTz = date_default_timezone_get();
        date_default_timezone_set($viewer->timezone);
        $start_time = strtotime($values['start_time']);
        $end_time = strtotime($values['end_time']);
        date_default_timezone_set($oldTz);
        $values['start_time'] = date('Y-m-d H:i:s', $start_time);
        $values['end_time'] = date('Y-m-d H:i:s', $end_time);

        $db = Engine_Api::_()->getDbtable('schedules', 'schedule')->getAdapter();
        $db->beginTransaction();

        try {
            // Create event
            $table = Engine_Api::_()->getDbtable('schedules', 'schedule');

//            $select = $table->select();
//            $select->where('owner_id = ?', $values['owner_id'])
//                    ->where('date = ?', $values['date'])
//                    ->where('start_time = ?', $values['start_time'])
//                    ->where('end_time = ?', $values['end_time']);
//
//            if (null != $table->fetchRow($select)) {
//                $form->start_time->addError('Event can\'t be added,  because this time field overlaps with other event.');
//                $form->end_time->addError('Event can\'t be added,  because this time field overlaps with other event.');
//                return;
//            }
//            if ($table->checkDateRange($values['start_time'], $values['end_time'], array(
//                        'date' => $values['date'],
//                        'user' => $viewer->getIdentity()
//                    )))
//                return $form->addError('This event\'s time overlaps with other event.');





            /*md code start here*/

            $appointmentTable = Engine_Api::_()->getDbtable('appointments','intechcore');
            $appointmentMemberTable = Engine_Api::_()->getDbtable('members','intechcore');

            //appointment array
            $appointmentData = array(
                'title' => $values['title'],
                'description' => $values['description'],
                'owner_type' => $viewer->getType(),
                'owner_id' => $viewer->getIdentity(),
                'start_time' => $values['start_time'],
                'end_time' => $values['end_time'],
                'date' =>  $values['date'],
            );

            //save data in appointment table
            $appointment = $appointmentTable->createRow();
            $appointment->setFromArray($appointmentData);
            $appointment->save();

            //insert into appointment table
            $toValues = explode(",",$values['toValues']);

            //merge the viewer and users id
            $usersId = array_merge($toValues, array(
                $viewer->getIdentity(),
            ));


            
            foreach ($usersId as $member) {    
                
                //data params of appointment members table
                $params = array(
                    'appointment_id' => $appointment->getIdentity(),
                    'user_id' => $member,
                    'added_date' => date('Y-m-d'),
                );

                //save the all users in members table
                $membersData = $appointmentMemberTable->createRow();
                $membersData->setFromArray($params);
                $membersData->save();

                //get the member object
                $userObject = Engine_Api::_()->getItem('user',$member);

                //data params of schedule table
                $scheduleParams = array(
                    'title' => $values['title'],
                    'description' => $values['description'],
                    'owner_type' => $userObject->getType(),
                    'owner_id' => $member,
                    'start_time' => $values['start_time'],
                    'end_time' => $values['end_time'],
                    'date' => $values['date'],
                );

                //save data in schedule table
                $event = $table->createRow();
                $event->setFromArray($scheduleParams);
                $event->save();


                //send notification and email
                if($member != $viewer->getIdentity()){

                    $message = 'A member booked an appointment with you, check your profile and get more information.';

                    $MailParams = array(
                            'date' => time(),
                            'sender_title' => $viewer->getTitle(),
                            'sender_email' => $viewer->email,
                            'message' => $message,
                    );

                    if($userObject){
                        
                        Engine_Api::_()->getDbtable('notifications','activity')->addNotification(
                            $userObject,
                            $viewer,
                            $viewer,
                            'appointment_new',
                            $MailParams
                       );
                    }
                }


            }



            


            $url = $event->getHref();

            // Auth
            $auth = Engine_Api::_()->authorization()->context;
            $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'everyone');

            if (empty($values['auth_view'])) {
                $values['auth_view'] = 'everyone';
            }

            $viewMax = array_search($values['auth_view'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
            }

            // Add action
            /*$activityApi = Engine_Api::_()->getDbtable('actions', 'activity');

            $action = $activityApi->addActivity($viewer, $event, 'schedule_create');

            if ($action) {
                $activityApi->attachActivity($action, $event);
            }*/

            if ($values['reminder'] != '-1') {
                $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
                $reminder = $reminderTable->createRow();


                if ($type == 'request') {
                    $reminder->user_id = $schedule->requester_id;
                } else {
                    $reminder->user_id = $viewer->getIdentity();
                }


                $reminder_date = strtotime($values['start_time']) - ($values['reminder'] * 60);
                $reminder->date = date('Y-m-d H:i:s', $reminder_date);
                $reminder->schedule_id = $event->getIdentity();
                $reminder->time = $values['reminder'];
                $reminder->save();
            }

            if ($values['recurring'] == 1) {
                if ($values['repeat'] <= 10) {
                    $period = 'day';
                    if (in_array($values['every'], array('wday', 'day', 'week', 'month', 'year')))
                        $period = $values['every'];

                    $skipWEnd = false;
                    if ($period == 'wday') {
                        $period = 'day';
                        $skipWEnd = true;
                    }

                    for ($c = 1; $c <= $values['repeat']; $c++) {
                        $date_tmp = new DateTime($values['start_time'], new DateTimeZone($viewer->timezone));

                        if ($values['every'] == 'month') {
                            $currentDay = $date_tmp->format('d');
                        }

                        $date_tmp->modify('+1 ' . $period);
                        $values['start_time'] = $toValidate = $date_tmp;

                        if ($values['every'] == 'month' && $date_tmp->format('d') != $currentDay) {
                            $db->rollback();
                            return $form->addError($this->view->translate('Event can\'t be added for all months'));
                        }

                        if ($skipWEnd && in_array($date_tmp->format('w'), array(6, 0))) {
                            switch ($date_tmp->format('w')) {
                                case 6:
                                    $date_tmp->modify('+2 days');
                                    $values['start_time'] = $toValidate = $date_tmp;
                                    break;
                                case 0:
                                    $date_tmp->modify('+1 ' . $period);
                                    $values['start_time'] = $toValidate = $date_tmp;
                                    break;

                                default:
                                    break;
                            }
                        }

                        $date_tmp = new DateTime($values['date'], new DateTimeZone($viewer->timezone));
                        $date_tmp->modify('+1 ' . $period);
                        $values['date'] = $date_tmp->format('Y-m-d');

                        $values['start_time'] = $values['start_time']->format('Y-m-d H:i:s');

                        $validator = new Schedule_Form_Validate_InRange();

                        if (!$validator->isValid($toValidate->format('H:i:s'), array('user' => $viewer->getIdentity(), 'date' => array('date' => $values['date'])))) {
                            $db->rollBack();
                            return $form->addError($this->view->translate('Recurring events can\'t be added because one of events overlaps with other event on %1$s', $this->view->locale()->toDate($values['date'])));
                        }

                        $date_tmp = new DateTime($values['end_time'], new DateTimeZone($viewer->timezone));
                        $date_tmp->modify('+1 ' . $period);
                        $values['end_time'] = $date_tmp->format('Y-m-d H:i:s');

                        $event = $table->createRow();

                        $event->setFromArray($values);
                        $event->save();

                        // Auth
                        $auth = Engine_Api::_()->authorization()->context;
                        $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

                        if (empty($values['auth_view'])) {
                            $values['auth_view'] = 'everyone';
                        }

                        $viewMax = array_search($values['auth_view'], $roles);

                        foreach ($roles as $i => $role) {
                            $auth->setAllowed($event, $role, 'view', ($i <= $viewMax));
                        }

                        // Add action
                        $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');

                        $action = $activityApi->addActivity($viewer, $event, 'schedule_create');

                        if ($action) {
                            $activityApi->attachActivity($action, $event);
                        }

                        if ($values['reminder'] != '-1') {
                            $reminderTable = Engine_Api::_()->getDbTable('reminders', 'schedule');
                            $reminder = $reminderTable->createRow();

                            if ($type == 'request') {
                                $reminder->user_id = $schedule->requester_id;
                            } else {
                                $reminder->user_id = $viewer->getIdentity();
                            }


                            $reminder_date = strtotime($values['start_time']) - $values['reminder'];
                            $reminder->date = date('Y-m-d H:i:s', $reminder_date);
                            $reminder->schedule_id = $event->getIdentity();
                            $reminder->save();
                        }
                    }
                }
            }

            // Commit
            $db->commit();
            if($schedule != null){
                $schedule->delete();
            }
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }


        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => $url,
            'messages' => array(Zend_Registry::get('Zend_Translate')->_('Appointment is booked successfully...')),
        ));
  }

   public function suggestUserAction(){
	    $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      $data = null;
	    } else {
	      $data = array();
	      $table = Engine_Api::_()->getItemTable('user');
	      
	      $select = Engine_Api::_()->getDbtable('users', 'user')->select();
	      $select->where('user_id <> ?', $viewer->user_id);
	      
	      $blockedUserIds = !$viewer->isAdmin() ? $viewer->getAllBlockedUserIds() : array();
	      if( $blockedUserIds ) {
	        $select->where('user_id NOT IN(?)', (array) $blockedUserIds);
	      }
	      if( 0 < ($limit = (int) $this->_getParam('limit', 10)) ) {
	        $select->limit($limit);
	      }

	      if( null !== ($text = $this->_getParam('search', $this->_getParam('value'))) ) {
	        $select->where('`'.$table->info('name').'`.`displayname` LIKE ?', '%'. $text .'%');
	      }
	      
	      $ids = array();
	      foreach( $select->getTable()->fetchAll($select) as $friend ) {
	        $data[] = array(
	          'type'  => 'user',
	          'id'    => $friend->getIdentity(),
	          'guid'  => $friend->getGuid(),
	          'label' => $friend->getTitle(),
	          'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
	          'url'   => $friend->getHref(),
	        );
	        $ids[] = $friend->getIdentity();
	        $friend_data[$friend->getIdentity()] = $friend->getTitle();
	      }
	    }

	    if( $this->_getParam('sendNow', true) ) {
	      return $this->_helper->json($data);
	    } else {
	      $this->_helper->viewRenderer->setNoRender(true);
	      $data = Zend_Json::encode($data);
	      $this->getResponse()->setBody($data);
	    }
	 }

}
