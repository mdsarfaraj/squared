<?php
/**
 
 */
class Intechcore_Model_Custum extends Core_Model_Item_Abstract
{
	
  public function setCustumBackground($photo) {

  	if( $photo instanceof Zend_Form_Element_File ) {
	      $file = $photo->getFileName();
	    } elseif( is_array($photo) && !empty($photo['tmp_name']) ) {
	      $file = $photo['tmp_name'];
	    } elseif( is_string($photo) && file_exists($photo) ) {
	      $file = $photo;
	    } else {
	      throw new Blog_Model_Exception('Invalid argument passed to setPhoto: ' . print_r($photo, 1));
	    }
	    $name = basename($file);
	    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
	    $params = array(
	      'parent_type' => 'intechcore_custum',
	      'parent_id' => $this->getIdentity()
	    );
	    // Save
	    $storage = Engine_Api::_()->storage();

	   
	    // Resize image (main)
	    $image = Engine_Image::factory();
	    $image->open($file)
	      ->resize(1400, 3000)
	      ->write($path . '/m_' . $name)
	      ->destroy();
	    // Resize image (profile)


	    // Store
	    $iMain = $storage->create($path . '/m_' . $name, $params);
	    
	    // Remove temp files
	    @unlink($path . '/m_' . $name);


	    // Update row
	    $this->updated_date = date('Y-m-d H:i:s');
	    $this->photo_id = $iMain->getIdentity();

	    $this->save();
	    return $this;

  }

}
