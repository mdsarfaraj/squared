<?php
/**
 
 */
return array(
  array(
    'title' => 'Live Support',
    'description' => 'Display the Live Chat Widget',
    'category' => 'Intechcore',
    'type' => 'widget',
    'name' => 'intechcore.live-support',
  ),
  array(
    'title' => 'Livesmart Meeting',
    'description' => 'Display the Livesmart Meeting Widget',
    'category' => 'Intechcore',
    'type' => 'widget',
    'name' => 'intechcore.livesmart-meeting',
  ),
  array(
    'title' => 'Social oi logo',
    'description' => 'Display the social oi',
    'category' => 'Intechcore',
    'type' => 'widget',
    'name' => 'intechcore.social-oi',
  ),

) ?>