<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'intechcore',
    'version' => '0.0.2',
    'path' => 'application/modules/Intechcore',
    'title' => 'intechcore',
    'description' => 'intechcore is a intechnique basic plugin',
    'author' => 'MD Sarfaraj',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Intechcore',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/intechcore.csv',
    ),
  ),

  'routes' => array(
    // Public
    'appointment_general' => array(
      'route' => 'appointment/:action/*',
      'defaults' => array(
        'module' => 'intechcore',
        'controller' => 'appointment',
        'action' => 'create',
      ),
    ),
  ),

  
); ?>