<?php

/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Folder
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */

class Folder_Widget_CategoriesController extends Radcodes_Widget_Categories_Abstract
{
    protected $categoryType = 'folder_category';
    protected $isOldFormat = true;
}
