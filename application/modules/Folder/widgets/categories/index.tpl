<?php

/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Folder
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */
 
?>
<?php
$params = array('display_style'=>$this->display_style, 'categories'=>$this->categories, 'showphoto'=>$this->showphoto, 'descriptionlength'=>$this->descriptionlength, 'params'=>$this->params, 'options'=>$this->options);
echo $this->partial('shared/_categories.tpl', 'radcodes', $params);
?>
