<?php


/**
 * Radcodes - SocialEngine Module
 *
 * @category   Application_Extensions
 * @package    Folder
 * @copyright  Copyright (c) 2009-2010 Radcodes LLC (http://www.radcodes.com)
 * @license    http://www.radcodes.com/license/
 * @version    $Id$
 * @author     Vincent Van <vincent@radcodes.com>
 */
 
 
?>

<div class="headline">
  <h2>
    <?php echo $this->translate('Folders');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>

<div class='layout_right'>
    <?php echo $this->partial('/folder/_info.tpl', 'folder', array('folder' => $this->folder, 'dashboardNavigation' => $this->dashboardNavigation));?>
</div>
<div class='layout_middle'>
  <?php echo $this->form->render($this);?>


  <script type="text/javascript">
      en4.core.runonce.add(function () {
          new Uploader('upload_file', {
              uploadLinkClass : 'buttonlink icon_photos_new',
              uploadLinkTitle : '<?php echo $this->translate("Add Files");?>',
              uploadLinkDesc : '<?php echo $this->translate("Click \"Add Files\" to select one or more files from your computer."
                . " After you have selected the files, they will begin to upload right away. "
                . "When your upload is finished, click the button below your file list to save them to your folder.");?>'
          });
      });
  </script>

</div>
