<?php

class Activitypoints_Form_Admin_Level extends Engine_Form
{


  public function init()
  {
    $this
      ->setTitle('Member Level Settings')
      ->setDescription('ACTIVITYPOINTS_FORM_ADMIN_LEVEL_DESCRIPTION');

    $this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOptions(array('tag' => 'h4', 'placement' => 'PREPEND'));

    // prepare user levels
    $table = Engine_Api::_()->getDbtable('levels', 'authorization');
    $select = $table->select();
    $user_levels = $table->fetchAll($select);

    foreach ($user_levels as $user_level){
      $levels_prepared[$user_level->level_id]= $user_level->getTitle();
    }

    // category field
    $this->addElement('Select', 'level_id', array(
          'label' => 'Member Level',
          'multiOptions' => $levels_prepared,
          'onchange' => 'javascript:fetchLevelSettings(this.value);',
          'ignore' => true
        ));

    $this->addElement('Radio', 'use', array(
      'label' => 'AP_100016629',
      'description' => 'AP_100016630',
      'multiOptions' => array(
        1 => 'AP_100016631',
        0 => 'AP_100016632',
      ),
      'value' => 0,
    ));

    $this->addElement('Radio', 'allow_transfer', array(
      'label' => 'AP_100016633',
      'description' => 'AP_100016634',
      'multiOptions' => array(
        1 => 'AP_100016635',
        0 => 'AP_100016636',
      ),
      'value' => 0,
    ));

    if(Engine_Api::_()->getDbTable('modules','core')->isModuleEnabled('activityrewards')) {

      $this->addElement('Radio', 'edit_spender', array(
        'label' => 'Moderation - Shop Items',
        'description' => 'Allow removing Shop item comments',
        'multiOptions' => array(
          2 => 'Yes, allow removing shop item comments.',
          0 => 'No, do not allow removing shop item comments.',
        ),
        'value' => 0,
      ));

      $this->addElement('Radio', 'edit_earner', array(
        'label' => 'Moderation - Offer Items',
        'description' => 'Allow removing Offer item comments',
        'multiOptions' => array(
          2 => 'Yes, allow removing offer item comments.',
          0 => 'No, do not allow removing offer item comments.',
        ),
        'value' => 0,
      ));

    }

    $this->addElement('text', 'max_transfer', array(
      'label' => 'AP_100016638',
      'description' => 'AP_100016637', // + 'AP_100016639' (enter 0 to allow unlimited transfers)
      'value' => 0,
    ));

    $this->addElement('text', 'max_receive', array(
      'label' => 'AP_100016638',
      'description' => 'ACTIVITYPOINTS_LEVEL_LIMIT_RECEIVE', // + 'AP_100016639' (enter 0 to allow unlimited transfers)
      'value' => 0,
    ));


    $this->addElement('Button', 'submit', array(
      'label' => 'Save Settings',
      'type' => 'submit',
      'ignore' => true
    ));

  }
}