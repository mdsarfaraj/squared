<?php
class Activitypoints_Installer extends Engine_Package_Installer_Module
{
  function onInstall()
  {

    $this->_addUserProfileContent();
    $this->_addUserHomeContent();

    parent::onInstall();
  }


  protected function _addUserProfileContent()
  {
    //
    // install content areas
    //
    $db     = $this->getDb();
    $select = new Zend_Db_Select($db);

    // profile page
    $select
      ->from('engine4_core_pages')
      ->where('name = ?', 'user_profile_index')
      ->limit(1);
    $pageId = $select->query()->fetchObject()->page_id;


    // activitypoints.profile-points

    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from('engine4_core_content')
      ->where('page_id = ?', $pageId)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'activitypoints.profile-points')
      ;
    $info = $select->query()->fetch();

    if( empty($info) ) {

      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from('engine4_core_content')
        ->where('page_id = ?', $pageId)
        ->where('type = ?', 'container')
        ->limit(1);
      $containerId = $select->query()->fetchObject()->content_id;

      // left_id (may not always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from('engine4_core_content')
        ->where('parent_content_id = ?', $containerId)
        ->where('type = ?', 'container')
        ->where('name = ?', 'left')
        ->limit(1);
      $leftId = $select->query()->fetchObject();
      if( $leftId && $leftId->content_id ) {
        $leftId = $leftId->content_id;
      } else {
        $leftId = null;
      }

	    if( !empty($leftId) ) {

	      // profile page
	      $db->insert('engine4_core_content', array(
	        'page_id' => $pageId,
	        'type'    => 'widget',
	        'name'    => 'activitypoints.profile-points',
	        'parent_content_id' => $leftId,
	        'order'   => 6,
	        'params'  => '{"title":"My Points"}',
	      ));
    	}

    }
  }


  protected function _addUserHomeContent()
  {
    //
    // install content areas
    //
    $db     = $this->getDb();
    $select = new Zend_Db_Select($db);

    // profile page
    $select
      ->from('engine4_core_pages')
      ->where('name = ?', 'user_index_home')
      ->limit(1);
    $pageId = $select->query()->fetchObject()->page_id;


    // activitypoints.userhome-points

    // Check if it's already been placed
    $select = new Zend_Db_Select($db);
    $select
      ->from('engine4_core_content')
      ->where('page_id = ?', $pageId)
      ->where('type = ?', 'widget')
      ->where('name = ?', 'activitypoints.userhome-points')
      ;
    $info = $select->query()->fetch();

    if( empty($info) ) {

      // container_id (will always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from('engine4_core_content')
        ->where('page_id = ?', $pageId)
        ->where('type = ?', 'container')
        ->limit(1);
      $containerId = $select->query()->fetchObject()->content_id;

      // left_id (may not always be there)
      $select = new Zend_Db_Select($db);
      $select
        ->from('engine4_core_content')
        ->where('parent_content_id = ?', $containerId)
        ->where('type = ?', 'container')
        ->where('name = ?', 'right')
        ->limit(1);
      $leftId = $select->query()->fetchObject();
      if( $leftId && $leftId->content_id ) {
        $leftId = $leftId->content_id;
      } else {
        $leftId = null;
      }

	    if( !empty($leftId) ) {

	      $db->insert('engine4_core_content', array(
	        'page_id' => $pageId,
	        'type'    => 'widget',
	        'name'    => 'activitypoints.userhome-points',
	        'parent_content_id' => $leftId,
	        'order'   => 1,
	        'params'  => '{"title":"My Points"}',
	      ));
    	}

    }
  }

}
