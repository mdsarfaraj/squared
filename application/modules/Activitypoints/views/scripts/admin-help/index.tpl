<h2><?php echo $this->translate("Activity Points Plugin") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render();
      
    ?>
  </div>
<?php endif; ?>

<script type="text/javascript">

  function activitypoints_faq_show(id,focus,hist_id,show_always) {

    if(typeof focus == 'undefined') {
      focus = 0;
    }
    if(typeof hist_id != 'undefined') {
      try {
        //History.push(document.location);
        history.pushState({}, "Control Panel", document.location);
      } catch(err) {

      }
    }
    if(typeof show_always == 'undefined') {
      show_always = 0;
    }
    
    if(show_always) {
      $(id).style.display = 'block';
      if(focus) {
        activitypoints_faq_focus(id);
      }
    } else {

      if($(id).style.display == 'block') {
        $(id).style.display = 'none';
      } else {
        $(id).style.display = 'block';
        if(focus) {
          activitypoints_faq_focus(id);
        }
      }

    }

  }

  function activitypoints_faq_focus(id) {
    var myFx = new Fx.Scroll($(document.body));
    myFx.toElement(id); 
  }

  var activitypoints_autocopy_hist = [];

  function activitypoints_autocopy(id) {
    try {

      //var copyTextarea = document.querySelector(id);
      //copyTextarea.select();
      if(activitypoints_autocopy_hist.contains(id)) {
        return;
      }
      activitypoints_autocopy_hist.push(id);

      if (document.selection) {
          var range = document.body.createTextRange();
          range.moveToElementText(document.getElementById(id));
          range.select();
      } else if (window.getSelection) {
          var range = document.createRange();
          range.selectNode(document.getElementById(id));
          window.getSelection().removeAllRanges();
          window.getSelection().addRange(range);
      }      

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }

    } catch(err) {
    }
  }
</script>

<p>
  <?php echo $this->translate("Find some quick answers on this page") ?>
</p>

<br />

<?php if( $this->show_notice ): ?>
<div style="padding: 10px">
<div class="tip">
<span>Thank you for installing the plugin! Please start from 'Getting started'</a></span>
</div>
</div>
<?php endif; ?>


<div class="admin_statistics">

  <h3> General </h3>
  <br>


  <div style='cursor: pointer; margin-bottom: 10px; padding: 5px 10px; font-weight: bold; background-color: #F6F6F6' onclick="activitypoints_faq_show('activitypoints_faq_0')">
    <?php echo $this->translate('Getting Started') ?>
  </div>
  <div style='border: 1px solid #F6F6F6; padding: 5px; display: none; margin-bottom: 40px' id='activitypoints_faq_0'>

    <table cellpadding='0' cellspacing='0' style='margin-top: 5px;'>
    <tr>
    <td class='activitypoints_step'>1</td>
    <td> Decide if you want to enable 'Top Users' page (<b><a target=_blank href="<?php echo $this->url(array('controller' => 'settings'),'admin_default') ?>">General Settings</a></b>) </td>
    </tr>
    <tr>
    <td class='activitypoints_step'>2</td>
    <td> All your website members start from zero points balance. You can <a target=_blank href="<?php echo $this->url(array('controller' => 'givepoints'),'admin_default') ?>">Give</a> your members some starting balance </td>
    </tr>

  <?php if(Engine_Api::_()->getDbTable('modules','core')->isModuleEnabled('activityrewards')) : ?>
    <tr>
    <td class='activitypoints_step'>3</td>
    <td> 
    <br>
    Decide if you want to enable Points Shop or Points Offers pages (<b><a target=_blank href="<?php echo $this->url(array('controller' => 'settings'),'admin_default') ?>">General Settings</a></b>) 
    <br>
    <br>
    Points Shop - your website members can purchase items using earned points
    <br>
    <br>
    Points Offers - your website members can earn points participating in various offers
    </td>
    </tr>
  <?php endif; ?>

    </table>
    
  </div>


  <div style='cursor: pointer; margin-bottom: 10px; padding: 5px 10px; font-weight: bold; background-color: #F6F6F6' onclick="($('activitypoints_faq_1').style.display=='block') ? $('activitypoints_faq_1').style.display='none' : $('activitypoints_faq_1').style.display='block';">
    <?php echo $this->translate('Resetting all points') ?>
  </div>
  <div style='border: 1px solid #F6F6F6; padding: 5px; display: none; margin-bottom: 40px' id='activitypoints_faq_1'>
    <a href="<?php echo $this->url(array('action' => 'reset')) ?>">Click here</a> if you would like to clear/reset ALL points for ALL users.
  </div>

  <?php if(Engine_Api::_()->getDbTable('modules','core')->isModuleEnabled('activityrewards')) : ?>

  <div style='cursor: pointer; margin-bottom: 10px; padding: 5px 10px; font-weight: bold; background-color: #F6F6F6' onclick="($('activitypoints_faq_20').style.display=='block') ? $('activitypoints_faq_20').style.display='none' : $('activitypoints_faq_20').style.display='block';">
    <?php echo $this->translate('Creating points/credits purchasing package') ?>
  </div>
  <div style='border: 1px solid #F6F6F6; padding: 5px; display: none; margin-bottom: 40px' id='activitypoints_faq_20'>
    If you would like to allow your users to purchase points using real money, make sure you have paypal payment gateway enabled and create the 'Purchase' type offer in the <a href="<?php echo $this->url(array('module' => 'activityrewards', 'controller' => 'offers')) ?>">Offers Shop</a>. The offer will be available for users in the Points Shop. <a href="<?php echo $this->url(array('module' => 'activityrewards', 'controller' => 'offers', 'action' => 'edit')) . '?offer_type=3&newitem=1' ?>">Click here</a> to create new purchase offer right now.
  </div>

  <?php endif; ?>

  <!--<div style='cursor: pointer; margin-bottom: 10px; padding: 5px 10px; font-weight: bold; background-color: #F6F6F6' onclick="($('activitypoints_faq_2').style.display=='block') ? $('activitypoints_faq_2').style.display='none' : $('activitypoints_faq_2').style.display='block';">-->
  <!--  <?php echo $this->translate('Q') ?>-->
  <!--</div>-->
  <!--<div style='border: 1px solid #F6F6F6; padding: 5px; display: none; margin-bottom: 40px' id='activitypoints_faq_2'>-->
  <!--  <?php echo $this->translate('A') ?>-->
  <!--</div>-->

</div>

<?php if($this->show !== ''): ?>
<script type="text/javascript">
<?php $faq_focus = ($this->show > 1) ? 1 : 0; ?>

en4.core.runonce.add(function() {
  activitypoints_faq_show('activitypoints_faq_<?php echo $this->show ?>',<?php echo $faq_focus ?>);
});
</script>
<?php endif; ?>