<?php

class Activitypoints_AdminSettingsController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {


    // if just installed, redirect to "Getting Started"
    if(Semods_Utils::getSetting('activitypoints_welcome',0) == 0) {
      Engine_Api::_()->getApi('settings', 'core')->reloadSettings();
      Semods_Utils::setSetting('activitypoints_welcome',1);
      return $this->_helper->_redirector->gotoRoute(array('module' => 'activitypoints', 'controller' => 'help', 'action' => 'index', 'show' => '0'), 'admin_default', true);
    }
    
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('activitypoints_admin_main', array(), 'activitypoints_admin_main_settings');
    
    $settings = Engine_Api::_()->getApi('settings', 'core');

    $this->view->form = $form = new Activitypoints_Form_Admin_Settings();
    
    if( $this->getRequest()->isPost()&& $form->isValid($this->getRequest()->getPost()))
    {
      $form->saveAdminSettings();
    }
  }


}