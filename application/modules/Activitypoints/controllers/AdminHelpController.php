<?php

class Activitypoints_AdminHelpController extends Core_Controller_Action_Admin
{


  public function indexAction()
  {

    // if just installed, show welcome note
    if(Semods_Utils::getSetting('activitypoints_welcome_1',0) == 0) {
      Semods_Utils::setSetting('activitypoints_welcome_1',1);
      $this->view->show_notice = true;
    } else {
      $this->view->show_notice = false;
    }

    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('activitypoints_admin_main', array(), 'activitypoints_admin_main_help');

    $this->view->show = $this->_getParam('show','');

  }


  public function resetAction()
  {
    
    Engine_Api::_()->getDbtable('points', 'activitypoints')->delete("1=1");
    Engine_Api::_()->getDbtable('counters', 'activitypoints')->delete("1=1");
    
    return $this->_helper->redirector->gotoRoute( array(
                                                        'module'      => 'activitypoints',
                                                        'controller'  => 'help',
                                                        'action'      => 'index'
                                                        )
                                                 );
    
  }
  
}