<?php

class Activitypoints_Model_Transaction extends Core_Model_Item_Abstract
{
  
  // Properties
  protected $_metadata = null;


  //protected $_searchColumns = array('userpointearner_title', 'userpointearner_body');
  
  //protected $_shortType = 'activitypoints_transaction';
  

  function getTransactionId()
  {
    return $this->uptransaction_id;
  
  }
  
  function addMetadata($key,$val)
  {
    if($this->_metadata == null) {
      $this->_metadata = unserialize($this->uptransaction_metadata);
    }
    $this->_metadata[$key] = $val;
  }
  
  function onPaymentSuccess()
  {
    
      Engine_Api::_()->getDbtable('transactions', 'activitypoints')->complete($this->uptransaction_id);
    
  }
  
  function onPaymentFailure()
  {
  
  }
  
  function onPaymentPending()
  {
    
  }
  
  function onPaymentIpn($order, $ipn)
  {
    if($order['state'] == 'complete') {
        Engine_Api::_()->getDbtable('transactions', 'activitypoints')->complete($this->uptransaction_id);
    } else if($order['state'] == 'cancelled' || $order['state'] == 'failed') {
      Engine_Api::_()->getDbtable('transactions', 'activitypoints')->cancel($this->uptransaction_id);
    }
    return true;
  }
  
}