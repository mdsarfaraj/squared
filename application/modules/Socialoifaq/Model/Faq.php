<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

class Socialoifaq_Model_Faq extends Core_Model_Item_Abstract
{
	public function getHref($params = array())
    {
	    $params = array_merge(array(
	      'route' => 'faq_view',
	      'reset' => true,
	      'faq_id' => $this->faq_id,
	    ), $params);
	    $route = $params['route'];
	    $reset = $params['reset'];
	    unset($params['route']);
	    unset($params['reset']);
	    return Zend_Controller_Front::getInstance()->getRouter()
	      ->assemble($params, $route, $reset);
   }

   public function getMostRecent()
    {
        // @todo decide how we want to handle multibyte string functions
        $tmpBody = strip_tags($this->title);
        return ( Engine_String::strlen($tmpBody) > 15 ? Engine_String::substr($tmpBody, 0, 15) . ' ...' : $tmpBody );
    }


}
