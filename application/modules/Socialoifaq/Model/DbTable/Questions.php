<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


class Socialoifaq_Model_DbTable_Questions extends Core_Model_Item_DbTable_Abstract 
{
  protected $_rowClass = "Socialoifaq_Model_Question";

}