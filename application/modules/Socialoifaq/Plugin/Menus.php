<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

class Socialoifaq_Plugin_Menus
{
  public function onMenuInitialize_FaqHomeMain($row)
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !$viewer->getIdentity() ) {
      return false;
    }

    $params = array();
    $params['route'] = 'faq_general';
    $params['icon'] = 'fa-question';
    return $params;
  }

  public function canAskQuestions() {

    //GET VIEWER
    $viewer = Engine_Api::_()->user()->getViewer();

    if(!$viewer->getIdentity()){
      return false;
    }

    return true;
    
  }


}