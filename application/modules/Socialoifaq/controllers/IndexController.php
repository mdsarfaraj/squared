<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


class Socialoifaq_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
    //search form
    $this->view->formFilter = $formFilter = new Socialoifaq_Form_Search();
    //faq table
    $table  =  Engine_Api::_()->getItemTable('socialoifaq_faq');
    $page   = $this->_getParam('page',0);
    $select = $table->select()
     		        ->order('creation_date DESC')
     		        ->where('status = ?',0)
     		        ;

    $values = array();
    if( $formFilter->isValid($this->_getAllParams()) ) {
      $values = $formFilter->getValues();
    }

    foreach( $values as $key => $value ) {
      if( null === $value ) {
        unset($values[$key]);
      }
    }

    $this->view->assign($values);
    //if record not found
     $this->view->question = $values['title'];
    $valuesCopy = array_filter($values);
    // Set up select info

    if( !empty($values['title']) ) {
        $select->where('title LIKE ?', '%' . $values['title'] . '%');
    }

    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
    $paginator->setCurrentPageNumber($page);
    $paginator->setItemCountPerPage(10);
    $this->view->formValues = $valuesCopy;

    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;

  }

  public function questionAction()
  { 

    $viewer = Engine_Api::_()->user()->getViewer();

    if(!$viewer->getIdentity()){
      return $this->_forward('notfound', 'error', 'core'); 
    }

    //CREATE FORM
    $this->view->form = $form = new Socialoifaq_Form_Question();

    $table = Engine_Api::_()->getDbtable('questions','socialoifaq');

    //check/validate
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    //process
    $formValues = $form->getValues();

    $db = $table->getAdapter();
    $db->beginTransaction();

    try{

        $params = array(
          'user_id' => $viewer->getIdentity(),
          'added_date' => date('Y-m-d H:i:s'),
        );

        $values = array_merge($formValues,$params);

        $question = $table->createRow();
        $question->setFromArray($values);
        $question->save();

        $email = Engine_Api::_()->getApi('settings', 'core')->core_mail_from;
        Engine_Api::_()->getApi('mail', 'core')->sendSystem($email, 'EMAIL_SOCIALOIFAQ_QUESTION', array(
            'site_title' => Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.site.title', 'Advertisement'),
            'message' => $question->title,
            'email' => $email,
            'queue' => true
        ));



      //COMMIT
      $db->commit();

      $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => 500,
          //'parentRefresh' => 10,
          'messages' => array('Your question has been submitted.')
      ));

    }catch(Exception $e){
      $db->rollBack();
      throw $e;
    }
   


  }


  public function viewAction(){
     $faq_id = $this->_getParam('faq_id',0);

     $table = Engine_Api::_()->getDbtable('faqs','socialoifaq');

     $select = $table->select()
                     ->where('faq_id = ?',$faq_id);

     $data = $table->fetchRow($select);

     $this->view->socialoifaq = $data;

      // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;

  }


}

