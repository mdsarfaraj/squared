<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */



class Socialoifaq_AdminManageController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {

  	 //search form
  	 $this->view->formFilter = $formFilter = new Socialoifaq_Form_Admin_Filter();

  	 $page = $this->_getParam('page');
     $table = Engine_Api::_()->getItemTable('socialoifaq_faq');
     $select = $table->select()
            ->order('creation_date DESC');

     // Process form
     $values = array();
     if( $formFilter->isValid($this->_getAllParams()) ) {
       $values = $formFilter->getValues();
     }

     foreach( $values as $key => $value ) {
       if( null === $value ) {
         unset($values[$key]);
       }
     }

     $this->view->assign($values);
     $valuesCopy = array_filter($values);

     if( !empty($values['title']) ) {
       $select->where('title LIKE ?', '%' . $values['title'] . '%');
     }
     if( !empty($values['status']) ) {
       $select->where('status LIKE ?', '%' . $values['status'] . '%');
     }

     $this->view->paginator = $paginator = Zend_Paginator::factory($select);
     $paginator->setCurrentPageNumber($page);
     $paginator->setItemCountPerPage(10);
     $this->view->formValues = $valuesCopy;
  }

  public function createAction(){
	    
	    //Make Form
	    $this->view->form = $form = new Socialoifaq_Form_Admin_Create();

	    // If not post or form not valid, return
	    if( !$this->getRequest()->isPost() ) {
	      return;
	    }

	    if( !$form->isValid($this->getRequest()->getPost()) ) {
	      return;
	    }

	    //faqs table
	    $table = Engine_Api::_()->getDbtable('faqs', 'socialoifaq');
	    $db = $table->getAdapter();
	    $db->beginTransaction();

	    try{
		    //get the form value
		     $formValues = $form->getValues();

	        //values
		     $values = array_merge($formValues, array(
	            'creation_date' => date('Y-m-d H:i:s'),
	            'modified_date' => date('Y-m-d H:i:s'),
	        ));
	    	
	        //save
		    $faq = $table->createRow();
	        $faq->setFromArray($values);
	        $faq->save();

	        $db->commit();

	      }catch( Exception $e ) {
	         $db->rollBack();
	         throw $e;
	    }
	    //redirect
	    return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
  }

  public function editAction(){
  	
  	$id = $this->_getParam('faq_id',0);
  	//Make Form
	$this->view->form = $form = new Socialoifaq_Form_Admin_Edit();

	//faq table
	$faq = Engine_Api::_()->getItem('socialoifaq_faq',$id);

	//populate
	$form->populate($faq->toArray());

	// If not post or form not valid, return
	if( !$this->getRequest()->isPost() ) {
	  return;
	}

	if( !$form->isValid($this->getRequest()->getPost()) ) {
	  return;
	}
      
    $db = $faq->getTable()->getAdapter();
    $db->beginTransaction();
     

    // Process   
    try {
        $values = $form->getValues();

        $faq->setFromArray($values);
        $faq->modified_date = date('Y-m-d H:i:s');
        $faq->save();

        $db->commit();

    } catch( Exception $e ) {
         $db->rollBack();
         throw $e;
    }

    //form message
    $form->addNotice('Changes is saved successfully...');
  }

  
  public function deleteAction(){
  	  // In smoothbox
      $this->_helper->layout->setLayout('admin-simple');
      $id = $this->getRequest()->getParam('faq_id',0);

      $this->view->faq_id = $id;
      // Check post
      if( $this->getRequest()->isPost() )
      {
        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        //faq table
        $faq = Engine_Api::_()->getItem('socialoifaq_faq',$id);

        try
        {
          $faq->delete();
          $db->commit();
        }

        catch( Exception $e )
        {
          $db->rollBack();
          throw $e;
        }

        $this->_forward('success', 'utility', 'core', array(
            'smoothboxClose' => 10,
            'parentRefresh'=> 10,
            'messages' => array('')
        ));
      }

      // Output
      $this->renderScript('admin-manage/delete.tpl');


  }


}