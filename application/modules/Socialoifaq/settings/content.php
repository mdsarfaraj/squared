<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

return array(
  array(
    'title' => 'Browse Search',
    'description' => 'Display the search widget',
    'category' => 'Socialoifaq',
    'type' => 'widget',
    'name' => 'socialoifaq.browse-search',
  ),
  array(
    'title' => 'Browse Menu',
    'description' => 'Display the top navigation menu',
    'category' => 'Socialoifaq',
    'type' => 'widget',
    'name' => 'socialoifaq.browse-menu',
  ),
   array(
    'title' => 'Browse Menu Quick',
    'description' => 'Display the quick navigation menu',
    'category' => 'Socialoifaq',
    'type' => 'widget',
    'name' => 'socialoifaq.browse-menu-quick',
  ),
  array(
    'title' => 'Recent Faqs',
    'description' => 'Display the most recent faqs',
    'category' => 'Socialoifaq',
    'type' => 'widget',
    'name' => 'socialoifaq.recent-faq',
  ),

) ?>