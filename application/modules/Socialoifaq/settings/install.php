<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Socialoifaq
 * @copyright  Copyright 2019-2020 Floating Number Digital Solution Pvt. Ltd.
 * @mentor     Abhisek Jaiswal
 * @author     MD Sarfaraj
 */
class Socialoifaq_Installer extends Engine_Package_Installer_Module
{
	 public function onInstall()
     {
        $this->_addFaqHomePage();
        $this->_addFaqViewPage();
        parent::onInstall();
    }

    protected function _addFaqHomePage()
    {
    	$db = $this->getDb();

        // profile page
        $pageId = $db->select()
            ->from('engine4_core_pages', 'page_id')
            ->where('name = ?', 'socialoifaq_index_index')
            ->limit(1)
            ->query()
            ->fetchColumn();

        // insert if it doesn't exist yet
        if( !$pageId ) {
            // Insert page
            $db->insert('engine4_core_pages', array(
                'name' => 'socialoifaq_index_index',
                'displayname' => 'FAQ Home Page',
                'title' => 'FAQ Home',
                'description' => 'This show all the faqs.',
                'custom' => 0,
            ));
            $pageId = $db->lastInsertId();

            // Insert top
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'top',
                'page_id' => $pageId,
                'order' => 1,
            ));
            $topId = $db->lastInsertId();

            // Insert main
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'main',
                'page_id' => $pageId,
                'order' => 2,
            ));
            $mainId = $db->lastInsertId();

            // Insert top-middle
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'middle',
                'page_id' => $pageId,
                'parent_content_id' => $topId,
            ));
            $topMiddleId = $db->lastInsertId();

            // Insert main-middle
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'middle',
                'page_id' => $pageId,
                'parent_content_id' => $mainId,
                'order' => 2,
            ));
            $mainMiddleId = $db->lastInsertId();

            // Insert main-right
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'right',
                'page_id' => $pageId,
                'parent_content_id' => $mainId,
                'order' => 1,
            ));
            $mainRightId = $db->lastInsertId();

            // Insert menu
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'socialoifaq.browse-menu',
                'page_id' => $pageId,
                'parent_content_id' => $topMiddleId,
                'order' => 1,
            ));

            // Insert content
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'core.content',
                'page_id' => $pageId,
                'parent_content_id' => $mainMiddleId,
                'order' => 1,
            ));

            // Insert search
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'socialoifaq.browse-search',
                'page_id' => $pageId,
                'parent_content_id' => $mainRightId,
                'order' => 1,
            ));

             // Insert search
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'socialoifaq.recent-faq',
                'page_id' => $pageId,
                'parent_content_id' => $mainRightId,
                'order' => 2,
            ));

            // Insert gutter menu
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'socialoifaq.browse-menu-quick',
                'page_id' => $pageId,
                'parent_content_id' => $mainRightId,
                'order' => 3,
            ));

        }
    }

    protected function _addFaqViewPage()
    {
    	 $db = $this->getDb();

        // profile page
        $pageId = $db->select()
            ->from('engine4_core_pages', 'page_id')
            ->where('name = ?', 'socialoifaq_index_view')
            ->limit(1)
            ->query()
            ->fetchColumn();

        // insert if it doesn't exist yet
        if( !$pageId ) {
            // Insert page
            $db->insert('engine4_core_pages', array(
                'name' => 'socialoifaq_index_view',
                'displayname' => 'FAQ View Page',
                'title' => 'FAQ View',
                'description' => 'This page show only one faq',
                'custom' => 0,
            ));
            $pageId = $db->lastInsertId();

            // Insert main
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'main',
                'page_id' => $pageId,
            ));
            $mainId = $db->lastInsertId();

            // Insert left
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'right',
                'page_id' => $pageId,
                'parent_content_id' => $mainId,
                'order' => 1,
            ));
            $leftId = $db->lastInsertId();

            // Insert middle
            $db->insert('engine4_core_content', array(
                'type' => 'container',
                'name' => 'middle',
                'page_id' => $pageId,
                'parent_content_id' => $mainId,
                'order' => 2,
            ));
            $middleId = $db->lastInsertId();

            // Insert gutter
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'socialoifaq.recent-faq',
                'page_id' => $pageId,
                'parent_content_id' => $leftId,
                'order' => 1,
            ));
         

            // Insert content
            $db->insert('engine4_core_content', array(
                'type' => 'widget',
                'name' => 'core.content',
                'page_id' => $pageId,
                'parent_content_id' => $middleId,
                'order' => 1,
            ));
            
        }
    }

}