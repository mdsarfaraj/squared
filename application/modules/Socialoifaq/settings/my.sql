/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Socialoifaq
 * @copyright  Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor     Abhisek Jaiswal
 * @author     MD Sarfaraj
 */


-- --------------------------------------------------------

--
-- Table structure for table `engine4_socialoifaq_faqs`
--

DROP TABLE IF EXISTS `engine4_socialoifaq_faqs`;

CREATE TABLE IF NOT EXISTS `engine4_socialoifaq_faqs` (
  `faq_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255)  NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `engine4_socialoifaq_questions`
--

DROP TABLE IF EXISTS `engine4_socialoifaq_questions`;
CREATE TABLE IF NOT EXISTS `engine4_socialoifaq_questions` (
  `question_id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL,
  `title` text NOT NULL,
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_menus`
--


INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`, `order`) VALUES
('socialoifaq_main', 'standard', 'Socialoifaq Home Navigation Menu', 999),
('Socialoifaq_quick', 'standard', 'Socialoifaq Quick Navigation Menu', 999);


-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_menuitems`
--


INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES


('faq_home_main', 'socialoifaq', 'FAQ', 'Socialoifaq_Plugin_Menus', '', 'core_main', NULL, 1, 0, 999),

('core_admin_main_plugins_faq', 'socialoifaq', 'FAQ', NULL, '{"route":"faq_manage"}', 'core_admin_main_plugins', NULL, 1, 0, 999),

('socialoifaq_main_home', 'socialoifaq', 'FAQs Home', 'Socialoifaq_Plugin_Menus::onMenuInitialize_FaqHomeMain', '{"route":"faq_general"}', 'socialoifaq_main', NULL, 1, 0, 999),

('socialoifaq_main_askquestion', 'socialoifaq', 'Ask Question ', 'Socialoifaq_Plugin_Menus::canAskQuestions', '{"class":"smoothbox","route":"faq_askquestion"}', 'socialoifaq_main', NULL, 1, 0, 999),

('Socialoifaq_quick_askquestion', 'socialoifaq', 'Ask Question', 'Socialoifaq_Plugin_Menus::canAskQuestions', '{"class":"smoothbox","route":"faq_askquestion"}', 'Socialoifaq_quick', NULL, 1, 0, 999);

-- -------------------------


-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_mailtemplates`
--

INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`, `module`, `vars`) VALUES
('socialoifaq_question', 'socialoifaq', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_name],[sender_email],[sender_link],[sender_photo],[message]');



INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('socialoifaq', 'socialoifaq', 'socialoifaq', '0.0.1', 1, 'extra') ;