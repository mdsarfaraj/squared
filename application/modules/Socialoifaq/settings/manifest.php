<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'socialoifaq',
    'version' => '0.0.1',
    'path' => 'application/modules/Socialoifaq',
    'title' => 'socialoifaq',
    'description' => 'socialoifaq',
    'author' => 'MD Sarfaraj',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'callback' => array(
      'path' => 'application/modules/Socialoifaq/settings/install.php',
      'class' => 'Socialoifaq_Installer',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Socialoifaq',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/socialoifaq.csv',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'socialoifaq',
    'socialoifaq_faq',
    'socialoifaq_question'
  ),
  // Routes --------------------------------------------------------------------
  'routes' => array(
    
    //https://rosehillhousing.net/socialoifaq/index/index
    'faq_general' => array(
      'route' => 'faq/:action/*',
      'defaults' => array(
        'module' => 'socialoifaq',
        'controller' => 'index',
        'action' => 'index',
      ),
    ),

    'faq_askquestion' => array(
      'route' => 'faq/askquestion/:action/*',
      'defaults' => array(
        'module' => 'socialoifaq',
        'controller' => 'index',
        'action' => 'question',
      ),
    ),

    'faq_view' => array(
      'route' => 'faq/view/:faq_id/*',
      'defaults' => array(
        'module' => 'socialoifaq',
        'controller' => 'index',
        'action' => 'view',
      ),
      'reqs' => array(
        'faq_id' => '\d+',
      ),
    ),



    // Admin
    //https://rosehillhousing.net/socialoifaq/admin-manage/index
    'faq_manage' => array(
      'route' => 'admin/faq/manage/:action/*',
      'defaults' => array(
        'module' => 'socialoifaq',
        'controller' => 'admin-manage',
        'action' => 'index',
      ),
    ),
    
    //https://rosehillhousing.net/socialoifaq/admin-manage/edit/faq_id/3
    'faq_specific' => array(
      'route' => 'admin/faq/:action/:faq_id/*',
      'defaults' => array(
        'module' => 'socialoifaq',
        'controller' => 'admin-manage',
        'action' => 'index',
      ),
      'reqs' => array(
        'faq_id' => '\d+',
        'action' => '(delete|edit)',
      ),
    ),

    


  ),

); ?>