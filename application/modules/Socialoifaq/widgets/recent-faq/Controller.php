<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

class Socialoifaq_Widget_RecentFaqController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
     //get the viewer
    $viewer = Engine_Api::_()->user()->getViewer();
    
    if(!$viewer->getIdentity()){
    	return $this->setNoRender();
    }

    $table = Engine_Api::_()->getDbtable('faqs','socialoifaq');
    
    $select = $table->select()
            ->where('status = ?', 0)
            ->order('creation_date DESC')
            ->limit(10);

    $this->view->paginator = $paginator = Zend_Paginator::factory($select);

    if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }


  }

}
