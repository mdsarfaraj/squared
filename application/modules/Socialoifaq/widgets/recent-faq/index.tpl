<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */
?>

<div class="generic_list_wrapper">
    <ul class="generic_list_widget">
      <?php foreach( $this->paginator as $item ): ?>
        <li>
          <div class="info">
            <div class="title">
              <?php echo $this->htmlLink($item->getHref(), $item->getMostRecent()) ?>
            </div>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>
</div>
