<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

class Socialoifaq_Widget_BrowseMenuController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Get navigation
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('socialoifaq_main');
    if( count($this->view->navigation) == 1 ) {
      $this->view->navigation = null;
    }
    
  }

}
