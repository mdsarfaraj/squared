<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @author      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

?>

<div class="headline">
  <h2>
    <?php echo $this->translate('FAQS');?>
  </h2>
  <?php if( count($this->navigation) > 0 ): ?>
    <div class="tabs">
      <?php
        // Render the menu
        echo $this->navigation()
          ->menu()
          ->setContainer($this->navigation)
          ->render();
      ?>
    </div>
  <?php endif; ?>
</div>
