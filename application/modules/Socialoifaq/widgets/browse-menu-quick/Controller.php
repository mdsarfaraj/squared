<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

class Socialoifaq_Widget_BrowseMenuQuickController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Get quick navigation
    $this->view->quickNavigation = $quickNavigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('Socialoifaq_quick');
    
  }

}
