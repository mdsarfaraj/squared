<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

?>

<?php if($this->paginator->getTotalItemCount() > 0): ?>
  <?php foreach ($this->paginator as $item): ?>

    <div class="accordion1">
      &nbsp;&nbsp;<?php echo $item->getTitle() ?>
    </div>
    <div class="panel1">
        <div class="answer-desc"><?php echo $item->getDescription() ?></div>
    </div>

  <?php endforeach; ?>
<?php else: ?>
 <div class="tip">
    <span>
      <?php echo $this->translate($this->question.' is not found.') ?>
    </span>
  </div>
<?php endif; ?>
<br />

<!--This is paginator-->
<div>
<?php echo $this->paginationControl($this->paginator, null, null, array(
  'pageAsQuery' => true,
  'query' => $this->formValues,
)); ?>
</div>
<!--style -->
<style>

.accordion1 {
  cursor: pointer;
  padding: 12px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 15px;
  line-height: 21px;
  overflow: hidden;
}

.accordion1:before{
  content: "Q.";
  font-size: 18px;
  font-weight: bolder;
}

.panel1 {
  display: none;
  overflow: hidden;
  margin-bottom: 35px;
  margin-left: 20px;
}
.accordion1:after {
  content: '\02795'; 
  float: right;
  margin-left: 5px;
}

.active1:after {
  content: "\2796"; 
}

.answer-desc{
  margin-top: 30px;
  text-align: justify;
  margin-left: 10px;
}

.liclass{
  margin-left: 20px;
  padding-top: 5px;
}

</style>

<script>
var acc = document.getElementsByClassName("accordion1");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active1");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>