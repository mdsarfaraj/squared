<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

?>

<div class="seaocore_gutter_view">
	<div class='seaocore_gutter_view_title sitefaq_view_title'>
		<h3><?php echo $this->socialoifaq->getTitle(); ?></h3>
	</div>
  
  <div class="seaocore_gutter_view_body sitefaq_faq_body">
  	<?php echo $this->socialoifaq->getDescription();  ?>
  </div>

</div>
