
<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

?>

<div class="global_form_popup">
  <?php echo $this->form->render($this) ?>
</div>
<style type="text/css">
.global_form div.form-label {
  width: 100px;
}
.global_form_popup #submit-wrapper, .global_form_popup #cancel-wrapper{
  float:none;
}
.global_form input[type="text"] {width:304px;}
.global_form textarea{width:350px;}
</style>