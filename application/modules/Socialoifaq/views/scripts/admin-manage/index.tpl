<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */

?>

<h2>
  <?php echo $this->translate('FAQ Plugin') ?>
</h2>


<!--Form Filter-->
<div class='admin_search'>
  <?php echo $this->formFilter->render($this) ?>
</div>

<br />
<!--Create Link button-->
<?php echo $this->htmlLink(array('action' => 'create', 'reset' => false), 
    $this->translate("Post New FAQ"),
    array(
      'class' => 'buttonlink',
      'style' => 'background-image: url(' . $this->layout()->staticBaseUrl . 'application/modules/Announcement/externals/images/admin/add.png);float:right;')) ?>
<br />

<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>

<div class='admin_results'>
  <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s faqs found", "%s faqs found", $count),
  $this->locale()->toNumber($count)) ?>
</div>

<!-- Paginator -->
<div>
  <?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
  )); ?>
</div>


<table class='admin_table'>
  <thead>
    <tr>
      <th style="width:1%;"><?php echo $this->translate("ID") ?></th>
      <th style="width:2%;" ><?php echo $this->translate("Question") ?></th>
      <th style="width:1%;"><?php echo $this->translate("Status") ?></th>
      <th style="width:2%;"><?php echo $this->translate("Date") ?></th>
      <th style="width:2%;"><?php echo $this->translate("Options") ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($this->paginator as $item): ?>
      <tr>
        <td><?php echo $item->getIdentity() ?></td>
        <td><?php echo $item->getTitle() ?></td>
        <td>
        	<?php if($item->status == 0){
        	  $photoUrl = "application/modules/Socialoifaq/externals/images/enabled1.gif"; ?>
               <img src="<?php echo $photoUrl; ?>" alt="" />
            <?php }else{
                     $photoUrl11 = "application/modules/Socialoifaq/externals/images/enabled0.gif"; 
            ?>
                <img src="<?php echo $photoUrl11; ?>" alt="" />
            <?php } ?>

        </td>

        <td><?php echo $this->locale()->toDateTime($item->creation_date) ?></td>
        <td>
          <?php echo $this->htmlLink(array('route' => 'faq_specific', 'module' => 'socialoifaq', 'controller' => 'admin-manage', 'action' => 'edit', 'faq_id' => $item->getIdentity()), $this->translate("view")) ?>
          &nbsp; | &nbsp;
          <?php echo $this->htmlLink(array('route' => 'faq_specific', 'module' => 'socialoifaq', 'controller' => 'admin-manage', 'action' => 'edit', 'faq_id' => $item->getIdentity()), $this->translate("edit")) ?>
           &nbsp; | &nbsp;
          <?php echo $this->htmlLink(array('route' => 'faq_specific', 'module' => 'socialoifaq', 'controller' => 'admin-manage', 'action' => 'delete', 'faq_id' => $item->getIdentity()), $this->translate("delete"),array('class' => 'smoothbox')) ?>

        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<?php else: ?>

  <div class="tip">
    <span>
      <?php echo $this->translate("There are not any faq yet.") ?>
    </span>
  </div>

<?php endif; ?>



