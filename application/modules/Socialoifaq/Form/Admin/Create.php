<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


class Socialoifaq_Form_Admin_Create extends Engine_Form
{
  public function init()
  {
      $this->setTitle('Add an FAQ')
         ->setDescription('');

      $this->addElement('Text', 'title', array(
        'label' => 'Question',
        'allowEmpty' => false,
        'required' => true,
        'filters' => array(
          new Engine_Filter_Censor(),
          'StripTags',
          new Engine_Filter_StringLength(array('max' => '63'))
        ),
        'autofocus' => 'autofocus',
      ));

      $editorOptions = array('html' => 1);
        $this->addElement('TinyMce', 'description', array(
            'label'=>'Answer',
            'disableLoadDefaultDecorators' => true,
            'editorOptions' => $editorOptions,
            'required' => true,
            'allowEmpty' => false,
            'decorators' => array(
              'ViewHelper',
              'Label'
        ),
          'filters' => array(
                new Engine_Filter_Censor(),
         ),
      ));
        
      $this->addElement('Select', 'status', array(
        'label' => 'Status',
        'multiOptions' => array("0"=>"Published", "1"=>"Saved As Draft"),
      ));

    $this->addElement('Button', 'submit', array(
      'label' => 'Post Entry',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => 'javascript:history.back();',
      'onclick' => 'parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');

  }
}