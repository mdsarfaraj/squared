<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


class Socialoifaq_Form_Admin_Edit extends Socialoifaq_Form_Admin_Create
{
  public function init()
  {
    parent::init();
    $this->setTitle('Edit Faq Entry')
      ->setDescription('');
    $this->submit->setLabel('Save Changes');
  }
}