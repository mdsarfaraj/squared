<?php 

/**
 * SocialEngine
 *
 * @category    Application_Extensions
 * @package     Socialoifaq
 * @copyright   Copyright 2019-2020 Floating Numbers Digital Solution Pvt. Ltd.
 * @mentor      Abhisek Jaiswal 
 * @author      MD Sarfaraj 

 */


class Socialoifaq_Form_Question extends Engine_Form
{
  public function init()
  {
	  $this->setTitle('Ask a Question');
		$this->setDescription("Have a question for us? Ask from below and we will get back to you.");

	    $this->addElement('Textarea', 'title', array(
	      'label' => 'Question',
	      'allowEmpty' => false,
	      'required' => true,
	      'autofocus' => true,
	      'filters' => array(
	        'StripTags',
	        new Engine_Filter_Censor(),
	    )));

	    $this->addElement('Button', 'submit', array(
	      'label' => 'Ask',
	      'type' => 'submit',
	      'ignore' => true,
	      'decorators' => array('ViewHelper')
	    ));

	    $this->addElement('Cancel', 'cancel', array(
	      'label' => 'cancel',
	      'link' => true,
	      'prependText' => ' or ',
	      'href' => '',
	      'onClick'=> 'javascript:parent.Smoothbox.close();',
	      'decorators' => array(
	        'ViewHelper'
	      )
	    ));
	    
	    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', array(
	      'decorators' => array(
	        'FormElements',
	        'DivDivDivWrapper',
	      ),
	    ));
    
  }

}