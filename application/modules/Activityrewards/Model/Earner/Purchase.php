<?php

class Activityrewards_Model_Earner_Purchase extends Activityrewards_Model_Earner_Abstract
{

  public function onTransactionStart(&$params) {

    $upearner = $params[0];
    $user = $params[1];
    $metadata = $params[2];
    $transaction_params = $params[3];
  
  
    // if requirements filled (called when actual vote is being casted)
    if( Semods_Utils::g($transaction_params, 'gotvars', 0) == 1 ) {
      // THIS SHOULD BE CALLED FROM A CALLBACK WITH SOME VARIABLE
  
      // ABORT TRANSACTION
      return false;
    }
  
    return true;

  }
  
  public function onTransactionSuccess(&$params) {

    $upearner = $params[0];
  
    
    $params['transaction_text'] = Zend_Registry::get('Zend_Translate')->_("Purchasing points") . " ({$upearner->userpointearner_title})";
  
    return true;
  }


  public function onTransactionFinished(&$params) {
  
    $upearner = $params[0];
    $user = $params[1];
    $metadata = $params[2];
    $transaction_params = $params[3];
  
	$session = new Zend_Session_Namespace('Activitypoints_Purchase');
	$session->transaction_id = $transaction_params['transaction_id'];

    $url = Zend_Controller_Front::getInstance()->getRouter()->assemble(
                            array('action'        => 'index',
                                  'module'        => 'activityrewards',
                                  'controller'    => 'purchase',
                                 ),
                            'default', true);          
  
  
    $params['redirect'] = $url;
    
    return true;
  }

}