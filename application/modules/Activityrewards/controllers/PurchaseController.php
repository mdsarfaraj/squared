<?php

class Activityrewards_PurchaseController extends Core_Controller_Action_Standard
{
  /**
   * @var User_Model_User
   */
  protected $_user;
  
  /**
   * @var Zend_Session_Namespace
   */
  protected $_session;

  public function init()
  {
    // If there are no enabled gateways or packages, disable
    if( Engine_Api::_()->getDbtable('gateways', 'payment')->getEnabledGatewayCount() <= 0 )
    {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
    
    // Get user and session
    $this->_user = Engine_Api::_()->user()->getViewer();
    $this->_session = new Zend_Session_Namespace('Activitypoints_Purchase');
    
    // Check viewer and user
    if( !$this->_user || !$this->_user->getIdentity() ) {
      //if( !empty($this->_session->user_id) ) {
      //  $this->_user = Engine_Api::_()->getItem('user', $this->_session->user_id);
      //}
      // If no user, redirect to home?
      //if( !$this->_user || !$this->_user->getIdentity() ) {
        $this->_session->unsetAll();
        return $this->_helper->redirector->gotoRoute(array(), 'default', true);
      //}
    }
  }
	
  public function indexAction()
  {

    $this->_helper->requireUser()->isValid();

    if( !$this->_helper->requireAuth()->setAuthParams('activitypoints', null, 'use')->isValid() ) return;

    //if(Semods_Utils::getSetting('activityrewards.enable_shop',0) == 0) {
    //  return $this->_helper->_redirector->gotoRoute(array(), 'activitypoints_vault');
    //}

    $api = Engine_Api::_()->getApi('core', 'activitypoints');
	$transaction_id = $this->_session->transaction_id;
		
		
	$transaction = Engine_Api::_()->getDbTable('transactions','activitypoints')->fetchRow(array('uptransaction_user_id=?' => $this->_user->getIdentity(), 'uptransaction_id=?' => $transaction_id));

	if(!$transaction) {// || $transaction->uptransaction_state == completed) {
      $this->_session->unsetAll();
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
	}
		
	$this->_session->item_id = $transaction->uptransaction_item_id;
		
    $upearner = Engine_Api::_()->getDbTable('earner','activityrewards')->fetchRow(array("userpointearner_id=?" => $transaction->uptransaction_item_id));

	if(!$upearner) {
      $this->_session->unsetAll();
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
	}
      
    $metadata = unserialize($upearner->userpointearner_metadata);

	$price = $metadata["price"];

		
		// SE order
		
		
		
		
		
		
		
    // Gateways
    $gatewayTable = Engine_Api::_()->getDbtable('gateways', 'payment');
    $gatewaySelect = $gatewayTable->select()
      ->where('enabled = ?', 1)
      ->where('title = ?', 'PayPal')	// only PayPal for now
      ;
    $gateways = $gatewayTable->fetchAll($gatewaySelect);

    $gatewayPlugins = array();
    foreach( $gateways as $gateway ) {
      $gatewayPlugins[] = array(
        'gateway' => $gateway,
        'plugin' => $gateway->getGateway(),
      );
    }
    $this->view->gateways = $gatewayPlugins;
		
		
	$this->view->price = $price;
	$this->view->upearner = $upearner;
	$this->view->currency = Engine_Api::_()->getApi('settings', 'core')->getSetting('payment.currency', 'USD');

  }

	
	
	public function processAction()
  {
    // Get gateway
    $gatewayId = $this->_getParam('gateway_id', $this->_session->gateway_id);
    if( !$gatewayId ||
        !($gateway = Engine_Api::_()->getItem('payment_gateway', $gatewayId)) ||
        !($gateway->enabled) ) {
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
    $this->view->gateway = $gateway;

		// Get Earner Item
    $upearner = Engine_Api::_()->getItem('activityrewards_earner', $this->_session->item_id);
    if(!$upearner) {
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
		}
		
    $this->view->upearner = $upearner;



    // Process
    
    // Create order
    $ordersTable = Engine_Api::_()->getDbtable('orders', 'payment');
    if( !empty($this->_session->order_id) ) {
      $previousOrder = $ordersTable->find($this->_session->order_id)->current();
      if( $previousOrder && $previousOrder->state == 'pending' ) {
        $previousOrder->state = 'incomplete';
        $previousOrder->save();
      }
    }
    $ordersTable->insert(array(
      'user_id' => $this->_user->getIdentity(),
      'gateway_id' => $gateway->gateway_id,
      'state' => 'pending',
      'creation_date' => new Zend_Db_Expr('NOW()'),
      'source_type' => 'activitypoints_transaction',
      'source_id' => $this->_session->transaction_id,
    ));
    $this->_session->order_id = $order_id = $ordersTable->getAdapter()->lastInsertId();

    // Unset certain keys
    unset($this->_session->item_id);
    unset($this->_session->gateway_id);

    
    // Get gateway plugin
    $this->view->gatewayPlugin = $gatewayPlugin = $gateway->getGateway();
    $plugin = $gateway->getPlugin();


    // Prepare host info
    $schema = 'http://';
    if( !empty($_ENV["HTTPS"]) && 'on' == strtolower($_ENV["HTTPS"]) ) {
      $schema = 'https://';
    }
    $host = $_SERVER['HTTP_HOST'];
    

    // Prepare transaction
    $params = array();
    $params['language'] = $this->_user->language;
    $localeParts = explode('_', $this->_user->language);
    if( count($localeParts) > 1 ) {
      $params['region'] = $localeParts[1];
    }
    $params['vendor_order_id'] = $order_id;
    $params['return_url'] = $schema . $host
      . $this->view->url(array('action' => 'return'))
      . '?order_id=' . $order_id
      . '&state=' . 'return';
    $params['cancel_url'] = $schema . $host
      . $this->view->url(array('action' => 'return'))
      . '?order_id=' . $order_id
      . '&state=' . 'cancel';
    $params['ipn_url'] = $schema . $host
      . $this->view->url(array('action' => 'index', 'controller' => 'ipn'))
      . '?order_id=' . $order_id;
    
	/// ****** PROCESS ****** ///
	if($plugin->title = 'PayPal') {
		$transaction = $this->_createTransactionPaypal($plugin, $upearner, $params);
	} else {
		// todo 2checkout
			
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
	}
    
    // Pull transaction params
    $this->view->transactionUrl = $transactionUrl = $gatewayPlugin->getGatewayUrl();
    $this->view->transactionMethod = $transactionMethod = $gatewayPlugin->getGatewayMethod();
    $this->view->transactionData = $transactionData = $transaction->getData();

    
    
    // Handle redirection
    if( $transactionMethod == 'GET' ) {
      $transactionUrl .= '?' . http_build_query($transactionData);
      return $this->_helper->redirector->gotoUrl($transactionUrl, array('prependBase' => false));
    }

    // Post will be handled by the view script
  }
	
	
	

	
	public function _createTransactionPaypal($gateway, $upearner, $params)
  {
	$points_transaction = Engine_Api::_()->getDbTable('transactions','activitypoints')->fetchRow(array('uptransaction_user_id=?' => $this->_user->getIdentity(), 'uptransaction_id=?' => $this->_session->transaction_id));

    // Process description
    $desc = $points_transaction->uptransaction_text;
    if( strlen($desc) > 127 ) {
      $desc = substr($desc, 0, 124) . '...';
    } else if( !$desc || strlen($desc) <= 0 ) {
      $desc = 'N/A';
    }
    if( function_exists('iconv') && strlen($desc) != iconv_strlen($desc) ) {
      // PayPal requires that DESC be single-byte characters
      $desc = @iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);
    }

    $metadata = unserialize($upearner->userpointearner_metadata);
		$price = $metadata["price"];
		
    // This is a one-time fee
    {
      $params['driverSpecificParams']['PayPal'] = array(
        'AMT' => $price,
        'DESC' => $desc,
        'CUSTOM' => $this->_session->transaction_id,
        'INVNUM' => $params['vendor_order_id'],
        'ITEMAMT' => $price,
        'ITEMS' => array(
          array(
            'NAME' => $desc,
            'DESC' => $desc,
            'AMT' => $price,
            'NUMBER' => $this->_session->transaction_id,
            'QTY' => 1,
          ),
        )
        //'BILLINGTYPE' => 'RecurringPayments',
        //'BILLINGAGREEMENTDESCRIPTION' => $package->getPackageDescription(),
      );

      // Should fix some issues with GiroPay
      if( !empty($params['return_url']) ) {
        $params['driverSpecificParams']['PayPal']['GIROPAYSUCCESSURL'] = $params['return_url']
          . ( false === strpos($params['return_url'], '?') ? '?' : '&' ) . 'giropay=1';
        $params['driverSpecificParams']['PayPal']['BANKTXNPENDINGURL'] = $params['return_url']
          . ( false === strpos($params['return_url'], '?') ? '?' : '&' ) . 'giropay=1';
      }
      if( !empty($params['cancel_url']) ) {
        $params['driverSpecificParams']['PayPal']['GIROPAYCANCELURL'] = $params['cancel_url']
          . ( false === strpos($params['return_url'], '?') ? '?' : '&' ) . 'giropay=1';
      }
    }
    
    // Create transaction
    $transaction = $gateway->createTransaction($params);

    return $transaction;
  }

	
  public function returnAction()
  {
		
    // Get order
    if( !$this->_user ||
        !($orderId = $this->_getParam('order_id', $this->_session->order_id)) ||
        !($order = Engine_Api::_()->getItem('payment_order', $orderId)) ||
        $order->user_id != $this->_user->getIdentity() ||
        $order->source_type != 'activitypoints_transaction' ||
        !($transaction = $order->getSource()) ||
        !($gateway = Engine_Api::_()->getItem('payment_gateway', $order->gateway_id)) ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }

		
    // Get gateway plugin
    $this->view->gatewayPlugin = $gatewayPlugin = $gateway->getGateway();
    $plugin = $gateway->getPlugin();

    // Process return
    unset($this->_session->errorMessage);
    try {
			if(get_class($plugin) == 'Payment_Plugin_Gateway_PayPal') {
				$status = $this->_onSubscriptionTransactionReturnPaypal($order, $gateway, $this->_getAllParams());
			} else {
				// todo 2checkout
				// todo Payment_Model_Exception - "unknown exception"
				throw Payment_Model_Exception("Gateway Not Implemented");
			}
    } catch( Payment_Model_Exception $e ) {
      $status = 'failure';
      $this->_session->errorMessage = $e->getMessage();
    }
    
    return $this->_finishPayment($status);
  }

	
  public function _onSubscriptionTransactionReturnPaypal(
      Payment_Model_Order $order, $gateway, array $params = array())
  {

	// Check that gateways match
    //if( $order->gateway_id != $this->_gatewayInfo->gateway_id ) {
    //  throw new Engine_Payment_Plugin_Exception('Gateways do not match');
    //}
    
    // Get related info
    $user = $order->getUser();
    $transaction = $order->getSource();
    $upearner = Engine_Api::_()->getDbTable('earner','activityrewards')->fetchRow(array("userpointearner_id=?" => $transaction->uptransaction_item_id));
    
	// tbd if transaction already processed
		
		
    // Check for cancel state - the user cancelled the transaction
    if( $params['state'] == 'cancel' ) {
      // Cancel order and subscription?
      $order->onCancel();
      $transaction->onPaymentFailure();
      // Error
      throw new Payment_Model_Exception('Your payment has been cancelled and ' .
          'not been charged. If this is not correct, please try again later.');
    }
    
    // Check params
    if( empty($params['token']) ) {
      // Cancel order and subscription?
      $order->onFailure();
      $transaction->onPaymentFailure();
      // This is a sanity error and cannot produce information a user could use
      // to correct the problem.
      throw new Payment_Model_Exception('There was an error processing your ' .
          'transaction. Please try again later.');
    }

    // Get details
    try {
      $data = $gateway->getService()->detailExpressCheckout($params['token']);
    } catch( Exception $e ) {
      // Cancel order and subscription?
      $order->onFailure();
      $transaction->onPaymentFailure();
      // This is a sanity error and cannot produce information a user could use
      // to correct the problem.
      throw new Payment_Model_Exception('There was an error processing your ' .
          'transaction. Please try again later.');
    }
    
    // Let's log it
    $gateway->getGateway()->getLog()->log('ExpressCheckoutDetail: '
        . print_r($data, true), Zend_Log::INFO);


    // One-time
    {

      // Do payment
      try {
        $rdata = $gateway->getService()->doExpressCheckoutPayment($params['token'],
              $params['PayerID'], array(
          'PAYMENTACTION' => 'Sale',
          'AMT' => $data['AMT'],
          'CURRENCYCODE' => $gateway->getGateway()->getCurrency(),
        ));
      } catch( Exception $e ) {
        // Log the error
        $gateway->getGateway()->getLog()->log('DoExpressCheckoutPaymentError: '
            . $e->__toString(), Zend_Log::ERR);
        
        // Cancel order and subscription?
        $order->onFailure();
        $transaction->onPaymentFailure();
        // This is a sanity error and cannot produce information a user could use
        // to correct the problem.
        throw new Payment_Model_Exception('There was an error processing your ' .
            'transaction. Please try again later.');
      }

      // Let's log it
      $gateway->getGateway()->getLog()->log('DoExpressCheckoutPayment: '
          . print_r($rdata, true), Zend_Log::INFO);

      // Get payment state
      $paymentStatus = null;
      $orderStatus = null;
      switch( strtolower($rdata['PAYMENTINFO'][0]['PAYMENTSTATUS']) ) {
        case 'created':
        case 'pending':
          $paymentStatus = 'pending';
          $orderStatus = 'complete';
          break;

        case 'completed':
        case 'processed':
        case 'canceled_reversal': // Probably doesn't apply
          $paymentStatus = 'okay';
          $orderStatus = 'complete';
          break;

        case 'denied':
        case 'failed':
        case 'voided': // Probably doesn't apply
        case 'reversed': // Probably doesn't apply
        case 'refunded': // Probably doesn't apply
        case 'expired':  // Probably doesn't apply
        default: // No idea what's going on here
          $paymentStatus = 'failed';
          $orderStatus = 'failed'; // This should probably be 'failed'
          break;
      }
      
      // Update order with profile info and complete status?
      $order->state = $orderStatus;
      $order->gateway_transaction_id = $rdata['PAYMENTINFO'][0]['TRANSACTIONID'];
      $order->save();

      // Insert transaction
      $transactionsTable = Engine_Api::_()->getDbtable('transactions', 'payment');
      $transactionsTable->insert(array(
        'user_id' => $order->user_id,
//        'gateway_id' => $this->_gatewayInfo->gateway_id,
        'gateway_id' => $order->gateway_id ,
        'timestamp' => new Zend_Db_Expr('NOW()'),
        'order_id' => $order->order_id,
        'type' => 'payment',
        'state' => $paymentStatus,
        'gateway_transaction_id' => $rdata['PAYMENTINFO'][0]['TRANSACTIONID'],
        'amount' => $rdata['AMT'], // @todo use this or gross (-fee)?
        'currency' => $rdata['PAYMENTINFO'][0]['CURRENCYCODE'],
      ));

      // Get benefit setting
      //$giveBenefit = Engine_Api::_()->getDbtable('transactions', 'payment')
      //    ->getBenefitStatus($user);
      
			// todo whether to confirm payment while status is still 'pending'  
			$giveBenefit = false;
			
      // Check payment status
      if( $paymentStatus == 'okay' ||
          ($paymentStatus == 'pending' && $giveBenefit) ) {

        // Update subscription info
//        $transaction->gateway_id = $this->_gatewayInfo->gateway_id;
        //$transaction->gateway_id = $order->gateway_id;
        //$transaction->gateway_profile_id = $rdata['PAYMENTINFO'][0]['TRANSACTIONID'];

        //$transaction->addMetadata('gateway_id',$order->gateway_id);
        //$transaction->addMetadata('gateway_profile_id', $rdata['PAYMENTINFO'][0]['TRANSACTIONID']);
		//$transaction->save();
				
        // Payment success
        $transaction->onPaymentSuccess();

        // send notification
        //if( ) {
        //  Engine_Api::_()->getApi('mail', 'core')->sendSystem($user, 'activityrewards_payment_success', array(
        //    'subscription_title' => $package->title,
        //    'subscription_description' => $package->description,
        //    'subscription_terms' => $package->getPackageDescription(),
        //    'object_link' => 'http://' . $_SERVER['HTTP_HOST'] .
        //        Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'user_login', true),
        //  ));
        //}
        
        return 'active';
      }
      else if( $paymentStatus == 'pending' ) {

        // Update subscription info
        $transaction->gateway_id = $order->gateway_id;
        $transaction->gateway_profile_id = $rdata['PAYMENTINFO'][0]['TRANSACTIONID'];

        // Payment pending
        $transaction->onPaymentPending();

        // send notification
        //if( ) {
        //  Engine_Api::_()->getApi('mail', 'core')->sendSystem($user, 'payment_subscription_pending', array(
        //    'subscription_title' => $package->title,
        //    'subscription_description' => $package->description,
        //    'subscription_terms' => $package->getPackageDescription(),
        //    'object_link' => 'http://' . $_SERVER['HTTP_HOST'] .
        //        Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'user_login', true),
        //  ));
        //}
        
        return 'pending';
      }
      else if( $paymentStatus == 'failed' ) {
        // Cancel order and subscription?
        $order->onFailure();
        $transaction->onPaymentFailure();
        // Payment failed
        throw new Payment_Model_Exception('Your payment could not be ' .
            'completed. Please ensure there are sufficient available funds ' .
            'in your account.');
      }
      else {
        // This is a sanity error and cannot produce information a user could use
        // to correct the problem.
        throw new Payment_Model_Exception('There was an error processing your ' .
            'transaction. Please try again later.');
      }
    }

    // Recurring
    //else {
    //  
    //}
  }	

  public function finishAction()
  {
    $this->view->status = $status = $this->_getParam('state');
    $this->view->error = $this->_session->errorMessage;
  }
	
  protected function _finishPayment($state = 'active')
	{
    $viewer = Engine_Api::_()->user()->getViewer();
    $user = $this->_user;

    // No user?
    if( !$this->_user ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }

    // Log the user in, if they aren't already
    if( ($state == 'active') &&
        $this->_user &&
        !$this->_user->isSelf($viewer) &&
        !$viewer->getIdentity() ) {
      Zend_Auth::getInstance()->getStorage()->write($this->_user->getIdentity());
      Engine_Api::_()->user()->setViewer();
      $viewer = $this->_user;
    }
    
    // Clear session
    $errorMessage = $this->_session->errorMessage;
    $userIdentity = $this->_session->user_id;
    $this->_session->unsetAll();
    $this->_session->user_id = $userIdentity;
    $this->_session->errorMessage = $errorMessage;

    // Redirect
    return $this->_helper->redirector->gotoRoute(array('action' => 'finish', 'state' => $state));
	}
	
}