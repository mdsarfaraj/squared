<?php

class Udc_IndexController extends Core_Controller_Action_Standard
{

  public function init(){

    //get the viewer
    $viewer = Engine_Api::_()->user()->getViewer();
    if(!$viewer->getIdentity()){
      return $this->_forward('notfound', 'error', 'core'); 
    }

    if(!$viewer->isAdmin()){
      return $this->_forward('notfound', 'error', 'core'); 
    }

  }

  public function indexAction() {

    $this->view->someVar = 'someVal';
    
    $this->view->formFilter = $formFilter = new User_Form_Admin_Manage_Filter();
    $page = $this->_getParam('page', 1);

    $table = Engine_Api::_()->getDbtable('users', 'user');
    $select = $table->select();

    // Process form
    $values = array();
    if( $formFilter->isValid($this->_getAllParams()) ) {
      $values = $formFilter->getValues();
    }

    foreach( $values as $key => $value ) {
      if( null === $value ) {
        unset($values[$key]);
      }
    }

    $values = array_merge(array(
      'order' => 'user_id',
      'order_direction' => 'DESC',
    ), $values);

    $this->view->assign($values);

    // Set up select info
    $select->order(( !empty($values['order']) ? $values['order'] : 'user_id' ) . ' ' . ( !empty($values['order_direction']) ? $values['order_direction'] : 'DESC' ));

    if( !empty($values['displayname']) ) {
      $select->where('displayname LIKE ?', '%' . $values['displayname'] . '%');
    }
    if( !empty($values['username']) ) {
      $select->where('username LIKE ?', '%' . $values['username'] . '%');
    }
    if( !empty($values['email']) ) {
      $select->where('email LIKE ?', '%' . $values['email'] . '%');
    }
    if( !empty($values['level_id']) ) {
      $select->where('level_id = ?', $values['level_id'] );
    }
    if( isset($values['enabled']) && $values['enabled'] != -1 ) {
      $select->where('enabled = ?', $values['enabled'] );
    }
    if( !empty($values['user_id']) ) {
      $select->where('user_id = ?', (int) $values['user_id']);
    }

    // Filter out junk
    $valuesCopy = array_filter($values);
    // Reset enabled bit
    if( isset($values['enabled']) && $values['enabled'] == 0 ) {
      $valuesCopy['enabled'] = 0;
    }

    // Make paginator
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
    $this->view->formValues = $valuesCopy;

    $table = Engine_Api::_()->getDbtable('activities', 'udc');
    // $activityList = $table->getActivityList();
    
    $activityData = $table->getActivityDetail();

    $dataArray = array();
    $labelArray = array();
    $colorArray = array();
    foreach($activityData as $item) {
      // $itemArray = array();
      // $itemArray['y'] = $item->score;
      // $itemArray['label'] = $item->type;
      // $activityDetailsArray[] = $itemArray;

      $dataArray[] = round($item->score,2);
      $labelArray[] = $item->type;
      $colorArray[] = $this->generateColor($colorArray);
      
    }

    $this->view->dataArray = json_encode($dataArray);
    $this->view->labelArray = json_encode($labelArray);
    $this->view->colorArray = json_encode($colorArray);

  }

  public function generateColor($colorArray) {
    $color = '#' . substr(md5(mt_rand()), 0, 6);

    if(!in_array($color, $colorArray)) {
      return $color;
    } else {
      $this->generateColor($colorArray);
    }
  }

  public function allAction() {
    $this->view->someVar = 'someVal';
    
  }
  
  public function detailsAction() {
    $this->view->someVar = 'someVal';
    $id = $this->_getParam("id");
    $this->view->user = $user = Engine_Api::_()->getItem('user', $id);



    $table = Engine_Api::_()->getDbtable('activities', 'udc');
    // $activityList = $table->getActivityList();
    
    $activityData = $table->getActivityDetail(array('user_id' => $id));

    $dataArray = array();
    $labelArray = array();
    $colorArray = array();
    foreach($activityData as $item) {
      $itemArray = array();

      $dataArray[] = round($item->score,2);
      $labelArray[] = $item->type;
      $colorArray[] = $this->generateColor($colorArray);
      // $itemArray[] = $item->score;
      // $itemArray['label'] = $item->type;
      // $activityDetailsArray[] = $itemArray;

    }

    $this->view->dataArray = json_encode($dataArray);
    $this->view->labelArray = json_encode($labelArray);
    $this->view->colorArray = json_encode($colorArray);
    
  }
}