<?php


class Udc_Api_Core extends Core_Api_Abstract
{

  public static $action_group_types =   array(
    0  => 'Unknown / Uncategorized',
    1  => 'Group',
    2  => 'Poll',
    3  => 'Events',
    4  => 'Classifieds',
    5  => 'Blog',
    6  => 'Media / Albums',
    9  => 'Music',
    10 => 'Video',
    11 => 'Forum',
    100  => 'General',
    101  => 'Signup / Marketing'
  );





  /*********************** MAIN "FINANCIAL" FUNCTIONS *********************/
  
  
  /*
   * Get header details
   * 
   * @return header details in array
   * 
   */
  function getHeaderDetails() {
    
    $request = new Zend_Controller_Request_Http();
    try {
      //code...
      foreach ($_SERVER as $name => $value) { 
        if (substr($name, 0, 5) == 'HTTP_') { 
          $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
        }
      }

      return $headers;


    } catch (Exception $th) {
      throw $th;
      return false;
    }

  }
  
}