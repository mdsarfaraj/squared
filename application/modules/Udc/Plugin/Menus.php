<?php
class Udc_Plugin_Menus
{
    public function onMenuInitialize_CoreMainUsageReports($row) {

        $viewer = Engine_Api::_()->user()->getViewer();
        if(!$viewer || !$viewer->getIdentity() || !$viewer->isAdmin()) {
            return false;
        }

        $route   = array();
        $route['route']  = 'udc_general';
        $route['icon'] = 'fa-pie-chart';

        return $route;

    }


}
