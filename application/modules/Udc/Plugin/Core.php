<?php

class Udc_Plugin_Core
{

    public function onSemodsAddActivity($event)
    {
  
      $payload = $event->getPayload();
  
      
      $user = $payload['subject'];
      $actiontype_name = $payload['type'];
  
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Activity";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
  
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
      
    }
    
  
    public function onFriendsinviterRefer($event) {
      
      $payload = $event->getPayload();
  
      $referrer = $payload['referrer'];
      $new_user = $payload['new_user'];
      
      // udc tracker
      $data = array();
      $data['user_id'] = $referrer->getIdentity();
      $data['activity_type'] = "Invite";
      $data['resource_type'] = "User";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
      
    }
  
    public function onFriendsinviterStats($event) {
      
      $payload = $event->getPayload();
      
      $user_id = isset($payload['user']) ? $payload['user']->getIdentity() : $payload['user_id'];
      $invites_count = $payload['invites_count'];
      
    }
    
  
    public function onAlbumCreateAfter($event)
    {
  
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'newalbum';
  
      
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Album";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
    }
  
    public function onCoreLikeCreateAfter($event)
    {
  
     /* $table = Engine_Api::_()->getDbtable('tests','udc');
      
      $data = array(
        'name' => 'MD Sarfaraj',
      );

      $table->insert($data);*/

      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'like';
      
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Like";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
    }
    
  
    public function onForumTopicCreateAfter($event)
    {
  
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'forum_topic';
      
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Forum Topic";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
    }
  
    public function onForumPostCreateAfter($event)
    {
  
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'forum_post';
      
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Forum Post";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
    }
    
  
    public function onMessagesMessageCreateAfter($event)
    {
  
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'message';
      
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Message Create";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
    }
  
    public function onMusicSongCreateAfter($event)
    {
    
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'newmusic';
      
    }
  
    public function onActivityCommentCreateAfter($event)
    {
    
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'comment_activity';
      
      // udc tracker
      $data = array();
      $data['user_id'] = $user->getIdentity();
      $data['activity_type'] = $actiontype_name;
      $data['resource_type'] = "Activity Comment";
      // $data['datetime'] = strtotime("now");
      
      // // inserting activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
      $udcActivity = $udcActivityTable->insert($data);
      
      // // Add raw data of the activity
      $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
      $udcActivityTable->insert(array(
          'user_id' => $data['user_id'],
          'activity_id' => $udcActivity,
          'rawdata' => json_encode($_SERVER)
      ));
      
  
    }
    
  
    public function onFieldsValuesSave($event)
    {
  
      $payload = $event->getPayload();
      
      $item = $payload['item'];
      $values = $payload['values'];
      
      $user = Engine_Api::_()->user()->getViewer();
      $actiontype_name = 'fields_change_generic';
  
  
      if( ($item instanceof User_Model_User) && ($user->getIdentity() == $item->getIdentity()) ) {
  
        Engine_Api::_()->getApi('core', 'activitypoints')->updatePoints( $user->getIdentity(), $actiontype_name );
        
        // udc tracker
        $data = array();
        $data['user_id'] = $user->getIdentity();
        $data['activity_type'] = $actiontype_name;
        $data['resource_type'] = "Activity Comment";
        // $data['datetime'] = strtotime("now");
        
        // // inserting activity
        $udcActivityTable = Engine_Api::_()->getDbTable('activities', 'udc');
        $udcActivity = $udcActivityTable->insert($data);
        
        // // Add raw data of the activity
        $udcActivityTable = Engine_Api::_()->getDbTable('activityrawdatas', 'udc');
        $udcActivityTable->insert(array(
            'user_id' => $data['user_id'],
            'activity_id' => $udcActivity,
            'rawdata' => json_encode($_SERVER)
        ));

      }
      
      
    }
  
    
    
  
    // public function onVideoRatingCreateAfter($event)
    // {
    
    //  $user = Engine_Api::_()->user()->getViewer();
    //  $actiontype_name = 'video_rate';
    
    //  Engine_Api::_()->getApi('core', 'activitypoints')->updatePoints( $user->getIdentity(), $actiontype_name );
     
    // }
  
    
    // public function onUserDeleteBefore($event)
    // {
    
    //   $payload = $event->getPayload();
    //   if( $payload instanceof User_Model_User ) {
  
    //     // Remove counters
    //     Engine_Api::_()->getDbtable('counters', 'activitypoints')->delete( array('userpointcounters_user_id = ?' => $payload->getIdentity() ) );
      
    //     // Remove transactions
    //     Engine_Api::_()->getDbtable('transactions', 'activitypoints')->delete( array('uptransaction_user_id = ?' => $payload->getIdentity() ) );
      
    //     // Remove user points
    //     Engine_Api::_()->getDbtable('points', 'activitypoints')->delete( array('userpoints_user_id = ?' => $payload->getIdentity() ) );
        
    //   }
    // }
    
}