<?php
class Udc_Model_DbTable_Activities extends Engine_Db_Table
{
  protected $_name = 'udc_activities';

  /*
   * Add activity
   * @params
   * array(
   *  user_id,
   *  activity_type,
   *  resource_type,
   *  resource_id,
   *  ip,
   * )
   * 
   * @return boolean
   *
   *
   */

  public function addActivity($data = array()) {
    try {
      //code...

      // Add Activity Details
      $data['datetime'] = "121";
      $data['ip'] = "";

      // Add Raw data of the activity
      return $this->insert($data);
      

    } catch ( Exception $ex ) {
      throw $ex;
      return false;

    }
  }

  public function getActivityList() {
    $select = $this->select()
                  ->distinct();

    $listsObj = $this->fetchAll();
    $listArray = array();
    foreach($listsObj as $item) {
      // $itemArray = array();
      $listArray[] = $item->activity_type;
    }

    return $listArray;
  }
  /*
   * get the Activity details
   * params = array()
   * 
   */
  public function getActivityDetail($params = array()) {
    
    $table_name = $this->info('name');
    $select = $this->select()
                    ->setIntegrityCheck(false) 
                    ->from($table_name, array(new Zend_Db_Expr('COUNT(*) AS total'), new Zend_Db_Expr('`activity_type` AS type'), new Zend_Db_Expr("(COUNT(*) * 100 / (Select COUNT(*) From {$table_name})) as score")))
                    ->group(array('activity_type'));

    // echo $select;

    if(isset($params['user_id']) && !empty($params['user_id'])) {
      $select->where("user_id = ?", $params['user_id']);
    }

    if(isset($params['activity_type']) && !empty($params['activity_type'])) {
      $select->where("activity_type = ?", $params['activity_type']);
    }

    $activityData = $this->fetchAll($select);
    return $activityData;
  }
}