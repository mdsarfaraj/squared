<?php
class Udc_Model_DbTable_Activityrawdatas extends Engine_Db_Table
{
  protected $_name = 'udc_activityrawdatas';

  /*
   * Add activity
   * @params
   * array(
   *  user_id,
   *  activity_type,
   *  resource_type,
   *  resource_id,
   *  ip,
   * )
   * 
   * @return boolean
   *
   *
   */

  public function addActivityRawData($data = array()) {
    try {
      //code...

      // Add Raw data of the activity
      return $this->insert($data);

    } catch ( Exception $ex ) {
      throw $ex;
      return false;

    }
  }
}