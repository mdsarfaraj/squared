<?php 
return array (
  'package' => array (
    'type' => 'module',
    'name' => 'udc',
    'version' => '4.0.0',
    'sku' => 'udc',
    'path' => 'application/modules/Udc',
    'title' => 'Usage Data Center',
    'description' => 'Usage Data Center',
    'author' => 'Abhisek Jaiswal',
    'callback' => array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => array (
      0 => 'application/modules/Udc',
    ),
    'files' => array (
      0 => 'application/languages/en/udc.csv',
    ),
  ),
  
  // Hooks ---------------------------------------------------------------------
  'hooks' => array (
    array(
      'event' => 'onFriendsinviterRefer',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onFriendsinviterStats',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onAlbumCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onCoreLikeCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onForumTopicCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onForumPostCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onMessagesMessageCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onMusicSongCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onActivityCommentCreateAfter',
      'resource' => 'Udc_Plugin_Core',
    ),
    array(
      'event' => 'onFieldsValuesSave',
      'resource' => 'Udc_Plugin_Core',
    ),
  ),
  'routes' => array(

    'udc_general' => array(
        'route' => 'udc/:controller/:action',
        'defaults' => array(
            'module' => 'udc',
            'controller' => 'index',
            'action' => 'index',
        ),
    ),

  ),

);
?>