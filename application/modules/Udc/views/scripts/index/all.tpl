<h1>
  <?php echo $this->translate("Total Network Usage Report") ?>
</h1>

<br>

<?php echo $this->htmlLink(array("module" => "udc", "controller" => "index", "actions" => "index"),
                  "back",
                  array('class' => 'buttonlink icon_previous')); ?>
<script>
window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	exportEnabled: true,
	animationEnabled: true,
	title: {
		text: "Usage Report of the total Network"
	},
	data: [{
		type: "pie",
		startAngle: 25,
		toolTipContent: "<b>{label}</b>: {y}%",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - {y}%",
		dataPoints: [
			{ y: 49.08, label: "Likes" },
			{ y: 27.34, label: "Comment" },
			{ y: 12.62, label: "Videos viewed" },
			{ y: 5.02, label: "Messages Sent" },
		]
	}]
});
chart.render();

}
</script>

<script src="/application/modules/Udc/externals/scripts/canvasjs.min.js"></script>

<div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
