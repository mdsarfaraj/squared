<h1>
  <?php echo $this->translate("Members Report") ?>
</h1>

<br>
<br><br><br>

<div id="canvas-holder" style="style="width:40%;display: block;height: 100%;min-height: 200px;"">
		<canvas id="chart-area"></canvas>
	</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<script>
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data: <?php echo $this->dataArray; ?>,
				backgroundColor: <?php echo $this->colorArray; ?>,
				label: 'Usage Data'
			}],
			labels: <?php echo $this->labelArray; ?>,
		},
		options: {
			responsive: true
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('chart-area').getContext('2d');
		window.myPie = new Chart(ctx, config);
	};

</script>


<div class='admin_results'>
  <div>
    <?php $count = $this->paginator->getTotalItemCount() ?>
    <?php echo $this->translate(array("%s member found", "%s members found", $count),
        $this->locale()->toNumber($count)) ?>
  </div>
</div>

<br />
<div style="overflow-x:auto;">
<div class="user_table_form">
  <table class='user_table'>
    <thead>
      <tr>
        <th style='width: 1%;'><a href="javascript:void(0);" ><?php echo $this->translate("Username") ?></a></th>
        <th style='width: 1%;'><a href="javascript:void(0);" ><?php echo $this->translate("Display Name") ?></a></th>
        <th style='width: 1%;'><a href="javascript:void(0);" ><?php echo $this->translate("Email") ?></a></th>
        <th style='width: 1%;'><a href="javascript:void(0);" ><?php echo $this->translate("Birthday") ?></a></th>
        <th style='width: 1%;' class='admin_table_centered'><a href="javascript:void(0);" ><?php echo $this->translate("Details") ?></a></th>
        <!--<th style='width: 1%;' class='admin_table_options'><?php echo $this->translate("Options") ?></th>-->
      </tr>
    </thead>
    <tbody>
        <?php

        ?>
      <?php if( count($this->paginator) ): ?>
        <?php foreach( $this->paginator as $item ):
          $user = $this->item('user', $item->user_id);
            ?>
            <?php
                    $valuesTable = Engine_Api::_()->fields()->getTable('user', 'values');
          
          $valuesTableName = $valuesTable->info('name');
          $userMetaTable = Engine_Api::_()->fields()->getTable('user', 'meta');
          $userMetaTableName = $userMetaTable->info('name');
            
          $selectQuery = $valuesTable->select()
                                    ->setIntegrityCheck(false)
                                    ->from($valuesTableName)
                                    ->joinLeft($userMetaTableName, "$valuesTableName.field_id = $userMetaTableName.field_id", 'label')
                                    ->where("$valuesTableName.item_id = ?", $user->getIdentity())
                                    ->where("$valuesTableName.field_id IN (5, 6, 28, 27, 19, 25)")
                                    ;

            $fetacData = $valuesTable->fetchAll($selectQuery);
            $details = array();
            foreach($fetacData as $itemData) {
                $details[$itemData->label] = $itemData->value;
            }
            ?>
          <tr>
            <td class='admin_table_user'><?php echo $this->htmlLink($this->item('user', $item->user_id)->getHref(), $this->item('user', $item->user_id)->username, array('target' => '_blank')) ?></td>
            <td class='admin_table_bold'>
              <?php echo $this->htmlLink($user->getHref(),
                  $this->string()->truncate($user->getTitle(), 10),
                  array('target' => '_blank'))?>
            </td>
            <td class='admin_table_email'>
              <?php if( !$this->hideEmails ): ?>
                <a href='mailto:<?php echo $item->email ?>'><?php echo $item->email ?></a>
              <?php else: ?>
                (hidden)
              <?php endif; ?>
            </td>
            
            <td class='admin_table_email'>
              <?php echo $details['Birthday'] ?>
            </td>
            <td class="admin_table_centered nowrap">
              <?php
              echo $this->htmlLink(array('route' => 'default', 'module' => 'udc', 'controller' => 'index', 'action' => 'details', 'id' => $item->getIdentity()), $this->translate('Usage'), array(
                'class' => ''
              ));
              ?>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
  <br />
</div>
<div>
</div>
<?php echo $this->paginationControl($this->paginator, null, null, array(
    'pageAsQuery' => true,
    'query' => $this->formValues,
    //'params' => $this->formValues,
)); ?>
</div>

<style type="text/css" >
    .user_table_form thead th {
        background-color: #ccc9c9;
    }
    .user_table_form thead th, .user_table_form tbody td {
        padding: 10px;
        border-bottom: 1px solid;
    }
    .user_table_form tbody tr:hover {
        background-color: #e6e2e2;
    }
</style>