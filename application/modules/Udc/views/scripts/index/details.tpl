<h1>
  <?php echo $this->translate("Members Report") ?>
</h1>

<br>

<?php echo $this->htmlLink(array("module" => "udc", "controller" => "index", "actions" => "index"),
                  "back",
                  array('class' => 'buttonlink icon_previous')); 
?>


<div id="canvas-holder" style="style="width:40%;display: block;height: 100%;min-height: 200px;"">
		<canvas id="chart-area"></canvas>
	</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<script>
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data: <?php echo $this->dataArray; ?>,
				backgroundColor: <?php echo $this->colorArray; ?>,
				label: 'Usage Data'
			}],
			labels: <?php echo $this->labelArray; ?>,
		},
		options: {
			responsive: true
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('chart-area').getContext('2d');
		window.myPie = new Chart(ctx, config);
	};

</script>