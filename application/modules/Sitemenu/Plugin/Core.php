<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2014 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Core.php 2014-05-26 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitemenu_Plugin_Core extends Zend_Controller_Plugin_Abstract
{

    public function onRenderLayoutDefault($event)
    {

        $view = $event->getPayload();
        $view->headTranslate(array("Forgot Password?", "Login with Twitter", "Login with Facebook", "Mark as Read", "Mark as Unread"));
        $view->headScript()
            ->appendFile($view->layout()->staticBaseUrl . 'application/modules/Sitemenu/externals/scripts/core.js');
        $view->headLink()->appendStylesheet($view->layout()->staticBaseUrl
        . 'application/modules/Sitemenu/externals/styles/style_sitemenu.css');
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
  {

    //IF SITEMENU IS ENABLED CHANGE THE MENU EDITOR PATH TO SITEMENU MENU EDITOR FOR MAIN MENU
    if( substr($request->getPathInfo(), 1, 5) != "admin" ) {
      return;
    }
    $module = $request->getModuleName();
    $controller = $request->getControllerName();
    $action = $request->getActionName();
    if( $module == 'core' && $controller == 'admin-menus' && $action == 'index' ) {
      $menuName = Zend_Controller_Front::getInstance()->getRequest()->getParam('name', 'sitemenu_core_main');
      $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
      if( $menuName == 'sitemenu_core_main' ) {
        $redirector->gotoRoute(array('module' => 'sitemenu', 'controller' => 'menu-settings', 'action' => 'editor'), 'admin_default', false);
      } else {

        $redirector->gotoRoute(array('module' => 'sitemenu', 'controller' => 'menus', 'action' => 'index', 'name' => $menuName), 'admin_default', false);
      }
    }
  }

  public function getAdminNotifications($event)
  {

    $url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'sitemenu', 'controller' => 'settings'), 'admin_default', true);

    $menuCache = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.cache.enable', 1);
    if( $menuCache ) {
      $event->addResponse('<div class="tip apc_tips"><span>You have enabled Menu Caching for \'Advanced Menus Plugin\'. If you want to disable it then <a href="' . $url . '">click here</a>.</span></div>');
    } else {
      $event->addResponse('<div class="tip apc_tips"><span>You have disabled Menu Caching for \'Advanced Menus Plugin\'. If you want to enable it then <a href="' . $url . '">click here</a>.</span></div>');
    }
    Engine_Api::_()->sitemenu()->integrateCoreMainMenu();
  }

  public function onRenderLayoutDefaultSimple($event)
  {
    // Forward
    return $this->onRenderLayoutDefault($event, 'simple');
  }

  public function onRenderLayoutMobileDefault($event)
  {
    // Forward
    return $this->onRenderLayoutDefault($event);
  }

  public function onRenderLayoutMobileDefaultSimple($event)
  {
    // Forward
    return $this->onRenderLayoutDefault($event);
  }
}
