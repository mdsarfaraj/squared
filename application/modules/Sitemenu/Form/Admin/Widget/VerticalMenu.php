<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Search.php 2015-05-15 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitemenu_Form_Admin_Widget_VerticalMenu extends Engine_Form {

  public function init() {
    $this->addElement('Select', 'pannelPositions', array(
      'label' => 'Select the position of vertical menu.',
      'multiOptions' => array(
        'left' => 'Left Side',
        'right' => 'Right Side',
      ),
      'value' => 'left',
    ));

    $this->addElement('Select', 'pannelColor', array(
      'label' => 'Select the color scheme for vertical menu.',
      'multiOptions' => array(
        'dark' => 'Dark color',
        'light' => 'Light color',
        'theme' => 'Theme color',
      ),
      'value' => 'theme',
    ));

    $this->addElement('Select', 'hidePannel', array(
      'label' => 'Do you want to always show the vertical menu?',
      'multiOptions' => array(
        '1' => 'No, open only when user clicks on the menu icon',
        '0' => 'Yes, always show the vertical menu',
      ),
      'value' => '1',
      'onchange' => 'SiteMenuVerticalMenu.hidePannel()'
    ));

    $this->addElement('Select', 'pannelType', array(
      'label' => 'Select how you want to display the vertical menu.',
      'multiOptions' => array(
        'overlay' => 'Overlay the page content',
        'fitpage' => 'Slide and adjust the page content based on available width',
        'shiftpage' => 'Shift the page content left/right side',
      ),
      'value' => 'overlay',
    ));

    $this->addElement('Select', 'openPannelOnLoad', array(
      'label' => 'Do you want to open the vertical menu automatically when page loads?',
      'multiOptions' => array(
        '1' => 'Yes, always open the vertical menu automatically when page loads',
        '0' => 'No, open the vertical menu on the click of menu icon',
      ),
      'value' => '0',
    ));
//    $this->addElement('Select', 'alwaysOpenSubmenu', array(
//      'label' => 'Do you want to always open the sub menus when page is load?',
//      'multiOptions' => array(
//        '1' => 'Always open',
//        '0' => 'Open on click of menu items',
//      ),
//      'value' => '0',
//    ));

    $this->addElement('Select', 'show_search_field', array(
      'label' => 'Do you want to display search field?',
      'multiOptions' => array(
        '2' => 'Yes, display for all users',
        '1' => 'Yes, display for all users except guest users',
        '0' => 'No'
      ),
      'value' => '0',
    ));

    $this->addElement('Select', 'sitemenu_on_logged_out', array(
      'label' => 'Do you want to show the widget to guest (non-logged in) users?',
      'multiOptions' => array(
        '1' => 'Yes',
        '0' => 'No',
      ),
      'value' => '1'
    ));

//    $this->addElement('Select', 'sitemenu_show_cart', array(
//      'label' => 'Do you want to show cart icon?',
//      'description' => '(Note: This setting will work only if you have our Stores / Marketplace - Ecommerce Plugin. After enabling this setting, caching of Main Menu will not work on your website.)',
//      'multiOptions' => array(
//        1 => 'Yes',
//        0 => 'No'
//      ),
//      'value' => 1
//    ));
  //  $this->addElement('Text', 'sitemenu_truncation_limit_content', array(
//      'label' => "Enter the title truncation limit for the content.",
//      'value' => 20,
//    ));
//
//    $this->addElement('Text', 'sitemenu_truncation_limit_category', array(
//      'label' => "Enter the title truncation limit for categories.",
//      'value' => 20,
//    ));
    $this->addElement('Hidden', 'title', array(
      'order' => 1000,
    ));
    $this->addElement('hidden', 'nomobile', array(
      'value' => 0,
      'order' => 1001,
    ));
  }

}
?>
<script type="text/javascript">
  var SiteMenuVerticalMenu = {
      hidePannel: function () {
          if ($('hidePannel').get('value') == 0) {
              $('pannelType-wrapper').setStyle('display', 'none');
              $('openPannelOnLoad-wrapper').setStyle('display', 'none');
          } else {
              $('pannelType-wrapper').setStyle('display', 'block');
              $('openPannelOnLoad-wrapper').setStyle('display', 'block');
          }
      }
  }
  window.addEvent('domready', function () {
      SiteMenuVerticalMenu.hidePannel();
  });
</script>