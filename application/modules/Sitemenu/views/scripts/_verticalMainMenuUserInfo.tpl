<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2014 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2014-05-26 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php if( $this->viewer_id ): ?>
  <?php $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('user_home'); ?>
  <div class="sm_panel_bg_block" <?php if( $this->coverUserPhoto ): ?> style="background-image: url(<?php echo $this->coverUserPhoto->getPhotoUrl('thumb.cover') ?>)"<?php endif; ?>>
    <div class="sm_user_photo">
      <?php echo $this->htmlLink($this->viewer()->getHref(), $this->itemPhoto($this->viewer(), 'thumb.icon')) ?>
    </div>
    <div class="sm_user_info">
      <div class="sm_user_name"><?php echo $this->htmlLink($this->viewer()->getHref(), $this->viewer()->getTitle()); ?></div>
      <?php if( count($navigation) > 0 ): ?>
        <a class="quicklink_arrow" id="sm_user_quicklinks_action" href="javascript:void(0);"><i class="fa fa-angle-down"></i></a>
      <?php endif; ?>
    </div>
  </div>
  <?php if( count($navigation) > 0 ): ?>
    <div class="sm_user_quicklinks">
      <div class="sm_quicklink_dropdown">
        <?php
        echo $this->navigation()
          ->menu()
          ->setContainer($navigation)
          ->setPartial(array('_navIcons.tpl', 'core'))
          ->render()
        ?>
      </div>
    </div>
  <?php endif; ?>
<?php endif; ?>