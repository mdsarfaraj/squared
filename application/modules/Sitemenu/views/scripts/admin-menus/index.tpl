<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9924 2013-02-16 02:16:02Z alex $
 * @author     John
 */
?>

<script type="text/javascript">

    var SortablesInstance;

    window.addEvent('domready', function() {
    $$('.item_label').addEvents({
        mouseover: showPreview,
        mouseout: showPreview
    });
    });

    var showPreview = function(event) {
    try {
        element = $(event.target);
        element = element.getParent('.admin_menus_item').getElement('.item_url');
        if( event.type == 'mouseover' ) {
        element.setStyle('display', 'block');
        } else if( event.type == 'mouseout' ) {
        element.setStyle('display', 'none');
        }
    } catch( e ) {
        //alert(e);
    }
    }


    window.addEvent('load', function() {
    SortablesInstance = new Sortables('menu_list', {
        clone: true,
        constrain: false,
        handle: '.item_label',
        onComplete: function(e) {
        reorder(e);
        }
        });
    });

    var reorder = function(e) {
     var menuitems = e.parentNode.childNodes;
     var ordering = {};
     var i = 1;
     for (var menuitem in menuitems)
     {
       var child_id = menuitems[menuitem].id;

       if ((child_id != undefined) && (child_id.substr(0, 5) == 'admin'))
       {
         ordering[child_id] = i;
         i++;
       }
     }
    ordering['menu'] = '<?php echo $this->selectedMenu->name; ?>';
    ordering['format'] = 'json';

    // Send request
    var url = '<?php echo $this->url(array('action' => 'order')) ?>';
    var request = new Request.JSON({
        'url' : url,
        'method' : 'POST',
        'data' : ordering,
        onSuccess : function(responseJSON) {
        }
    });

    request.send();
    }

    function ignoreDrag()
    {
        event.stopPropagation();
        return false;
    }

</script>

<h2><?php echo 'Advanced Menus Plugin - Interactive and Attractive Navigation'; ?></h2>

<div class='tabs'>
    <?php
echo $this->navigation()->menu()->setContainer($this->navigation)->render();
?>
</div>

<h3 style="margin-bottom:6px;"><?php echo "Menu Editor"; ?></h3>

<p>
    <?php echo 'Use this area to manage the various navigation menus that appear in your community. When you select the menu you wish to edit, a list of the menu items it contains will be shown. You can drag these items up and down to change their order. You can create sub menus and 3rd level menus. You can edit a menu and can select content to be shown in respective menu.'; ?>
</p>
<br/>


<div class="admin_menus_filter">
  <form action="<?php echo $this->url() ?>" method="get">
    <b><?php echo $this->translate("Editing:") ?></b>
    <?php echo $this->formSelect('name', $this->selectedMenu->name, array('onchange' => 'fetchSettings(this.value)'), $this->menuList) ?>
  </form>
</div>

<br />

<div class="admin_menus_options">
  <?php echo $this->htmlLink(array('reset' => false, 'action' => 'create', 'name' => $this->selectedMenu->name), $this->translate('Add Item'), array('class' => 'buttonlink admin_menus_additem smoothbox')) ?>
  <?php echo $this->htmlLink(array('reset' => false, 'action' => 'create-menu'), $this->translate('Add Menu'), array('class' => 'buttonlink admin_menus_addmenu smoothbox')) ?>
  <?php if ($this->selectedMenu->type == 'custom'): ?>
    <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete-menu', 'name' => $this->selectedMenu->name), $this->translate('Delete Menu'), array('class' => 'buttonlink admin_menus_deletemenu smoothbox')) ?>
  <?php endif?>
</div>

<br />

<ul class="admin_menus_items" id='menu_list'>
  <?php foreach ($this->menuItems as $menuItem): ?>
    <li class="admin_menus_item<?php if (isset($menuItem->enabled) && !$menuItem->enabled) {
    echo ' disabled';
}
?>" id="admin_menus_item_<?php echo $menuItem->name ?>">
      <span class="item_wrapper">
        <span class="item_options">
          <?php echo $this->htmlLink(array('reset' => false, 'action' => 'edit', 'name' => $menuItem->name), $this->translate('edit'), array('class' => 'smoothbox')) ?>
          <?php if ($menuItem->custom && strpos($menuItem->name, 'custom_') === 0): ?>
            | <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete', 'name' => $menuItem->name), $this->translate('delete'), array('class' => 'smoothbox')) ?>
          <?php endif;?>
        </span>
        <span class="item_label">
          <?php echo $this->translate($menuItem->label) ?>
        </span>
        <span class="item_url">
          <?php
$href = '';
if (isset($menuItem->params['uri'])) {
    echo $this->htmlLink($menuItem->params['uri'], $menuItem->params['uri']);
} else if (!empty($menuItem->plugin)) {
    echo '<a>(' . $this->translate('variable') . ')</a>';
} else {
    echo $this->htmlLink($this->htmlLink()->url($menuItem->params), $this->htmlLink()->url($menuItem->params));
}
?>
        </span>
      </span>
    </li>
  <?php endforeach;?>
</ul>


<script type="text/javascript">
        function fetchSettings(menu_name)
        {
            if(menu_name != 'sitemenu_core_main') {
                window.location.href= en4.core.baseUrl+'admin/sitemenu/menus/index/name/' + menu_name;
        } else {
             window.location.href= en4.core.baseUrl+'admin/sitemenu/menu-settings/editor/name/' + menu_name;
        }
        }
</script>