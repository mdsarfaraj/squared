<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2014 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2014-05-26 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
//WORK FOR MAIN LOOP STARTS HERE

$moreMenu = $tempMenuArray = array();
$moreCountFlag = 1;
$arrValues = $this->arr_values;
$isTitleInside = $parentIsTitleInside = $tempIsTitleInside = 0;
?>
<!-- <?php if (!empty($this->show_cart)) : ?>
                   <div id="main_menu_cart" class="fleft">
                     <a href="<?php echo $this->url(array('action' => 'cart'), 'sitestoreproduct_product_general', true) ?>" title="<?php echo $this->translate('Your Shopping Cart') ?>">
                       <span class="navicon fleft" style="background-image:url(<?php echo $this->layout()->staticBaseUrl ?>application/modules/Sitemenu/externals/images/cart-icon-white.png);"></span>
  <?php if (!empty($this->itemCount)) : ?>
                                   <span id="main_menu_cart_item_count" class="seaocore_pulldown_count"><?php echo $this->itemCount ?></span>
  <?php else: ?>
                                   <span id="main_menu_cart_item_count"></span>
  <?php endif; ?>
                     </a>
                   </div>
<?php endif; ?>-->
<div class="sitemenu_pannel_icons" >
    <ul class="sitemenu_toggle_button">
        <li onclick="sitemenuMainMenuToggleClass();">
            <i class="menuiconbar"></i>
            <i class="menuiconbar"></i>
            <i class="menuiconbar"></i>
        </li>
    </ul>
</div>
<div class="sitemenu_vertical_menu_background_wrapper" onclick="sitemenuMainMenuToggleClass();"></div>
<div class="sitemenu_vertical_menu_wrapper  sitemenu_panel_<?php echo $this->pannelColor?>">
    <div class="menu_item_container" id="mobile_menu_container_<?php echo $this->identity; ?>">
      <?php if($this->pannelStartFromTop):?>
      <div id="sitemenu_fixed_pannel" class="sitemenu_fixed_pannel">
        <div class="sitemenu_fixed_pannel_header">
            <div class="sitemenu_toggle_button_wapper">
              <ul class="sitemenu_toggle_button">
                <li onclick="sitemenuMainMenuToggleClass();">
                  <i class="menuiconbar"></i>
                  <i class="menuiconbar"></i>
                  <i class="menuiconbar"></i>
                </li>
              </ul>
            </div>
            <div class="sitemenu_v_logo" id="sitemenu_v_logo">
            </div>
          </div>
        </div>
        <?php endif; ?>
        <div class="menu_item_container_inner scrollbars">
            <?php if ($this->viewer_id): ?>
            <?php /*We are use the cache for main menu, but this section need to update for all user So are use the veriable which are replace leter*/ ?>
            VERTICAL_MENU_USER_PROFILE_INFO_SECTION
            <?php endif; ?>
            <?php if ($this->show_search_field): ?>  
              <div class="sitemenu_v_search">
                  <form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get">
                      <input type="text" class="text suggested" name="query" id="_titleAjax" size="20" placeholder="Search..." alt='<?php echo $this->translate("Search") ?>'>
                      <i class="fa fa-search" onclick="this.getParent('form').submit()"></i>
                  </form>
              </div>
            <?php endif; ?>
            <div class="sitemenu_links">
                <ul class="base_level_menuitems" id="sitemenu_vertical_main_menu">
                    <?php
                    if (!empty($this->mainMenusArray)):
                      foreach ($this->mainMenusArray as $mainMenuTabArray) :
                        if (empty($mainMenuTabArray['info']))
                          continue;

                        $mainMenuTabMenuObj = $mainMenuTabArray['info']['menuObj'];
                        $menuObjParams = $mainMenuTabMenuObj->params;
                        if ($mainMenuTabMenuObj->custom && empty($menuObjParams['show_to_guest']) && !empty($this->isGuest)) {
                          continue;
                        }

                        $menuItemViewType = 1;

                        $navMenuIdArray = null;

                        if (!empty($menuObjParams['nav_menu'])) {
                          $navMenuIdArray = !empty($menuObjParams['sub_navigation']) ? unserialize($menuObjParams['sub_navigation']) : array();
                        }


                        $tempMenuId = $mainMenuTabMenuObj->id;
                        $tempShowInTab = !empty($menuObjParams['show_in_tab']) ? $menuObjParams['show_in_tab'] : 0;
                        $icon = !empty($menuObjParams['icon']) ? $menuObjParams['icon'] : "";
                        $menuTarget = !empty($menuObjParams['target']) ? $menuObjParams['target'] : "";
                        $mainMenuTabObj = $mainMenuTabArray['info']['zendObj'];

                        unset($mainMenuTabArray['info']);
                        // IF THERE IS MORE LINK IN MAIN NAVIGATION
                        if (!empty($this->noOfTabs) && ($moreCountFlag++ > $this->noOfTabs)):
                          if (empty($this->moreLink)) :
                            break;
                          endif;
                          $tempMenuArray['label'] = $mainMenuTabObj->getLabel();
                          $tempMenuArray['menuUri'] = $mainMenuTabObj->getHref();
                          $tempMenuArray['target'] = $menuTarget;
                          $tempMenuArray['tempShowInTab'] = $tempShowInTab;
                          $tempMenuArray['icon'] = $icon;
                          $moreMenu[] = $tempMenuArray;
                          continue;
                        endif;

                        $subMenus = COUNT($mainMenuTabArray);
                        ?>

                    <li class="<?php echo $mainMenuTabObj->get('active') ? 'active' : '' ?>">

                            <?php $mainMenuTaburl = $mainMenuTabObj->getHref(); ?>
                            <a id="mobile_main_menu_<?php echo $tempMenuId; ?>" class="level-top" <?php if (!empty($mainMenuTaburl)) { ?>href=" <?php echo $mainMenuTabObj->getHref(); ?>"  <?php } ?> <?php if (!empty($menuTarget)): ?> target="_blank" <?php endif; ?>>

                                <?php if (((!empty($tempShowInTab) || $tempShowInTab == 2) && !empty($icon))): ?>
                                  <span><i <?php echo (Zend_Uri::check($icon)) ? 'style="background-image:url(' . $icon . ')"' : 'class="fa ' . $icon . '"' ?>></i></span>
                                <?php endif; ?>
                                <?php if ((empty($tempShowInTab) || $tempShowInTab == 2) || !empty($this->isMobile)): ?>
                                  <span><?php echo $this->translate($mainMenuTabObj->getLabel()) ?></span>
                                <?php endif; ?>

                            </a>

                            <?php include APPLICATION_PATH . '/application/modules/Sitemenu/widgets/vertical-menu-main/_navigationMenu.tpl'; ?>

                        </li>
                        <?php
                      endforeach;
                    endif;
                    ?>
                </ul>
            </div>
            <div class="sitemenu_pannel_bottom_icons" >
                <a class="sitemenu_pannel_bottom_toggle" onclick="sitemenuMainMenuToggleClass();">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<!--WORK FOR MAIN LOOP ENDS-->


<script type="text/javascript">
  var sitemenuVerticalMenuPositions = '<?php echo $this->pannelPositions ?>';
  function sitemenuMainMenuToggleClass() {
      var el = document.getElementsByTagName('body')[0];
      if (el.hasClass('sitemenu-pannel-toggle')) {
        el.removeClass('sitemenu-pannel-toggle');
        return;
      }
      el.addClass('sitemenu-pannel-toggle');
      refreshSitemenuPanelLayout();
  }
  var sitemenuPanelScrollBar;
  var setupOnRendersitemenuMainMenu = function () {
      var bodyClass = sitemenuVerticalMenuPositions == 'left' ? 'sitemenu-vertical-left-mainmenu' : 'sitemenu-vertical-right-mainmenu';
      var el = document.getElementsByTagName('body')[0];
      bodyClass = bodyClass + ' ' + 'sitemenu-vertical-<?php echo $this->pannelType; ?>';
      <?php if($this->pannelStartFromTop):?>
        bodyClass = bodyClass + ' ' + 'sitemenu-vertical-start-top';
      <?php endif;?>
      el.addClass(bodyClass);
      <?php if ($this->hidePannel): ?>
        el.addClass('sitemenu-vertical-toggole-button');
        el.addEvent('click', function(event) {
          $el = $(event.target);
          if(!$el.getParent('.layout_sitemenu_vertical_menu_main') && !$el.getParent('.layout_sitemenu_vertical_header_toggle_button') && !$el.getParent('.sitemenu_vertical_toggle_ignore')) {
            var el = document.getElementsByTagName('body')[0];
            if (el.hasClass('sitemenu-pannel-toggle')) {
              el.removeClass('sitemenu-pannel-toggle');
            }
          }
        });
      <?php endif;?>

      var menuContentWrapper = $('mobile_menu_container_<?php echo $this->identity; ?>').getElement('.menu_item_container_inner');
      menuContentWrapper.scrollbars({
          scrollBarSize: 10,
          fade: !("ontouchstart" in document.documentElement),
          barOverContent: true
      });

      if (el.getElement('.layout_page_header .layout_main')) {
        new Element('div', {
          'class' : 'generic_layout_container layout_sitemenu_vertical_header_toggle_button',
          'id': 'sitemenu_vertical_header_toggle_button',
          'html': el.getElement('.sitemenu_pannel_icons').get('html')
        }).inject(el.getElement('.layout_page_header .layout_main'), 'top');

        if ($('sitemenu_v_logo') && el.getElement('.layout_page_header .layout_main .layout_core_menu_logo')) {
          $('sitemenu_v_logo').set('html', el.getElement('.layout_page_header .layout_main .layout_core_menu_logo').get('html'));
        }
      }
    sitemenuPanelScrollBar = menuContentWrapper.retrieve('scrollbars');
    sitemenuPanelScrollBar.element.getElement('.scrollbar-content-wrapper').setStyle('float', 'none');

  };
  var refreshSitemenuPanelLayout = function () {
      var el = document.getElementsByTagName('body')[0];
      var headerHeight = 0;
      <?php if($this->pannelStartFromTop):?>
      headerHeight = $('sitemenu_fixed_pannel').offsetHeight;
      <?php else: ?>    
      headerHeight = $$('.layout_page_header')[0].offsetHeight;
      $('mobile_menu_container_<?php echo $this->identity; ?>').setStyle('top', headerHeight + 'px');
      el.setStyle('marginTop', headerHeight + 'px');
      $$('.layout_page_header').addClass('layout_page_header_fixed');
      <?php endif;?>
      if ($('mobile_menu_container_<?php echo $this->identity; ?>').getElement('.menu_item_container_inner').getCoordinates().height != (window.getSize().y - headerHeight)) {
        $('mobile_menu_container_<?php echo $this->identity; ?>').getElement('.menu_item_container_inner').setStyle('height', window.getSize().y - headerHeight + 'px');
        sitemenuPanelScrollBar.updateScrollBars();
      }
      var paddingSpace = 0;
      if (sitemenuVerticalMenuPositions == 'left'  || en4.orientation == 'rtl') {
        paddingSpace = el.getElement('.layout_page_header .layout_main').getCoordinates().left - el.getElement('.layout_page_header').getCoordinates().left;
      } else {
       paddingSpace = el.getElement('.layout_page_header').getCoordinates().right - el.getElement('.layout_page_header .layout_main').getCoordinates().right;
      }
      if( paddingSpace < 51) {
        el.getElement('.layout_page_header').addClass('sitemenu-vertical_padding');
      } else {
        el.getElement('.layout_page_header').removeClass('sitemenu-vertical_padding');
      }
  };
  window.addEvent('resize', function() {
    refreshSitemenuPanelLayout();
  });
  setupOnRendersitemenuMainMenu();
  en4.core.runonce.add(function() {
      var el = document.getElementsByTagName('body')[0];
      $('sm_user_quicklinks_action') && $('sm_user_quicklinks_action').addEvent('click', function(){      
        $('mobile_menu_container_<?php echo $this->identity; ?>').getElement('.sm_user_quicklinks').toggleClass('sm_user_quicklinks_show');
        sitemenuPanelScrollBar.updateScrollBars();
      });
     <?php if ($this->openPannelOnLoad): ?>
        sitemenuMainMenuToggleClass();
      <?php else: ?>
        refreshSitemenuPanelLayout();
      <?php endif;?>
        var activeElement = $('sitemenu_vertical_main_menu').getElement('li.active');
        while(activeElement && activeElement.getParent('ul') && activeElement.getParent('ul').get('id') != 'sitemenu_vertical_main_menu') {
          activeElement = activeElement.getParent('ul').getParent('li');
          activeElement.addClass('active');
        }
        var activeElement = $('sitemenu_vertical_main_menu').getElement('li.active');
        while(activeElement) {
          var prevActiveElement = activeElement;
        activeElement =  activeElement.getElement('li.active');
        if(activeElement) {
          prevActiveElement.getElement('.collapse_icon').click();
        }

      }
  });
</script>
<!---------Hide main menu icons--------->
<?php if( empty($this->sitemenu_show_link_icon) ) :?>
  <style>
    #sitemenu_vertical_main_menu > li > a.level-top span:first-child i {
      display: none;
    }
  </style>
<?php endif; ?>
