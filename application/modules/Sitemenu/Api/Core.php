<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2014 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Core.php 2014-05-26 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitemenu_Api_Core extends Core_Api_Abstract {
   protected $_mainMenuName = 'sitemenu_core_main';

  public function integrateCoreMainMenu()
  {
    // Get menu items
    $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
    $addLinkmenuItems = $menuItemsTable->select()
      ->from($menuItemsTable->info('name'), 'name')
      ->where('menu = ?', $this->_mainMenuName)
      ->order('order')
      ->query()
      ->fetchAll(Zend_Db::FETCH_COLUMN);
    $menuItemsSelect = $menuItemsTable->select()
      ->where('menu = ?', 'core_main')
      ->order('order');
    $menuItems = $menuItemsTable->fetchAll($menuItemsSelect);
    try {
      $addedMenuItemList = array();
      foreach( $menuItems as $menuItem ) {
        $menuName = 'sitemenu_' . $menuItem->name;
        if( in_array('sitemenu_' . $menuItem->name, $addLinkmenuItems) ) {
          continue;
        }
        $data = $menuItem->toArray();
        if( strstr($menuItem->module, "sitereview") ) {
          $sitereviewMenuNameArray = explode("core_main_sitereview_listtype_", $menuItem->name);
          $listingtypeId = $sitereviewMenuNameArray[1];
          $menuTypeName = 'sitereview_main_listtype_' . $listingtypeId;
        } else {
          $menuTypeName = $menuItem['module'] . '_main';
        }
        $tempNavMenuArray = $menuItemsTable->select()
          ->from($menuItemsTable->info('name'), 'id')
          ->where('menu = ?', $menuTypeName)
          ->where('menu != ?', 'core_main')
          ->where("module != 'core'")
          ->where("custom = 0")
          ->query()
          ->fetchAll(Zend_Db::FETCH_COLUMN);
        if( !empty($tempNavMenuArray) ) {
          $data['params']['sub_navigation'] = serialize($tempNavMenuArray);
          $data['params']['nav_menu'] = $menuTypeName;
        }
        unset($data['id']);
        $data['name'] = $menuName;
        $data['menu'] = $this->_mainMenuName;
        $menuRow = $menuItemsTable->createRow();
        $menuRow->setFromArray($data);
        $menuRow->save();
        $addedMenuItemList[$menuItem->id] = $menuRow;
      }
      foreach($addedMenuItemList as $menu) {
        $params = $menu->params;
        if (!empty($params['root_id']) && !empty($addedMenuItemList[$params['root_id']])) {
          $params['root_id'] = $addedMenuItemList[$params['root_id']]->id;
        }
        if (!empty($params['parent_id']) && !empty($addedMenuItemList[$params['parent_id']])) {
          $params['parent_id'] = $addedMenuItemList[$params['parent_id']]->id;
        }
        $menu->params = $params;
        $menu->save();
      }
      $menuItemsTable->delete(array('menu = ?'=> 'core_main', 'name LIKE ?' => 'custom_%', 'params LIKE ?' => '%"sub_navigation":%'));
    } catch( Exception $e ) {
      
    }
  }

  /**
     * Returns an customized sql query.
     *
     * @params array column name with value
     * @return string
     */
    public function getMenusQuery($params) {
        $getEnabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $menuItemsSelect = $menuItemsTable->select()->order('order');

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $menuItemsSelect->where($key . ' = ?', $value);
            }
        }

        if (!empty($getEnabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $getEnabledModuleNames);
        }
        return $menuItemsSelect;
    }

    /**
     * Returns object of fetched menus from core.
     *
     * @params array column name with value
     * @return object
     */
    public function getMenuObject($params = array('menu' => 'sitemenu_core_main')) {
        $selectQuery = $this->getMenusQuery($params);
        return Engine_Api::_()->getDbtable('menuItems', 'core')->fetchAll($selectQuery);
    }

  public function getNavigation($name, array $options = array(), $activeItem = null)
  {
    $pages = $this->getMenuParams($name, $options, $activeItem);
    $navigation = new Zend_Navigation();
    $navigation->addPages($pages);
    return $navigation;
  }

  public function getMenuParams($name, array $options = array(), $activeItem = null)
  {
    $menu = Engine_Api::_()->getApi('menus', 'core')->getMenu($name);
    $pages = array();

    foreach( $menu as $key => $row ) {
      $orgName = $row->name;
      $row->name = str_replace('sitemenu_', '', $row->name);
      // Check enabled
      if( isset($row->enabled) && !$row->enabled ) {
        continue;
      }

      // Plugin
      $page = null;
      $multi = false;
      if( !empty($row->plugin) ) {

        // Support overriding the method
        if( strpos($row->plugin, '::') !== false ) {
          list($pluginName, $method) = explode('::', $row->plugin);
        } else {
          $pluginName = $row->plugin;
          $method = 'onMenuInitialize_' . $this->_formatMenuName($row->name);
        }

        // Load the plugin
        try {
          $plugin = Engine_Api::_()->loadClass($pluginName);
        } catch( Exception $e ) {
          $e;
          // Silence exceptions
          continue;
        }

        // Run plugin
        try {
          $result = $plugin->$method($row);
        } catch( Exception $e ) {
          $e;
          // Silence exceptions
          continue;
        }

        if( $result === true ) {
          // Just generate normally
        } else if( $result === false ) {
          // Don't generate
          continue;
        } else if( is_array($result) ) {
          // We got either page params or multiple page params back
          // Single
          if( array_values($result) !== $result ) {
            $page = $result;
          }
          // Multi
          else {
            // We have to do this manually
            foreach( $result as $key => $value ) {
              if( is_numeric($key) ) {
                if( !empty($options) ) {
                  $value = array_merge_recursive($value, $options);
                }
                if( !isset($result['label']) )
                  $result['label'] = $row->label;
                $pages[] = $value;
              }
            }
            continue;
          }
        } else if( $result instanceof Zend_Db_Table_Row_Abstract && $result->getTable() instanceof Core_Model_DbTable_MenuItems ) {
          // We got the row (or a different row?) back ...
          $row = $result;
        } else {
          // We got a weird data type back
          continue;
        }
      }

      // No page was made, use row
      if( null === $page ) {
        $page = (array) $row->params;
      }

      // Add label
      if( !isset($page['label']) ) {
        $page['label'] = $row->label;
      }

      // Add type for URI
      if( isset($page['uri']) && !isset($page['type']) ) {
        $page['type'] = 'uri';
      }

      // Add custom options
      if( !empty($options) ) {
        $page = array_merge_recursive($page, $options);
      }

      // Standardize arguments
      if( !isset($page['reset_params']) ) {
        $page['reset_params'] = true;
      }

      // Set page as active, if necessary
      if( !isset($page['active']) && null !== $activeItem && $activeItem == $row->name ) {
        $page['active'] = true;
      }

      $page['class'] = (!empty($page['class']) ? $page['class'] . ' ' : '' ) . 'menu_core_main ' . 'menu_' . $name;
      $page['class'] .= " " . $row->name. " ".$orgName;

      // Get submenu
      if( $row->submenu ) {
        $page['pages'] = $this->getMenuParams($row->submenu);
      }

      // Maintain menu item order
      $page['order'] = $row->order;

      $pages[] = $page;
    }
    return $pages;
  }
  protected function _formatMenuName($name)
  {
    $name = str_replace('_', ' ', $name);
    $name = ucwords($name);
    $name = str_replace(' ', '', $name);
    return $name;
  }
  /**
     * Returns an array of menu items with all sub tabs.
     *
     * @menuName string menu name.
     * @return array
     * Important Note:- This functions returns an array of menus. All the menus are present in array but the info part of that menu which is not displayable is not present. If info part of that menu is accessed anywhere it would give a notice, therefore needs a check for empty of info part anywhere it is used.
     */
    public function getMainMenuArray($menuName) {
        $data = array();
        $menus = $this->getNavigation($menuName);

        $tempSitemenuLtype = $tempHostType = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.global.view', 0);
        foreach ($menus as $menu) {
            $getTabNameArray = explode(" ", $menu->getClass());

            // CHECK TYPE THAT CONTENT AVAILABLE OR NOT FOR THIS ROW.
            $isModuleTypeAvailable = Engine_Api::_()->getDbtable('modules', 'sitemenu')->isModuleTypeAvailable($menu, array('menu' => $menuName));
            if (empty($isModuleTypeAvailable))
                continue;

            $selectQuery = $this->getMenusQuery(array('name' => end($getTabNameArray), 'menu' => $menuName));
            $getTabNameObj = Engine_Api::_()->getDbtable('menuItems', 'core')->fetchRow($selectQuery);

            if (!empty($getTabNameObj)) {
//                continue;
                $params = $getTabNameObj->params;

                $root_id = !empty($params['root_id']) ? $params['root_id'] : 0;
                $parent_id = !empty($params['parent_id']) ? $params['parent_id'] : 0;

                if (empty($root_id) && empty($parent_id)) {
                    $data[$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj, 'zendObj' => $menu);
                } else if (!empty($root_id) && empty($parent_id)) {
                    $data[$root_id][$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj, 'zendObj' => $menu);
                } else if (!empty($parent_id) && !empty($root_id)) {
                    $data[$root_id][$parent_id][$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj, 'zendObj' => $menu);
                }
            }
        }
        return $data;
    }

    public function markMessageReadUnread($conversation_id, $is_read = false) {
        if (empty($is_read))
            Engine_Api::_()->getDbtable('recipients', 'messages')->update(array('inbox_read' => 0), array('conversation_id =?' => $conversation_id));
        else
            Engine_Api::_()->getDbtable('recipients', 'messages')->update(array('inbox_read' => 1), array('conversation_id =?' => $conversation_id));
    }

    /**
     * Returns an array of menu items with all sub tabs for admin side.
     *
     * @return array
     * Important Note:- This functions returns an array of menus. All the menus are present in array but the info part of that menu which is not displayable is not present. If info part of that menu is accessed anywhere it would give a notice, therefore needs a check for empty of info part anywhere it is used.
     */
    public function getMenuEditorArray() {
        $data = array();

        $getEnabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
        // Get menu items
        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $menuItemsSelect = $menuItemsTable->select()
                ->where("menu = 'sitemenu_core_main'")
                ->order('order');
        if (!empty($getEnabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $getEnabledModuleNames);
        }
        $menus = $menuItemsTable->fetchAll($menuItemsSelect);

        foreach ($menus as $getTabNameObj) {
            $params = $getTabNameObj->params;
            $root_id = !empty($params['root_id']) ? $params['root_id'] : 0;
            $parent_id = !empty($params['parent_id']) ? $params['parent_id'] : 0;

            if (empty($root_id) && empty($parent_id)) {
                $data[$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj);
            } else if (!empty($root_id) && empty($parent_id)) {
                $data[$root_id][$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj);
            } else if (!empty($parent_id) && !empty($root_id)) {
                $data[$root_id][$parent_id][$getTabNameObj->id]['info'] = array('menuObj' => $getTabNameObj);
            }
        }
        return $data;
    }

    /**
     * Toggles the enabled column for the given menu item id.
     *
     * @menu_id int id.
     */
    public function toggleSubMenuStatus($menu_id, $status) {

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $menuItemsSelect = $menuItemsTable->select()
                ->from($menuItemsTable->info('name'), array('params', 'id', 'enabled'))
                ->where("menu = 'sitemenu_core_main'")
                ->order('order');
        $getEnabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
        if (!empty($getEnabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $getEnabledModuleNames);
        }
        $coreMainMenus = $menuItemsTable->fetchAll($menuItemsSelect);

        foreach ($coreMainMenus as $menus) {
            if (isset($menus->params['root_id']) || isset($menus->params['parent_id'])) {
                if ((isset($menus->params['parent_id']) && $menus->params['parent_id'] == $menu_id) || (isset($menus->params['root_id']) && $menus->params['root_id'] == $menu_id )) {
                    $menuItemsTable->update(array('enabled' => $status), array('id = ?' => $menus->id));
                }
            }
        }
    }

    public function isPaidListingPackageEnabled($tempMenuClassArray = array()) {
        $PackageCount = 0;
        if (!empty($tempMenuClassArray) && is_array($tempMenuClassArray) && !empty($tempMenuClassArray[1]) && strstr($tempMenuClassArray[1], 'sitereview_main_create_listtype_')) {
            $explodedRoute = explode('sitereview_main_create_listtype_', $tempMenuClassArray[1]);
            $tabs_listingtype_id = end($explodedRoute);
            if (Engine_Api::_()->sitereview()->hasPackageEnable()) {
                $PackageCount = Engine_Api::_()->getDbTable('packages', 'sitereviewpaidlisting')->getPackageCount($tabs_listingtype_id);
                if ($PackageCount > 0)
                    return $tabs_listingtype_id;
            }
        }
        return false;
    }

    public function isEventPackageEnabled($tempMenuClassArray = array()) {
        if (!empty($tempMenuClassArray) && is_array($tempMenuClassArray) && !empty($tempMenuClassArray[1]) && strstr($tempMenuClassArray[1], 'siteevent_main_create')) {

            if (Engine_Api::_()->siteevent()->hasPackageEnable()) {
                return true;
            }
        }
        return false;
    }

    public function getCachedMenus($menuName) {
        $cache = Zend_Registry::get('Zend_Cache');

        if ($menuName == 'core_footer') {
            $cacheName = 'footer_menu_cache';
            $data = $cache->load($cacheName);
            if (!empty($data)) {
                return $data;
            } else {
                $data = Engine_Api::_()->getApi('menus', 'core')->getNavigation('core_footer');
                $cache->setLifetime(Engine_Api::_()->sitemenu()->cacheLifeInSec());
                $cache->save($data, $cacheName);
                return $data;
            }
        }

        if ($menuName == 'sitemenu_core_main') {
            $viewer = Engine_Api::_()->user()->getViewer();
            $viewer_id = $viewer->getIdentity();
            if (!empty($viewer_id)) {
                $viewer_level_id = Engine_Api::_()->user()->getViewer()->level_id;
            } else {
                $viewer_level_id = Engine_Api::_()->getDbtable('levels', 'authorization')->fetchRow(array('type = ?' => "public"))->level_id;
            }
            $cacheName = 'main_menu_cache_level_' . $viewer_level_id;
            $data = $cache->load($cacheName);
            if (!empty($data)) {
                return $data;
            } else {
                $data = $this->getMainMenuArray('sitemenu_core_main');
                $cache->setLifetime(Engine_Api::_()->sitemenu()->cacheLifeInSec());
                $cache->save($data, $cacheName);
                return $data;
            }
        }
    }

    //FUNCTION TO RETURNS SECONDS CONVERTED FROM DAYS
    public function cacheLifeInSec() {
        return $cacheTimeSec = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.cache.lifetime', 7) * 86400;
    }
    
    public function isCurrentTheme($currentTheme = ''){
        
        if(empty($currentTheme))
            return false;
        
        //Start work for responsive theme/media query
        $view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
        $theme = '';
        $themeArray = $view->layout()->themes;
        if (isset($themeArray[0])) {
            $theme = $view->layout()->themes[0];
        }

        if ($theme == $currentTheme) {
            return true;
        }
        
        return false;
    }

    /**
     * Get Widgetized PageId
     * @param $params
     */
    public function getWidgetizedPageId($params = array()) {
        //GET CORE CONTENT TABLE
        $tableNamePages = Engine_Api::_()->getDbtable('pages', 'core');
        $page_id = $tableNamePages->select()
                ->from($tableNamePages->info('name'), 'page_id')
                ->where('name =?', $params['name'])
                ->query()
                ->fetchColumn();
        return $page_id;
    }

}
