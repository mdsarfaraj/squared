<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl 2018-02-18 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

?>
<?php $userInfo = $this->partial(
      '_verticalMainMenuUserInfo.tpl', 'sitemenu', $this->vars
    );
?>
<?php echo str_replace('VERTICAL_MENU_USER_PROFILE_INFO_SECTION', $userInfo, $this->response); ?>
