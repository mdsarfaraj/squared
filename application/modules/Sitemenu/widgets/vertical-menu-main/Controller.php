<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitemenu
 * @copyright  Copyright 2013-2018 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.tpl 2018-02-18 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Sitemenu_Widget_VerticalMenuMainController extends Engine_Content_Widget_Abstract
{

  public function indexAction()
  {

    // DON'T SHOW WIDGET, IF PLUGIN NOT ACTIVATED.
    $isPluginActivate = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.isActivate', false);
    if( empty($isPluginActivate) ) {
      return $this->setNoRender();
    }

    $vars['isMobile'] = $isMobile = Engine_Api::_()->seaocore()->isMobile();
//    $isTabletDevice = Engine_Api::_()->seaocore()->isTabletDevice();
//
////    if( empty($isMobile) && empty($isTabletDevice) ) {
////      return $this->setNoRender();
////    }

    $vars['viewer'] = $viewer = Engine_Api::_()->user()->getViewer();
    $vars['viewer_id'] = $viewer_id = $viewer->getIdentity();
    if( !$viewer->getIdentity() ) {
      $showOnLoggedOut = $this->_getParam('sitemenu_on_logged_out', 1);
      $vars['isGuest'] = 1;
      if( empty($showOnLoggedOut) ) {
        return $this->setNoRender();
      }
    }
    if( !empty($viewer_id) ) {
      $viewer_level_id = Engine_Api::_()->user()->getViewer()->level_id . '_' . $viewer_id;
    } else {
      $viewer_level_id = Engine_Api::_()->getDbtable('levels', 'authorization')->fetchRow(array('type = ?' => "public"))->level_id;
    }

    $cache = Zend_Registry::get('Zend_Cache');
    $cacheName = $this->_getParam('cachePrefix', '') . 'vertical_main_menu_html_for_' . $viewer_level_id;
    $vars['noOfTabs'] = 50; // max tabs
    $vars['moreLink'] = 0;
    $vars['showOption'] = 1;
    $vars['sitemenu_more_link_icon'] = 1;
    $vars['show_extra'] = 1;
    $vars['pannelStartFromTop'] = $this->_getParam('pannelStartFromTop', 0);
    $vars['truncationLimitContent'] = $this->_getParam('sitemenu_truncation_limit_content', 200);
    $vars['truncationLimitCategory'] = $this->_getParam('sitemenu_truncation_limit_category', 200);
    $vars['viewType'] = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu_view_type', 1);

    if( Engine_Api::_()->sitemenu()->isCurrentTheme('luminous') ) {
      $vars['viewType'] = 1;
    }



    $show_cart = $this->_getParam('sitemenu_show_cart', 1);
    if( !empty($show_cart) ) {
      $vars['sitestoreproductEnable'] = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitestoreproduct');
      if( !empty($vars['sitestoreproductEnable']) ) {
        $vars['show_cart'] = $show_cart;
        $vars['itemCount'] = Engine_Api::_()->getDbtable('carts', 'sitestoreproduct')->getProductCounts();
      } else {
        $vars['show_cart'] = $show_cart = 0;
      }
    }
    $vars['coverUserPhoto'] = $this->getUserCoverPhoto();
    //WORK FOR CACHEING
    $isCacheEnabled = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.cache.enable', true);
    $data = $cache->load($cacheName);
    $this->view->vars = $vars;
    if( !empty($isCacheEnabled) && !empty($data) && empty($show_cart) ) {
      return $this->view->response = $data;
    }

    if( !empty($isCacheEnabled) )
      $getMainMenuArray = Engine_Api::_()->sitemenu()->getCachedMenus('sitemenu_core_main');
    else
      $getMainMenuArray = Engine_Api::_()->sitemenu()->getMainMenuArray('sitemenu_core_main');

    $vars['mainMenusArray'] = $getMainMenuArray;

    $sitemenu_check_main_menu = Zend_Registry::isRegistered('sitemenu_check_main_menu') ? Zend_Registry::get('sitemenu_check_main_menu') : null;
    $tempHostType = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.global.view', 0);
    $sitemenuManageType = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.manage.type', 1);
    $sitemenuGlobalType = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitemenu.global.type', null);

    $hostType = str_replace('www.', '', strtolower($_SERVER['HTTP_HOST']));
    for( $check = 0; $check < strlen($hostType); $check++ ) {
      $tempHostType += @ord($hostType[$check]);
    }

    if( empty($sitemenuGlobalType) && (empty($sitemenu_check_main_menu) || ($sitemenuManageType != $tempHostType)) )
      return $this->setNoRender();

    $arr_values = array();
    $arr_values['sitemenu_totalmenu'] = 50;

    $arr_values['sitemenu_is_fixed'] = 0;
    $arr_values['sitemenu_fixed_height'] = 0;

    $arr_values['sitemenu_style'] = 1;

    $vars['arr_values'] = $arr_values;
    $vars['requestAllParams'] = $requestAllParams = Zend_Controller_Front::getInstance()->getRequest()->getParams();

    $vars['sitemenu_show_link_icon'] = $this->_getParam('sitemenu_show_link_icon', 1);
    $vars['identity'] = $this->view->identity;

    $vars['pannelType'] = $this->_getParam('pannelType', 'overlay');
    $vars['pannelPositions'] = $this->_getParam('pannelPositions', 'left');
    $vars['pannelColor'] = $this->_getParam('pannelColor', 'theme');

//    $vars['alwaysOpenSubmenu'] = $this->_getParam('alwaysOpenSubmenu', 0);
    $vars['hidePannel'] = $this->_getParam('hidePannel', 1);
    if( $vars['hidePannel'] == 0 ) {
      $vars['pannelType'] = 'fitpage';
    }
    $vars['show_search_field'] = $this->_getParam('show_search_field', 2);
    $vars['openPannelOnLoad'] = $vars['hidePannel'] == 0 || $this->_getParam('openPannelOnLoad', 0);
    if( empty($viewer_id) ) {
      $vars['show_search_field'] = $vars['show_search_field'] == 2;
    }

    $this->view->vars = $vars;
    $this->view->response = $this->view->partial(
      '_verticalMainMenu.tpl', 'sitemenu', $vars
    );
    if( !empty($isCacheEnabled) && empty($show_cart) ) {
      $cache->setLifetime(Engine_Api::_()->sitemenu()->cacheLifeInSec());
      $cache->save($this->view->response, $cacheName);
    }
  }

  public function getUserCoverPhoto()
  {
    $user = Engine_Api::_()->user()->getViewer();
    $coverPhoto = null;
    if( !$user->getIdentity() ) {
      return $coverPhoto;
    }
    $albumType = Engine_Api::_()->hasModuleBootstrap('advalbum') ? 'advalbum_photo' : 'album_photo';
    if( Engine_Api::_()->hasModuleBootstrap('siteusercoverphoto') ) {
      $has_advalbum = Engine_Api::_()->hasModuleBootstrap('advalbum');
      if( isset($user->user_cover) && $user->user_cover ) {
        $coverPhoto = Engine_Api::_()->getItem($albumType, $user->user_cover);
      } elseif( !empty($coverId = Engine_Api::_()->getApi("settings", "core")->getSetting("siteusercoverphoto.cover.photo.preview.level.$user->level_id.id")) ) {
        $coverPhoto = Engine_Api::_()->getItem('storage_file', $coverId);
      }
    } elseif( isset($user->coverphoto) ) {
      if( Engine_Api::_()->authorization()->isAllowed('user', $user, 'coverphotoupload') && $user->coverphoto ) {

        $coverPhoto = Engine_Api::_()->getItem('storage_file', $user->coverphoto);
      }
      $coverId = Engine_Api::_()->getApi("settings", "core")
        ->getSetting("usercoverphoto.preview.level.id." . $user->level_id);
      if( empty($coverPhoto) && $coverId ) {
        $coverPhoto = Engine_Api::_()->getItem('storage_file', $coverId);
      }
    }
    return $coverPhoto;
  }

}
