<div class="vertical_mainmenu_bar">
	<?php echo $this->content()->renderWidget('sitemenu.vertical-menu-main', array(
		'cachePrefix' => 'horizontl_',
		'openPannelOnLoad' => 0,
		'pannelType' => 'overlay',
		'pannelPositions' => 'left',
		'pannelColor' => 'theme',
		'hidePannel' => 1,
		'show_search_field' => 0,
		'pannelStartFromTop' => 0,
	));
	?>
</div>
<div class="horizontal_mainmenu_bar">
	<?php echo $this->response; ?>
</div>
