<?php

/**
 * SocialEngine
 *
 * @category   Application_Theme
 * @package    Default
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manifest.php 9747 2012-07-26 02:08:08Z john $
 * @author     Alex
 */
return array(
	'package' => array(
		'type' => 'theme',
		'name' => $manifest_theme_name,
		'version' => '4.10.3p3',
		'revision' => '$Revision: 9747 $',
		'path' => 'application/themes/sitepwa/' . $manifest_theme_name,
		'repository' => 'socialengineaddOns.com',
		'title' => 'SEAO: Progresive Web APP ' . $manifest_theme_name,
		'thumb' => 'theme.jpg',
		'author' => 'SocialEngineAddons',
//		'actions' => array(
//			'install',
//			'upgrade',
//			'refresh',
//			'remove',
//		),
//		'callback' => array(
//			'class' => 'Engine_Package_Installer_Theme',
//		),
		'directories' => array(
			'application/themes/sitepwa/' . $manifest_theme_name,
		),
	),
	'files' => array(
		'custom.css',
		'colorConstants.css',
	),
	)
?>
