<?php

/**
 * SocialEngine
 *
 * @category   Application_Theme
 * @package    Default
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manifest.php 9747 2012-07-26 02:08:08Z john $
 * @author     Alex
 */
return array(
  'package' => array(
    'type' => 'theme',
    'name' => $manifest_theme_name,
    'revision' => '$Revision: 9747 $',
    'path' => 'application/themes/allure/' . $manifest_theme_name,
    'repository' => 'socialengineaddOns.com',
    'title' => 'Responsive Allure Theme ' . $manifest_theme_name,
    'thumb' => 'theme.jpg',
    'author' => 'SocialEngineAddOns',
    'directories' => array(
      'application/themes/allure/' . $manifest_theme_name,
    ),
  ),
  'files' => array(
//		'constants.css',
    'colorConstants.css',
  ),
  )
?>
