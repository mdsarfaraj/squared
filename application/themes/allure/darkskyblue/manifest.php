<?php

/**
 * SocialEngine
 *
 * @category   Application_Theme
 * @package    Default
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manifest.php 9747 2012-07-26 02:08:08Z john $
 * @author     Alex
 */
$manifest_theme_name = basename(dirname(__FILE__));
return include(realpath(dirname(dirname(__FILE__))) . DS . 'default_manifest.php');
?>
